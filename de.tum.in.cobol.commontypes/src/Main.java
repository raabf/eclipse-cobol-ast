import java.util.ArrayList;
import java.util.List;

import de.tum.in.cobol.commontypes.BinaryExpression;
import de.tum.in.cobol.commontypes.Label;
import de.tum.in.cobol.commontypes.NumericConstantExpression;
import de.tum.in.cobol.commontypes.Operator;
import de.tum.in.cobol.commontypes.Procedure;
import de.tum.in.cobol.commontypes.RegisterVariable;
import de.tum.in.cobol.commontypes.SourcePosition;
import de.tum.in.cobol.commontypes.SourceRange;
import de.tum.in.cobol.commontypes.MemoryVariable;
import de.tum.in.cobol.commontypes.Picture;
import de.tum.in.cobol.commontypes.instructions.AssignmentInstruction;
import de.tum.in.cobol.commontypes.instructions.DeclareGlobalInstruction;
import de.tum.in.cobol.commontypes.instructions.EndProcedureInstruction;
import de.tum.in.cobol.commontypes.instructions.ExecInstruction;
import de.tum.in.cobol.commontypes.instructions.Instruction;
import de.tum.in.cobol.commontypes.instructions.JumpInstruction;
import de.tum.in.cobol.commontypes.instructions.JumpZeroInstruction;
import de.tum.in.cobol.commontypes.instructions.NewInstruction;
import de.tum.in.cobol.commontypes.instructions.NoOpInstruction;

public class Main {

    public static void main(String[] args) {
        List<Instruction> instructions = new ArrayList<Instruction>();

        RegisterVariable registerErr = new RegisterVariable("err");
        RegisterVariable registerAcc = new RegisterVariable("acc");
        RegisterVariable registerCnt = new RegisterVariable("cnt");
        RegisterVariable registerIdx = new RegisterVariable("idx");
        RegisterVariable registerIni = new RegisterVariable("ini");

        MemoryVariable variableA = new MemoryVariable("x_a");
        MemoryVariable variableB = new MemoryVariable("x_b");
        MemoryVariable variableC = new MemoryVariable("x_c");
        MemoryVariable variableX = new MemoryVariable("x_x");

        Procedure paragraphP0 = new Procedure("p0");
        Procedure paragraphBegin = new Procedure("begin");

        NumericConstantExpression zeroLiteral = new NumericConstantExpression(new Double(0));
        NumericConstantExpression oneLiteral = new NumericConstantExpression(new Double(1));
        NumericConstantExpression twoLiteral = new NumericConstantExpression(new Double(2));
        NumericConstantExpression threeLiteral = new NumericConstantExpression(new Double(3));
        NumericConstantExpression fourLiteral = new NumericConstantExpression(new Double(4));
        NumericConstantExpression fiveLiteral = new NumericConstantExpression(new Double(5));
        NumericConstantExpression sevenLiteral = new NumericConstantExpression(new Double(7));

        Label labelLBegin = new Label("L_Begin");
        Label labelL0 = new Label("L_O");
        Label labelL1 = new Label("L_1");
        Label labelL2 = new Label("L_2");
        Label labelL3 = new Label("L_3");
        Label labelL00 = new Label("L_0,0");
        Label labelL01 = new Label("L_O,1");
        Label labelL10 = new Label("L_1,O");
        Label labelL11 = new Label("L_1,1");
        Label labelLp0 = new Label("L_p0");

        Picture typeNumber = new Picture("9");

        // jumpz ini L_O
        instructions.add(new JumpZeroInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), registerIni, labelL0));

        // declare a
        instructions
                .add(new NewInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), 1));
        instructions.add(new DeclareGlobalInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), "x_a", 0, typeNumber));
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, variableA, fiveLiteral));

        // declare b
        instructions
                .add(new NewInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), 1));
        instructions.add(new DeclareGlobalInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), "x_b", 0, typeNumber));
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, variableB, sevenLiteral));

        // declare c
        instructions
                .add(new NewInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), 1));
        instructions.add(new DeclareGlobalInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), "x_c", 0, typeNumber));
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, variableC, twoLiteral));

        // declare x
        instructions
                .add(new NewInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), 1));
        instructions.add(new DeclareGlobalInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), "x_x", 0, typeNumber));
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, variableX, fourLiteral));

        // L_0: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL0));
        // L_Begin:
        instructions.add(new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                labelLBegin));
        // L_0,0:
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL00));

        // err := 0
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, false, registerErr, zeroLiteral));
        // acc := x_a + x_b + x_c
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, false, registerAcc, new BinaryExpression(
                                new BinaryExpression(variableA, variableB, Operator.Plus), variableC, Operator.Plus)));
        // x_b := x_b + acc
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, variableB, new BinaryExpression(variableB, registerAcc, Operator.Plus)));
        // x_x := x_x + acc
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, variableX, new BinaryExpression(variableX, registerAcc, Operator.Plus)));
        // jumpz err L_1
        instructions.add(new JumpZeroInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), registerErr, labelL1));

        // acc = 3.0
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerAcc, threeLiteral));
        // idx = idx + 1.0
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerIdx, new BinaryExpression(registerIdx, oneLiteral, Operator.Plus)));
        // cnt = acc
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerCnt, registerAcc));
        // acc = acc > 0.0
        instructions.add(new AssignmentInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), false, true, registerAcc,
                new BinaryExpression(registerAcc, zeroLiteral, Operator.GreaterThan)));
        // jumpz acc L_3
        instructions.add(new JumpZeroInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), registerAcc, labelL3));
        // L_2: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL2));
        // jumpz cnt L_3
        instructions.add(new JumpZeroInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), registerCnt, labelL3));
        // exec p0 p0
        instructions.add(new ExecInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                paragraphP0, paragraphP0));
        // cnt = cnt - 1.0
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerCnt, new BinaryExpression(registerCnt, oneLiteral, Operator.Minus)));
        // jump L_2
        instructions.add(
                new JumpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL2));
        // L_3: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL3));
        // idx = idx - 1.0
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerIdx, new BinaryExpression(registerIdx, oneLiteral, Operator.Minus)));
        // L_1: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL1));
        // L_O,1: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL01));
        // endp begin
        instructions.add(new EndProcedureInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), paragraphBegin));
        // L_p0: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelLp0));
        // L_1,O: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL10));
        // L_1,1: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL11));
        // endp p0
        instructions.add(new EndProcedureInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), paragraphP0));

        for (Instruction instruction : instructions) {
            System.out.println(instruction);
        }

    }

}
