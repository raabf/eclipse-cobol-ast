package de.tum.in.cobol.commontypes;

public class Subscript extends Identifier implements Expression {
	
	protected Variable variable;
	protected Expression subscript;

	public Subscript(Variable variable, Expression subscript) {
		super();
		this.variable = variable;
		this.subscript = subscript;
	}
	
	public String getName() {
		return Subscript.combinedString(variable.toString(), subscript.toString());
	}
	
	public static String combinedString(String name, String subscript) {
		return subscript == null ? name : name + "(" + subscript + ")";
	}

}
