package de.tum.in.cobol.commontypes;

public abstract class Identifier implements IdentifierOrExpression, IdentifierOrConstant {

	public Identifier() {
	}
	
	public abstract String getName();
	
	public String toString() {
		return getName();
	}
}
