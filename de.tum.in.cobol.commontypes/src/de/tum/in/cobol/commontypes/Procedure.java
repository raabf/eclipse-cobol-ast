package de.tum.in.cobol.commontypes;

public class Procedure extends Label {
	/*
	 * Design Desition: In Cobol a procedure is a special component in the
	 * source code. But after the translation the procedure is marked with just
	 * a label and not more. But sometimes, espcially during the translation, it
	 * is helpful to have the information that this label represents a procedure
	 * and teh information of the original procedure name, so Procedure is a
	 * subclass of Label.
	 */

	/**
	 * Name of the procedure like in Cobol source code. Can be different from
	 * the Label.name.
	 */
	private final String procedureName;

	public Procedure(String name) {
		super(name);
		this.procedureName = name;
	}

	public Procedure(String labelName, String procedureName) {
		super(labelName);
		this.procedureName = procedureName;
	}

	public String getProcedureName() {
		return procedureName;
	}
}
