package de.tum.in.cobol.commontypes;

public class IsNotZeroCondition extends Condition {
    public IsNotZeroCondition(RegisterVariable register) {
        super(register);
    }

    @Override
    public String toString() {
        return register + " != 0";
    }
}
