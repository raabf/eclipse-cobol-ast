package de.tum.in.cobol.commontypes;

public class FigurativeConstantExpression extends ConstantExpression {
	
	protected FigurativeConstant figurativeConstant;
	
	public FigurativeConstantExpression(FigurativeConstant figurativeConstant) {
		this.figurativeConstant = figurativeConstant;
	}

	@Override
	public String toString() {
		return figurativeConstant.toString();
	}

}
