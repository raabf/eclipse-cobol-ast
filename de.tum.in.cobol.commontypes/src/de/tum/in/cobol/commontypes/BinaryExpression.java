package de.tum.in.cobol.commontypes;

public class BinaryExpression implements Expression {
	private final Expression leftOperand;
	private final Expression rightOperand;
	private final Operator operator;

	public BinaryExpression(Expression leftOperand, Expression rightOperand, Operator operator) {
		this.leftOperand = leftOperand;
		this.rightOperand = rightOperand;
		this.operator = operator;
	}

	public Expression getLeftOperand() {
		return leftOperand;
	}

	public Expression getRightOperand() {
		return rightOperand;
	}

	public Operator getOperator() {
		return operator;
	}

	@Override
	public String toString() {
		return leftOperand + " " + operator + " " + rightOperand;
	}

}
