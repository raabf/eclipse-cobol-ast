package de.tum.in.cobol.commontypes;

public abstract class Condition {
    protected final RegisterVariable register;

    protected Condition(RegisterVariable register) {
        this.register = register;
    }

    public Expression getRegister() {
        return register;
    }

    @Override
    public abstract String toString();
}
