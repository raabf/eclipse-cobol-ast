package de.tum.in.cobol.commontypes;

public class Picture {
    private final String type;

    public Picture(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return type;
    }
}
