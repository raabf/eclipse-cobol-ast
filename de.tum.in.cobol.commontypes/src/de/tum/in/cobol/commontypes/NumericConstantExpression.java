package de.tum.in.cobol.commontypes;

import java.math.BigInteger;

public class NumericConstantExpression extends ConstantExpression {
	private final Double constant;

	/*
	 * FIXME The type is always double. But sometimes other types is the only
	 * number type which makes sense. For example Integer for indices. Maybe add
	 * children with more specific types?
	 */

	public NumericConstantExpression(double constant) {
		this.constant = constant;
	}

	public NumericConstantExpression(int constant) {
		this.constant = (double) constant;
	}

	public NumericConstantExpression(String constant) {

		// Hex numbers
		if (constant.startsWith("X\"") && constant.endsWith("\"")) {
			String[] splitted = constant.split("\"");
			this.constant = new BigInteger(splitted[1], 16).doubleValue();

		} else {
			// Integer numbers
			this.constant = Double.parseDouble(constant);
		}
	}

	public Double getExpression() {
		return constant;
	}

	@Override
	public String toString() {
		return constant.toString();
	}
}
