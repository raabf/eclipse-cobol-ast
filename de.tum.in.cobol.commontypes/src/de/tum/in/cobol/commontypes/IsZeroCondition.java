package de.tum.in.cobol.commontypes;

public class IsZeroCondition extends Condition {
    public IsZeroCondition(RegisterVariable register) {
        super(register);
    }

    @Override
    public String toString() {
        return register + " == 0";
    }
}
