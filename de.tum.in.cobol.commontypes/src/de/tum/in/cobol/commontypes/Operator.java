package de.tum.in.cobol.commontypes;

public enum Operator {
    Plus("+"),
    Minus("-"),
    Multiply("*"),
    Power("**"),
    Divide("/"),
    Mod("%"),
    Equal("=="), 
    NotEqual("!="),
    GreaterThan(">"), 
    LessThan("<"),
    GreaterThanOrEqualTo(">="),
    LessThanOrEqualTo("<=");

	private final String operatorSign;

	private Operator(String value) {
		operatorSign = value;
	}

	@Override
	public String toString() {
		return operatorSign;
	}
}
