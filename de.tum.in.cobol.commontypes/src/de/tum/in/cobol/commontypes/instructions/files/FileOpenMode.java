package de.tum.in.cobol.commontypes.instructions.files;

public enum FileOpenMode {
    in("in"), 
    out("out"), 
    inout("inout"), 
    ext("ext"), 
    bottom("bottom");

    private final String fileOpenMode;

    private FileOpenMode(String value) {
        fileOpenMode = value;
    }

    @Override
    public String toString() {
        return fileOpenMode;
    }
}
