package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.SourceRange;

public class PushReturnValueInstruction extends Instruction {
    public PushReturnValueInstruction(SourceRange location) {
        super(location);
    }

    @Override
    public String toMiniCobolString() {
        return "pushret";
    }
}
