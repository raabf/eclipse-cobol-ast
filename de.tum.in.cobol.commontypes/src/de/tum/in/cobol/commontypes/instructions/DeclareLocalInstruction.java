package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.SourceRange;
import de.tum.in.cobol.commontypes.Picture;

public class DeclareLocalInstruction extends Instruction {
    private final String name;
    private final int offset;
    private final Picture type;

    public DeclareLocalInstruction(SourceRange location, String name, int offset, Picture type) {
        super(location);

        this.name = name;
        this.offset = offset;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public int getOffset() {
        return offset;
    }

    public Picture getType() {
        return type;
    }

    @Override
    public String toMiniCobolString() {
        return "declarel " + name + " " + offset + " " + type;
    }
}
