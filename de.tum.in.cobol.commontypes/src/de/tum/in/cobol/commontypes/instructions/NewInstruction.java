package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.SourceRange;

public class NewInstruction extends Instruction {
    private final int wordLength;

    public NewInstruction(SourceRange location, int wordLength) {
        super(location);

        this.wordLength = wordLength;
    }

    public int getLength() {
        return wordLength;
    }

    @Override
    public String toMiniCobolString() {
        return "new " + wordLength;
    }
}
