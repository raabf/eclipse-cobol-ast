package de.tum.in.cobol.commontypes.instructions.files;

import de.tum.in.cobol.commontypes.FileVariable;
import de.tum.in.cobol.commontypes.SourceRange;
import de.tum.in.cobol.commontypes.instructions.Instruction;

public class FileCloseInstruction extends Instruction {
	private final FileVariable fileVariable;

	public FileCloseInstruction(SourceRange location, FileVariable fileVariable) {
		super(location);

		this.fileVariable = fileVariable;
	}

	public FileVariable getFileVariable() {
		return fileVariable;
	}

	@Override
	public String toMiniCobolString() {
		return "fclose " + fileVariable;
	}
}
