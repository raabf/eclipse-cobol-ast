package de.tum.in.cobol.commontypes.instructions.files;

import de.tum.in.cobol.commontypes.FileVariable;
import de.tum.in.cobol.commontypes.SourceRange;
import de.tum.in.cobol.commontypes.instructions.Instruction;

public class FileReadInstruction extends Instruction {
	private final FileVariable fileVariable;
	private final FileAccess fileAccess;

	public FileReadInstruction(SourceRange location, FileVariable fileVariable, FileAccess fileAccess) {
		super(location);

		this.fileVariable = fileVariable;
		this.fileAccess = fileAccess;
	}

	public FileVariable getFileVariable() {
		return fileVariable;
	}

	public FileAccess getFileAccess() {
		return fileAccess;
	}

	@Override
	public String toMiniCobolString() {
		return "fread " + fileVariable;
	}
}
