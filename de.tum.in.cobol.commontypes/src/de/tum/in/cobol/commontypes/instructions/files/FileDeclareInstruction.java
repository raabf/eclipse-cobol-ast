package de.tum.in.cobol.commontypes.instructions.files;

import de.tum.in.cobol.commontypes.FileVariable;
import de.tum.in.cobol.commontypes.SourceRange;
import de.tum.in.cobol.commontypes.MemoryVariable;
import de.tum.in.cobol.commontypes.instructions.Instruction;

public class FileDeclareInstruction extends Instruction {
	private final FileVariable fileVariable;
	private final String fileName;
	private final FileOrganization fileOrganization;
	private final FileOpenMode fileOpenMode;
	private final MemoryVariable variable;

	public FileDeclareInstruction(SourceRange location, FileVariable fileVariable, String fileName,
			FileOrganization fileOrganization, FileOpenMode fileOpenMode, MemoryVariable variable) {
		super(location);

		this.fileVariable = fileVariable;
		this.fileName = fileName;
		this.fileOrganization = fileOrganization;
		this.fileOpenMode = fileOpenMode;
		this.variable = variable;
	}

	public FileVariable getFileVariable() {
		return fileVariable;
	}

	public String getFileName() {
		return fileName;
	}

	public FileOrganization getFileOrganization() {
		return fileOrganization;
	}

	public FileOpenMode getFileOpenMode() {
		return fileOpenMode;
	}

	public MemoryVariable getVariable() {
		return variable;
	}

	@Override
	public String toMiniCobolString() {
		return "fdeclare " + fileVariable + " " + fileName + " " + fileOrganization + " " + fileOpenMode + " "
				+ variable;
	}
}
