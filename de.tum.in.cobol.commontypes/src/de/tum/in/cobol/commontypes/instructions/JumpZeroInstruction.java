package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.Label;
import de.tum.in.cobol.commontypes.RegisterVariable;
import de.tum.in.cobol.commontypes.SourceRange;

public class JumpZeroInstruction extends Instruction {
    private final RegisterVariable register;
    private final Label jumpTarget;

    public JumpZeroInstruction(SourceRange location, RegisterVariable register, Label jumpTarget) {
        super(location);

        this.register = register;
        this.jumpTarget = jumpTarget;
    }

    public RegisterVariable getRegister() {
        return register;
    }

    public Label getJumpTarget() {
        return jumpTarget;
    }

    @Override
    public String toMiniCobolString() {
        return "jumpz " + register + " " + jumpTarget;
    }
}
