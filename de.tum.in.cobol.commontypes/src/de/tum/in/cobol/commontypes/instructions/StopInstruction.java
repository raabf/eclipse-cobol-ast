package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.SourceRange;

public class StopInstruction extends Instruction {
    public StopInstruction(SourceRange location) {
        super(location);
    }

    @Override
    public String toMiniCobolString() {
        return "stop";
    }
}
