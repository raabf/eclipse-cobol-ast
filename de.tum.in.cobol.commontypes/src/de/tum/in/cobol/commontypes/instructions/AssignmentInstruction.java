package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.SourceRange;
import de.tum.in.cobol.commontypes.Expression;
import de.tum.in.cobol.commontypes.Identifier;

public class AssignmentInstruction extends Instruction {
    private final boolean rounded;
    private final boolean error;
    private final Identifier assignmentTarget;
    private final Expression expression;

    public AssignmentInstruction(SourceRange location, boolean rounded, boolean error,
    		Identifier assignmentTarget, Expression expression) {
        super(location);

        this.rounded = rounded;
        this.error = error;
        this.assignmentTarget = assignmentTarget;
        this.expression = expression;
    }

    public boolean isRounded() {
        return rounded;
    }

    public boolean isError() {
        return error;
    }

    public Expression getExpression() {
        return expression;
    }

    public Identifier getAssignmentTarget() {
        return assignmentTarget;
    }

    @Override
    public String toMiniCobolString() {
        return assignmentTarget + " := " + expression;
    }
}
