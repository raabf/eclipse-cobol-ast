package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.Label;
import de.tum.in.cobol.commontypes.SourceRange;

public abstract class Instruction {
    protected final SourceRange location;
    protected Label label;

    public Instruction(SourceRange location) {
        this.location = location;
        this.label = null;
    }

    public Instruction(SourceRange location, Label label) {
        this.location = location;
        this.label = label;
    }

    public SourceRange getLocation() {
        return location;
    }

    public Label getLabel() {
        return label;
    }
    
    public void setLabel(Label label) {
        this.label = label;
    }

    public boolean hasLabel() {
        return label != null;
    }

    public abstract String toMiniCobolString();

    @Override
    public String toString() {
        String labelPrefix = (label == null) ? "" : (label + ":");

        return String.format("%-28s%-36s %s", labelPrefix, toMiniCobolString(), location);
    }
}
