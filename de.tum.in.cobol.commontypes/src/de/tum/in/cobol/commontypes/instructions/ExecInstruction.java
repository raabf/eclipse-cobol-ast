package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.Procedure;
import de.tum.in.cobol.commontypes.SourceRange;

public class ExecInstruction extends Instruction {
    private final Procedure startProcedure;
    private final Procedure endParagraph;

    public ExecInstruction(SourceRange location, Procedure startParagraph, Procedure endParagraph) {
        super(location);

        this.startProcedure = startParagraph;
        this.endParagraph = endParagraph;
    }

    public Procedure getStartProcedure() {
        return startProcedure;
    }

    public Procedure getEndProcedure() {
        return endParagraph;
    }

    @Override
    public String toMiniCobolString() {
        return "exec " + startProcedure + " " + endParagraph;
    }
}
