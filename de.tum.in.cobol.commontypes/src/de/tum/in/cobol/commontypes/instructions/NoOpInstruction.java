package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.Label;
import de.tum.in.cobol.commontypes.SourceRange;

public class NoOpInstruction extends Instruction {
    public NoOpInstruction(SourceRange location, Label label) {
        super(location, label);
    }

    @Override
    public String toMiniCobolString() {
        return "noop";
    }
}
