package de.tum.in.cobol.commontypes.instructions.files;

public enum FileAccess {
    seq("seq"), 
    rnd("rnd"), 
    dyn("dyn");

    private final String fileAccess;

    private FileAccess(String value) {
        fileAccess = value;
    }

    @Override
    public String toString() {
        return fileAccess;
    }
}
