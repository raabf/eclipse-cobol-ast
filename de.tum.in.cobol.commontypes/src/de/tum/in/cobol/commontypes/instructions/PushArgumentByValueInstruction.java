package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.MemoryVariable;
import de.tum.in.cobol.commontypes.SourceRange;

public class PushArgumentByValueInstruction extends Instruction {
    private final int index;
    private final MemoryVariable memoryVariable;

    public PushArgumentByValueInstruction(SourceRange location, int index, MemoryVariable memoryVariable) {
        super(location);

        this.index = index;
        this.memoryVariable = memoryVariable;
    }

    public int getIndex() {
        return index;
    }

    public MemoryVariable getArgumentName() {
        return memoryVariable;
    }

    @Override
    public String toMiniCobolString() {
        return "pushargv " + index + " " + memoryVariable;
    }
}
