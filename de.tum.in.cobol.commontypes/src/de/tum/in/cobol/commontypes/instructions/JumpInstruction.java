package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.Label;
import de.tum.in.cobol.commontypes.SourceRange;

public class JumpInstruction extends Instruction {
    private final Label target;

    public JumpInstruction(SourceRange location, Label target) {
        super(location);

        this.target = target;
    }

    public Label getTarget() {
        return target;
    }

    @Override
    public String toMiniCobolString() {
        return "jump " + target;
    }
}
