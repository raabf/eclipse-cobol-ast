package de.tum.in.cobol.commontypes.instructions.files;

public enum FileOrganization {
    seq("seq"),
    rel("rel"),
    ind("ind");
    
    private final String fileOrganization;

    private FileOrganization(String value) {
        fileOrganization = value;
    }

    @Override
    public String toString() {
        return fileOrganization;
    }
}
