package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.IdentifierOrConstant;
import de.tum.in.cobol.commontypes.Label;
import de.tum.in.cobol.commontypes.SourceRange;

public class DisplayInstruction extends Instruction {
	
	IdentifierOrConstant displays;

	public DisplayInstruction(SourceRange location, IdentifierOrConstant displays) {
		super(location);
		this.displays = displays;
	}

	public DisplayInstruction(SourceRange location, IdentifierOrConstant displays, Label label) {
		super(location, label);
		this.displays = displays;
	}

	@Override
	public String toMiniCobolString() {
		return "display " + displays;
	}

}
