package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.SourceRange;
import de.tum.in.cobol.commontypes.MemoryVariable;

public class LinkArgumentsInstruction extends Instruction {
    private final int index;
    private final MemoryVariable variable;

    public LinkArgumentsInstruction(SourceRange location, int index, MemoryVariable variable) {
        super(location);

        this.index = index;
        this.variable = variable;
    }

    public int getIndex() {
        return index;
    }

    public MemoryVariable getVariable() {
        return variable;
    }

    @Override
    public String toMiniCobolString() {
        return "linkargs " + index + " " + variable;
    }
}
