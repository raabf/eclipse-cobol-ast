package de.tum.in.cobol.commontypes.instructions.files;

import de.tum.in.cobol.commontypes.FileVariable;
import de.tum.in.cobol.commontypes.SourceRange;
import de.tum.in.cobol.commontypes.instructions.Instruction;

public class FileOpenInstruction extends Instruction {
	private final FileVariable fileVariable;
	private final FileOpenMode fileOpenMode;

	public FileOpenInstruction(SourceRange location, FileVariable fileVariable, FileOpenMode fileOpenMode) {
		super(location);

		this.fileVariable = fileVariable;
		this.fileOpenMode = fileOpenMode;
	}

	public FileVariable getFileVariable() {
		return fileVariable;
	}

	public FileOpenMode getFileOpenMode() {
		return fileOpenMode;
	}

	@Override
	public String toMiniCobolString() {
		return "fopen " + fileVariable + " " + fileOpenMode;
	}
}
