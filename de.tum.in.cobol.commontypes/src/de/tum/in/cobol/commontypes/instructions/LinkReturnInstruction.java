package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.SourceRange;
import de.tum.in.cobol.commontypes.MemoryVariable;

public class LinkReturnInstruction extends Instruction {
    private final MemoryVariable variable;

    public LinkReturnInstruction(SourceRange location, MemoryVariable variable) {
        super(location);

        this.variable = variable;
    }

    public MemoryVariable getVariable() {
        return variable;
    }

    @Override
    public String toMiniCobolString() {
        return "linkret " + variable;
    }
}
