package de.tum.in.cobol.commontypes.instructions.files;

import de.tum.in.cobol.commontypes.SourceRange;
import de.tum.in.cobol.commontypes.MemoryVariable;
import de.tum.in.cobol.commontypes.instructions.Instruction;

public class FileWriteInstruction extends Instruction {
	private final MemoryVariable variable;

	public FileWriteInstruction(SourceRange location, MemoryVariable variable) {
		super(location);

		this.variable = variable;
	}

	public MemoryVariable getVariable() {
		return variable;
	}

	@Override
	public String toMiniCobolString() {
		return "fwrite " + variable;
	}
}
