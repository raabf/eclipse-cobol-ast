package de.tum.in.cobol.commontypes.instructions.files;

import de.tum.in.cobol.commontypes.FileVariable;
import de.tum.in.cobol.commontypes.SourceRange;
import de.tum.in.cobol.commontypes.MemoryVariable;
import de.tum.in.cobol.commontypes.instructions.Instruction;

public class FileBufferInstruction extends Instruction {
	private final FileVariable fileVariable;
	private final MemoryVariable variable;

	public FileBufferInstruction(SourceRange location, FileVariable fileVariable, MemoryVariable variable) {
		super(location);

		this.fileVariable = fileVariable;
		this.variable = variable;
	}

	public FileVariable getFileVariable() {
		return fileVariable;
	}

	public MemoryVariable getVariable() {
		return variable;
	}

	@Override
	public String toMiniCobolString() {
		return "fbuffer " + fileVariable + " " + variable;
	}
}
