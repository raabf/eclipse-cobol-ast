package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.Label;
import de.tum.in.cobol.commontypes.Procedure;
import de.tum.in.cobol.commontypes.SourceRange;

public class CallInstruction extends Instruction {
    private final Procedure procedure;

    public CallInstruction(SourceRange location, Procedure procedure) {
        super(location);

        this.procedure = procedure;
    }
    
    public CallInstruction(SourceRange location, Procedure procedure, Label label) {
        super(location, label);

        this.procedure = procedure;
    }

    public Procedure getParagraph() {
        return procedure;
    }

    @Override
    public String toMiniCobolString() {
        return "call " + this.procedure;
    }

}
