package de.tum.in.cobol.commontypes.instructions;

import de.tum.in.cobol.commontypes.Label;
import de.tum.in.cobol.commontypes.Procedure;
import de.tum.in.cobol.commontypes.SourceRange;

public class EndProcedureInstruction extends Instruction {
    private final Procedure procedure;

    public EndProcedureInstruction(SourceRange location, Procedure procedure) {
        super(location);

        this.procedure = procedure;
    }
    
    public EndProcedureInstruction(SourceRange location, Procedure procedure, Label label) {
        super(location, label);

        this.procedure = procedure;
    }

    public Procedure getProcedure() {
        return procedure;
    }

    @Override
    public String toMiniCobolString() {
        return "endp " + this.procedure;
    }
}
