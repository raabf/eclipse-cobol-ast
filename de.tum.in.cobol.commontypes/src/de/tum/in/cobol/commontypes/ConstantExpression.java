package de.tum.in.cobol.commontypes;

public abstract class ConstantExpression implements Expression, IdentifierOrConstant {
    @Override
    public abstract String toString();
}
