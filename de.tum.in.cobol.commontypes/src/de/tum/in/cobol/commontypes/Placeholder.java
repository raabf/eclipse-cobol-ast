package de.tum.in.cobol.commontypes;

import java.util.List;

/**
 * A Placeholder for something you do not want to fully evaluate.
 * 
 * @author raabf
 *
 */
public class Placeholder implements Expression {

	protected String image;
	protected List<Identifier> involvedIdentifier;

	public Placeholder(String image, List<Identifier> involvedIdentifier) {
		this.image = image;
		this.involvedIdentifier = involvedIdentifier;
	}
	
	@Override
	public String toString() {
		return image;
	}

}
