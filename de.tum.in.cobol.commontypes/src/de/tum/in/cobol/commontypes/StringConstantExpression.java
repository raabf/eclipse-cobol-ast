package de.tum.in.cobol.commontypes;

public class StringConstantExpression extends ConstantExpression {
    private final String string;
    
    public StringConstantExpression(String string) {
        this.string = string;
    }

    public String getExpression() {
        return string;
    }
    
    @Override
    public String toString() {
        return "\"" + string + "\"";
    }
}
