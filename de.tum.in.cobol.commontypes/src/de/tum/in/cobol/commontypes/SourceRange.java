package de.tum.in.cobol.commontypes;

public class SourceRange {
    private final SourcePosition source;
    private final SourcePosition target;

    public SourceRange(SourcePosition source, SourcePosition target) {
        this.source = source;
        this.target = target;
    }

    public SourcePosition getSource() {
        return source;
    }

    public SourcePosition getTarget() {
        return target;
    }
    
    @Override
    public String toString() {
		return source.toString() + " - " + target.toString(); 
	}
}
