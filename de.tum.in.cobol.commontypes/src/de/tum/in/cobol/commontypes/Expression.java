package de.tum.in.cobol.commontypes;

public interface Expression extends IdentifierOrExpression {
    @Override
    public abstract String toString();
}
