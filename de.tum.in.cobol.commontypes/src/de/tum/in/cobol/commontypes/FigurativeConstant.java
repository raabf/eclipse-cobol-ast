package de.tum.in.cobol.commontypes;

public enum FigurativeConstant {

	/*
	 * The commented one are just aliases.
	 */
	HIGH_VALUE("HIGH-VALUE"),
	// HIGH_VALUES("HIGH-VALUES"),
	LOW_VALUE("LOW-VALUE"),
	// LOW_VALUES("LOW-VALUES"),
	QUOTE("QUOTE"),
	// QUOTES("QUOTES"),
	SPACE("SPACE"),
	// SPACES("SPACES"),
	// ZEROS("ZEROS"),
	// ZEROES("ZEROES"),
	ZERO("ZERO");

	private final String cobolSymbol;

	private FigurativeConstant(String cobolSymbol) {
		this.cobolSymbol = cobolSymbol;
	}

	@Override
	public String toString() {
		return cobolSymbol;
	}

	public static FigurativeConstant cobolSymbol(String cobolSymbol) {
		switch (cobolSymbol.toUpperCase()) {
		case "HIGH-VALUE":
			return FigurativeConstant.HIGH_VALUE;
		case "HIGH-VALUES":
			return FigurativeConstant.HIGH_VALUE;
		case "LOW-VALUE":
			return FigurativeConstant.LOW_VALUE;
		case "LOW-VALUES":
			return FigurativeConstant.LOW_VALUE;
		case "QUOTE":
			return FigurativeConstant.QUOTE;
		case "QUOTES":
			return FigurativeConstant.QUOTE;
		case "SPACE":
			return FigurativeConstant.SPACE;
		case "SPACES":
			return FigurativeConstant.SPACE;
		case "ZERO":
			return FigurativeConstant.ZERO;
		case "ZEROS":
			return FigurativeConstant.ZERO;
		case "ZEROES":
			return FigurativeConstant.ZERO;
		default:
			return null;
		}

	}
}
