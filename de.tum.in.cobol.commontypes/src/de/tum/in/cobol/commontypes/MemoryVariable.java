package de.tum.in.cobol.commontypes;

public class MemoryVariable extends Variable implements Expression {

	public MemoryVariable(String name) {
		super(name);
	}

}