package de.tum.in.cobol.commontypes;

public abstract class Variable extends Identifier {
	public final String name;

	public Variable(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
