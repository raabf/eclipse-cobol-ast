package de.tum.in.cobol.test.visualization;

import de.tum.in.cobol.controlflow.*;
import de.tum.in.cobol.controlflow.dummydata.DummyData;
import de.tum.in.cobol.visualization.CFGVComponent;

public class TestCFGVComponent {
	public static void main(String[] args) {
		ControlFlowGraph CFG = DummyData.generateControlFlowGraph();
		String homePath = System.getProperty("user.home");
		
		CFGVComponent.visualize(CFG, homePath);
	}
}
