package parser.analysis.cobol;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import convex.step2.parser.ASTSqlClause;
import convex.step2.parser.ASTSql_list;
import convex.step2.parser.ASTSql_stat;
import convex.step2.parser.CobolParserDefaultVisitor;
import convex.step2.parser.ParseException;
import convex.step2.parser.SimpleNode;
import parser.analysis.cobol.CobolAnalysisWorkspace;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.errorhandling.ILowLevelErrorHandler;
import parser.errorhandling.Message;

public class XMLVisitor extends CobolParserDefaultVisitor {

	private PrintWriter bf;

	public XMLVisitor(PrintWriter bf) {
		// TODO Auto-generated constructor stub
		this.bf = bf;
	}

	public Object defaultVisit(SimpleNode node, RepresentationFactory data) {
		bf.println("<" + node.getClass().getSimpleName() + ">" + node.jjtGetFirstToken().toString());
		node.childrenAccept(this, data);
		bf.println("</" + node.getClass().getSimpleName() + ">" + node.jjtGetLastToken().toString());
		return data;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PrintWriter bf = null;
		try {
			bf = new PrintWriter("output.xml");
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		try {
			File file = new File("../de.itestra.cobol.parser/test-resources/test3.cbl");
			File file2 = new File("../../cobol-lab-resources/benchmarks/itestra/pb.cbl");

			CobolAnalysisWorkspace workspace = new CobolAnalysisWorkspace(new ILowLevelErrorHandler() {
				@Override
				public void onWarning(SimpleNode node, Message warning) {
					// TODO Auto-generated method stub
				}

				@Override
				public void onError(ParseException e) {
					// TODO Auto-generated method stub
				}
			});

			SimpleNode root = (SimpleNode) workspace.getAST(file);

			XMLVisitor visitor = new XMLVisitor(bf);

			root.childrenAccept(visitor, null);

			// visitor.getInstructions().stream().forEach(System.out::println);

			bf.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

}
