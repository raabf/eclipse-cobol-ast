package de.tum.in.cobol.visualization;

import java.util.List;
import java.util.Hashtable;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.layout.FillLayout; 
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphNode; 
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.core.widgets.GraphConnection;

import de.tum.in.cobol.controlflow.*;
import de.tum.in.cobol.controlflow.importers.Size;
import de.tum.in.cobol.controlflow.importers.DotNode;
import de.tum.in.cobol.controlflow.importers.DotGraph;
import de.tum.in.cobol.controlflow.importers.DotParser;

public class CFGVComponent {

	private static DotGraph readDotGraph(String folderPath) {
		DotGraph dotG = null;
		
		try {
			Process proc = Runtime.getRuntime().exec("dot -Tdot " + folderPath + "/dot-graph.txt -o " + folderPath + "/graph.txt");
			proc.waitFor();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		
		try {
			String content = new String(Files.readAllBytes(Paths.get(folderPath, "graph.txt")));
			dotG = DotParser.parseGraphDescription(content);
		} catch (IOException e) {
			System.out.println(e.toString());
		}
		
		return dotG;
	}
	
	private static Size drawGraph(Display display, Shell shell, ControlFlowGraph CFG, String path) {
		Hashtable<Integer,GraphNode> IDGraphNodes = new Hashtable<Integer,GraphNode>();
		Hashtable<GraphNode,GraphConnection> GraphSourceConnections = new Hashtable<GraphNode,GraphConnection>();
		List<Node> nodes = CFG.getNodes();
		List<Edge> edges = EdgeCollector.collectAllEdges(nodes);
		
		final Graph graph = new Graph(shell, SWT.NONE);
		
		DotGraph dotG = readDotGraph(path);
		List<DotNode> dotNodes = dotG.getNodes();
		
		Size sizeG = dotG.getSize();
		
		for (DotNode dotN : dotNodes) {
			GraphNode graphN = new GraphNode(graph, ZestStyles.CONNECTIONS_DIRECTED, Integer.toString(dotN.getId()));
			graphN.setLocation(2*dotN.getPosition().getX(), sizeG.getHeight() - dotN.getPosition().getY());
			System.out.println(dotN.getId() + " : " + graphN.getLocation().x + ", " + graphN.getLocation().y);
			IDGraphNodes.put(dotN.getId(), graphN);
		}
		
		for (Edge e : edges) {
			Node source = e.getSource();
			Node target = e.getTarget();
			GraphNode v1 = IDGraphNodes.get(source.getId());
			GraphNode v2 = IDGraphNodes.get(target.getId());
			
			// create the GraphConnection edge
			GraphConnection conn = new GraphConnection(graph, ZestStyles.CONNECTIONS_DIRECTED, v1, v2);
			conn.setText(e.toString());
			// if there existed already an edge between the two nodes, make the new one curved
			if ((GraphSourceConnections.get(v1) != null && GraphSourceConnections.get(v1).getDestination() == v2)
				|| (GraphSourceConnections.get(v2) != null && GraphSourceConnections.get(v2).getDestination() == v1)) {
				conn.setCurveDepth(100);
			}
			conn.addListener(SWT.MouseDoubleClick, new Listener() {

				@Override
				public void handleEvent(Event event) {
					System.out.println(conn.getText());
				}
				
			});
			GraphSourceConnections.put(v1, conn);
		}
		
		return sizeG;
	}
	
	public static void visualize(ControlFlowGraph CFG, String folderPath) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setText("ControlFlowGraph"); 
		shell.setLayout(new FillLayout()); 
		
		Size sizeG = drawGraph(display, shell, CFG, folderPath);
		
		int xSize = 2*((int) sizeG.getWidth() + 1);
		int ySize = (int) sizeG.getHeight() + 1;
		
		shell.setSize(xSize, ySize);
		
		shell.open();
		while (!shell.isDisposed()) { 
			while (!display.readAndDispatch()) { 
				display.sleep(); 
			} 
		}
	}
	
	public static void main(String[] args) {
		
	}
}