/**
 * 
 */
package de.tum.in.www2.cobol.ast2minicobol;

/**
 * @author raabf
 *
 */
public class UnexpectedToken extends Exception {

	/**
	 * 
	 */
	public UnexpectedToken() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param message
	 */
	public UnexpectedToken(String expected, String given) {
		super(given + " was given, but " +  expected + " Token expected.");
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public UnexpectedToken(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public UnexpectedToken(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnexpectedToken(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnexpectedToken(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
