package de.tum.in.www2.cobol.ast2minicobol;

public class UnexpectedASTNode extends Exception {

	public UnexpectedASTNode() {
		// TODO Auto-generated constructor stub
	}
	
	public UnexpectedASTNode(String expected, String given) {
		super(given + " was given, but " +  expected + " AST Node expected.");
	}

	public UnexpectedASTNode(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UnexpectedASTNode(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public UnexpectedASTNode(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UnexpectedASTNode(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
