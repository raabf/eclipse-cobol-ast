/**
 * 
 */
package de.tum.in.www2.cobol.ast2minicobol;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import convex.step2.parser.ParseException;
import convex.step2.parser.SimpleNode;
import de.tum.in.cobol.commontypes.instructions.Instruction;
import parser.analysis.cobol.CobolAnalysisWorkspace;
import parser.errorhandling.ILowLevelErrorHandler;
import parser.errorhandling.Message;

/**
 * @author raabf
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		File file = new File("../de.itestra.cobol.parser/test-resources/add.cbl");
		File file2 = new File("../../cobol-lab-resources/benchmarks/itestra/pb.cbl");
		File file3 = new File("../../cobol-lab-resources/benchmarks/itestra/dckonv.cbl");
		File file4 = new File("../../cobol-lab-resources/benchmarks/itestra/starta.cbl");
		
		getInstructionsFromFile(file4).stream().forEach(System.out::println);
	}

	public static List<Instruction> getInstructionsFromFile(File file) {
		try {

			CobolAnalysisWorkspace workspace = new CobolAnalysisWorkspace(new ILowLevelErrorHandler() {
				@Override
				public void onWarning(SimpleNode node, Message warning) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onError(ParseException e) {
					// TODO Auto-generated method stub

				}
			});

			SimpleNode root = (SimpleNode) workspace.getAST(file);

			AST2MiniCobolVisitor visitor = new AST2MiniCobolVisitor();

			root.childrenAccept(visitor, null);

			return visitor.getInstructions();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return null;
	}

}
