/**
 * @author Fabian Raab <fabian.raab@tum.de>
 */
package de.tum.in.www2.cobol.ast2minicobol;

import java.lang.reflect.InvocationTargetException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.stream.StreamSupport;

import convex.step2.parser.ASTAddStatement;
import convex.step2.parser.ASTArithmeticExpression;
import convex.step2.parser.ASTCallByContent;
import convex.step2.parser.ASTCallByReference;
import convex.step2.parser.ASTCallStatement;
import convex.step2.parser.ASTCloseStatement;
import convex.step2.parser.ASTComputeStatement;
import convex.step2.parser.ASTCondition;
import convex.step2.parser.ASTContinueStatement;
import convex.step2.parser.ASTDisplayAttribute;
import convex.step2.parser.ASTDisplayStatement;
import convex.step2.parser.ASTDivideStatement;
import convex.step2.parser.ASTFigurativeConstant;
import convex.step2.parser.ASTGivingClause;
import convex.step2.parser.ASTGotoStatement;
import convex.step2.parser.ASTIdentifier;
import convex.step2.parser.ASTIfStatement;
import convex.step2.parser.ASTLeftOperand;
import convex.step2.parser.ASTMoveStatement;
import convex.step2.parser.ASTMultiplyStatement;
import convex.step2.parser.ASTNextStatement;
import convex.step2.parser.ASTNonNumericConstant;
import convex.step2.parser.ASTNumericConstant;
import convex.step2.parser.ASTOpenStatement;
import convex.step2.parser.ASTParagraph;
import convex.step2.parser.ASTParagraphName;
import convex.step2.parser.ASTParagraphs;
import convex.step2.parser.ASTPerformProcedureScopeClause;
import convex.step2.parser.ASTPerformStatement;
import convex.step2.parser.ASTProcedureBody;
import convex.step2.parser.ASTProcedureDivision;
import convex.step2.parser.ASTProcedureName;
import convex.step2.parser.ASTProcedureSection;
import convex.step2.parser.ASTQualifiedDataName;
import convex.step2.parser.ASTReadStatement;
import convex.step2.parser.ASTRightOperand;
import convex.step2.parser.ASTSectionHeader;
import convex.step2.parser.ASTSentence;
import convex.step2.parser.ASTSubscript;
import convex.step2.parser.ASTSubtractStatement;
import convex.step2.parser.ASTWriteStatement;
import convex.step2.parser.CobolParserConstants;
import convex.step2.parser.CobolParserDefaultVisitor;
import convex.step2.parser.SimpleNode;
import convex.step2.parser.Token;
import de.tum.in.cobol.commontypes.BinaryExpression;
import de.tum.in.cobol.commontypes.Expression;
import de.tum.in.cobol.commontypes.FigurativeConstant;
import de.tum.in.cobol.commontypes.FigurativeConstantExpression;
import de.tum.in.cobol.commontypes.FileVariable;
import de.tum.in.cobol.commontypes.Identifier;
import de.tum.in.cobol.commontypes.IdentifierOrConstant;
import de.tum.in.cobol.commontypes.IdentifierOrExpression;
import de.tum.in.cobol.commontypes.Label;
import de.tum.in.cobol.commontypes.Marker;
import de.tum.in.cobol.commontypes.MemoryVariable;
import de.tum.in.cobol.commontypes.NumericConstantExpression;
import de.tum.in.cobol.commontypes.Operator;
import de.tum.in.cobol.commontypes.Placeholder;
import de.tum.in.cobol.commontypes.Procedure;
import de.tum.in.cobol.commontypes.RegisterVariable;
import de.tum.in.cobol.commontypes.SourcePosition;
import de.tum.in.cobol.commontypes.SourceRange;
import de.tum.in.cobol.commontypes.StringConstantExpression;
import de.tum.in.cobol.commontypes.Subscript;
import de.tum.in.cobol.commontypes.Variable;
import de.tum.in.cobol.commontypes.instructions.AssignmentInstruction;
import de.tum.in.cobol.commontypes.instructions.CallInstruction;
import de.tum.in.cobol.commontypes.instructions.DisplayInstruction;
import de.tum.in.cobol.commontypes.instructions.EndProcedureInstruction;
import de.tum.in.cobol.commontypes.instructions.ExecInstruction;
import de.tum.in.cobol.commontypes.instructions.Instruction;
import de.tum.in.cobol.commontypes.instructions.JumpInstruction;
import de.tum.in.cobol.commontypes.instructions.JumpZeroInstruction;
import de.tum.in.cobol.commontypes.instructions.NoOpInstruction;
import de.tum.in.cobol.commontypes.instructions.PopArgumentsInstruction;
import de.tum.in.cobol.commontypes.instructions.PushArgumentByReferenceInstruction;
import de.tum.in.cobol.commontypes.instructions.PushArgumentByValueInstruction;
import de.tum.in.cobol.commontypes.instructions.files.FileAccess;
import de.tum.in.cobol.commontypes.instructions.files.FileCloseInstruction;
import de.tum.in.cobol.commontypes.instructions.files.FileOpenInstruction;
import de.tum.in.cobol.commontypes.instructions.files.FileOpenMode;
import de.tum.in.cobol.commontypes.instructions.files.FileReadInstruction;
import de.tum.in.cobol.commontypes.instructions.files.FileWriteInstruction;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;

/**
 * @author Fabian Raab <fabian.raab@tum.de>
 *
 *         realted issue:
 *         https://versioncontrolseidl.in.tum.de/cobol-lab/cobol-lab/issues/27
 */
public class AST2MiniCobolVisitor extends CobolParserDefaultVisitor {

	public final String COBOL_LOW_VALUE = "X\"00\"";
	public final String COBOL_HIGH_VALUE = "X\"FF\"";

	public final String ACCUMULATOR_REGISTER_STRING = "acc";
	public final RegisterVariable ACCUMULATOR_REGISTER = new RegisterVariable(ACCUMULATOR_REGISTER_STRING);

	public final String ERROR_REGISTER_STRING = "err";
	public final RegisterVariable ERROR_REGISTER = new RegisterVariable(ERROR_REGISTER_STRING);

	public final String INDEX_REGISTER_STRING = "idx";
	public final RegisterVariable INDEX_REGISTER = new RegisterVariable(INDEX_REGISTER_STRING);

	public final String COUNTER_REGISTER_STRING = "cnt";
	public final RegisterVariable COUTNER_REGISTER = new RegisterVariable(COUNTER_REGISTER_STRING);

	public final String FILE_STATE_REGISTER_STRING = "fst";
	public final RegisterVariable FILE_STATE_REGISTER = new RegisterVariable(FILE_STATE_REGISTER_STRING);

	/** hold all used memoryVariables in the input program */
	private PutIfAbsentDefaultedHashMap<String, MemoryVariable> memoryVariables;
	/** hold all used fileVariables in the input program */
	private PutIfAbsentDefaultedHashMap<String, FileVariable> fileVariables;
	/** on ordered list of all instructions creted in the visits */
	private List<Instruction> instructions;
	/**
	 * Maintains a list of paragraph Names (like written in cobol Source code)
	 * for the actual Section.
	 */
	private List<String> paragraphs;
	/**
	 * Maintains a list of Section and Paragraph Names (like written in cobol
	 * Source code) to its Procedure Object.
	 */
	private PutIfAbsentDefaultedHashMap<String, Procedure> sectionsParagraphs;
	/**
	 * Maintains a Label generation count for a given unique string.
	 * Additionally the section where th String is last observed, is stored.
	 * Should be only accessed by {@link AST2MiniCobolVisitor#labelFactory()
	 * labelFactory()} functions.
	 */
	private Map<String, SimpleEntry<Boolean, Integer>> labelCounts;

	/**
	 * During the visit of leftOperands they store themselve here for processing
	 * in the statements
	 */
	private List<SimpleEntry<Expression, SimpleNode>> leftOperands;
	/**
	 * During the visit of rightOperands they store themselve here for
	 * processing in the statements
	 */
	private List<SimpleEntry<IdentifierOrExpression, SimpleNode>> rightOperands;
	/**
	 * During the visit of givingClauses they store themselve here for
	 * processing in the statements
	 */
	private List<SimpleEntry<IdentifierOrExpression, SimpleNode>> givingClauses;

	/** hold a Procedure object created by a child to push it to its parent */
	private Stack<Procedure> paragraphNames;
	/**
	 * A list of paragraphs as they are visited (top to bottom of cobol file).
	 * The top of stack is the paragraph you are actual in. Also includes
	 * sections (sections can begin with sentences). Contains a dummy Procedure
	 * "PROCEDURE-DIVISION" for the beginning.
	 */
	private Stack<Procedure> visitedProcedures;
	/**
	 * A list of Sections as they are visited (top to bottom of cobol file). The
	 * top of stack is the sections you are actual in. Contains a dummy
	 * Procedure "PROCEDURE-DIVISION" for the beginning.
	 */
	private Stack<Procedure> visitedSections;
	/**
	 * The labels were a sentence begins. The bottom is the first sentence of a
	 * procedure. The top is the NEXT SENTENCE of the actual processed sentence.
	 */
	private Stack<Label> sentenceLabels;
	/** stores temporary exec Instruction until it is popped by a parent node */
	private Stack<ExecInstruction> execInstructions;
	/**
	 * stores temporary identifiersExpressions until it is popped by a parent
	 * node
	 */
	private Stack<IdentifierOrExpression> identifiersExpressions;
	/**
	 * stores temporary the subscript until is is popped from the parent again.
	 */
	private Stack<Expression> subscripts;

	public AST2MiniCobolVisitor() {
		this.memoryVariables = new PutIfAbsentDefaultedHashMap<String, MemoryVariable>(String.class,
				MemoryVariable.class);
		this.fileVariables = new PutIfAbsentDefaultedHashMap<String, FileVariable>(String.class, FileVariable.class);
		this.sectionsParagraphs = new PutIfAbsentDefaultedHashMap<String, Procedure>(String.class, Procedure.class);
		this.paragraphs = new ArrayList<>();
		this.instructions = new ArrayList<>();

		this.leftOperands = new ArrayList<>();
		this.rightOperands = new ArrayList<>();
		this.givingClauses = new ArrayList<>();

		this.paragraphNames = new Stack<>();
		this.visitedProcedures = new Stack<>();
		this.visitedSections = new Stack<>();
		this.labelCounts = new HashMap<>();
		this.sentenceLabels = new Stack<>();
		this.execInstructions = new Stack<>();
		this.identifiersExpressions = new Stack<>();
		this.subscripts = new Stack<>();
	}

	public Object defaultVisit(SimpleNode node, RepresentationFactory data) {
		node.childrenAccept(this, data);
		return data;
	}

	/**
	 * Iterable over the tokens which belogs to a node in the AST. It begins at
	 * `firstToken` and ends with `lastToken`.
	 */
	private class SimpleNodeTokenIterable implements Iterable<Token> {

		/** pointer to firstToken savend in the node */
		private Token firstToken;
		/** pointer to lastToken savend in the node */
		private Token lastToken;

		/**
		 * @param node
		 *            a SimpleNode of the ASTTree with a Token range.
		 */
		public SimpleNodeTokenIterable(SimpleNode node) {
			this.firstToken = node.jjtGetFirstToken();
			this.lastToken = node.jjtGetLastToken();
		}

		@Override
		public Iterator<Token> iterator() {
			return new Iterator<Token>() {

				/** actual token */
				private Token token = firstToken;
				/**
				 * true if the iterator has passed the lastToken saved in the
				 * node
				 */
				private Boolean passedLastToken = false;

				@Override
				public boolean hasNext() {
					return !passedLastToken;
				}

				@Override
				public Token next() {
					if (this.token == lastToken)
						this.passedLastToken = true;

					Token prev = token;
					this.token = token.next;
					return prev;
				}
			};
		}

	}

	/**
	 * An Iterable for the Children of a SimpleNode. It uses the uncomfortable
	 * public Interface of SimpleNode for iterating through its children for a
	 * standard conform Iterable.
	 * 
	 * @author raabf
	 *
	 */
	private class SimpleNodeChildIterable implements Iterable<SimpleNode> {

		protected SimpleNode parent;
		protected int max;

		/**
		 * A comfortable Iterable out of the public interface of SimpleNode.
		 * 
		 * @param parent
		 *            A SimpleNode node with children.
		 */
		public SimpleNodeChildIterable(SimpleNode parent) {
			this.parent = parent;
			this.max = parent.jjtGetNumChildren();
		}

		@Override
		public Iterator<SimpleNode> iterator() {
			return new Iterator<SimpleNode>() {

				private int i = 0;

				@Override
				public boolean hasNext() {
					return this.i < max;
				}

				@Override
				public SimpleNode next() {
					return (SimpleNode) parent.jjtGetChild(this.i++);
				}
			};
		}

	}

	/**
	 * Exact the same as HashMap but with an additional putIfAbsentDefaulted()
	 * function. If the key does not already exist in the Map, it will put the
	 * key into the list with a new Object of type V into the list, where the
	 * Contructor of V is called with key as parameter.
	 * 
	 * @author raabf
	 *
	 * @param <K>
	 *            Key Class
	 * @param <V>
	 *            value Class
	 */
	private class PutIfAbsentDefaultedHashMap<K, V> extends HashMap<K, V> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Class<K> keyClass;
		private Class<V> valueClass;

		/**
		 * If the key not aleady exists, it is put before with a new Instance of
		 * V. The Constructor of V is called with key as parameter.
		 * 
		 * @param keyClass
		 *            The class of K. Just pass K.class.
		 * @param valueClass
		 *            The class of V. Just pass V.class
		 */
		public PutIfAbsentDefaultedHashMap(Class<K> keyClass, Class<V> valueClass) {
			this.keyClass = keyClass;
			this.valueClass = valueClass;
		}

		public V putIfAbsentDefaulted(K key) {
			V v = this.get(key);
			if (v == null) {
				V value = null;
				try {
					value = valueClass.getDeclaredConstructor(this.keyClass).newInstance(key);
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (SecurityException e) {
					e.printStackTrace();
				}
				this.put(key, value);
				v = this.get(key);
			}
			return v;

		}
	}

	/**
	 * Returns a SourceRange of SourcePositions with the begin Line & Column of
	 * node and with the end line & column of node.
	 * 
	 * @param node
	 *            begin line & column and end line & column Node
	 * @return SourceRange between begin Node and End Node
	 */
	private SourceRange sourceRangeFactory(SimpleNode node) {
		return sourceRangeFactory(node, node);
	}

	/**
	 * Returns a SourceRange of SourcePositions with the begin Line & Column of
	 * begin Node and with the end line & column of end Node.
	 * 
	 * @param begin
	 *            begin line & column Node
	 * @param end
	 *            end line & column Node
	 * @return SourceRange between begin Node and End Node
	 */
	private SourceRange sourceRangeFactory(SimpleNode begin, SimpleNode end) {
		Token firstToken = begin.jjtGetFirstToken();
		Token lastToken = end.jjtGetLastToken();

		return new SourceRange(new SourcePosition(firstToken.beginLine, firstToken.beginColumn),
				new SourcePosition(lastToken.endLine, lastToken.endColumn));
	}

	/**
	 * Initializes temporary lists with new Objects and therefore empties them.
	 */
	private void clearTemporaryArithmeticLists() {
		leftOperands.clear();
		rightOperands.clear();
		givingClauses.clear();
	}

	/**
	 * returns Expression Objects according to the kind of the token.
	 * 
	 * @deprecated
	 */
	protected Expression tokenKind2Expression(Token token) {
		Expression ret = null;
		switch (token.kind) {
		case CobolParserConstants.COBOL_WORD:
			ret = memoryVariables.putIfAbsentDefaulted(token.image);
			break;
		case CobolParserConstants.LEVEL_NUMBER:
			// FIXME
			// https://versioncontrolseidl.in.tum.de/cobol-lab/cobol-lab/issues/31
			ret = new NumericConstantExpression(token.image);
			break;

		default:
			break;
		}
		return ret;
	}

	public List<Instruction> getInstructions() {
		return this.instructions;
	}

	/**
	 * A label factory which generates a new Label with an unique name. The
	 * Label name will contain position information like section/paragraph.
	 * 
	 * @return A new unused Label
	 */
	protected Label labelFactory() {
		return labelFactory(null);
	}

	/**
	 * A label factory which generates a new Label with an unique name. The
	 * Label name will contain position information like section/paragraph.
	 * Appends suffix to Label name with an '-' as seperator.
	 * 
	 * @param suffix
	 *            String will be appended to Label name
	 * @return A new unused Label with suffix appended
	 */
	protected Label labelFactory(String suffix) {
		Procedure procedure = visitedProcedures.peek();
		return labelFactory(procedure.getProcedureName(), suffix, false);
	}

	/**
	 * A label factory which generates a new Label with an unique name. The
	 * Label name will contain position information like section/paragraph.
	 * Appends suffix to Label name with an '-' as seperator.
	 * 
	 * @param paragraphName
	 *            The paragraphName in which the Label will be placed
	 * @param suffix
	 *            String will be appended to Label name
	 * @param isParagraphHeaderLabel
	 *            is true if the returned Label is placed at the paragraph
	 *            Header nad if it is used at the `endp` instruction, else
	 *            false.
	 * @return A new unused Label with suffix appended
	 */
	protected Label labelFactory(String paragraphName, String suffix, Boolean isParagraphHeaderLabel) {
		Procedure section = visitedSections.peek();
		String combined = section.getProcedureName() + "-" + paragraphName;

		SimpleEntry<Boolean, Integer> simpleEntry = labelCounts.get(paragraphName);
		if (simpleEntry != null && simpleEntry.getKey()) {
			return labelFactory(combined, suffix, isParagraphHeaderLabel ? paragraphName : null);
		} else {
			return labelFactory(paragraphName, suffix, isParagraphHeaderLabel ? paragraphName : null);
		}

	}

	/**
	 * Mark the actual paragraph as finished (no further sentences in actual
	 * paragraph). labelFactory() will generate Labels with the section in his
	 * name, if a paragraph with the same name occurs again.
	 */
	protected void labelSetTrueEndParagraph() {
		Procedure procedure = visitedProcedures.peek();
		SimpleEntry<Boolean, Integer> simpleEntry = labelCounts.get(procedure.getProcedureName());
		labelCounts.put(procedure.getProcedureName(), new SimpleEntry<Boolean, Integer>(true, simpleEntry.getValue()));
	}

	/**
	 * A label factory which generates a new Label with an unique name. prefix
	 * an suffix will be concatenated with an '-' as sperator. No position
	 * information will be added.
	 * 
	 * @param prefix
	 *            A prefix for your label name. The same string could be reused.
	 * @param suffic
	 *            A suffix for your label name. The same string could be reused.
	 * @return A new unues Label, which contains prefix + '-' + suffix.
	 */
	protected Label labelFactory(String prefix, String suffix, String procedureName) {
		String prefixSuffix = suffix == null || suffix.equals("") ? prefix : prefix + "-" + suffix;
		SimpleEntry<Boolean, Integer> simpleEntry = labelCounts.get(prefixSuffix);
		Integer count = null;
		if (simpleEntry != null) {
			count = simpleEntry.getValue();
		} else {
			simpleEntry = new SimpleEntry<Boolean, Integer>(false, count);
		}

		Label label = null;
		if (procedureName == null) {
			label = new Label(count == null ? prefixSuffix : prefixSuffix + "_" + count.toString());
		} else {
			label = new Procedure(count == null ? prefixSuffix : prefixSuffix + "_" + count.toString(), procedureName);
		}

		count = count == null ? 0 : count;
		simpleEntry.setValue(++count);
		labelCounts.put(prefixSuffix, simpleEntry);

		return label;
	}

	/**
	 * For a given name in the source code, this method returns the
	 * corresponding Procedure object. A new Procedure Object is creted if it
	 * not exist yet.
	 * 
	 * @param name
	 *            The name of the paragraph or section as seen in the source
	 *            code.
	 * @return corresponding Procedure Object
	 */
	protected Procedure getProcedure(String name) {
		name = name.toUpperCase();
		Procedure procedure = sectionsParagraphs.get(name);

		if (procedure == null) {
			procedure = (Procedure) labelFactory(name, null, true);
			sectionsParagraphs.put(name, procedure);
			return procedure;
		} else {
			return procedure;
		}

	}

	/**
	 * Generates an `err := 0` instruction and add it to the instructions list.
	 * 
	 * @param node
	 *            A SimpleNode for the Source Position
	 */
	private void generateErrRegisterToZeroInstruction(SimpleNode node) {
		// err := 0;
		NumericConstantExpression const0 = new NumericConstantExpression(0);

		AssignmentInstruction err0AI = new AssignmentInstruction(sourceRangeFactory(node, node), false, false,
				ERROR_REGISTER, const0);
		instructions.add(err0AI);
	}

	public Object visit(ASTProcedureDivision node, RepresentationFactory data) {

		Procedure section = (Procedure) labelFactory("PROCEDURE-DIVISION", null, "PROCEDURE-DIVISION");
		visitedProcedures.add(section);
		visitedSections.add(section);

		sentenceLabels.add(section);

		defaultVisit(node, data);
		return data;
	}

	public Object visit(ASTSectionHeader node, RepresentationFactory data) {
		/*
		 * It is exprected that {@link
		 * AST2MiniCobolVisitor#visit(ASTSectionHeader, RepresentationFactory)
		 * visit(ASTSectionHeader, ..)} is always run directly before {@link
		 * AST2MiniCobolVisitor#visit(ASTParagraphs, RepresentationFactory)
		 * visit(ASTParagraphs, ..). You can see both methods as one.
		 */

		// Paragraphs must be unique only in SENTENCEs, so remove all from
		// previous Section
		for (Iterator<String> iterator = paragraphs.iterator(); iterator.hasNext();) {
			String name = iterator.next();
			sectionsParagraphs.remove(name);
		}
		paragraphs.clear();

		/*
		 * There is one not meaningful ASTParagraphs at the beginning which is
		 * just empty and cause therefore several errors, so we jump over that
		 * special one.
		 */
		if (!(node.jjtGetParent() instanceof ASTProcedureBody)) {
			/*
			 * Set the last label of the previous paragraph/section. It is
			 * necessary if the last sentence of the previous paragraph/section
			 * contains a NEXT SENTENCE, then it would jump here. Since it is a
			 * NoOp instruction, it is equivalent to jump to the Procedure
			 * Header Label.
			 */
			Instruction lastLabelI = new NoOpInstruction(sourceRangeFactory(node, node), sentenceLabels.peek());
			instructions.add(lastLabelI);
		}

		Procedure section = getProcedure(node.jjtGetFirstToken().image);
		visitedProcedures.push(section);
		visitedSections.push(section);

		// Label for the section header
		instructions.add(new NoOpInstruction(sourceRangeFactory(node), section));

		defaultVisit(node, data);
		return data;
	}

	public Object visit(ASTSentence node, RepresentationFactory data) {
		Instruction labelI = new NoOpInstruction(sourceRangeFactory(node, node), sentenceLabels.peek());
		instructions.add(labelI);
		
		Label nextLabel = labelFactory();
		sentenceLabels.push(nextLabel);
		
		defaultVisit(node, data);
		return data;
	}

	public Object visit(ASTIdentifier node, RepresentationFactory data) {
		defaultVisit(node, data);

		/*
		 * FIXME In the subscript field there are only arithmetic expressions
		 * allowed. Maybe just Expression is too general. See cobol iso
		 * standart.
		 */

		if (subscripts.empty()) {
			Variable variable = (Variable) identifiersExpressions.pop();
			identifiersExpressions.push(variable);
		} else {
			Variable variable = (Variable) identifiersExpressions.pop();
			Subscript subscript = new Subscript(variable, subscripts.pop());
			identifiersExpressions.push(subscript);
		}

		return data;
	}

	public Object visit(ASTSubscript node, RepresentationFactory data) {
		defaultVisit(node, data);

		// required to detect by the parent that subscript is used
		subscripts.push((Expression) identifiersExpressions.pop());

		return data;
	}

	public Object visit(ASTQualifiedDataName node, RepresentationFactory data) {
		defaultVisit(node, data);

		Variable variable = memoryVariables.putIfAbsentDefaulted(node.jjtGetFirstToken().image);
		identifiersExpressions.push(variable);

		return data;
	}

	public Object visit(ASTNumericConstant node, RepresentationFactory data) {
		defaultVisit(node, data);

		String image = node.jjtGetFirstToken().image;

		/*
		 * FIXME Correctly implement COBOL_LOW_VALUE & COBOL_HIGH_VALUE This is
		 * just a workaround since they are both not integers, so I ignore them,
		 * see
		 * https://versioncontrolseidl.in.tum.de/cobol-lab/cobol-lab/issues/36
		 */
		if (!image.equals(COBOL_LOW_VALUE) && !image.equals(COBOL_HIGH_VALUE)) {
			Expression expression = new NumericConstantExpression(node.jjtGetFirstToken().image);
			identifiersExpressions.push(expression);
		}

		return data;
	}

	public Object visit(ASTNonNumericConstant node, RepresentationFactory data) {
		defaultVisit(node, data);

		String image = node.jjtGetFirstToken().image;
		if (image.startsWith("\"") && image.endsWith("\"")) {
			image = image.substring(1, image.length() - 1);
		}

		Expression expression = new StringConstantExpression(image);
		identifiersExpressions.push(expression);
		return data;
	}

	public Object visit(ASTFigurativeConstant node, RepresentationFactory data) {
		defaultVisit(node, data);

		FigurativeConstant figurativeConstant = FigurativeConstant.cobolSymbol(node.jjtGetFirstToken().image);
		identifiersExpressions.push(new FigurativeConstantExpression(figurativeConstant));

		return data;
	}

	public Object visit(ASTAddStatement node, RepresentationFactory data) {
		// err := 0;
		generateErrRegisterToZeroInstruction(node);

		// generate the three lists
		// TODO call only the Operand childs
		defaultVisit(node, data);

		// is the same in every following case
		Expression addE = leftOperands.stream().map(e -> e.getKey()).reduce(null,
				(exp, val) -> exp == null ? val : new BinaryExpression(exp, val, Operator.Plus));

		// acc := .. + .. + ...
		if (!rightOperands.isEmpty() && givingClauses.isEmpty()) {
			// ADD ... TO ... error ... END-ADD

			AssignmentInstruction accI = new AssignmentInstruction(
					sourceRangeFactory(node, leftOperands.get(leftOperands.size() - 1).getValue()), false, false,
					ACCUMULATOR_REGISTER, addE);
			instructions.add(accI);

			for (Iterator<SimpleEntry<IdentifierOrExpression, SimpleNode>> iterator = rightOperands.iterator(); iterator
					.hasNext();) {
				SimpleEntry<IdentifierOrExpression, SimpleNode> simpleEntry = (SimpleEntry<IdentifierOrExpression, SimpleNode>) iterator
						.next();
				IdentifierOrExpression rightOperand = simpleEntry.getKey();
				Expression rightPlusAcc = new BinaryExpression((Expression) rightOperand, ACCUMULATOR_REGISTER,
						Operator.Plus);

				AssignmentInstruction rightOperandI = new AssignmentInstruction(
						sourceRangeFactory(simpleEntry.getValue(), simpleEntry.getValue()), false, false,
						(Identifier) rightOperand, rightPlusAcc);
				instructions.add(rightOperandI);
			}

		} else if (rightOperands.isEmpty() && !givingClauses.isEmpty()) {
			// ADD ... GIVING ... error ... END-ADD

			AssignmentInstruction accI = new AssignmentInstruction(
					sourceRangeFactory(node, leftOperands.get(leftOperands.size() - 1).getValue()), false, false,
					ACCUMULATOR_REGISTER, addE);
			instructions.add(accI);

			for (Iterator<SimpleEntry<IdentifierOrExpression, SimpleNode>> iterator = givingClauses.iterator(); iterator
					.hasNext();) {
				SimpleEntry<IdentifierOrExpression, SimpleNode> simpleEntry = (SimpleEntry<IdentifierOrExpression, SimpleNode>) iterator
						.next();
				Identifier givingOperand = (Identifier) simpleEntry.getKey();

				AssignmentInstruction givingOperandI = new AssignmentInstruction(
						sourceRangeFactory(simpleEntry.getValue(), simpleEntry.getValue()), false, false, givingOperand,
						ACCUMULATOR_REGISTER);
				instructions.add(givingOperandI);
			}

		} else if (!rightOperands.isEmpty() && !givingClauses.isEmpty()) {
			// ADD ... TO ... GIVING ... error ... END-ADD

			// in this case right operands can be threated as an additional
			// summands for the ADD
			addE = rightOperands.stream().map(e -> (Expression) e.getKey()).reduce(addE,
					(exp, val) -> exp == null ? val : new BinaryExpression(exp, val, Operator.Plus));

			AssignmentInstruction accI = new AssignmentInstruction(
					sourceRangeFactory(node, rightOperands.get(rightOperands.size() - 1).getValue()), false, false,
					ACCUMULATOR_REGISTER, addE);
			instructions.add(accI);

			// and the givingOperands can be threated as TO operands
			for (Iterator<SimpleEntry<IdentifierOrExpression, SimpleNode>> iterator = givingClauses.iterator(); iterator
					.hasNext();) {
				SimpleEntry<IdentifierOrExpression, SimpleNode> simpleEntry = (SimpleEntry<IdentifierOrExpression, SimpleNode>) iterator
						.next();
				IdentifierOrExpression givingOperand = simpleEntry.getKey();
				Expression givingPlusAcc = new BinaryExpression((Expression) givingOperand, ACCUMULATOR_REGISTER,
						Operator.Plus);

				AssignmentInstruction givingOperandI = new AssignmentInstruction(
						sourceRangeFactory(simpleEntry.getValue(), simpleEntry.getValue()), false, false,
						(Identifier) givingOperand, givingPlusAcc);
				instructions.add(givingOperandI);
			}

		}

		clearTemporaryArithmeticLists();
		return data;
	}

	public Object visit(ASTSubtractStatement node, RepresentationFactory data) {
		// err := 0;
		generateErrRegisterToZeroInstruction(node);

		// generate the three lists
		// FIXME call only the Operand childs. Necessary for error handling
		defaultVisit(node, data);

		if (givingClauses.isEmpty()) {
			// SUBTRACT .. FROM .. END-SUBTRACT

			// acc := leftOperand0 + ... + leftOperandN
			Expression addE = leftOperands.stream().map(e -> e.getKey()).reduce(null,
					(exp, val) -> exp == null ? val : new BinaryExpression(exp, val, Operator.Plus));
			AssignmentInstruction accI = new AssignmentInstruction(
					sourceRangeFactory(node, leftOperands.get(leftOperands.size() - 1).getValue()), false, false,
					ACCUMULATOR_REGISTER, addE);
			instructions.add(accI);

			// rightOperand := rightOperand - acc
			for (Iterator<SimpleEntry<IdentifierOrExpression, SimpleNode>> iterator = rightOperands.iterator(); iterator
					.hasNext();) {

				SimpleEntry<IdentifierOrExpression, SimpleNode> simpleEntry = (SimpleEntry<IdentifierOrExpression, SimpleNode>) iterator
						.next();
				IdentifierOrExpression rightOperandIE = simpleEntry.getKey();

				Expression rightMinusAcc = new BinaryExpression((Expression) rightOperandIE, ACCUMULATOR_REGISTER,
						Operator.Minus);

				AssignmentInstruction rightOperandI = new AssignmentInstruction(
						sourceRangeFactory(simpleEntry.getValue(), simpleEntry.getValue()), false, false,
						(Identifier) rightOperandIE, rightMinusAcc);
				instructions.add(rightOperandI);
			}

		} else {
			// SUBTRACT .. FROM .. GIVING .. END-SUBTRACT

			assert (rightOperands.size() == 1);

			// acc := rightOperand - leftOperand0 - ... - leftOperandN
			Expression subtractE = leftOperands.stream().map(e -> (Expression) e.getKey()).reduce(
					(Expression) rightOperands.get(0).getKey(),
					(exp, val) -> new BinaryExpression(exp, val, Operator.Minus));

			AssignmentInstruction accI = new AssignmentInstruction(
					sourceRangeFactory(node, rightOperands.get(0).getValue()), false, false, ACCUMULATOR_REGISTER,
					subtractE);
			instructions.add(accI);

			// givingOperand := acc
			for (Iterator<SimpleEntry<IdentifierOrExpression, SimpleNode>> iterator = givingClauses.iterator(); iterator
					.hasNext();) {
				SimpleEntry<IdentifierOrExpression, SimpleNode> simpleEntry = (SimpleEntry<IdentifierOrExpression, SimpleNode>) iterator
						.next();

				AssignmentInstruction givingOperandI = new AssignmentInstruction(
						sourceRangeFactory(simpleEntry.getValue(), simpleEntry.getValue()), false, false,
						(MemoryVariable) simpleEntry.getKey(), ACCUMULATOR_REGISTER);
				instructions.add(givingOperandI);
			}

		}

		clearTemporaryArithmeticLists();
		return data;
	}

	public Object visit(ASTMultiplyStatement node, RepresentationFactory data) {
		// err := 0;
		generateErrRegisterToZeroInstruction(node);

		// generate the three lists
		// FIXME call only the Operand childs. Necessary for error handling
		defaultVisit(node, data);

		if (givingClauses.isEmpty()) {
			// MULTIPLY .. BY .. END-MULTIPLY
			SimpleEntry<Expression, SimpleNode> leftOperandEntry = leftOperands.get(0);

			// acc := leftOperand
			instructions.add(new AssignmentInstruction(
					sourceRangeFactory(leftOperandEntry.getValue(), leftOperandEntry.getValue()), false, false,
					ACCUMULATOR_REGISTER, leftOperandEntry.getKey()));

			// rightOperand := rightOperand + acc
			rightOperands.stream().forEach(rightOperandEntry -> {
				IdentifierOrExpression rightOperandIE = rightOperandEntry.getKey();
				SimpleNode rightOperandN = rightOperandEntry.getValue();

				Expression multiplyE = new BinaryExpression((Expression) rightOperandIE, ACCUMULATOR_REGISTER,
						Operator.Multiply);
				instructions.add(new AssignmentInstruction(sourceRangeFactory(rightOperandN, rightOperandN), false,
						false, (Identifier) rightOperandIE, multiplyE));
			});
		} else {
			// MULTIPLY .. BY .. GIVING .. END-MULTIPLY
			SimpleEntry<Expression, SimpleNode> leftOperandEntry = leftOperands.get(0);
			SimpleEntry<IdentifierOrExpression, SimpleNode> rightOperandEntry = rightOperands.get(0);

			// acc := leftOperand + rightOperand
			instructions.add(new AssignmentInstruction(
					sourceRangeFactory(leftOperandEntry.getValue(), rightOperandEntry.getValue()), false, false,
					ACCUMULATOR_REGISTER, new BinaryExpression(leftOperandEntry.getKey(),
							(Expression) rightOperandEntry.getKey(), Operator.Multiply)));

			givingClauses.stream().forEach(givingOperandEntry -> {
				instructions.add(new AssignmentInstruction(
						sourceRangeFactory(givingOperandEntry.getValue(), givingOperandEntry.getValue()), false, false,
						(Identifier) givingOperandEntry.getKey(), ACCUMULATOR_REGISTER));
			});

		}

		clearTemporaryArithmeticLists();
		return data;
	}

	public Object visit(ASTDivideStatement node, RepresentationFactory data) {
		defaultVisit(node, data);

		boolean hasRemainder = StreamSupport.stream(new SimpleNodeChildIterable(node).spliterator(), false)
				.anyMatch(e -> e instanceof ASTIdentifier);

		// switch "DIVIDE var1 BY var2 ..." to "DIVIDE var2 INTO var1 ..."
		if (StreamSupport.stream(new SimpleNodeTokenIterable(node).spliterator(), false)
				.anyMatch(e -> e.kind == CobolParserConstants.BY)) {
			/*
			 * Basically you switch the content of the lists leftOperands and
			 * rightOperands here. Because of casting problems, the code is a
			 * bit longer.
			 */
			// BY clause is only allowed with those parameter amounts:
			assert (leftOperands.size() == 1);
			assert (rightOperands.size() == 1);

			// left
			SimpleEntry<Expression, SimpleNode> leftOperandEntry = leftOperands.get(0);
			leftOperands.clear();

			Expression leftOperandE = leftOperandEntry.getKey();
			SimpleNode leftOperandN = leftOperandEntry.getValue();

			SimpleEntry<IdentifierOrExpression, SimpleNode> newLeftOperandEntry = new SimpleEntry<>(leftOperandE,
					leftOperandN);

			// right
			SimpleEntry<IdentifierOrExpression, SimpleNode> rightOperandEntry = rightOperands.get(0);
			rightOperands.clear();

			Expression rightOperandE = (Expression) rightOperandEntry.getKey();
			SimpleNode rightOperandN = rightOperandEntry.getValue();

			SimpleEntry<Expression, SimpleNode> newRightOperandEntry = new SimpleEntry<>(rightOperandE, rightOperandN);

			// add again
			leftOperands.add(newRightOperandEntry);
			rightOperands.add(newLeftOperandEntry);

		}

		// err := 0
		generateErrRegisterToZeroInstruction(node);

		if (!leftOperands.isEmpty() && !rightOperands.isEmpty() && givingClauses.isEmpty() && !hasRemainder) {
			assert (leftOperands.size() == 1);
			// acc := leftOperand
			SimpleEntry<Expression, SimpleNode> leftOperandEntry = leftOperands.get(0);
			instructions.add(new AssignmentInstruction(sourceRangeFactory(leftOperandEntry.getValue()), false, false,
					ACCUMULATOR_REGISTER, leftOperandEntry.getKey()));

			// rightOperand := rightOperand / acc
			for (Iterator<SimpleEntry<IdentifierOrExpression, SimpleNode>> iterator = rightOperands.iterator(); iterator
					.hasNext();) {
				SimpleEntry<IdentifierOrExpression, SimpleNode> rightOperandEntry = iterator.next();

				Expression divideE = new BinaryExpression((Expression) rightOperandEntry.getKey(), ACCUMULATOR_REGISTER,
						Operator.Divide);
				instructions.add(new AssignmentInstruction(sourceRangeFactory(rightOperandEntry.getValue()), false,
						false, (Identifier) rightOperandEntry.getKey(), divideE));
			}

		} else if (!leftOperands.isEmpty() && !rightOperands.isEmpty() && !givingClauses.isEmpty() && !hasRemainder) {
			assert (leftOperands.size() == 1);
			assert (rightOperands.size() == 1);

			SimpleEntry<Expression, SimpleNode> leftOperandEntry = leftOperands.get(0);
			SimpleEntry<IdentifierOrExpression, SimpleNode> rightOperandEntry = rightOperands.get(0);

			// acc := rightOperand / leftOperand
			Expression divideE = new BinaryExpression((Expression) rightOperandEntry.getKey(),
					leftOperandEntry.getKey(), Operator.Divide);
			instructions.add(new AssignmentInstruction(
					sourceRangeFactory(leftOperandEntry.getValue(), rightOperandEntry.getValue()), false, false,
					ACCUMULATOR_REGISTER, divideE));

			// giving Operand := acc
			for (Iterator<SimpleEntry<IdentifierOrExpression, SimpleNode>> iterator = givingClauses.iterator(); iterator
					.hasNext();) {
				SimpleEntry<IdentifierOrExpression, SimpleNode> givingOperandEntry = iterator.next();

				instructions.add(new AssignmentInstruction(sourceRangeFactory(givingOperandEntry.getValue()), false,
						false, (Identifier) givingOperandEntry.getKey(), ACCUMULATOR_REGISTER));
			}
		} else if (!leftOperands.isEmpty() && !rightOperands.isEmpty() && !givingClauses.isEmpty() && hasRemainder) {
			// it is only allowed with those amount of parameters
			assert (leftOperands.size() == 1);
			assert (rightOperands.size() == 1);
			assert (givingClauses.size() == 1);

			SimpleEntry<Expression, SimpleNode> leftOperandEntry = leftOperands.get(0);
			SimpleEntry<IdentifierOrExpression, SimpleNode> rightOperandEntry = rightOperands.get(0);
			SimpleEntry<IdentifierOrExpression, SimpleNode> givingOperandEntry = givingClauses.get(0);

			// acc := rightOperand % leftOperand
			Expression moduloE = new BinaryExpression((Expression) rightOperandEntry.getKey(),
					leftOperandEntry.getKey(), Operator.Mod);
			instructions.add(new AssignmentInstruction(
					sourceRangeFactory(leftOperandEntry.getValue(), rightOperandEntry.getValue()), false, false,
					ACCUMULATOR_REGISTER, moduloE));

			// givingOperand := rightOperand / leftoperand
			Expression divideE = new BinaryExpression((Expression) rightOperandEntry.getKey(),
					leftOperandEntry.getKey(), Operator.Divide);
			instructions.add(new AssignmentInstruction(
					sourceRangeFactory(leftOperandEntry.getValue(), givingOperandEntry.getValue()), false, false,
					(Identifier) givingOperandEntry.getKey(), divideE));

			// remainderOperand := acc
			SimpleNode remainderNode = StreamSupport.stream(new SimpleNodeChildIterable(node).spliterator(), false)
					.filter(e -> e instanceof ASTIdentifier).findFirst().get();
			Identifier remainderOperand = (Identifier) identifiersExpressions.pop();

			instructions.add(new AssignmentInstruction(sourceRangeFactory(remainderNode), false, false,
					remainderOperand, ACCUMULATOR_REGISTER));

		}

		clearTemporaryArithmeticLists();
		return data;

	}

	public Object visit(ASTComputeStatement node, RepresentationFactory data) {
		generateErrRegisterToZeroInstruction(node);

		Marker marker = new Marker();
		identifiersExpressions.add(marker);

		// should be always only one
		SimpleNode arithmeticExpression = StreamSupport.stream(new SimpleNodeChildIterable(node).spliterator(), false)
				.filter(e -> e instanceof ASTArithmeticExpression).findFirst().get();

		arithmeticExpression.jjtAccept(this, data);

		List<Identifier> involvedIdentifiers = new ArrayList<>();

		IdentifierOrExpression identifierOrExpression = identifiersExpressions.pop();
		while (!(identifierOrExpression == marker)) {
			if (identifierOrExpression instanceof Identifier) {
				involvedIdentifiers.add((Identifier) identifierOrExpression);
			}
			identifierOrExpression = identifiersExpressions.pop();
		}

		String imageArithmeticExpression = StreamSupport
				.stream(new SimpleNodeTokenIterable(arithmeticExpression).spliterator(), false).map(e -> e.image)
				.reduce(null, (a, s) -> a == null ? s : a + " " + s);

		Placeholder placeholder = new Placeholder(imageArithmeticExpression, involvedIdentifiers);

		instructions.add(new AssignmentInstruction(sourceRangeFactory(arithmeticExpression), false, false,
				ACCUMULATOR_REGISTER, placeholder));

		StreamSupport.stream(new SimpleNodeChildIterable(node).spliterator(), false)
				.filter(e -> e instanceof ASTIdentifier).forEach(identifierNode -> {
					identifierNode.jjtAccept(this, data);
					instructions.add(new AssignmentInstruction(sourceRangeFactory(identifierNode), false, false,
							(Identifier) identifiersExpressions.pop(), ACCUMULATOR_REGISTER));
				});

		return data;
	}

	public Object visit(ASTLeftOperand node, RepresentationFactory data) {
		defaultVisit(node, data);
		leftOperands.add(new SimpleEntry<>((Expression) identifiersExpressions.pop(), node));

		return data;
	}

	public Object visit(ASTRightOperand node, RepresentationFactory data) {
		defaultVisit(node, data);
		rightOperands.add(new SimpleEntry<>((Expression) identifiersExpressions.pop(), node));

		return data;
	}

	public Object visit(ASTGivingClause node, RepresentationFactory data) {

		for (Iterator<SimpleNode> iterator = new SimpleNodeChildIterable(node).iterator(); iterator.hasNext();) {
			SimpleNode childNode = iterator.next();
			childNode.jjtAccept(this, data);

			givingClauses.add(new SimpleEntry<>((Identifier) identifiersExpressions.pop(), childNode));
		}

		return data;
	}

	public Object visit(ASTGotoStatement node, RepresentationFactory data) {
		defaultVisit(node, data);

		if (node.jjtGetNumChildren() == 1) {
			// GO TO procedure
			Procedure procedure = paragraphNames.pop();
			JumpInstruction jumpInstruction = new JumpInstruction(sourceRangeFactory(node, node), procedure);
			instructions.add(jumpInstruction);
		} else {
			// GO TO procedure0 ... procedureN DEPENDING ON identifier

			// acc := identifier
			Expression depending = (Expression) identifiersExpressions.pop();
			instructions.add(new AssignmentInstruction(sourceRangeFactory(node, node), false, false,
					ACCUMULATOR_REGISTER, depending));

			Stack<Procedure> reversedStack = new Stack<>();
			StreamSupport.stream(new SimpleNodeChildIterable(node).spliterator(), false)
					.filter(e -> e instanceof ASTProcedureName).forEach(e -> {
						reversedStack.push(paragraphNames.pop());
					});

			// acc := acc -1
			// jumpz acc procedure
			StreamSupport.stream(new SimpleNodeChildIterable(node).spliterator(), false)
					.filter(e -> e instanceof ASTProcedureName).forEach(e -> {
						Expression accDecrementE = new BinaryExpression(ACCUMULATOR_REGISTER,
								new NumericConstantExpression(1), Operator.Minus);
						this.instructions.add(new AssignmentInstruction(sourceRangeFactory(e, e), false, false,
								ACCUMULATOR_REGISTER, accDecrementE));
						this.instructions.add(new JumpZeroInstruction(sourceRangeFactory(e, e), ACCUMULATOR_REGISTER,
								reversedStack.pop()));
					});
		}

		return data;
	}

	public Object visit(ASTProcedureSection node, RepresentationFactory data) {
		defaultVisit(node, data);
		return data;
	}

	/**
	 * ASTParagraphs is, except from SectionHeader, the content of a Section
	 * (bad naming). Section relted stuff is done here, although there exist a
	 * {@link AST2MiniCobolVisitor#visit(ASTProcedureSection, RepresentationFactory)
	 * visit(ASTProcedureSection, ..)} parent (it is not guaranteed that
	 * {@link AST2MiniCobolVisitor#visit(ASTProcedureSection, RepresentationFactory)
	 * visit(ASTProcedureSection, ..)} is the parent).
	 */
	public Object visit(ASTParagraphs node, RepresentationFactory data) {

		/*
		 * There is one not meaningful ASTParagraphs at the beginning which is
		 * just empty and cause therefore several errors, so we jump over that
		 * special one.
		 */
		if (!(node.jjtGetParent() instanceof ASTProcedureBody)) {
			/*
			 * ASTSectionHeader should have been already visited and therefore
			 * there already exist an entry on top of paragraphName which names
			 * the actual beginning of the sentence.
			 */
			Label label = labelFactory();
			sentenceLabels.push(label);
		}
		defaultVisit(node, data);

		if (!(node.jjtGetParent() instanceof ASTProcedureBody)) {
			EndProcedureInstruction endProcedureInstruction = new EndProcedureInstruction(
					sourceRangeFactory(node, node), visitedSections.peek(), sentenceLabels.peek());
			instructions.add(endProcedureInstruction);

			/*
			 * Add the last label of the paragraph to the stack, so that the
			 * next section/paragraph could add it.
			 */
			Label lastLabel = labelFactory();
			sentenceLabels.add(lastLabel);
		}

		return data;
	}

	public Object visit(ASTParagraph node, RepresentationFactory data) {
		/*
		 * Set the last label of the previous paragraph. It is necessary if the
		 * last sentence of the previous paragraph contains a NEXT SENTENCE,
		 * then it would jump here. Since it is a NoOp instruction it is
		 * equivalent, to jump to the Procedure Header Label.
		 */
		Instruction prevLastLabelI = new NoOpInstruction(sourceRangeFactory(node, node), sentenceLabels.peek());
		instructions.add(prevLastLabelI);

		// first child is the ParagraphName
		SimpleNode paragraphName = (SimpleNode) node.jjtGetChild(0);
		assert (paragraphName instanceof ASTParagraphName);
		paragraphName.jjtAccept(this, data);

		Procedure procedure = paragraphNames.pop();
		visitedProcedures.push(procedure);
		paragraphs.add(procedure.getProcedureName());
		NoOpInstruction paragraphHeaderLabelI = new NoOpInstruction(sourceRangeFactory(node, node), procedure);
		instructions.add(paragraphHeaderLabelI);

		// The rest of children should be sentences
		Label firstSentence = labelFactory();
		sentenceLabels.push(firstSentence);

		StreamSupport.stream(new SimpleNodeChildIterable(node).spliterator(), false)
				.filter(e -> !(e instanceof ASTParagraphName)).forEach(e -> e.jjtAccept(this, data));

		EndProcedureInstruction endProcedureInstruction = new EndProcedureInstruction(sourceRangeFactory(node, node),
				procedure, sentenceLabels.peek());
		instructions.add(endProcedureInstruction);

		/*
		 * Add the last label of the paragraph to the stack, so that the next
		 * section/paragraph could add it (the next must be able to and also
		 * have to pop an element from sentenceLabels).
		 */
		Label lastLabel = labelFactory();
		sentenceLabels.add(lastLabel);

		labelSetTrueEndParagraph();

		return data;
	}

	public Object visit(ASTProcedureName node, RepresentationFactory data) {
		defaultVisit(node, data);
		return data;
	}

	public Object visit(ASTParagraphName node, RepresentationFactory data) {
		defaultVisit(node, data);

		String name = node.jjtGetFirstToken().image;
		Procedure procedure = getProcedure(name);
		paragraphNames.push(procedure);

		return data;
	}

	public Object visit(ASTPerformStatement node, RepresentationFactory data) {
		defaultVisit(node, data);

		if (node.jjtGetNumChildren() == 1 && node.jjtGetChild(0) instanceof ASTPerformProcedureScopeClause) {
			// "PERFORM paragraph."
			// In this case here we have only a plain PERFORM without any
			// special perform mode.

			instructions.add(execInstructions.pop());
		}

		return data;
	}

	public Object visit(ASTPerformProcedureScopeClause node, RepresentationFactory data) {
		defaultVisit(node, data);

		ExecInstruction execInstruction = null;

		switch (node.jjtGetNumChildren()) {
		case 1:
			Procedure procedure = paragraphNames.pop();

			execInstruction = new ExecInstruction(sourceRangeFactory(node, node), procedure, procedure);
			break;
		case 2:
			Procedure toProcedure = paragraphNames.pop();
			Procedure fromProcedure = paragraphNames.pop();

			execInstruction = new ExecInstruction(sourceRangeFactory(node, node), fromProcedure, toProcedure);
			break;

		default:
			try {
				throw new UnexpectedASTNode("The PERFORM has too many procedure names.");
			} catch (UnexpectedASTNode e) {
				e.printStackTrace();
			}
			break;
		}

		// FIXME ALL perform modes are still missing

		execInstructions.push(execInstruction);

		return data;
	}

	public Object visit(ASTOpenStatement node, RepresentationFactory data) {
		SimpleNode simpleNode = (SimpleNode) node;

		// create iterator over tokens, and skip the first one ("OPEN")
		Iterator<Token> simpleNodeTokenIterator = new SimpleNodeTokenIterable(simpleNode).iterator();
		simpleNodeTokenIterator.next();

		// second token is file open mode
		String fileOpenModeName = simpleNodeTokenIterator.next().toString();
		FileOpenMode fileOpenMode = null;
		switch (fileOpenModeName.toUpperCase()) {
		case "INPUT":
			fileOpenMode = FileOpenMode.in;
			break;
		case "OUTPUT":
			fileOpenMode = FileOpenMode.out;
			break;
		case "I-O":
			fileOpenMode = FileOpenMode.inout;
			break;
		case "EXTEND":
			fileOpenMode = FileOpenMode.ext;
			break;
		default:
			try {
				throw new UnexpectedASTNode("ASTQualifiedDataName", fileOpenModeName);
			} catch (UnexpectedASTNode e) {
				e.printStackTrace();
			}
			break;
		}

		// the third token is file name
		String fileVariableName = simpleNodeTokenIterator.next().toString();
		FileVariable fileVariable = fileVariables.putIfAbsentDefaulted(fileVariableName);

		FileOpenInstruction fileOpenInstruction = new FileOpenInstruction(sourceRangeFactory(simpleNode, simpleNode),
				fileVariable, fileOpenMode);
		instructions.add(fileOpenInstruction);

		return data;
	}

	/**
	 * Specific visit method for traversing CLOSE statement.
	 * 
	 * @param node
	 *            - instance of {@link ASTCloseStatement}
	 * @param data
	 *            - previous data.
	 * @return instance of {@link RepresentationFactory}.
	 */
	public Object visit(ASTCloseStatement node, RepresentationFactory data) {
		SimpleNode simpleNode = (SimpleNode) node;

		String fileVariableName = simpleNode.jjtGetLastToken().image;
		FileVariable fileVariable = fileVariables.putIfAbsentDefaulted(fileVariableName);

		FileCloseInstruction fileCloseInstruction = new FileCloseInstruction(sourceRangeFactory(simpleNode, simpleNode),
				fileVariable);
		instructions.add(fileCloseInstruction);

		return data;
	}

	public Object visit(ASTMoveStatement node, RepresentationFactory data) {
		defaultVisit(node, data);

		/*
		 * FIXME MOVE CORRESPONDING not implemented! Actual there is a
		 * assignment from the entire variable. It must splitted into its
		 * components to be fully correct instructions.
		 */

		Identifier assignmentTarget = (Identifier) identifiersExpressions.pop();
		Expression source = (Expression) identifiersExpressions.pop();

		instructions
				.add(new AssignmentInstruction(sourceRangeFactory(node, node), false, false, assignmentTarget, source));

		return data;
	}

	public Object visit(ASTCondition node, RepresentationFactory data) {

		Marker marker = new Marker();
		identifiersExpressions.add(marker);

		defaultVisit(node, data);

		String image = StreamSupport.stream(new SimpleNodeTokenIterable(node).spliterator(), false).map(e -> e.image)
				.reduce(null, (a, s) -> a == null ? s : a + " " + s);

		ArrayList<Identifier> involvedIdentifiers = new ArrayList<>();
		IdentifierOrExpression identifierOrExpression = identifiersExpressions.pop();
		while (identifierOrExpression != marker) {
			identifierOrExpression = identifiersExpressions.pop();
			if (identifierOrExpression instanceof Identifier) {
				involvedIdentifiers.add((Identifier) identifierOrExpression);
			}
		}

		Placeholder placeholder = new Placeholder(image, involvedIdentifiers);
		AssignmentInstruction accAI = new AssignmentInstruction(sourceRangeFactory(node, node), false, false,
				ACCUMULATOR_REGISTER, placeholder);
		instructions.add(accAI);

		return data;
	}

	public Object visit(ASTDisplayStatement node, RepresentationFactory data) {
		defaultVisit(node, data);

		instructions.add(new DisplayInstruction(sourceRangeFactory(node, node),
				(IdentifierOrConstant) identifiersExpressions.pop()));

		/*
		 * FIXME "DISPLAY .. UPON .." and "DISPLAY .. AT .." missing, see
		 * https://versioncontrolseidl.in.tum.de/cobol-lab/cobol-lab/issues/39
		 * https://versioncontrolseidl.in.tum.de/cobol-lab/cobol-lab/issues/40
		 */

		return data;
	}

	/**
	 * contains only visability information (background, ...) and is therefore
	 * not translated.
	 * 
	 * @param node
	 * @param data
	 * @return
	 */
	public Object visit(ASTDisplayAttribute node, RepresentationFactory data) {
		return data;
	}

	public Object visit(ASTContinueStatement node, RepresentationFactory data) {
		defaultVisit(node, data);

		instructions.add(new NoOpInstruction(sourceRangeFactory(node), null));

		return data;
	}

	public Object visit(ASTIfStatement node, RepresentationFactory data) {
		/*
		 * FIXME The acutal AST does not save which children of the IfStatement
		 * are in the THEN part and which children are in the ELSE. My actual
		 * workaround is to seach for the ELSE token, save the next token and
		 * compare if one child has this token. see
		 * https://versioncontrolseidl.in.tum.de/cobol-lab/cobol-lab/issues/34
		 */
		Token firstElseToken = null;
		for (Iterator<Token> iterator = new SimpleNodeTokenIterable(node).iterator(); iterator.hasNext();) {
			Token token = iterator.next();
			if (token.kind == CobolParserConstants.ELSE) {
				firstElseToken = token.next;
				break;
			}
		}

		Iterator<SimpleNode> iterator = new SimpleNodeChildIterable(node).iterator();
		// IF
		SimpleNode condition = iterator.next();
		assert (condition instanceof ASTCondition);
		condition.jjtAccept(this, data);
		Label thenLabel = labelFactory("IF");
		Instruction jumpz = new JumpZeroInstruction(sourceRangeFactory(node, condition), ACCUMULATOR_REGISTER,
				thenLabel);
		instructions.add(jumpz);

		// THEN
		SimpleNode child = null;
		while (iterator.hasNext()) {
			child = iterator.next();
			if (child.jjtGetFirstToken() == firstElseToken) {
				break;
			}
			child.jjtAccept(this, data);
		}

		// ELSE
		if (firstElseToken != null) {
			Label elseLabel = labelFactory("IF");
			Instruction jumpEndIf = new JumpInstruction(sourceRangeFactory(child, child), elseLabel);
			instructions.add(jumpEndIf);
			Instruction thenLabelI = new NoOpInstruction(sourceRangeFactory(child, child), thenLabel);
			instructions.add(thenLabelI);

			child.jjtAccept(this, data);
			while (iterator.hasNext()) {
				child = iterator.next();
				child.jjtAccept(this, data);
			}

			Instruction endIfLabelI = new NoOpInstruction(sourceRangeFactory(child, child), elseLabel);
			instructions.add(endIfLabelI);

		} else {
			Instruction endIfLabelI = new NoOpInstruction(sourceRangeFactory(child, child), thenLabel);
			instructions.add(endIfLabelI);
		}
		return data;
	}

	public Object visit(ASTNextStatement node, RepresentationFactory data) {
		defaultVisit(node, data);
		Instruction jumpI = new JumpInstruction(sourceRangeFactory(node, node), sentenceLabels.peek());
		instructions.add(jumpI);
		return data;
	}

	/**
	 * Specific visit method for traversing READ statement.
	 * 
	 * @param node
	 *            - instance of {@link ASTReadStatement}.
	 * @param data
	 *            - previous data.
	 * @return instance of {@link RepresentationFactory}.
	 */
	public Object visit(ASTReadStatement node, RepresentationFactory data) {
		// check if clauses are present within the READ statement
		boolean atEndClausePresent = checkIfTokenExists(node, "AT");
		boolean notAtEndClausePresent = checkIfTokenExists(node, "NOT") && checkIfTokenExists(node, "AT");
		boolean invalidKeyClausePresent = checkIfTokenExists(node, "INVALID");
		boolean notInvalidKeyClausePresent = checkIfTokenExists(node, "INVALID") && checkIfTokenExists(node, "NOT");

		// declare parameters of file READ instruction
		FileReadInstruction fileReadInstruction = null;
		FileVariable fileVariable = fileVariables.putIfAbsentDefaulted(node.jjtGetLastToken().image);
		FileAccess fileAccess = null;

		// Decide on the type of file access used based on the clauses in the
		// statement
		if (atEndClausePresent || notAtEndClausePresent) {
			fileAccess = FileAccess.seq;
		} else if (invalidKeyClausePresent || notInvalidKeyClausePresent) {
			fileAccess = FileAccess.rnd;
		} else {
			fileAccess = FileAccess.dyn;
		}
		fileReadInstruction = new FileReadInstruction(sourceRangeFactory(node, node), fileVariable, fileAccess);

		instructions.add(fileReadInstruction);

		// visit and translate all of the clauses in the READ statement
		translateFileStateClause(node, data, atEndClausePresent, notAtEndClausePresent, invalidKeyClausePresent,
				notInvalidKeyClausePresent);

		return data;
	}

	/**
	 * Sepcific visit method for traversing WRITE statement.
	 * 
	 * @param node-
	 *            instance of {@link ASTWriteStatement}.
	 * @param data
	 *            - previous data.
	 * @return instance of {@link RepresentationFactory}.
	 */
	public Object visit(ASTWriteStatement node, RepresentationFactory data) {
		boolean atEndClausePresent = checkIfTokenExists(node, "AT");
		boolean notAtEndClausePresent = checkIfTokenExists(node, "NOT") && checkIfTokenExists(node, "AT");
		boolean invalidKeyClausePresent = checkIfTokenExists(node, "INVALID");
		boolean notInvalidKeyClausePresent = checkIfTokenExists(node, "INVALID") && checkIfTokenExists(node, "NOT");

		FileWriteInstruction fileWriteInstruction = null;
		MemoryVariable memoryVariable = memoryVariables.putIfAbsentDefaulted(node.jjtGetLastToken().image);

		SimpleNode childNode = (SimpleNode) node.jjtGetChild(0);
		MemoryVariable memoryVariable2 = memoryVariables.putIfAbsentDefaulted(childNode.jjtGetFirstToken().image);

		// if there are more children, then FROM clause is present
		if (node.jjtGetNumChildren() > 1) {
			AssignmentInstruction assignmentInstruction = new AssignmentInstruction(
					sourceRangeFactory(childNode, childNode), false, false, memoryVariable2, memoryVariable);
			instructions.add(assignmentInstruction);
			fileWriteInstruction = new FileWriteInstruction(sourceRangeFactory(node, node), memoryVariable2);
			instructions.add(fileWriteInstruction);
		} else {
			fileWriteInstruction = new FileWriteInstruction(sourceRangeFactory(node, node), memoryVariable);
			instructions.add(fileWriteInstruction);
		}

		translateFileStateClause(node, data, atEndClausePresent, notAtEndClausePresent, invalidKeyClausePresent,
				notInvalidKeyClausePresent);

		return data;
	}

	/**
	 * Check if file-state clause is present in the READ/WRITE statement and
	 * translate it to MiniCOBOL. Since there is no other way to know if there
	 * are (NOT) AT END or (NOT) INVALID KEY statements, we have to parse the
	 * tokens.
	 * 
	 * @param node
	 *            - READ or WRITE node
	 * @param data
	 *            - data to be passed to visitors
	 * @param atEndClausePresent
	 *            - indicates whether AT END clause is present
	 * @param notAtEndClausePresent
	 *            - indicates whether NOT AT END clause is present
	 * @param invalidKeyClausePresent
	 *            - indicates whether INVALID KEY clause is present
	 * @param notInvalidKeyClausePresent
	 *            - indicates whether NOT INVALID KEY clause is present
	 */
	public void translateFileStateClause(SimpleNode node, RepresentationFactory data, boolean atEndClausePresent,
			boolean notAtEndClausePresent, boolean invalidKeyClausePresent, boolean notInvalidKeyClausePresent) {

		Label labelL0 = labelFactory("READ");
		Label labelL1 = labelFactory("READ");

		NoOpInstruction noOpInstructionL0 = new NoOpInstruction(sourceRangeFactory(node, node), labelL0);
		NoOpInstruction noOpInstructionL1 = new NoOpInstruction(sourceRangeFactory(node, node), labelL1);

		NumericConstantExpression literal10 = new NumericConstantExpression(10);
		NumericConstantExpression literal23 = new NumericConstantExpression(23);

		//
		if (atEndClausePresent || notAtEndClausePresent) {
			AssignmentInstruction assignmentInstruction = new AssignmentInstruction(sourceRangeFactory(node, node),
					false, false, ACCUMULATOR_REGISTER,
					new BinaryExpression(FILE_STATE_REGISTER, literal10, Operator.Equal));
			instructions.add(assignmentInstruction);
			Instruction jumpz = new JumpZeroInstruction(sourceRangeFactory(node, node), ACCUMULATOR_REGISTER, labelL0);
			instructions.add(jumpz);

		} else if (invalidKeyClausePresent || notInvalidKeyClausePresent) {
			AssignmentInstruction assignmentInstruction = new AssignmentInstruction(sourceRangeFactory(node, node),
					false, false, ACCUMULATOR_REGISTER,
					new BinaryExpression(FILE_STATE_REGISTER, literal23, Operator.Equal));
			instructions.add(assignmentInstruction);
			Instruction jumpz = new JumpZeroInstruction(sourceRangeFactory(node, node), ACCUMULATOR_REGISTER, labelL0);
			instructions.add(jumpz);

		}

		if ((atEndClausePresent && !notAtEndClausePresent) || (!atEndClausePresent && notAtEndClausePresent)
				|| (invalidKeyClausePresent && !notInvalidKeyClausePresent)
				|| (!invalidKeyClausePresent && notInvalidKeyClausePresent)) {

			node.jjtGetChild(1).jjtAccept(this, data);
			instructions.add(noOpInstructionL0);

		} else if ((atEndClausePresent && notAtEndClausePresent)
				|| (invalidKeyClausePresent && notInvalidKeyClausePresent)) {
			node.jjtGetChild(1).jjtAccept(this, data);
			JumpInstruction jumpInstruction = new JumpInstruction(sourceRangeFactory(node, node), labelL1);
			instructions.add(jumpInstruction);
			instructions.add(noOpInstructionL0);
			node.jjtGetChild(2).jjtAccept(this, data);
			instructions.add(noOpInstructionL1);
		}
	}

	/**
	 * Checks whether a given token is present in the node.
	 * 
	 * @param node
	 *            - instance of {@link ASTCallStatement}; node to be traversed
	 *            through.
	 * @param token
	 *            - desired token.
	 * @return true if token exists.
	 */
	public boolean checkIfTokenExists(SimpleNode node, String token) {
		Iterator<Token> simpleNodeTokenIterator = new SimpleNodeTokenIterable(node).iterator();

		while (simpleNodeTokenIterator.hasNext()) {
			Token currentToken = (Token) simpleNodeTokenIterator.next();
			if (currentToken.toString().equals(token)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Specific visitor method for traversing ASTCallStatement node.
	 * 
	 * @param node
	 *            - instance of {@link ASTCallStatement}
	 * @param data
	 *            - previous data.
	 * @return instance of {@link RepresentationFactory}.
	 */
	public Object visit(ASTCallStatement node, RepresentationFactory data) {
		/*
		 * FIXME replace iterative procedure with recursive calls of the
		 * children for better code quality.
		 */

		// parse children, starting from the 1st; check whether args are passed
		// by ref or content
		for (int i = 1; i < node.jjtGetNumChildren(); i++) {
			SimpleNode childNode = (SimpleNode) node.jjtGetChild(i);
			if (childNode instanceof ASTCallByReference) {
				MemoryVariable memoryVariable = memoryVariables.putIfAbsentDefaulted(childNode.jjtGetLastToken().image);
				PushArgumentByReferenceInstruction pushArgumentByReferenceInstruction = new PushArgumentByReferenceInstruction(
						sourceRangeFactory(childNode, childNode), i - 1, memoryVariable);
				instructions.add(pushArgumentByReferenceInstruction);
			} else if (childNode instanceof ASTCallByContent) {
				MemoryVariable memoryVariable = memoryVariables.putIfAbsentDefaulted(childNode.jjtGetLastToken().image);
				PushArgumentByValueInstruction pushArgumentByValueInstruction = new PushArgumentByValueInstruction(
						sourceRangeFactory(childNode, childNode), i - 1, memoryVariable);
				instructions.add(pushArgumentByValueInstruction);
			}
		}

		// the first child node contains the CALL target
		SimpleNode firstNode = (SimpleNode) node.jjtGetChild(0);
		Procedure procedure = getProcedure(firstNode.jjtGetFirstToken().image);
		CallInstruction callInstruction = new CallInstruction(sourceRangeFactory(firstNode, firstNode), procedure);
		instructions.add(callInstruction);

		// if the last child node is not an argument, then it is the RETURNING
		// clause
		SimpleNode lastNode = (SimpleNode) node.jjtGetChild(node.jjtGetNumChildren() - 1);
		PopArgumentsInstruction popArgumentsInstruction = null;
		if (lastNode instanceof ASTIdentifier) {
			MemoryVariable memoryVariable = memoryVariables.putIfAbsentDefaulted(lastNode.jjtGetLastToken().image);
			popArgumentsInstruction = new PopArgumentsInstruction(sourceRangeFactory(lastNode, lastNode),
					memoryVariable);
		} else {
			popArgumentsInstruction = new PopArgumentsInstruction(sourceRangeFactory(lastNode, lastNode), null);
		}
		instructions.add(popArgumentsInstruction);

		return data;
	}

}
