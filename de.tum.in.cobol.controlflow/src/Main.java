import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.file.Paths;

import de.tum.in.cobol.commontypes.instructions.Instruction;
import de.tum.in.cobol.controlflow.ControlFlowGraph;
import de.tum.in.cobol.controlflow.ControlFlowGraphBuilder;
import de.tum.in.cobol.controlflow.dummydata.DummyData;
import de.tum.in.cobol.controlflow.exporters.DotExporter;

public class Main {
	public static void main(String[] args) {
		mainAST2MiniCobol();
	}

	public static void mainDummyData() {
		ControlFlowGraph graph = DummyData.generateControlFlowGraph();

		DotExporter exporter = new DotExporter();
		String dotGraph = exporter.export(graph);

		String homePath = System.getProperty("user.home");
		String filePath = Paths.get(homePath, "dot-graph.dot").toString();

		try (PrintWriter fileWriter = new PrintWriter(filePath)) {
			fileWriter.println(dotGraph);
		} catch (FileNotFoundException e) {
			System.out.print("Couldn't write to file ");
		}
	}

	public static void mainAST2MiniCobol() {
		File add_file = new File("../de.itestra.cobol.parser/test-resources/add.cbl");
		File subtract_file = new File("../de.itestra.cobol.parser/test-resources/subtract.cbl");
		File divide_file = new File("../de.itestra.cobol.parser/test-resources/divide.cbl");
		File multiply_file = new File("../de.itestra.cobol.parser/test-resources/divide.cbl");
		File goto_file = new File("../de.itestra.cobol.parser/test-resources/controlflow/goto.cob");
		File if_file = new File("../de.itestra.cobol.parser/test-resources/controlflow/if2.cbl");
		File procedures_simple_file = new File(
				"../de.itestra.cobol.parser/test-resources/controlflow/procedures_simple.cbl");
		File procedures_gotos_file = new File(
				"../de.itestra.cobol.parser/test-resources/controlflow/procedures_gotos.cbl");

		/*
		 * Requires https://versioncontrolseidl.in.tum.de/cobol-lab/resources
		 * checkout in cobol-lab-resources
		 */
		File call_file = new File("../de.itestra.cobol.parser/test-resources/call.cbl");
		File pb_file = new File("../../cobol-lab-resources/benchmarks/itestra/pb.cbl");
		File dckonv_file = new File("../../cobol-lab-resources/benchmarks/itestra/dckonv.cbl");
		File starta_file = new File("../../cobol-lab-resources/benchmarks/itestra/starta.cbl");

		File file = subtract_file;

		ControlFlowGraphBuilder builder = new ControlFlowGraphBuilder();

		List<Instruction> instructions = de.tum.in.www2.cobol.ast2minicobol.Main.getInstructionsFromFile(file);

		instructions.forEach(System.out::println);

		ControlFlowGraph graph = builder.build(instructions);

		DotExporter exporter = new DotExporter();
		String dotGraph = exporter.export(graph);

		String homePath = System.getProperty("user.home");
		String filePath = Paths.get(homePath, "cobol-lab", file.getName().toString() + ".dot").toString();

		try (PrintWriter fileWriter = new PrintWriter(filePath)) {
			fileWriter.println(dotGraph);
		} catch (FileNotFoundException e) {
			System.err.print("Couldn't write to file " + filePath);
		}
	}
}
