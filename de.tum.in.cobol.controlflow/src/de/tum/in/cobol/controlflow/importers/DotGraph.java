package de.tum.in.cobol.controlflow.importers;

import java.util.List;

public class DotGraph {
    private final Size size;
    private final List<DotNode> nodes;

    public DotGraph(Size size, List<DotNode> nodes) {
        this.size = size;
        this.nodes = nodes;
    }

    public Size getSize() {
        return size;
    }

    public List<DotNode> getNodes() {
        return nodes;
    }
}
