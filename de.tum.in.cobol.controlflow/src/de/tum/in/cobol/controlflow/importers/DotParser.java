package de.tum.in.cobol.controlflow.importers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DotParser {
    public static DotGraph parseGraphDescription(String dotDescription) {
        Size graphSize = parseGraphSize(dotDescription);
        List<DotNode> nodes = parseNodes(dotDescription);

        return new DotGraph(graphSize, nodes);
    }

    private static Size parseGraphSize(String dotDescription) {
        Pattern pattern = Pattern.compile("graph\\s*\\[.*?bb=\"([^\"]+)\"", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(dotDescription);
        matcher.find();

        String boundingBox = matcher.group(1);
        String[] boundingBoxComponents = boundingBox.split(",");

        double width = Double.parseDouble(boundingBoxComponents[2]);
        double height = Double.parseDouble(boundingBoxComponents[3]);

        return new Size(width, height);
    }

    private static List<DotNode> parseNodes(String dotDescription) {
        List<DotNode> nodes = new ArrayList<DotNode>();

        Pattern pattern = Pattern.compile(";\\s*(\\d+)\\s*\\[[^\\]]*?\\bpos=\"([^\"]+)\"");
        Matcher matcher = pattern.matcher(dotDescription);

        while (matcher.find()) {
            String nodeIdGroup = matcher.group(1);
            String posGroup = matcher.group(2);

            int nodeId = Integer.parseInt(nodeIdGroup);
            String[] xAndY = posGroup.split(",");

            double x = Double.parseDouble(xAndY[0]);
            double y = Double.parseDouble(xAndY[1]);

            Position position = new Position(x, y);
            nodes.add(new DotNode(nodeId, position));
        }

        return nodes;
    }
}
