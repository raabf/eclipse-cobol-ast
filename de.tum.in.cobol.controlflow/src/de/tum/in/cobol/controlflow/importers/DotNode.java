package de.tum.in.cobol.controlflow.importers;

public class DotNode {
    private final int id;
    private final Position position;

    public DotNode(int id, Position position) {
        this.id = id;
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public Position getPosition() {
        return position;
    }
}
