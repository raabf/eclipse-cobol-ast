package de.tum.in.cobol.controlflow.exporters;

import de.tum.in.cobol.commontypes.instructions.CallInstruction;
import de.tum.in.cobol.commontypes.instructions.ExecInstruction;
import de.tum.in.cobol.commontypes.instructions.Instruction;
import de.tum.in.cobol.controlflow.Edge;
import de.tum.in.cobol.controlflow.Node;

class Colorizer {
    private static final String BLACK = "black";
    private static final String BLUE = "#1B70E0";
    private static final String RED = "#FE4365";
    
    public static String getColorForNode(Node node) {
        return node.getAnnotation() == null ? BLACK : BLUE;
    }

    public static String getColorForEdge(Edge edge) {
        if (edge.isConditional()) {
            return RED;
        }

        Instruction instruction = edge.getInstruction();

        if (instruction instanceof CallInstruction || instruction instanceof ExecInstruction) {
            return BLUE;
        }

        return BLACK;
    }
}
