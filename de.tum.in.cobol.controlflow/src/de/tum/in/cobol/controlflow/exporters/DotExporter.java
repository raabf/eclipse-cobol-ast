package de.tum.in.cobol.controlflow.exporters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import de.tum.in.cobol.controlflow.ControlFlowGraph;
import de.tum.in.cobol.controlflow.Edge;
import de.tum.in.cobol.controlflow.Node;

public class DotExporter {
    public String export(ControlFlowGraph graph) {
        List<Node> nodes = graph.getNodes();
        int exitNodeId = graph.getExitNode().getId();

        Collection<String> lines = new ArrayList<String>();
        lines.add("digraph control_flow_graph {");

        // Nodes
        for (Node node : nodes) {
            if (node.getId() == exitNodeId) {
                continue;
            }
            
            lines.add(stringifyNode(node));
        }
        
        // Exit node
        lines.add("");
        lines.add("    node [shape=doublecircle]");
        lines.add("    " + exitNodeId);
        lines.add("");
        
        // Edges
        for (Node node : nodes) {
            for (Edge edge : node.getOutgoingEdges()) {
                lines.add(stringifyEdge(edge));
            }
        }

        lines.add("}");

        return String.join("\n", lines);
    }
    
    private static String stringifyNode(Node node) {
        int id = node.getId();
        String annotation = node.getAnnotation();
        
        boolean isAnnotatedNode = annotation != null;
        
        String label = isAnnotatedNode ? annotation : id + "";
        String shape = isAnnotatedNode ? "rectangle" : "circle";
        String color = Colorizer.getColorForNode(node);
        
        return String.format("    %s [label=\"%s\", shape=\"%s\", color=\"%s\", fontcolor=\"%s\"]",
            id, label, shape, color, color);
    }

    private static String stringifyEdge(Edge edge) {
        int sourceId = edge.getSource().getId();
        int targetId = edge.getTarget().getId();

        String edgeColor = Colorizer.getColorForEdge(edge);
        String label = edge.toString();

        return String.format("    %d -> %d [color=\"%s\", fontcolor=\"%s\", label=<  %s    >]",
            sourceId, targetId, edgeColor, edgeColor, label);
    }
}
