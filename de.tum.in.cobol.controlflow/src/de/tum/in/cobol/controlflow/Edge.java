package de.tum.in.cobol.controlflow;

import de.tum.in.cobol.commontypes.Condition;
import de.tum.in.cobol.commontypes.SourcePosition;
import de.tum.in.cobol.commontypes.SourceRange;
import de.tum.in.cobol.commontypes.instructions.Instruction;

public class Edge {
    private Node source;
    private Node target;

    private final Instruction instruction;
    private final Condition condition;

    public Edge(Node source, Node target, Instruction instruction) {
        this.source = source;
        this.target = target;
        this.instruction = instruction;
        this.condition = null;
    }

    public Edge(Node source, Node target, Condition condition) {
        this.source = source;
        this.target = target;
        this.condition = condition;
        this.instruction = null;
    }

    public Node getSource() {
        return source;
    }

    public void setSource(Node newSource) {
        source = newSource;
    }

    public Node getTarget() {
        return target;
    }

    public void setTarget(Node newTarget) {
        target = newTarget;
    }

    public boolean isConditional() {
        return condition != null;
    }

    public Instruction getInstruction() {
        return instruction;
    }

    public Condition getCondition() {
        return condition;
    }

    @Override
    public String toString() {
        if (condition != null) {
            return condition.toString();
        }

        if (instruction == null) {
            return "";
        }
        
        SourceRange location = instruction.getLocation();
        SourcePosition position = location.getSource();
        int line = position.getLine();

        return instruction.toMiniCobolString()
            + " <font color='#b5b5b5'>(" + line + ")</font>";
    }
}
