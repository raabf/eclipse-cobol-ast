package de.tum.in.cobol.controlflow;

import java.util.ArrayList;
import java.util.List;

import de.tum.in.cobol.commontypes.Condition;
import de.tum.in.cobol.commontypes.instructions.Instruction;

public class Node {
    private final int id;
    private final String annotation;
    private final List<Edge> incomingEdges;
    private final List<Edge> outgoingEdges;

    public Node(int id) {
        this(id, null);
    }

    public Node(int id, String annotation) {
        this.id = id;
        this.annotation = annotation;
        this.incomingEdges = new ArrayList<Edge>();
        this.outgoingEdges = new ArrayList<Edge>();
    }

    public int getId() {
        return id;
    }

    public String getAnnotation() {
        return annotation;
    }

    public List<Edge> getIncomingEdges() {
        return incomingEdges;
    }

    public List<Edge> getOutgoingEdges() {
        return outgoingEdges;
    }

    public Node appendTo(Node source) {
        return appendTo(source, (Instruction)null);
    }

    public Node appendTo(Node source, Instruction instruction) {
        Edge edge = new Edge(source, this, instruction);

        source.outgoingEdges.add(edge);
        this.incomingEdges.add(edge);

        return this;
    }

    public Node appendTo(Node source, Condition condition) {
        Edge edge = new Edge(source, this, condition);

        source.outgoingEdges.add(edge);
        this.incomingEdges.add(edge);

        return this;
    }

    @Override
    public String toString() {
        return id + "";
    }
}
