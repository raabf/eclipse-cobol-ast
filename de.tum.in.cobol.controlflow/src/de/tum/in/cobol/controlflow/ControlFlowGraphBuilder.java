package de.tum.in.cobol.controlflow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

import de.tum.in.cobol.commontypes.*;
import de.tum.in.cobol.commontypes.instructions.Instruction;
import de.tum.in.cobol.commontypes.instructions.JumpInstruction;
import de.tum.in.cobol.commontypes.instructions.JumpZeroInstruction;
import de.tum.in.cobol.controlflow.passes.EpsilonEdgeRemovalPass;

public class ControlFlowGraphBuilder {
    public ControlFlowGraph build(Collection<Instruction> instructions) {
        ControlFlowGraph graph = createGraph(instructions);

        // Get rid of epsilon edges
        new EpsilonEdgeRemovalPass().applyTo(graph);

        return graph;
    }

    private ControlFlowGraph createGraph(Collection<Instruction> instructions) {
        Collection<Label> labels = getLabels(instructions);

        ControlFlowGraph graph = new ControlFlowGraph();
        Hashtable<String, Node> labeledNodes = createNodesForLabels(labels, graph);

        // We start at the CFG's entry node
        Node currentNode = graph.getEntryNode();

        for (Instruction instruction : instructions) {
            if (instruction.hasLabel()) {
                // We've encountered a labeled instruction.
                // Therefore, we look up the node that we've created for the label beforehand ...
                String labelName = instruction.getLabel().name;
                Node labeledNode = labeledNodes.get(labelName);

                // ... and append our current node to it to make the current node a possible jump target.
                currentNode.appendTo(labeledNode);
            }

            // Is the current instruction a conditional jump?
            if (instruction instanceof JumpZeroInstruction) {
                JumpZeroInstruction jumpz = (JumpZeroInstruction) instruction;
                RegisterVariable register = jumpz.getRegister();
                Node jumpTargetNode = labeledNodes.get(jumpz.getJumpTarget().name);

                jumpTargetNode.appendTo(currentNode, new IsZeroCondition(register));

                // Create a new node that is entered when the register is *not* zero (and no jump occurs)
                currentNode = graph.createNode().appendTo(currentNode, new IsNotZeroCondition(register));
            }

            // Is the current instruction an unconditional jump?
            else if (instruction instanceof JumpInstruction) {
                JumpInstruction jump = (JumpInstruction) instruction;

                String jumpTargetName = jump.getTarget().name;
                Node targetNode = labeledNodes.get(jumpTargetName);
                
                if (targetNode == null) {
                    throw new RuntimeException("Attempted to jump to label \""
                        + jumpTargetName
                        + "\" but couldn't find any instruction labeled with it");
                }
                
                targetNode.appendTo(currentNode, jump);

                // Create a new node that no other node points to -- it's unreachable!
                currentNode = graph.createNode();
            }

            // Otherwise
            else {
                // Append a new node that is reached through regular control flow
                currentNode = graph.createNode().appendTo(currentNode, instruction);
            }
        }

        // Finally, append the exit node to the current node
        Node exitNode = graph.getExitNode();
        exitNode.appendTo(currentNode);

        return graph;
    }

    private Collection<Label> getLabels(Collection<Instruction> instructions) {
        Collection<Label> labels = new ArrayList<Label>();

        for (Instruction instruction : instructions) {
            Label label = instruction.getLabel();

            if (label != null) {
                labels.add(label);
            }
        }

        return labels;
    }

    private Hashtable<String, Node> createNodesForLabels(Collection<Label> labels, ControlFlowGraph graph) {
        Hashtable<String, Node> labeledNodes = new Hashtable<String, Node>();

        for (Label label : labels) {
            String labelName = label.getName();
            String annotation = label instanceof Procedure
                ? ((Procedure)label).getProcedureName()
                : null;

            Node node = graph.createNode(annotation);
            labeledNodes.put(labelName, node);
        }

        return labeledNodes;
    }
}
