package de.tum.in.cobol.controlflow.dummydata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.tum.in.cobol.commontypes.BinaryExpression;
import de.tum.in.cobol.commontypes.Label;
import de.tum.in.cobol.commontypes.NumericConstantExpression;
import de.tum.in.cobol.commontypes.Operator;
import de.tum.in.cobol.commontypes.Procedure;
import de.tum.in.cobol.commontypes.RegisterVariable;
import de.tum.in.cobol.commontypes.SourcePosition;
import de.tum.in.cobol.commontypes.SourceRange;
import de.tum.in.cobol.commontypes.MemoryVariable;
import de.tum.in.cobol.commontypes.Picture;
import de.tum.in.cobol.commontypes.instructions.*;
import de.tum.in.cobol.controlflow.ControlFlowGraph;
import de.tum.in.cobol.controlflow.ControlFlowGraphBuilder;

public class DummyData {
    public static ControlFlowGraph generateControlFlowGraph() {
        Collection<Instruction> instructions = generateInstructions4();
        ControlFlowGraphBuilder builder = new ControlFlowGraphBuilder();

        return builder.build(instructions);
    }

    public static Collection<Instruction> generateInstructions1() {
        List<Instruction> instructions = new ArrayList<Instruction>();

        RegisterVariable registerAcc = new RegisterVariable("acc");

        Label labelLoop = new Label("LOOP");
        Label labelEnd = new Label("END");

        NumericConstantExpression literal1 = new NumericConstantExpression(new Double(1));
        NumericConstantExpression literal3 = new NumericConstantExpression(new Double(3));
        NumericConstantExpression literal4 = new NumericConstantExpression(new Double(4));
        NumericConstantExpression literal1337 = new NumericConstantExpression(new Double(1337));

        // acc = 3
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerAcc, literal3));

        // LOOP:
        instructions.add(new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                labelLoop));

        // acc = acc - 1
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerAcc, new BinaryExpression(registerAcc, literal1, Operator.Minus)));

        // jumpz acc END
        instructions.add(new JumpZeroInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), registerAcc, labelEnd));

        // jump LOOP
        instructions.add(new JumpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                labelLoop));

        // acc = 1337
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerAcc, literal1337));
        
        // END:
        instructions.add(new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                labelEnd));
        
        // acc = 4
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerAcc, literal4));

        return instructions;
    }

    public static Collection<Instruction> generateInstructions2() {
        List<Instruction> instructions = new ArrayList<Instruction>();

        RegisterVariable registerErr = new RegisterVariable("err");
        RegisterVariable registerAcc = new RegisterVariable("acc");
        RegisterVariable registerCnt = new RegisterVariable("cnt");
        RegisterVariable registerIdx = new RegisterVariable("idx");
        RegisterVariable registerIni = new RegisterVariable("ini");

        MemoryVariable variableA = new MemoryVariable("x_a");
        MemoryVariable variableB = new MemoryVariable("x_b");
        MemoryVariable variableC = new MemoryVariable("x_c");
        MemoryVariable variableX = new MemoryVariable("x_x");

        Procedure paragraphP0 = new Procedure("p0");
        Procedure paragraphBegin = new Procedure("begin");

        NumericConstantExpression zeroLiteral = new NumericConstantExpression(new Double(0));
        NumericConstantExpression oneLiteral = new NumericConstantExpression(new Double(1));
        NumericConstantExpression twoLiteral = new NumericConstantExpression(new Double(2));
        NumericConstantExpression threeLiteral = new NumericConstantExpression(new Double(3));
        NumericConstantExpression fourLiteral = new NumericConstantExpression(new Double(4));
        NumericConstantExpression fiveLiteral = new NumericConstantExpression(new Double(5));
        NumericConstantExpression sevenLiteral = new NumericConstantExpression(new Double(7));

        Label labelLBegin = new Label("L_Begin");
        Label labelL0 = new Label("L_O");
        Label labelL1 = new Label("L_1");
        Label labelL2 = new Label("L_2");
        Label labelL3 = new Label("L_3");
        Label labelL00 = new Label("L_0,0");
        Label labelL01 = new Label("L_O,1");
        Label labelL10 = new Label("L_1,O");
        Label labelL11 = new Label("L_1,1");
        Label labelLp0 = new Label("L_p0");

        Picture typeNumber = new Picture("9");

        // jumpz ini L_O
        instructions.add(new JumpZeroInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), registerIni, labelL0));

        // declare a
        instructions
                .add(new NewInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), 1));
        instructions.add(new DeclareGlobalInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), "x_a", 0, typeNumber));
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, variableA, fiveLiteral));

        // declare b
        instructions
                .add(new NewInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), 1));
        instructions.add(new DeclareGlobalInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), "x_b", 0, typeNumber));
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, variableB, sevenLiteral));

        // declare c
        instructions
                .add(new NewInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), 1));
        instructions.add(new DeclareGlobalInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), "x_c", 0, typeNumber));
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, variableC, twoLiteral));

        // declare x
        instructions
                .add(new NewInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), 1));
        instructions.add(new DeclareGlobalInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), "x_x", 0, typeNumber));
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, variableX, fourLiteral));

        // L_0: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL0));
        // L_Begin:
        instructions.add(new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                labelLBegin));
        // L_0,0:
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL00));

        // err := 0
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, false, registerErr, zeroLiteral));
        // acc := x_a + x_b + x_c
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, false, registerAcc, new BinaryExpression(
                                new BinaryExpression(variableA, variableB, Operator.Plus), variableC, Operator.Plus)));
        // x_b := x_b + acc
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, variableB, new BinaryExpression(variableB, registerAcc, Operator.Plus)));
        // x_x := x_x + acc
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, variableX, new BinaryExpression(variableX, registerAcc, Operator.Plus)));
        // jumpz err L_1
        instructions.add(new JumpZeroInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), registerErr, labelL1));

        // acc = 3.0
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerAcc, threeLiteral));
        // idx = idx + 1.0
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerIdx, new BinaryExpression(registerIdx, oneLiteral, Operator.Plus)));
        // cnt = acc
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerCnt, registerAcc));
        // acc = acc > 0.0
        instructions.add(new AssignmentInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), false, true, registerAcc,
                new BinaryExpression(registerAcc, zeroLiteral, Operator.GreaterThan)));
        // jumpz acc L_3
        instructions.add(new JumpZeroInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), registerAcc, labelL3));
        // L_2: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL2));
        // jumpz cnt L_3
        instructions.add(new JumpZeroInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), registerCnt, labelL3));
        // exec p0 p0
        instructions.add(new ExecInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                paragraphP0, paragraphP0));
        // cnt = cnt - 1.0
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerCnt, new BinaryExpression(registerCnt, oneLiteral, Operator.Minus)));
        // jump L_2
        instructions.add(
                new JumpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL2));
        // L_3: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL3));
        // idx = idx - 1.0
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerIdx, new BinaryExpression(registerIdx, oneLiteral, Operator.Minus)));
        // L_1: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL1));
        // L_O,1: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL01));
        // endp begin
        instructions.add(new EndProcedureInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), paragraphBegin));
        // L_p0: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelLp0));
        // L_1,O: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL10));
        // L_1,1: noop
        instructions.add(
                new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), labelL11));
        // endp p0
        instructions.add(new EndProcedureInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), paragraphP0));

        return instructions;
    }

    private static Collection<Instruction> generateInstructions3() {
        List<Instruction> instructions = new ArrayList<Instruction>();

        RegisterVariable registerAcc = new RegisterVariable("acc");
        Label labelEnd = new Label("END");
        NumericConstantExpression literal0 = new NumericConstantExpression(new Double(0));

        // acc = 3
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerAcc, literal0));

        // jumpz acc END
        instructions.add(new JumpZeroInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), registerAcc, labelEnd));

        // acc = 3
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerAcc, literal0));

        // jumpz acc END
        instructions.add(new JumpZeroInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), registerAcc, labelEnd));

        // END:
        instructions.add(new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                labelEnd));

        return instructions;
    }

    private static Collection<Instruction> generateInstructions4() {
        List<Instruction> instructions = new ArrayList<Instruction>();

        RegisterVariable registerAcc = new RegisterVariable("acc");
        Label labelEnd = new Label("END");
        NumericConstantExpression literal0 = new NumericConstantExpression(new Double(0));
        Procedure procedure = new Procedure("TEST-PARA");

        // acc = 3
        instructions
                .add(new AssignmentInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                        false, true, registerAcc, literal0));

        // jumpz acc END
        instructions.add(new JumpZeroInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), registerAcc, labelEnd));

        // call TEST-PARA
        instructions.add(new CallInstruction(
                new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)), procedure));

        // END:
        instructions.add(new NoOpInstruction(new SourceRange(new SourcePosition(10, 15), new SourcePosition(11, 20)),
                labelEnd));

        return instructions;
    }
}
