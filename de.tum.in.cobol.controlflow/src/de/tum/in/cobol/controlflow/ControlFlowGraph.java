package de.tum.in.cobol.controlflow;

import java.util.ArrayList;
import java.util.List;

public class ControlFlowGraph {
    private int runningNodeId;

    private Node entryNode;
    private Node exitNode;

    private final List<Node> nodes;

    public ControlFlowGraph() {
        nodes = new ArrayList<Node>();
        
        runningNodeId = 0;
        entryNode = createNode();
        exitNode = createNode();
    }

    public Node getEntryNode() {
        return entryNode;
    }

    public void setEntryNode(Node newEntryNode) {
        entryNode = newEntryNode;
    }

    public Node getExitNode() {
        return exitNode;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public Node createNode() {
        return createNode(null);
    }

    public Node createNode(String annotation) {
        Node node = new Node(++runningNodeId, annotation);
        nodes.add(node);

        return node;
    }

    public void removeNode(Node node) {
        if (node == entryNode) {
            entryNode = null;
        }

        if (node == exitNode) {
            exitNode = null;
        }

        nodes.remove(node);
    }
}
