package de.tum.in.cobol.controlflow;

import java.util.ArrayList;
import java.util.List;

public class EdgeCollector {
    public static List<Edge> collectAllEdges(Iterable<Node> nodes) {
        List<Edge> edges = new ArrayList<Edge>();

        for (Node node : nodes) {
            edges.addAll(node.getOutgoingEdges());
        }

        return edges;
    }
}
