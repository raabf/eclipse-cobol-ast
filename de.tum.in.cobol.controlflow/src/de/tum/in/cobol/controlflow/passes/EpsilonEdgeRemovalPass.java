package de.tum.in.cobol.controlflow.passes;

import java.util.List;
import java.util.stream.Collectors;

import de.tum.in.cobol.commontypes.instructions.Instruction;
import de.tum.in.cobol.commontypes.instructions.JumpInstruction;
import de.tum.in.cobol.commontypes.instructions.NoOpInstruction;
import de.tum.in.cobol.controlflow.ControlFlowGraph;
import de.tum.in.cobol.controlflow.Edge;
import de.tum.in.cobol.controlflow.EdgeCollector;
import de.tum.in.cobol.controlflow.Node;

public class EpsilonEdgeRemovalPass implements IControlFlowGraphOptimizationPass {
    @Override
    public void applyTo(ControlFlowGraph graph) {
        for (Node node : getNodesWithOutgoingEpsilonEdge(graph)) {
            // We only want to remove nodes that don't have a special annotation
            if (node.getAnnotation() == null) { 
                removeTransitSourceNode(node, graph);
            }
        }
    }

    private Iterable<Node> getNodesWithOutgoingEpsilonEdge(ControlFlowGraph graph) {
        List<Node> allNodes = graph.getNodes();
        List<Edge> allEdges = EdgeCollector.collectAllEdges(allNodes);

        return allEdges
            .stream()
            .filter(EpsilonEdgeRemovalPass::isEpsilonEdge)
            .map(edge -> edge.getSource())
            .collect(Collectors.toList());
    }

    private static boolean isEpsilonEdge(Edge edge) {
        Instruction instruction = edge.getInstruction();

        return instruction == null && !edge.isConditional()
            || instruction instanceof JumpInstruction
            || instruction instanceof NoOpInstruction;
    }

    private void removeTransitSourceNode(Node node, ControlFlowGraph graph) {
        List<Edge> incomingEdges = node.getIncomingEdges();
        List<Edge> outgoingEdges = node.getOutgoingEdges();

        Edge epsilonEdge = outgoingEdges.get(0);

        Node newTarget = epsilonEdge.getTarget();
        List<Edge> incomingEdgesAtNewTarget = newTarget.getIncomingEdges();

        for (Edge edge : incomingEdges) {
            edge.setTarget(newTarget);
            incomingEdgesAtNewTarget.add(edge);
        }

        incomingEdges.clear();

        outgoingEdges.remove(epsilonEdge);
        newTarget.getIncomingEdges().remove(epsilonEdge);

        if (node == graph.getEntryNode()) {
            graph.setEntryNode(newTarget);
        }

        graph.removeNode(node);
    }
}
