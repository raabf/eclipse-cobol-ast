package de.tum.in.cobol.controlflow.passes;

import de.tum.in.cobol.controlflow.ControlFlowGraph;

public interface IControlFlowGraphOptimizationPass {
    void applyTo(ControlFlowGraph graph);
}
