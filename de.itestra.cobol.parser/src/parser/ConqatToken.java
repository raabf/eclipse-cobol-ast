package parser;

import org.conqat.lib.scanner.IToken;

import convex.step2.parser.Token;

public class ConqatToken extends Token {
	private static final long serialVersionUID = 1L;

	private final IToken conqatToken;
	
	public ConqatToken(IToken conqatToken) {
		this.conqatToken = conqatToken;
	}
	
	@Override
	public Object getValue() {
		return conqatToken;
	}
}
