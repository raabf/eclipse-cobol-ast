package parser;

import java.io.IOException;
import java.util.List;

import org.conqat.lib.scanner.IToken;

public interface ITokenManager {

	/*
	 * This method should return the next token from the scanner.
	 * This allows filtering of the token stream.
	 */
	public IToken nextToken() throws IOException;
	
	/*
	 * Returns list of filtered tokens.
	 * Filtered tokens do not contain tokens ignored during parsing phase.
	 */
	public List<IToken> getFilteredTokens();
}
