package parser;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.conqat.lib.scanner.ELanguage;
import org.conqat.lib.scanner.ILenientScanner;
import org.conqat.lib.scanner.IToken;
import org.conqat.lib.scanner.ScannerFactory;

import parser.analysis.cobol.CobolAnalysisFactory;
import parser.errorhandling.IErrorHandler;
import parser.errorhandling.ILowLevelErrorHandler;
import parser.errorhandling.Message;
import parser.tokenprovider.ExcludeCopybooksTokenProvider;
import parser.tokenprovider.ITokenProvider;
import parser.tokenprovider.filter.AmbigiousTokenFilter;
import parser.tokenprovider.filter.ExecSQLFilter;
import parser.tokenprovider.filter.IgnoredTokensFilter;
import parser.tokenprovider.filter.SpecialTokenHandler;
import convex.step2.parser.CobolParser;
import convex.step2.parser.Node;
import convex.step2.parser.ParseException;
import convex.step2.parser.SimpleNode;
import de.itestra.swgcommons.preprocessing.ExtraColumnBehaviour;
import de.itestra.swgcommons.preprocessing.OnTheFlyPreprocessingFilter;

/**
 * Central point of all parser specific methods and/or settings. Uses the facade
 * pattern to provide uniform access to scanner and parser methods.
 *
 * @author Andreas Wagner
 *
 */
public class CobolParserComponent implements IParser {
	/**
	 * token manager
	 */
	private AbstractCobolTokenManager tm;
	/**
	 * scanner
	 */
	private ILenientScanner scanner;
	/**
	 * root node of AST
	 */
	private Node root;
	/**
	 * the actual parser
	 */
	private CobolParser parser;
	/**
	 * factory for all analysis
	 */
	private CobolAnalysisFactory analysisFactory;
	/**
	 * components to notify if parse errors occur
	 */
	private List<IErrorHandler> errorHandler;
	/**
	 * preprocessor
	 */
	private OnTheFlyPreprocessingFilter preprocessor;
	/**
	 * filename of file to be parsed
	 */
	private String filename;

	public CobolParserComponent(String content, String filename) throws IOException {
		this.filename = filename;
		initPreprocessor(content);
		init();
	}

	public CobolParserComponent(File file) throws IOException {
		filename = file.getPath();
		initPreprocessor(file);
		init();
	}

	private void initPreprocessor(String content) throws IOException {
		initPreprocessor();
		this.preprocessor.reset(new ByteArrayInputStream(content.getBytes()));
	}

	private void initPreprocessor(File file) throws IOException {
		initPreprocessor();
		this.preprocessor.reset(file);
	}

	private void initPreprocessor() {
		preprocessor = new OnTheFlyPreprocessingFilter();
		// only column 6 to 71 are relevant
		preprocessor.setMinRelevantColumn(6);
		preprocessor.setMaxRelevantColumn(71);
		// if extra columns are present, replace them by blanks
		preprocessor.setExtraColumnBehaviour(ExtraColumnBehaviour.blank);
	}

	private void init() throws IOException {
		// setup scanner
		scanner = ScannerFactory.newLenientScanner(ELanguage.COBOL, preprocessor, filename);

		// setup token manager
		initTM(scanner);

		// setup parser
		parser = new CobolParser(tm);

		// setup low-level error handling
		parser.add_error_handler(new ILowLevelErrorHandler() {

			@Override
			public void onError(ParseException e) {
				notifyErrorHandler(e.currentToken.beginLine, e.currentToken.beginColumn,
						((IToken) e.currentToken.getValue()).getOffset(), e.getMessage());
			}

			@Override
			public void onWarning(SimpleNode node, Message warning) {
				// TODO Auto-generated method stub

			}
		});

		// init remaining fields
		analysisFactory = new CobolAnalysisFactory();
		errorHandler = new ArrayList<IErrorHandler>();
	}

	private void initTM(ILenientScanner s) throws IOException {
		ITokenProvider filterChain = new ExcludeCopybooksTokenProvider(s);
		filterChain = new ExecSQLFilter(filterChain);
		filterChain = new IgnoredTokensFilter(filterChain);
		filterChain = new AmbigiousTokenFilter(filterChain);
		filterChain = new SpecialTokenHandler(filterChain);
		tm = new CustomTokenManager(filterChain);
	}

	@Override
	public void parse() throws ParseException {
		parser.ReInit(tm);
		parser.CompilationUnit();
		root = parser.getRootNode();
		analysisFactory.refreshAnalysis(root);
	}

	@Override
	public void reparse(String s) throws IOException, ParseException {
		rescan(s);
		parse();
	}

	@Override
	public void rescan(String s) throws IOException {
		initPreprocessor(s);
		init();
	}

	@Override
	public void rescan(File f) throws IOException {
		initPreprocessor(f);
		init();
	}

	@Override
	public List<IToken> getFilteredTokens() {
		return tm.getFilteredTokens();
	}

	@Override
	public CobolAnalysisFactory getAnalysisFactory() {
		return this.analysisFactory;
	}

	@Override
	public void addErrorHandler(IErrorHandler handler) {
		errorHandler.add(handler);
	}

	@Override
	public void removeErrorHandler(IErrorHandler handler) {
		errorHandler.remove(handler);
	}

	protected void notifyErrorHandler(int line, int column, int offset, String message) {
		for (IErrorHandler h : errorHandler) {
			h.onParseError(line, column, offset, message);
		}
	}

}
