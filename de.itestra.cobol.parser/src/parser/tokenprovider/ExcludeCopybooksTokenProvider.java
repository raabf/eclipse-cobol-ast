package parser.tokenprovider;

import java.io.IOException;

import org.conqat.lib.scanner.ETokenType;
import org.conqat.lib.scanner.ILenientScanner;
import org.conqat.lib.scanner.IToken;


public class ExcludeCopybooksTokenProvider extends ScannerBasedTokenProvider {
	private final ILenientScanner scanner;

	public ExcludeCopybooksTokenProvider(ILenientScanner scanner) {
		this.scanner = scanner;
	}

	@Override
	public IToken nextToken() throws IOException {
		IToken result = nextScannerToken();

		while (result.getType() == ETokenType.COPY) {
			while (result.getType() != ETokenType.DOT) {
				result = nextScannerToken();
			}
			result = nextScannerToken();
		}

		return result;
	}

	@Override
	protected ILenientScanner getCurrentScanner() {
		return scanner;
	}
}
