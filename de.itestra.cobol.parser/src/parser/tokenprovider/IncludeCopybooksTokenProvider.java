package parser.tokenprovider;

import java.io.IOException;
import java.util.Stack;

import org.conqat.lib.scanner.ETokenType;
import org.conqat.lib.scanner.ILenientScanner;
import org.conqat.lib.scanner.IToken;


public class IncludeCopybooksTokenProvider extends ScannerBasedTokenProvider {
	private final CopybookResolver provider;
	private Stack<ILenientScanner> scanners = new Stack<ILenientScanner>();
	
	public IncludeCopybooksTokenProvider(ILenientScanner scanner, CopybookResolver provider) {
		scanners.push(scanner);
		this.provider = provider;
	}

	@Override
	public IToken nextToken() throws IOException {
		IToken result = nextScannerToken();

		while (result.getType() == ETokenType.COPY){
			//TODO there exists example code where the copybook name is quoted (dckonv.cbl). not sure if this is correct cobol syntax though
			String copybookName = nextScannerToken().getText().replace("\"", "");
			String libraryName = null;
			
			while(result.getType() != ETokenType.DOT){
				result = nextScannerToken();
				if (result.getType().equals(ETokenType.IN) || result.getType().equals(ETokenType.OF)) {
					result = nextScannerToken();
					libraryName = result.getText();
				}
				else if (result.getType().equals(ETokenType.REPLACING)) {
					//TODO handle the replacing stuff...
				}
			}
			
			scanners.push(provider.resolve(libraryName, copybookName));
			
			result = nextScannerToken();
			
		}
		
		//ignore EOF if we encounter it in a copybook, instead continue scanning the source of the copy statement
		if (result.getType().equals(ETokenType.EOF) && scanners.size() > 1) {
			scanners.pop();
			result = nextScannerToken();
		}
		
		return result;
	}
	
	@Override
	protected ILenientScanner getCurrentScanner() {
		return scanners.peek();
	}
}
