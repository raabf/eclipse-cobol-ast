package parser.tokenprovider;

import java.io.IOException;

import org.conqat.lib.scanner.ILenientScanner;
import org.conqat.lib.scanner.IToken;


public abstract class ScannerBasedTokenProvider implements ITokenProvider {
	protected final IToken nextScannerToken() throws IOException {
		return getCurrentScanner().getNextToken();
	}
	
	protected abstract ILenientScanner getCurrentScanner();
}
