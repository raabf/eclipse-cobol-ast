package parser.tokenprovider.filter;

import java.io.IOException;

import org.conqat.lib.scanner.IToken;

import parser.tokenprovider.ITokenProvider;

public class SpecialTokenHandler extends AbstractTokenFilter {
	public SpecialTokenHandler(ITokenProvider predecessor) {
		super(predecessor);
	}

	@Override
	public IToken nextToken() throws IOException {
		IToken result = super.nextToken();

		while (specialTokens.contains(result.getText().toUpperCase())) {
			int currentLine = result.getLineNumber();
			while (result.getLineNumber() == currentLine) {
				result = super.nextToken();
			}
		}

		return result;
	}
}
