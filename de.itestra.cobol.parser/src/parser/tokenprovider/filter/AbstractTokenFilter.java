package parser.tokenprovider.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.conqat.lib.scanner.ETokenType;
import org.conqat.lib.scanner.IToken;

import parser.tokenprovider.ITokenProvider;

public abstract class AbstractTokenFilter implements ITokenProvider {
	private final ITokenProvider predecessor;
	
	public AbstractTokenFilter(ITokenProvider predecessor) {
		this.predecessor = predecessor;
	}

	/**
	 * set of ignored tokens - will not be recognized by the parser
	 */
	protected static Set<ETokenType> ignoreTokens = new HashSet<ETokenType>(Arrays.asList(new ETokenType[]{
		ETokenType.ILLEGAL_CHARACTER,
		ETokenType.TRADITIONAL_COMMENT,
		ETokenType.COMMENT,
	}));
	
	/**
	 * set of tokens which might indicate a comment and require special handling
	 */
	protected static Set<ETokenType> commentTokens = new HashSet<ETokenType>(Arrays.asList(new ETokenType[]{
		ETokenType.MULT,
	}));
	
	/**
	 * set of tokens which have to be handled separately
	 */
	protected static Set<String> specialTokens = new HashSet<String>(Arrays.asList(new String[]{
		"$",
		"$SET"
	}));
	
	@Override
	public IToken nextToken() throws IOException {
		return predecessor.nextToken();
	}
}
