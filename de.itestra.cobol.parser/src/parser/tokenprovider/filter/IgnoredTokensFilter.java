package parser.tokenprovider.filter;

import java.io.IOException;

import org.conqat.lib.scanner.IToken;

import parser.tokenprovider.ITokenProvider;

/*
 * This class filters tokens which should not be forwarded to the parser.
 */
public class IgnoredTokensFilter extends AbstractTokenFilter {
	public IgnoredTokensFilter(ITokenProvider predecessor) {
		super(predecessor);
	}

	@Override
	public IToken nextToken() throws IOException {
		IToken result = super.nextToken();

		while (ignoreTokens.contains(result.getType())) {
			result = super.nextToken();
		}

		return result;
	}
}
