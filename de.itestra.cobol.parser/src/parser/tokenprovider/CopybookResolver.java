package parser.tokenprovider;

import java.io.IOException;

import org.conqat.lib.scanner.ILenientScanner;

public interface CopybookResolver {
	/**
	 * resolve a copybook by name, scope may be empty for local search
	 * 
	 * @param scope the library name to search in for the copybook
	 * @param name the name of the copybook
	 * @return a {@link ILenientScanner} of the copybook
	 * @throws IOException 
	 */
	public ILenientScanner resolve(String scope, String name) throws IOException;
}
