package parser.tokenprovider;

import java.io.IOException;

import org.conqat.lib.scanner.IToken;

public interface ITokenProvider {
	public IToken nextToken() throws IOException;
}
