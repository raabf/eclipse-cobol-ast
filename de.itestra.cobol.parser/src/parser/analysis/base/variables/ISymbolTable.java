package parser.analysis.base.variables;

public interface ISymbolTable {
	
	/*
	 * Return locations of symbol declarations
	 */
	public int getDeclarationStart(String varname);
	
	public int getDeclarationEnd(String varname);
	
}
