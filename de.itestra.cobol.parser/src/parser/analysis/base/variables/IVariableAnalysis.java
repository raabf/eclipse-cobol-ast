package parser.analysis.base.variables;


public interface IVariableAnalysis<AST> {
	/**
	 * run the analysis on the given AST
	 *
	 * @param ast
	 *            the AST to analyze
	 */
	public void run(AST ast);

	public ISymbolTable getSymbolTable();
}
