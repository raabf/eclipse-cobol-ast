package parser.analysis.base;

import java.io.File;
import java.net.URL;

import parser.analysis.AnalysisException;
import parser.analysis.base.definition.representation.IDefinitionUseRepresentation;
import parser.analysis.base.structure.representation.IStructureRepresentation;

/**
 * This is the main entry point into the analysis framework. Use this to manage
 * files to analyze
 */
public interface IAnalysisWorkspace {
	/**
	 * Get the structure representation of the resource at the specified
	 * {@link URL}. The resource will be analyzed if it has not been analyzed
	 * yet.
	 *
	 * @param file
	 *            the {@link File} of the resource
	 * @return the structure representation of the resource
	 * @throws AnalysisException
	 */
	public IStructureRepresentation getStructureRepresentation(File file) throws AnalysisException;

	/**
	 * Analyze the structure of the resource at the specified {@link URL}.
	 *
	 * @param file
	 *            the {@link File} of the resource
	 * @return the {@link IStructureRepresentation} of the resource
	 * @throws AnalysisException
	 */
	public IStructureRepresentation analyzeStructure(File file) throws AnalysisException;

	/**
	 * Get the data flow representation of the resource at the specified
	 * {@link URL}. The resource will be analyzed if it has not been analyzed
	 * yet.
	 *
	 * @param file
	 *            the {@link File} of the resource in question
	 * @return the {@link IDefinitionUseRepresentation} representation of the
	 *         resource
	 */
	public IDefinitionUseRepresentation getDataflowRepresentation(File file);

	/**
	 * Analyze the data flow of the resource at the specified {@link URL}
	 *
	 * @param file
	 *            the {@link File} of the resource in question
	 * @return the {@link IDefinitionUseRepresentation} of the resource
	 */
	public IDefinitionUseRepresentation analyzeDataflow(File file);

	/**
	 * Remove all analyses of the resource at the specified URL
	 *
	 * @param file
	 *            the {@link File} of the resource in question
	 */
	public void clearAnalyses(File file);
}
