package parser.analysis.base.structure;

import java.util.List;

import parser.analysis.base.structure.representation.IStructureRepresentation;

public interface IStructureAnalysis<AST> {
	/**
	 * run the analysis on the given AST
	 *
	 * @param ast
	 *            the AST to analyze
	 */
	public void run(AST ast);

	/*
	 * Obtain program structure
	 */
	public IStructureRepresentation getProgramStructure();

	/*
	 * Obtain flat representation of program structure
	 */
	public List<? extends IStructureRepresentation> getProgramStructureFlat();
}
