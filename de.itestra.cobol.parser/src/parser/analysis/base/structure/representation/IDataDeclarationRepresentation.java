package parser.analysis.base.structure.representation;

import java.util.Collection;

import parser.analysis.base.definition.representation.IDefinitionRepresentation;
import parser.analysis.base.definition.representation.IReadDataRepresentation;
import parser.analysis.base.definition.representation.IWriteDataRepresentation;

public interface IDataDeclarationRepresentation extends IStructureRepresentation, IDefinitionRepresentation {
	void addRead(IReadDataRepresentation read);

	Collection<IReadDataRepresentation> getReads();

	void addWrite(IWriteDataRepresentation write);

	Collection<IWriteDataRepresentation> getWrites();

	IDataDeclarationRepresentation getChildRecursive(String name);
}
