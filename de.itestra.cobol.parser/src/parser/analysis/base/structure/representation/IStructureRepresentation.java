package parser.analysis.base.structure.representation;

import java.util.List;

import org.conqat.lib.scanner.IToken;

import de.itestra.swgcommons.util.Scope;

/*
 * This class represents the program structure in a tree-like manner.
 * This fits nicely into an OutlineContentProvider.
 */

public interface IStructureRepresentation {

	/*
	 * Parent node
	 */
	public IStructureRepresentation getParent();

	/*
	 * Child nodes
	 */
	public List<? extends IStructureRepresentation> getChildren();

	/*
	 * Return ranges of the corresponding code segment
	 */
	public int getStartOffset();

	public int getEndOffset();

	/*
	 * Return a human readable string representation
	 */
	public String getName();

	public String getDisplayName();

	public IToken getStartToken();

	public IToken getEndToken();

	public Scope getScope();

	public Scope getSelfScope();
}
