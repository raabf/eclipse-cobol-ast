package parser.analysis.base.structure.representation;

import parser.analysis.base.definition.representation.IDefinitionRepresentation;

public interface ICallableDeclarationRepresentation extends IStructureRepresentation, IDefinitionRepresentation {
	// TODO just a marker?
}
