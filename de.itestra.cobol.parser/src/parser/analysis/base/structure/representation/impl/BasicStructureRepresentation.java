package parser.analysis.base.structure.representation.impl;

import org.conqat.lib.scanner.IToken;

import parser.analysis.base.structure.representation.IStructureRepresentation;
import de.itestra.swgcommons.util.Scope;

public abstract class BasicStructureRepresentation implements IStructureRepresentation {

	private String name;
	private IToken startToken;
	private IToken endToken;

	private final Scope scope;

	public BasicStructureRepresentation(IToken startToken, IToken endToken, String displayName, Scope scope) {
		this.startToken = startToken;
		this.endToken = endToken;
		this.name = displayName;
		this.scope = scope;
	}

	@Override
	public int getStartOffset() {
		return startToken.getOffset();
	}

	@Override
	public int getEndOffset() {
		return endToken.getEndOffset();
	}

	@Override
	public String getName() {
		return getDisplayName().toUpperCase();
	}

	@Override
	public String getDisplayName() {
		return this.name;
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public IToken getStartToken() {
		return startToken;
	}

	@Override
	public IToken getEndToken() {
		return endToken;
	}

	@Override
	public Scope getScope() {
		return scope;
	}

	@Override
	public Scope getSelfScope() {
		return Scope.getChild(getScope(), getName());
	}
}
