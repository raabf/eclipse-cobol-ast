package parser.analysis.base.definition.representation;

import java.util.Collection;
import java.util.List;

import parser.analysis.base.structure.representation.IStructureRepresentation;

public interface ICallableDefinitionRepresentation {
	public String getName();

	public List<? extends ICallRepresentation> getChildren();

	public boolean isResolved();

	public IStructureRepresentation getStructureRepresentation();

	void addCaller(ICallRepresentation caller);

	Collection<ICallRepresentation> getCallers();
}
