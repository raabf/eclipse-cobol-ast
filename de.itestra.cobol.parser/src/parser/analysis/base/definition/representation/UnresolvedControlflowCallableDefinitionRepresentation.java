package parser.analysis.base.definition.representation;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import parser.analysis.base.structure.representation.IStructureRepresentation;

public class UnresolvedControlflowCallableDefinitionRepresentation implements ICallableDefinitionRepresentation {
	private final String name;
	private final Collection<ICallRepresentation> callers = new HashSet<ICallRepresentation>();

	public UnresolvedControlflowCallableDefinitionRepresentation(List<String> qualifiedName) {
		name = qualifiedName.get(qualifiedName.size() - 1);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<? extends ICallRepresentation> getChildren() {
		return Collections.emptyList();
	}

	@Override
	public boolean isResolved() {
		return false;
	}

	@Override
	public IStructureRepresentation getStructureRepresentation() {
		// FIXME what to throw here?
		throw new RuntimeException("cannot get structure representation of unresolved callee");
	}

	@Override
	public void addCaller(ICallRepresentation caller) {
		callers.add(caller);
	}

	@Override
	public Collection<ICallRepresentation> getCallers() {
		return callers;
	}
}
