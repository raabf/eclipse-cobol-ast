package parser.analysis.base.definition.representation;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import parser.analysis.base.structure.representation.IStructureRepresentation;

public class UnknownProgramCallableDefinitionRepresentation implements ICallableDefinitionRepresentation {

	@Override
	public String getName() {
		return "";
	}

	@Override
	public List<? extends ICallRepresentation> getChildren() {
		return Collections.emptyList();
	}

	@Override
	public boolean isResolved() {
		return false;
	}

	@Override
	public IStructureRepresentation getStructureRepresentation() {
		// FIXME what to throw here?
		throw new RuntimeException("cannot get structure representation of unresolved program");
	}

	@Override
	public void addCaller(ICallRepresentation caller) {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<ICallRepresentation> getCallers() {
		return Collections.emptySet();
	}
}
