package parser.analysis.base.definition.representation;

import java.util.Collection;
import java.util.List;

import parser.analysis.ParserWarning;
import parser.analysis.base.structure.representation.IDataDeclarationRepresentation;
import parser.analysis.base.structure.representation.IStructureRepresentation;

import com.google.common.collect.Multimap;

public interface IDefinitionUseRepresentation {
	/**
	 * @return mapping from {@link IDataDeclarationRepresentation} to
	 *         {@link IStructureRepresentation}s reading the declared variable
	 */
	public Multimap<? extends IDataDeclarationRepresentation, ? extends IReadDataRepresentation> getReadAccesses();

	public Collection<IReadDataRepresentation> getReadAccesses(IDataDeclarationRepresentation declaration);

	/**
	 * @return a mapping from {@link IDataDeclarationRepresentation} to
	 *         {@link IStructureRepresentation}s writing into the declared
	 *         variable
	 */
	public Multimap<? extends IDataDeclarationRepresentation, ? extends IWriteDataRepresentation> getWriteAccesses();

	public Collection<IWriteDataRepresentation> getWriteAccesses(IDataDeclarationRepresentation declaration);

	public Multimap<ICallableDefinitionRepresentation, ICallRepresentation> getCalls();

	public Collection<? extends IDataDeclarationRepresentation> getDataDefinitions();

	public Collection<? extends ICallableDefinitionRepresentation> getCallableDefinitions();

	public void addRead(IDataDeclarationRepresentation of, IReadDataRepresentation by);

	public void addWrite(IDataDeclarationRepresentation of, IWriteDataRepresentation by);

	public void addCall(ICallableDefinitionRepresentation of, ICallRepresentation by);

	public IDataDeclarationRepresentation getDeclarationByQualifiedName(List<String> qualifiedName)
			throws ParserWarning;
}
