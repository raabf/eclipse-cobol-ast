package parser.analysis.base.definition.representation;

import java.util.List;

public class UnresolvedDynamicCallCallableDefinitionRepresentation extends UnresolvedControlflowCallableDefinitionRepresentation {
	public UnresolvedDynamicCallCallableDefinitionRepresentation(List<String> qualifiedName) {
		super(qualifiedName);
	}

	@Override
	public String getName() {
		return "@" + super.getName();
	}
}
