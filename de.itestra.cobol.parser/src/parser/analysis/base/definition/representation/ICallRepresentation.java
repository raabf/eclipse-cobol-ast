package parser.analysis.base.definition.representation;

import org.conqat.lib.scanner.IToken;

import parser.analysis.base.structure.representation.IStructureRepresentation;
import de.itestra.swgcommons.util.Scope;

public interface ICallRepresentation extends IUseRepresentation {
	public ICallableDefinitionRepresentation getTarget();

	public IStructureRepresentation getContainingStructure();

	ICallableDefinitionRepresentation getParent();

	public Scope getScope();

	public IToken getStartToken();

	public IToken getEndToken();
}
