package parser.analysis.base.definition.representation;

import parser.analysis.base.structure.representation.IDataDeclarationRepresentation;
import parser.analysis.base.structure.representation.IStructureRepresentation;

public interface IDataAccessRepresentation extends IUseRepresentation {
	public IDataDeclarationRepresentation getDeclaration();

	public IStructureRepresentation getStructureRepresentation();
}
