package parser.analysis.base.definition;

import parser.analysis.base.definition.representation.IDefinitionUseRepresentation;

public interface IDataflowAnalysis<AST> {
	/**
	 * run the analysis on the given AST
	 *
	 * @param ast
	 *            the AST to analyze
	 */
	public void run(AST ast);

	public IDefinitionUseRepresentation getRepresentation();
}
