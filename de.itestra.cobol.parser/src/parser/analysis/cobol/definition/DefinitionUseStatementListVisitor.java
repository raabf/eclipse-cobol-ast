package parser.analysis.cobol.definition;

import parser.analysis.cobol.definition.representation.CobolCallableDefinitionRepresentation;
import parser.analysis.cobol.definition.representation.CobolDataflowRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.errorhandling.ILowLevelErrorHandler;
import convex.step2.parser.ASTAcceptStatement;
import convex.step2.parser.ASTAddStatement;
import convex.step2.parser.ASTCallStatement;
import convex.step2.parser.ASTComputeStatement;
import convex.step2.parser.ASTCondition;
import convex.step2.parser.ASTDivideStatement;
import convex.step2.parser.ASTInitializeStatement;
import convex.step2.parser.ASTMoveStatement;
import convex.step2.parser.ASTMultiplyStatement;
import convex.step2.parser.ASTPerformUntilClause;
import convex.step2.parser.ASTPerformVaryingPhrase;
import convex.step2.parser.ASTSetStatement;
import convex.step2.parser.ASTSubtractStatement;
import convex.step2.parser.CobolParserDefaultVisitor;

public class DefinitionUseStatementListVisitor extends CobolParserDefaultVisitor {
	private final CobolCallableDefinitionRepresentation parent;
	private final ICallResolver analysis;
	private final CobolDataflowRepresentation dataflow;
	private final ILowLevelErrorHandler errorHandler;

	public DefinitionUseStatementListVisitor(CobolDataflowRepresentation dataflow, ICallResolver analysis,
			CobolCallableDefinitionRepresentation parent, ILowLevelErrorHandler errorHandler) {
		this.parent = parent;
		this.analysis = analysis;
		this.dataflow = dataflow;
		this.errorHandler = errorHandler;
	}

	@Override
	public Object visit(ASTCallStatement node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseCallStatementVisitor(dataflow, analysis, parent, errorHandler), data);
	}

	@Override
	public Object visit(ASTMoveStatement node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseMoveStatementVisitor(dataflow, errorHandler), data);
	}

	@Override
	public Object visit(ASTAcceptStatement node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseAcceptStatementVisitor(dataflow, errorHandler), data);
	}

	@Override
	public Object visit(ASTSetStatement node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseSetStatementVisitor(dataflow, errorHandler), data);
	}

	@Override
	public Object visit(ASTComputeStatement node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseComputeStatementVisitor(dataflow, analysis, parent, errorHandler), data);
	}

	@Override
	public Object visit(ASTMultiplyStatement node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseCalculationStatementVisitor(dataflow, analysis, parent, errorHandler),
				data);
	}

	@Override
	public Object visit(ASTDivideStatement node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseCalculationStatementVisitor(dataflow, analysis, parent, errorHandler),
				data);
	}

	@Override
	public Object visit(ASTAddStatement node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseCalculationStatementVisitor(dataflow, analysis, parent, errorHandler),
				data);
	}

	@Override
	public Object visit(ASTSubtractStatement node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseCalculationStatementVisitor(dataflow, analysis, parent, errorHandler),
				data);
	}

	@Override
	public Object visit(ASTPerformUntilClause node, RepresentationFactory data) {
		// TODO add dataflows
		// System.out.println("perform until => read");
		// node.dump("");
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTPerformVaryingPhrase node, RepresentationFactory data) {
		// TODO add dataflows
		// System.out.println("perform varying => write");
		// node.dump("");
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTInitializeStatement node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseWriteIdentifierVisitor(dataflow, errorHandler), data);
	}

	@Override
	public Object visit(ASTCondition node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseReadIdentifierVisitor(dataflow, errorHandler), data);
	}
}
