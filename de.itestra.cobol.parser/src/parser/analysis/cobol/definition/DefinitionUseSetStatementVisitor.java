package parser.analysis.cobol.definition;

import parser.analysis.cobol.definition.representation.CobolDataflowRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.errorhandling.ILowLevelErrorHandler;
import convex.step2.parser.ASTSetChangeStatement;
import convex.step2.parser.ASTSetSourceStatement;
import convex.step2.parser.ASTSetTargetStatement;
import convex.step2.parser.CobolParserDefaultVisitor;

public class DefinitionUseSetStatementVisitor extends CobolParserDefaultVisitor {
	private final CobolDataflowRepresentation dataflow;
	private final ILowLevelErrorHandler errorHandler;

	public DefinitionUseSetStatementVisitor(CobolDataflowRepresentation dataflow, ILowLevelErrorHandler errorHandler) {
		this.dataflow = dataflow;
		this.errorHandler = errorHandler;
	}

	@Override
	public Object visit(ASTSetSourceStatement node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseReadIdentifierVisitor(dataflow, errorHandler), data);
	}

	@Override
	public Object visit(ASTSetTargetStatement node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseWriteIdentifierVisitor(dataflow, errorHandler), data);
	}

	@Override
	public Object visit(ASTSetChangeStatement node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseReadIdentifierVisitor(dataflow, errorHandler), data);
	}
}
