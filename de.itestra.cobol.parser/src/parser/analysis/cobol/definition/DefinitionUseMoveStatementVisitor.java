package parser.analysis.cobol.definition;

import java.util.LinkedList;
import java.util.List;

import parser.analysis.ParserWarning;
import parser.analysis.base.structure.representation.IDataDeclarationRepresentation;
import parser.analysis.cobol.IdentifierVisitor;
import parser.analysis.cobol.definition.representation.CobolDataflowRepresentation;
import parser.analysis.cobol.definition.representation.CobolReadFieldRepresentation;
import parser.analysis.cobol.definition.representation.CobolWriteFieldRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.errorhandling.ExceptionMessage;
import parser.errorhandling.ILowLevelErrorHandler;
import convex.step2.parser.ASTCobolWord;
import convex.step2.parser.ASTIdentifier;
import convex.step2.parser.ASTMoveSourceStatement;
import convex.step2.parser.CobolParserDefaultVisitor;
import de.itestra.swgcommons.util.Scope;

public class DefinitionUseMoveStatementVisitor extends CobolParserDefaultVisitor {
	private final CobolDataflowRepresentation dataflow;
	private final ILowLevelErrorHandler errorHandler;

	public DefinitionUseMoveStatementVisitor(CobolDataflowRepresentation dataflow, ILowLevelErrorHandler errorHandler) {
		this.dataflow = dataflow;
		this.errorHandler = errorHandler;
	}

	@Override
	public Object visit(ASTMoveSourceStatement node, RepresentationFactory data) {
		return node.childrenAccept(new MoveStatementSourceVisitor(dataflow, errorHandler), data);
	}

	// TODO fully qualified Identifier?
	@Override
	public Object visit(ASTIdentifier node, RepresentationFactory data) {
		IdentifierVisitor visitor = new IdentifierVisitor();
		Object result = node.childrenAccept(visitor, data);
		List<ASTCobolWord> qualifiedName = visitor.getQualifiedName();
		// FIXME root?

		List<String> qualifiers = new LinkedList<String>();
		int i = 0;
		for (; i < qualifiedName.size() - 1; i++) {
			ASTCobolWord word = qualifiedName.get(i);
			qualifiers.add(word.jjtGetValue().toString());
			IDataDeclarationRepresentation declaration;
			try {
				declaration = dataflow.getDeclarationByQualifiedName(qualifiers);
				if (declaration != null) {
					// FIXME what if decl is not found?
					dataflow.addRead(declaration, new CobolReadFieldRepresentation(word, qualifiers, Scope.getRoot(),
							declaration));
				}
			} catch (ParserWarning e) {
				errorHandler.onWarning(node, new ExceptionMessage(e));
			}
		}
		qualifiers.add(qualifiedName.get(i).jjtGetValue().toString().toUpperCase());

		IDataDeclarationRepresentation declaration;
		try {
			declaration = dataflow.getDeclarationByQualifiedName(qualifiers);
			if (declaration != null) {
				// FIXME what if decl is not found?
				dataflow.addWrite(declaration, new CobolWriteFieldRepresentation(node, qualifiers, Scope.getRoot(),
						declaration));
			}
		} catch (ParserWarning e) {
			errorHandler.onWarning(node, new ExceptionMessage(e));
		}
		return result;
	}

	private static class MoveStatementSourceVisitor extends CobolParserDefaultVisitor {
		private final CobolDataflowRepresentation dataflow;
		private final ILowLevelErrorHandler errorHandler;

		public MoveStatementSourceVisitor(CobolDataflowRepresentation dataflow, ILowLevelErrorHandler errorHandler) {
			this.dataflow = dataflow;
			this.errorHandler = errorHandler;
		}

		@Override
		public Object visit(ASTIdentifier node, RepresentationFactory data) {
			IdentifierVisitor visitor = new IdentifierVisitor();
			Object result = node.childrenAccept(visitor, data);
			List<ASTCobolWord> qualifiedName = visitor.getQualifiedName();
			// FIXME root?

			List<String> qualifiers = new LinkedList<String>();
			int i = 0;
			for (; i < qualifiedName.size() - 1; i++) {
				ASTCobolWord word = qualifiedName.get(i);
				qualifiers.add(word.jjtGetValue().toString());
				IDataDeclarationRepresentation declaration;
				try {
					declaration = dataflow.getDeclarationByQualifiedName(qualifiers);
					if (declaration != null) {
						// FIXME what if decl is not found?
						dataflow.addRead(declaration,
								new CobolReadFieldRepresentation(word, qualifiers, Scope.getRoot(), declaration));
					}
				} catch (ParserWarning e) {
					errorHandler.onWarning(node, new ExceptionMessage(e));
				}
			}
			qualifiers.add(qualifiedName.get(i).jjtGetValue().toString().toUpperCase());

			IDataDeclarationRepresentation declaration;
			try {
				declaration = dataflow.getDeclarationByQualifiedName(qualifiers);
				if (declaration != null) {
					// FIXME what if decl is not found?
					dataflow.addRead(declaration, new CobolReadFieldRepresentation(node, qualifiers, Scope.getRoot(),
							declaration));
				}
			} catch (ParserWarning e) {
				errorHandler.onWarning(node, new ExceptionMessage(e));
			}
			return result;
		}
	}
}