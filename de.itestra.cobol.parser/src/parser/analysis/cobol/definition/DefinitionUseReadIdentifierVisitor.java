package parser.analysis.cobol.definition;

import java.util.LinkedList;
import java.util.List;

import parser.analysis.ParserWarning;
import parser.analysis.base.structure.representation.IDataDeclarationRepresentation;
import parser.analysis.cobol.IdentifierVisitor;
import parser.analysis.cobol.definition.representation.CobolDataflowRepresentation;
import parser.analysis.cobol.definition.representation.CobolReadFieldRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.errorhandling.ExceptionMessage;
import parser.errorhandling.ILowLevelErrorHandler;
import convex.step2.parser.ASTCobolWord;
import convex.step2.parser.ASTIdentifier;
import de.itestra.swgcommons.util.Scope;

public class DefinitionUseReadIdentifierVisitor extends DefinitionUseAbstractIdentifierVisitor {

	public DefinitionUseReadIdentifierVisitor(CobolDataflowRepresentation dataflow, ILowLevelErrorHandler errorHandler) {
		super(dataflow, errorHandler);
	}

	@Override
	protected void handle(ASTIdentifier node, List<ASTCobolWord> qualifiedName, RepresentationFactory factory) {
		IdentifierVisitor visitor = new IdentifierVisitor();
		node.childrenAccept(visitor, factory);
		// FIXME root?

		List<String> qualifiers = new LinkedList<String>();
		int i = 0;
		for (; i < qualifiedName.size() - 1; i++) {
			ASTCobolWord word = qualifiedName.get(i);
			qualifiers.add(word.jjtGetValue().toString());
			IDataDeclarationRepresentation declaration;
			try {
				declaration = getDataflow().getDeclarationByQualifiedName(qualifiers);
				if (declaration != null) {
					// FIXME what if decl is not found?
					getDataflow().addRead(declaration,
							new CobolReadFieldRepresentation(word, qualifiers, Scope.getRoot(), declaration));
				}
			} catch (ParserWarning e) {
				errorHandler.onWarning(node, new ExceptionMessage(e));
			}
		}
		qualifiers.add(qualifiedName.get(i).jjtGetValue().toString().toUpperCase());

		IDataDeclarationRepresentation declaration;
		try {
			declaration = getDataflow().getDeclarationByQualifiedName(qualifiers);
			if (declaration != null) {
				// FIXME what if decl is not found?
				getDataflow().addRead(declaration,
						new CobolReadFieldRepresentation(node, qualifiers, Scope.getRoot(), declaration));
			}
		} catch (ParserWarning e) {
			errorHandler.onWarning(node, new ExceptionMessage(e));
		}
	}
}