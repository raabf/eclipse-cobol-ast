package parser.analysis.cobol.definition;

import java.util.LinkedList;
import java.util.List;

import parser.analysis.ParserWarning;
import parser.analysis.base.structure.representation.IDataDeclarationRepresentation;
import parser.analysis.cobol.definition.representation.CobolDataflowRepresentation;
import parser.analysis.cobol.definition.representation.CobolReadFieldRepresentation;
import parser.analysis.cobol.definition.representation.CobolWriteFieldRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.errorhandling.ExceptionMessage;
import parser.errorhandling.ILowLevelErrorHandler;
import convex.step2.parser.ASTCobolWord;
import convex.step2.parser.ASTIdentifier;
import de.itestra.swgcommons.util.Scope;

public class DefinitionUseCallByReferenceVisitor extends DefinitionUseAbstractIdentifierVisitor {
	public DefinitionUseCallByReferenceVisitor(CobolDataflowRepresentation parent, ILowLevelErrorHandler errorHandler) {
		super(parent, errorHandler);
	}

	@Override
	protected void handle(ASTIdentifier node, List<ASTCobolWord> qualifiedName, RepresentationFactory factory) {
		// FIXME root?

		// FIXME need to add read accesses to qualifiers in qualified name
		List<String> qualifiers = new LinkedList<String>();
		for (ASTCobolWord word : qualifiedName) {
			qualifiers.add(word.jjtGetValue().toString());
		}
		IDataDeclarationRepresentation declaration;
		try {
			declaration = getDataflow().getDeclarationByQualifiedName(qualifiers);
			if (declaration != null) {
				// FIXME what if decl is not found?
				getDataflow().addRead(declaration,
						new CobolReadFieldRepresentation(node, qualifiers, Scope.getRoot(), declaration));
				getDataflow().addWrite(declaration,
						new CobolWriteFieldRepresentation(node, qualifiers, Scope.getRoot(), declaration));
			}
		} catch (ParserWarning e) {
			errorHandler.onWarning(node, new ExceptionMessage(e));
		}
	}

}
