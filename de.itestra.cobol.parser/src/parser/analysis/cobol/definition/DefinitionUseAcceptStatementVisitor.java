package parser.analysis.cobol.definition;

import parser.analysis.cobol.definition.representation.CobolDataflowRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.errorhandling.ILowLevelErrorHandler;
import convex.step2.parser.ASTIdentifier;
import convex.step2.parser.ASTLineColPhrase;
import convex.step2.parser.CobolParserDefaultVisitor;

public class DefinitionUseAcceptStatementVisitor extends CobolParserDefaultVisitor {
	private final CobolDataflowRepresentation dataflow;
	private final ILowLevelErrorHandler errorHandler;

	public DefinitionUseAcceptStatementVisitor(CobolDataflowRepresentation dataflow, ILowLevelErrorHandler errorHandler) {
		this.dataflow = dataflow;
		this.errorHandler = errorHandler;
	}

	@Override
	public Object visit(ASTIdentifier node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseReadIdentifierVisitor(dataflow, errorHandler), data);
	}

	@Override
	public Object visit(ASTLineColPhrase node, RepresentationFactory data) {
		// do not descend deeper for now, as there may be further identifiers
		// within the LineColPhrase, if those are required, a new Visitor should
		// be created to visit this LineColPhrase
		return data;
	}
}
