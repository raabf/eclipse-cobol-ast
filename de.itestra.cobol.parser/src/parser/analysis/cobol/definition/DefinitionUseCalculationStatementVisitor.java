package parser.analysis.cobol.definition;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import parser.analysis.ParserWarning;
import parser.analysis.base.structure.representation.IDataDeclarationRepresentation;
import parser.analysis.cobol.IdentifierVisitor;
import parser.analysis.cobol.definition.representation.CobolCallableDefinitionRepresentation;
import parser.analysis.cobol.definition.representation.CobolDataflowRepresentation;
import parser.analysis.cobol.definition.representation.CobolReadFieldRepresentation;
import parser.analysis.cobol.definition.representation.CobolWriteFieldRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.errorhandling.ExceptionMessage;
import parser.errorhandling.ILowLevelErrorHandler;

import com.google.common.collect.Sets;

import convex.step2.parser.ASTCobolWord;
import convex.step2.parser.ASTGivingClause;
import convex.step2.parser.ASTIdentifier;
import convex.step2.parser.ASTLeftOperand;
import convex.step2.parser.ASTRightOperand;
import convex.step2.parser.ASTStatementList;
import convex.step2.parser.CobolParserDefaultVisitor;
import convex.step2.parser.SimpleNode;
import de.itestra.swgcommons.util.Scope;

public class DefinitionUseCalculationStatementVisitor extends CobolParserDefaultVisitor {
	private final CobolCallableDefinitionRepresentation parent;
	private final ICallResolver analysis;
	private CobolDataflowRepresentation dataflow;
	private final ILowLevelErrorHandler errorHandler;

	public DefinitionUseCalculationStatementVisitor(CobolDataflowRepresentation dataflow, ICallResolver analysis,
			CobolCallableDefinitionRepresentation parent, ILowLevelErrorHandler errorHandler) {
		this.parent = parent;
		this.analysis = analysis;
		this.dataflow = dataflow;
		this.errorHandler = errorHandler;
	}

	@Override
	public Object visit(ASTLeftOperand node, RepresentationFactory data) {
		return node.childrenAccept(new OperandVisitor(dataflow, errorHandler, OperandHandler.READ), data);
	}

	@Override
	public Object visit(ASTRightOperand node, RepresentationFactory data) {
		// check whether there exists a giving clause in the parent and produce
		// the corresponding visitor for the right operand
		RightOperandVisitorFactory factory = new RightOperandVisitorFactory();
		node.jjtGetParent().jjtAccept(factory, data);
		return node.childrenAccept(factory.getRightOperandVisitor(dataflow), data);
	}

	@Override
	public Object visit(ASTGivingClause node, RepresentationFactory data) {
		return node.childrenAccept(new OperandVisitor(dataflow, errorHandler, OperandHandler.WRITE), data);
	}

	@Override
	public Object visit(ASTStatementList node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseStatementListVisitor(dataflow, analysis, parent, errorHandler),
				data);
	}

	private final class RightOperandVisitorFactory extends CobolParserDefaultVisitor {
		private Set<OperandHandler> handlers = Sets.newHashSet(OperandHandler.READ, OperandHandler.WRITE);

		@Override
		public Object visit(ASTGivingClause node, RepresentationFactory data) {
			handlers.remove(OperandHandler.WRITE);
			return data;
		}

		private OperandVisitor getRightOperandVisitor(CobolDataflowRepresentation dataflow) {
			return new OperandVisitor(dataflow, handlers, errorHandler);
		}
	}

	private static class OperandVisitor extends CobolParserDefaultVisitor {
		private final CobolDataflowRepresentation dataflow;
		private Collection<OperandHandler> handlers;
		private final ILowLevelErrorHandler errorHandler;

		public OperandVisitor(CobolDataflowRepresentation dataflow, ILowLevelErrorHandler errorHandler,
				OperandHandler... handlers) {
			this.dataflow = dataflow;
			this.handlers = Sets.newHashSet(handlers);
			this.errorHandler = errorHandler;
		}

		public OperandVisitor(CobolDataflowRepresentation dataflow, Collection<OperandHandler> handlers,
				ILowLevelErrorHandler errorHandler) {
			this.dataflow = dataflow;
			this.handlers = Sets.newHashSet(handlers);
			this.errorHandler = errorHandler;
		}

		@Override
		public Object visit(ASTIdentifier node, RepresentationFactory data) {
			IdentifierVisitor visitor = new IdentifierVisitor();
			Object result = node.childrenAccept(visitor, data);
			for (OperandHandler handler : handlers) {
				try {
					handler.handle(node, visitor.getQualifiedName(), dataflow, data);
				} catch (ParserWarning e) {
					errorHandler.onWarning(node, new ExceptionMessage(e));
				}
			}
			return result;
		}

	}

	private static enum OperandHandler {
		READ {
			@Override
			protected void handle(SimpleNode node, List<ASTCobolWord> qualifiedName,
					CobolDataflowRepresentation dataflow, RepresentationFactory factory) {
				// FIXME root?

				List<String> qualifiers = new LinkedList<String>();
				for (ASTCobolWord word : qualifiedName) {
					qualifiers.add(word.jjtGetValue().toString());
					IDataDeclarationRepresentation declaration;
					try {
						declaration = dataflow.getDeclarationByQualifiedName(qualifiers);
						if (declaration != null) {
							// FIXME what if decl is not found?
							dataflow.addRead(declaration,
									new CobolReadFieldRepresentation(word, qualifiers, Scope.getRoot(), declaration));
						}
					} catch (ParserWarning e) {
						// TODO find errorhandler
					}
				}
			}
		},
		WRITE {
			@Override
			protected void handle(SimpleNode node, List<ASTCobolWord> qualifiedName,
					CobolDataflowRepresentation dataflow, RepresentationFactory factory) throws ParserWarning {
				// FIXME root?

				List<String> qualifiers = new LinkedList<String>();
				int i = 0;
				for (; i < qualifiedName.size() - 1; i++) {
					ASTCobolWord word = qualifiedName.get(i);
					qualifiers.add(word.jjtGetValue().toString());
					IDataDeclarationRepresentation declaration = dataflow.getDeclarationByQualifiedName(qualifiers);
					if (declaration != null) {
						// FIXME what if decl is not found?
						dataflow.addRead(declaration,
								new CobolReadFieldRepresentation(word, qualifiers, Scope.getRoot(), declaration));
					}
				}
				qualifiers.add(qualifiedName.get(i).jjtGetValue().toString());
				IDataDeclarationRepresentation declaration = dataflow.getDeclarationByQualifiedName(qualifiers);
				if (declaration != null) {
					// FIXME what if decl is not found?
					dataflow.addWrite(declaration, new CobolWriteFieldRepresentation(node, qualifiers, Scope.getRoot(),
							declaration));
				}
			}
		};

		protected abstract void handle(SimpleNode node, List<ASTCobolWord> qualifiedName,
				CobolDataflowRepresentation dataflow, RepresentationFactory factory) throws ParserWarning;
	}
}
