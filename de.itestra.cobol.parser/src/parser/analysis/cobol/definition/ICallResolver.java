package parser.analysis.cobol.definition;

import parser.analysis.base.structure.representation.IStructureRepresentation;
import parser.analysis.cobol.definition.representation.CobolCallableDefinitionRepresentation;

public interface ICallResolver {
	CobolCallableDefinitionRepresentation resolveCall(IStructureRepresentation parent, String name);
}
