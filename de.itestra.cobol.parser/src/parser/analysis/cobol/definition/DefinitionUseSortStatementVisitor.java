package parser.analysis.cobol.definition;

import parser.analysis.base.definition.representation.IDefinitionUseRepresentation;
import parser.analysis.cobol.definition.representation.CobolCallableDefinitionRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import convex.step2.parser.ASTProcedureName;
import convex.step2.parser.CobolParserDefaultVisitor;

public class DefinitionUseSortStatementVisitor extends CobolParserDefaultVisitor {
	private final CobolCallableDefinitionRepresentation parent;
	private final ICallResolver analysis;
	private final IDefinitionUseRepresentation dataflow;

	public DefinitionUseSortStatementVisitor(IDefinitionUseRepresentation dataflow, ICallResolver analysis,
			CobolCallableDefinitionRepresentation parent) {
		this.parent = parent;
		this.analysis = analysis;
		this.dataflow = dataflow;
	}

	@Override
	public Object visit(ASTProcedureName node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUsePerformedProcedureNameVisitor(dataflow, analysis, parent), data);
	}
}