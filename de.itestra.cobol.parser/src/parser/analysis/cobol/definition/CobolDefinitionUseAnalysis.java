package parser.analysis.cobol.definition;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.apache.log4j.Logger;

import parser.analysis.base.definition.IDataflowAnalysis;
import parser.analysis.base.definition.representation.IDefinitionUseRepresentation;
import parser.analysis.base.structure.representation.IStructureRepresentation;
import parser.analysis.cobol.definition.representation.CobolCallRepresentation;
import parser.analysis.cobol.definition.representation.CobolCallableDefinitionRepresentation;
import parser.analysis.cobol.definition.representation.CobolDataflowRepresentation;
import parser.analysis.cobol.definition.representation.CobolReadFieldRepresentation;
import parser.analysis.cobol.definition.representation.CobolWriteFieldRepresentation;
import parser.analysis.cobol.structure.representation.impl.AbstractCobolStructureRepresentation;
import parser.analysis.cobol.structure.representation.impl.DataDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.DeclarativesRepresentation;
import parser.analysis.cobol.structure.representation.impl.EnvironmentDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.FieldDeclarationRepresentation;
import parser.analysis.cobol.structure.representation.impl.IdentificationDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.InternalCobolStructureRepresentationVisitor;
import parser.analysis.cobol.structure.representation.impl.InternalDefaultCobolStructureRepresentationVisitor;
import parser.analysis.cobol.structure.representation.impl.ParagraphRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProcedureDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProcedureSectionRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProgramRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.analysis.cobol.structure.representation.impl.SectionRepresentation;
import parser.errorhandling.ILowLevelErrorHandler;
import convex.step2.parser.ASTCallStatement;
import convex.step2.parser.ASTGotoStatement;
import convex.step2.parser.ASTParagraph;
import convex.step2.parser.ASTPerformStatement;
import convex.step2.parser.ASTSortStatement;
import convex.step2.parser.CobolParserDefaultVisitor;
import de.itestra.swgcommons.util.Scope;

public class CobolDefinitionUseAnalysis extends InternalDefaultCobolStructureRepresentationVisitor implements
		IDataflowAnalysis<ProgramRepresentation>, ICallResolver {
	private static final Logger logger = Logger.getLogger(CobolDefinitionUseAnalysis.class);

	private CobolDataflowRepresentation dataflow = new CobolDataflowRepresentation();
	private final ILowLevelErrorHandler errorHandler;

	private Stack<CobolCallableDefinitionRepresentation> callStack = new Stack<CobolCallableDefinitionRepresentation>();
	private Map<String, Map<Scope, CobolCallableDefinitionRepresentation>> nodes = new HashMap<String, Map<Scope, CobolCallableDefinitionRepresentation>>();
	private CobolCallableDefinitionRepresentation root;

	public CobolDefinitionUseAnalysis(ILowLevelErrorHandler errorHandler) {
		this.errorHandler = errorHandler;
	}

	private void defaultVisit(AbstractCobolStructureRepresentation representation) {
		representation.childrenAccept(this);
	}

	@Override
	public void run(ProgramRepresentation ast) {
		long start = System.currentTimeMillis();

		ast.accept(new InternalDefaultCobolStructureRepresentationVisitor() {

			@Override
			public void visit(ProcedureSectionRepresentation representation) {
				putNode(representation);
				super.visit(representation);
			}

			@Override
			public void visit(ParagraphRepresentation representation) {
				putNode(representation);
				super.visit(representation);
			}

			@Override
			public void visit(ProgramRepresentation representation) {
				root = new CobolCallableDefinitionRepresentation(representation);
				CobolDefinitionUseAnalysis.this.dataflow.addCallableDefinition(root);
				callStack.push(root);
				super.visit(representation);
			}
		});

		ast.accept(this);

		ast.accept(new ControlflowVisitor());

		logger.debug("Dataflow analysis duration: " + (System.currentTimeMillis() - start) + " ms");
	}

	@Override
	public IDefinitionUseRepresentation getRepresentation() {
		return dataflow;
	}

	@Override
	public void visit(FieldDeclarationRepresentation representation) {
		this.dataflow.addDataDeclaration(representation);
		defaultVisit(representation);
	}

	@Override
	public void visit(ProcedureDivisionRepresentation representation) {
		representation.getAst().childrenAccept(new DefinitionUseStatementListVisitor(dataflow, this, root, errorHandler),
				new RepresentationFactory());
		defaultVisit(representation);
	}

	@Override
	public void visit(IdentificationDivisionRepresentation representation) {
		// do not descend, no dataflow in here
	}

	@Override
	public void visit(EnvironmentDivisionRepresentation representation) {
		// do not descend, no dataflow in here
	}

	private void putNode(AbstractCobolStructureRepresentation representation) {
		CobolCallableDefinitionRepresentation node = new CobolCallableDefinitionRepresentation(representation);
		dataflow.addCallableDefinition(node);
		if (!nodes.containsKey(representation.getName())) {
			nodes.put(representation.getName(), new HashMap<Scope, CobolCallableDefinitionRepresentation>());
		}

		Map<Scope, CobolCallableDefinitionRepresentation> scope2Node = nodes.get(representation.getName());
		scope2Node.put(representation.getScope(), node);
	}

	private CobolCallableDefinitionRepresentation nodeByScope(IStructureRepresentation representation) {
		CobolCallableDefinitionRepresentation target = null;
		if (nodes.containsKey(representation.getName())) {
			target = nodes.get(representation.getName()).get(representation.getScope());
		}
		return target;
	}

	@Override
	public CobolCallableDefinitionRepresentation resolveCall(IStructureRepresentation parent, String name) {
		CobolCallableDefinitionRepresentation target = null;

		// first try to resolve unique
		if (nodes.containsKey(name)) {
			Map<Scope, CobolCallableDefinitionRepresentation> scope2node = nodes.get(name);
			if (scope2node.size() == 1) {
				target = scope2node.values().iterator().next();
			} else {
				target = nodes.get(name).get(parent.getSelfScope());
			}
		}

		return target;
	}

	private class ControlflowVisitor extends CobolParserDefaultVisitor implements
			InternalCobolStructureRepresentationVisitor {
		private final RepresentationFactory factory = new RepresentationFactory();

		private void defaultVisit(AbstractCobolStructureRepresentation representation) {
			representation.childrenAccept(this);
		}

		@Override
		public void visit(DeclarativesRepresentation representation) {
			representation.childrenAccept(new InternalDefaultCobolStructureRepresentationVisitor() {
				@Override
				public void visit(ProcedureSectionRepresentation representation) {
					CobolCallableDefinitionRepresentation target = nodeByScope(representation);
					// TODO do we really want to have source and target the same
					// for starting sections?
					if (!callStack.isEmpty()) {
						callStack.peek().addChild(
								new CobolCallRepresentation(callStack.peek(), representation.getStartToken(),
										representation.getEndToken(), target));
					}
					callStack.push(target);
					representation.getAst().childrenAccept(ControlflowVisitor.this, factory);
					defaultVisit(representation);
					callStack.pop();
				}
			});
		}

		@Override
		public void visit(ProcedureSectionRepresentation representation) {
			CobolCallableDefinitionRepresentation target = nodeByScope(representation);
			// TODO do we really want to have source and target the same for
			// starting sections?
			if (!callStack.isEmpty()) {
				callStack.peek().addChild(
						new CobolCallRepresentation(callStack.peek(), representation.getStartToken(), representation
								.getEndToken(), target));
			} else {
				// FIXME this adds all sections as root to the set of connected
				// components! fix this to only have unreached sections
				// additionally in the set
			}
			callStack.push(target);
			representation.getAst().childrenAccept(this, factory);
			defaultVisit(representation);
		}

		@Override
		public void visit(ParagraphRepresentation representation) {
			CobolCallableDefinitionRepresentation child = nodeByScope(representation);
			if (!callStack.isEmpty()) {
				callStack.peek().addChild(
						new CobolCallRepresentation(callStack.peek(), representation.getStartToken(), representation
								.getEndToken(), child));
			}
			representation.getAst().childrenAccept(this, factory);
			defaultVisit(representation);
		}

		@Override
		public Object visit(ASTParagraph node, RepresentationFactory data) {
			// do not descend deeper, paragraphs will be investigated during
			// visitation of ParagraphRepresentation
			return data;
		}

		@Override
		public Object visit(ASTPerformStatement node, RepresentationFactory data) {
			return node.childrenAccept(new DefinitionUsePerformStatementVisitor(dataflow,
					CobolDefinitionUseAnalysis.this, callStack.peek()), data);
		}

		@Override
		public Object visit(ASTGotoStatement node, RepresentationFactory data) {
			return node.childrenAccept(new DefinitionUseGotoStatementVisitor(dataflow, CobolDefinitionUseAnalysis.this,
					callStack.peek()), data);
		}

		@Override
		public Object visit(ASTSortStatement node, RepresentationFactory data) {
			return node.childrenAccept(new DefinitionUseSortStatementVisitor(dataflow, CobolDefinitionUseAnalysis.this,
					callStack.peek()), data);
		}

		@Override
		public Object visit(ASTCallStatement node, RepresentationFactory data) {
			return node.childrenAccept(new DefinitionUseCallStatementVisitor(dataflow, CobolDefinitionUseAnalysis.this,
					callStack.peek(), errorHandler), data);
		}

		@Override
		public void visit(AbstractCobolStructureRepresentation representation) {
			defaultVisit(representation);
		}

		@Override
		public void visit(ProgramRepresentation representation) {
			defaultVisit(representation);
		}

		@Override
		public void visit(IdentificationDivisionRepresentation representation) {
			// do not descend, no controlflow in here
		}

		@Override
		public void visit(EnvironmentDivisionRepresentation representation) {
			// do not descend, no controlflow in here
		}

		@Override
		public void visit(DataDivisionRepresentation representation) {
			// do not descend, no controlflow in here
		}

		@Override
		public void visit(ProcedureDivisionRepresentation representation) {
			defaultVisit(representation);
		}

		@Override
		public void visit(SectionRepresentation representation) {
			defaultVisit(representation);
		}

		@Override
		public void visit(FieldDeclarationRepresentation representation) {
			defaultVisit(representation);
		}

		@Override
		public void visit(CobolReadFieldRepresentation representation) {
			defaultVisit(representation);
		}

		@Override
		public void visit(CobolWriteFieldRepresentation representation) {
			defaultVisit(representation);
		}
	}
}
