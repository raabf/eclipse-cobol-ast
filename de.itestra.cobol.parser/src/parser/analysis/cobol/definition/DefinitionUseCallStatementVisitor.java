package parser.analysis.cobol.definition;

import java.util.LinkedList;
import java.util.List;

import org.conqat.lib.scanner.IToken;

import parser.analysis.ParserWarning;
import parser.analysis.base.definition.representation.ICallableDefinitionRepresentation;
import parser.analysis.base.definition.representation.UnresolvedControlflowCallableDefinitionRepresentation;
import parser.analysis.base.definition.representation.UnresolvedDynamicCallCallableDefinitionRepresentation;
import parser.analysis.base.structure.representation.IDataDeclarationRepresentation;
import parser.analysis.cobol.IdentifierVisitor;
import parser.analysis.cobol.definition.representation.CobolCallRepresentation;
import parser.analysis.cobol.definition.representation.CobolCallableDefinitionRepresentation;
import parser.analysis.cobol.definition.representation.CobolDataflowRepresentation;
import parser.analysis.cobol.definition.representation.CobolReadFieldRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.errorhandling.ExceptionMessage;
import parser.errorhandling.ILowLevelErrorHandler;

import com.google.common.collect.Lists;

import convex.step2.parser.ASTCallByContent;
import convex.step2.parser.ASTCallByReference;
import convex.step2.parser.ASTCallByValue;
import convex.step2.parser.ASTCallTargetStatement;
import convex.step2.parser.ASTCobolWord;
import convex.step2.parser.ASTIdentifier;
import convex.step2.parser.ASTLiteral;
import convex.step2.parser.CobolParserDefaultVisitor;
import de.itestra.swgcommons.util.Scope;

public class DefinitionUseCallStatementVisitor extends CobolParserDefaultVisitor {
	private final CobolCallableDefinitionRepresentation parent;
	private final ICallResolver analysis;
	private final CobolDataflowRepresentation dataflow;
	private final ILowLevelErrorHandler errorHandler;

	public DefinitionUseCallStatementVisitor(CobolDataflowRepresentation dataflow, ICallResolver analysis,
			CobolCallableDefinitionRepresentation parent, ILowLevelErrorHandler errorHandler) {
		this.parent = parent;
		this.analysis = analysis;
		this.dataflow = dataflow;
		this.errorHandler = errorHandler;
	}

	@Override
	public Object visit(ASTCallTargetStatement node, RepresentationFactory data) {
		return node.childrenAccept(new CallTargetStatementVisitor(dataflow, analysis, parent, errorHandler), data);
	}

	@Override
	public Object visit(ASTCallByReference node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseCallByReferenceVisitor(dataflow, errorHandler), data);
	}

	@Override
	public Object visit(ASTCallByValue node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseCallByContentVisitor(dataflow, errorHandler), data);
	}

	@Override
	public Object visit(ASTCallByContent node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseCallByContentVisitor(dataflow, errorHandler), data);
	}

	private static class CallTargetStatementVisitor extends CobolParserDefaultVisitor {
		private final CobolCallableDefinitionRepresentation parent;
		private final ICallResolver analysis;
		private final CobolDataflowRepresentation dataflow;
		private final ILowLevelErrorHandler errorHandler;

		public CallTargetStatementVisitor(CobolDataflowRepresentation dataflow, ICallResolver analysis,
				CobolCallableDefinitionRepresentation parent, ILowLevelErrorHandler errorHandler) {
			this.parent = parent;
			this.analysis = analysis;
			this.dataflow = dataflow;
			this.errorHandler = errorHandler;
		}

		@Override
		public Object visit(ASTLiteral node, RepresentationFactory data) {
			String name = node.jjtGetFirstToken().image;

			if (!isBuiltinCall(name)) {
				name = unquote(name);
				ICallableDefinitionRepresentation callee = analysis.resolveCall(parent.getStructureRepresentation(),
						name);
				if (callee == null) {
					callee = new UnresolvedControlflowCallableDefinitionRepresentation(Lists.newArrayList(name));
				}
				CobolCallRepresentation child = new CobolCallRepresentation(parent, ((IToken) node.jjtGetFirstToken()
						.getValue()), ((IToken) node.jjtGetLastToken().getValue()), callee);
				callee.addCaller(child);
				dataflow.addCall(callee, child);
				parent.addChild(child);
			}

			return super.visit(node, data);
		}

		@Override
		public Object visit(ASTIdentifier node, RepresentationFactory data) {
			// get qualified name of identifier
			IdentifierVisitor visitor = new IdentifierVisitor();
			Object result = node.childrenAccept(visitor, data);
			List<ASTCobolWord> qualifiedName = visitor.getQualifiedName();

			// add reads to all identifiers in qualified name
			List<String> qualifiers = new LinkedList<String>();
			for (ASTCobolWord word : qualifiedName) {
				qualifiers.add(word.jjtGetValue().toString());
				IDataDeclarationRepresentation declaration;
				try {
					declaration = dataflow.getDeclarationByQualifiedName(qualifiers);
					dataflow.addRead(declaration, new CobolReadFieldRepresentation(word, qualifiers, Scope.getRoot(),
							declaration));
				} catch (ParserWarning e) {
					errorHandler.onWarning(node, new ExceptionMessage(e));
					e.printStackTrace();
				}
			}

			// add call
			ICallableDefinitionRepresentation callee = new UnresolvedDynamicCallCallableDefinitionRepresentation(
					qualifiers);
			CobolCallRepresentation child = new CobolCallRepresentation(parent, ((IToken) node.jjtGetFirstToken()
					.getValue()), ((IToken) node.jjtGetLastToken().getValue()), callee);
			callee.addCaller(child);
			dataflow.addCall(callee, child);
			parent.addChild(child);

			return result;
		}

		private boolean isBuiltinCall(String target) {
			return target.startsWith("X");
		}

		private String unquote(String target) {
			target = target.substring(1, target.length() - 1);
			return target;
		}
	}
}
