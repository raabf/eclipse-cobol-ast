package parser.analysis.cobol.definition.representation;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import parser.analysis.AmbiguousIdentifierException;
import parser.analysis.ParserWarning;
import parser.analysis.UnresolvedIdentifierException;
import parser.analysis.base.definition.representation.ICallRepresentation;
import parser.analysis.base.definition.representation.ICallableDefinitionRepresentation;
import parser.analysis.base.definition.representation.IDefinitionUseRepresentation;
import parser.analysis.base.definition.representation.IReadDataRepresentation;
import parser.analysis.base.definition.representation.IWriteDataRepresentation;
import parser.analysis.base.structure.representation.IDataDeclarationRepresentation;
import parser.analysis.cobol.structure.representation.impl.FieldDeclarationRepresentation;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public class CobolDataflowRepresentation implements IDefinitionUseRepresentation {
	private Multimap<String, IDataDeclarationRepresentation> dataDefinitions = HashMultimap.create();
	private Multimap<String, ICallableDefinitionRepresentation> callableDefinitions = HashMultimap.create();
	private Multimap<IDataDeclarationRepresentation, IReadDataRepresentation> reads = HashMultimap.create();
	private Multimap<IDataDeclarationRepresentation, IWriteDataRepresentation> writes = HashMultimap.create();
	private Multimap<ICallableDefinitionRepresentation, ICallRepresentation> calls = HashMultimap.create();

	@Override
	public Multimap<? extends IDataDeclarationRepresentation, ? extends IReadDataRepresentation> getReadAccesses() {
		return reads;
	}

	@Override
	public Collection<IReadDataRepresentation> getReadAccesses(IDataDeclarationRepresentation declaration) {
		return reads.get(declaration);
	}

	@Override
	public Multimap<? extends IDataDeclarationRepresentation, ? extends IWriteDataRepresentation> getWriteAccesses() {
		return writes;
	}

	@Override
	public Collection<IWriteDataRepresentation> getWriteAccesses(IDataDeclarationRepresentation declaration) {
		return writes.get(declaration);
	}

	@Override
	public Collection<? extends IDataDeclarationRepresentation> getDataDefinitions() {
		return dataDefinitions.values();
	}

	@Override
	public void addRead(IDataDeclarationRepresentation of, IReadDataRepresentation by) {
		reads.put(of, by);
	}

	@Override
	public void addWrite(IDataDeclarationRepresentation of, IWriteDataRepresentation by) {
		writes.put(of, by);
	}

	public void addDataDeclaration(FieldDeclarationRepresentation representation) {
		dataDefinitions.put(representation.getName(), representation);
	}

	public void addCallableDefinition(ICallableDefinitionRepresentation callee) {
		callableDefinitions.put(callee.getName(), callee);
	}

	public Collection<? extends IDataDeclarationRepresentation> getDeclarationsByName(String name) {
		return dataDefinitions.get(name);
	}

	public IDataDeclarationRepresentation getDeclarationByName(String name) throws ParserWarning {
		Collection<IDataDeclarationRepresentation> all = dataDefinitions.get(name.toUpperCase());
		if (all.size() == 1) {
			return all.iterator().next();
		} else if (all.size() > 1) {
			System.err.println("Could not determine declaration with name " + name);
			throw new AmbiguousIdentifierException();
			// return null;
		} else {
			throw new UnresolvedIdentifierException();
			// return null;
		}
	}

	@Override
	public IDataDeclarationRepresentation getDeclarationByQualifiedName(List<String> qualifiedName)
			throws ParserWarning {
		Iterator<String> iterator = qualifiedName.iterator();
		Preconditions.checkArgument(iterator.hasNext());
		IDataDeclarationRepresentation current = getDeclarationByName(iterator.next());

		while (current != null && iterator.hasNext()) {
			current = current.getChildRecursive(iterator.next());
		}

		return current;
	}

	@Override
	public Collection<? extends ICallableDefinitionRepresentation> getCallableDefinitions() {
		return callableDefinitions.values();
	}

	@Override
	public void addCall(ICallableDefinitionRepresentation of, ICallRepresentation by) {
		calls.put(of, by);
	}

	@Override
	public Multimap<ICallableDefinitionRepresentation, ICallRepresentation> getCalls() {
		return calls;
	}
}
