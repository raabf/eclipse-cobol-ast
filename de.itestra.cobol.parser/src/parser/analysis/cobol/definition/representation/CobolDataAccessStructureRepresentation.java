package parser.analysis.cobol.definition.representation;

import java.util.List;

import parser.analysis.base.definition.representation.IDataAccessRepresentation;
import parser.analysis.base.structure.representation.IDataDeclarationRepresentation;
import parser.analysis.base.structure.representation.IStructureRepresentation;
import parser.analysis.cobol.structure.representation.impl.AbstractCobolStructureRepresentation;
import convex.step2.parser.SimpleNode;
import de.itestra.swgcommons.util.Scope;

public abstract class CobolDataAccessStructureRepresentation extends AbstractCobolStructureRepresentation implements
		IDataAccessRepresentation {
	private final List<String> qualifiedName;
	private final IDataDeclarationRepresentation declaration;

	public CobolDataAccessStructureRepresentation(SimpleNode ast, List<String> qualifiedName, Scope scope,
			IDataDeclarationRepresentation declaration) {
		super(ast, qualifiedName.get(qualifiedName.size() - 1), scope);
		this.qualifiedName = qualifiedName;
		this.declaration = declaration;
	}

	@Override
	public IDataDeclarationRepresentation getDeclaration() {
		return declaration;
	}

	@Override
	public IStructureRepresentation getStructureRepresentation() {
		return this;
	}
}
