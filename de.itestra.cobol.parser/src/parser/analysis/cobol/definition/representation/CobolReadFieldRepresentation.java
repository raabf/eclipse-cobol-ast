package parser.analysis.cobol.definition.representation;

import java.util.List;

import parser.analysis.base.definition.representation.IReadDataRepresentation;
import parser.analysis.base.structure.representation.IDataDeclarationRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentationVisitor;
import parser.analysis.cobol.structure.representation.impl.InternalCobolStructureRepresentationVisitor;
import convex.step2.parser.SimpleNode;
import de.itestra.swgcommons.util.Scope;

public class CobolReadFieldRepresentation extends CobolDataAccessStructureRepresentation implements
		IReadDataRepresentation {
	public CobolReadFieldRepresentation(SimpleNode ast, List<String> qualifiedName, Scope scope,
			IDataDeclarationRepresentation declaration) {
		super(ast, qualifiedName, scope, declaration);
		declaration.addRead(this);
	}

	@Override
	public void accept(CobolStructureRepresentationVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void accept(InternalCobolStructureRepresentationVisitor visitor) {
		visitor.visit(this);
	}
}
