package parser.analysis.cobol.definition.representation;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import parser.analysis.base.definition.representation.ICallRepresentation;
import parser.analysis.base.definition.representation.ICallableDefinitionRepresentation;
import parser.analysis.cobol.structure.representation.impl.AbstractCobolStructureRepresentation;

import com.google.common.collect.Lists;

public class CobolCallableDefinitionRepresentation implements ICallableDefinitionRepresentation {
	private final AbstractCobolStructureRepresentation structure;
	private final List<CobolCallRepresentation> children = Lists.newArrayList();
	private final Collection<ICallRepresentation> callers = new HashSet<ICallRepresentation>();

	public CobolCallableDefinitionRepresentation(AbstractCobolStructureRepresentation structure) {
		this.structure = structure;
	}

	@Override
	public String getName() {
		return structure.getName();
	}

	@Override
	public List<? extends ICallRepresentation> getChildren() {
		return children;
	}

	@Override
	public boolean isResolved() {
		return true;
	}

	@Override
	public AbstractCobolStructureRepresentation getStructureRepresentation() {
		return structure;
	}

	public void addChild(CobolCallRepresentation child) {
		children.add(child);
	}

	@Override
	public void addCaller(ICallRepresentation caller) {
		callers.add(caller);
	}

	@Override
	public Collection<ICallRepresentation> getCallers() {
		return callers;
	}
}