package parser.analysis.cobol.definition.representation;

import org.conqat.lib.scanner.IToken;

import parser.analysis.base.definition.representation.ICallRepresentation;
import parser.analysis.base.definition.representation.ICallableDefinitionRepresentation;
import parser.analysis.base.structure.representation.IStructureRepresentation;
import de.itestra.swgcommons.util.Scope;

public class CobolCallRepresentation implements ICallRepresentation {
	private final ICallableDefinitionRepresentation parent;
	private final IToken startToken;
	private final IToken endToken;
	private final ICallableDefinitionRepresentation target;

	public CobolCallRepresentation(ICallableDefinitionRepresentation parent, IToken startToken, IToken endToken,
			ICallableDefinitionRepresentation target) {
		this.parent = parent;
		this.startToken = startToken;
		this.endToken = endToken;
		this.target = target;
	}

	@Override
	public IStructureRepresentation getContainingStructure() {
		return parent.getStructureRepresentation();
	}

	@Override
	public ICallableDefinitionRepresentation getParent() {
		return parent;
	}

	@Override
	public ICallableDefinitionRepresentation getTarget() {
		return target;
	}

	@Override
	public Scope getScope() {
		return getContainingStructure().getSelfScope();
	}

	@Override
	public IToken getStartToken() {
		return startToken;
	}

	@Override
	public IToken getEndToken() {
		return endToken;
	}
}
