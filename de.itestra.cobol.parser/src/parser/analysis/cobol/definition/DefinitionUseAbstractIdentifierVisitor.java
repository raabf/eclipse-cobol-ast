package parser.analysis.cobol.definition;

import java.util.List;

import parser.analysis.cobol.IdentifierVisitor;
import parser.analysis.cobol.definition.representation.CobolDataflowRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.errorhandling.ILowLevelErrorHandler;
import convex.step2.parser.ASTCobolWord;
import convex.step2.parser.ASTIdentifier;
import convex.step2.parser.CobolParserDefaultVisitor;

public abstract class DefinitionUseAbstractIdentifierVisitor extends CobolParserDefaultVisitor {
	private final CobolDataflowRepresentation dataflow;
	protected final ILowLevelErrorHandler errorHandler;

	public DefinitionUseAbstractIdentifierVisitor(CobolDataflowRepresentation dataflow, ILowLevelErrorHandler errorHandler) {
		this.dataflow = dataflow;
		this.errorHandler = errorHandler;
	}

	@Override
	public Object visit(ASTIdentifier node, RepresentationFactory data) {
		IdentifierVisitor visitor = new IdentifierVisitor();
		Object result = node.childrenAccept(visitor, data);
		handle(node, visitor.getQualifiedName(), data);
		return result;
	}

	protected final CobolDataflowRepresentation getDataflow() {
		return dataflow;
	}

	protected abstract void handle(ASTIdentifier node, List<ASTCobolWord> qualifiedName, RepresentationFactory factory);
}