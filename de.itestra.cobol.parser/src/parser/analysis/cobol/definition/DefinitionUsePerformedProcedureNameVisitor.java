package parser.analysis.cobol.definition;

import org.conqat.lib.scanner.IToken;

import parser.analysis.base.definition.representation.ICallableDefinitionRepresentation;
import parser.analysis.base.definition.representation.IDefinitionUseRepresentation;
import parser.analysis.base.definition.representation.UnresolvedControlflowCallableDefinitionRepresentation;
import parser.analysis.cobol.definition.representation.CobolCallRepresentation;
import parser.analysis.cobol.definition.representation.CobolCallableDefinitionRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;

import com.google.common.collect.Lists;

import convex.step2.parser.ASTCobolWord;
import convex.step2.parser.CobolParserDefaultVisitor;

public class DefinitionUsePerformedProcedureNameVisitor extends CobolParserDefaultVisitor {
	private final CobolCallableDefinitionRepresentation parent;
	private final ICallResolver analysis;
	private final IDefinitionUseRepresentation dataflow;

	public DefinitionUsePerformedProcedureNameVisitor(IDefinitionUseRepresentation dataflow, ICallResolver analysis,
			CobolCallableDefinitionRepresentation parent) {
		this.parent = parent;
		this.analysis = analysis;
		this.dataflow = dataflow;
	}

	// TODO fully qualified identifier?
	@Override
	public Object visit(ASTCobolWord node, RepresentationFactory data) {
		String name = node.jjtGetValue().toString();
		ICallableDefinitionRepresentation callee = analysis.resolveCall(parent.getStructureRepresentation(), name);
		if (callee == null) {
			callee = new UnresolvedControlflowCallableDefinitionRepresentation(Lists.newArrayList(name));
		}
		CobolCallRepresentation child = new CobolCallRepresentation(parent, ((IToken) node.jjtGetFirstToken()
				.getValue()), ((IToken) node.jjtGetLastToken().getValue()), callee);
		dataflow.addCall(callee, child);
		callee.addCaller(child);
		parent.addChild(child);
		return super.visit(node, data);
	}
}