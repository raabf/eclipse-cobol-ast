package parser.analysis.cobol.definition;

import parser.analysis.cobol.definition.representation.CobolCallableDefinitionRepresentation;
import parser.analysis.cobol.definition.representation.CobolDataflowRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.errorhandling.ILowLevelErrorHandler;
import convex.step2.parser.ASTArithmeticExpression;
import convex.step2.parser.ASTIdentifier;
import convex.step2.parser.ASTStatementList;
import convex.step2.parser.CobolParserDefaultVisitor;

public class DefinitionUseComputeStatementVisitor extends CobolParserDefaultVisitor {
	private final CobolCallableDefinitionRepresentation parent;
	private final ICallResolver analysis;
	private final CobolDataflowRepresentation dataflow;
	private final ILowLevelErrorHandler errorHandler;

	public DefinitionUseComputeStatementVisitor(CobolDataflowRepresentation dataflow, ICallResolver analysis,
			CobolCallableDefinitionRepresentation parent, ILowLevelErrorHandler errorHandler) {
		this.parent = parent;
		this.analysis = analysis;
		this.dataflow = dataflow;
		this.errorHandler = errorHandler;
	}

	@Override
	public Object visit(ASTIdentifier node, RepresentationFactory data) {
		return new DefinitionUseWriteIdentifierVisitor(dataflow, errorHandler).visit(node, data);
	}

	@Override
	public Object visit(ASTArithmeticExpression node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseReadIdentifierVisitor(dataflow, errorHandler), data);
	}

	@Override
	public Object visit(ASTStatementList node, RepresentationFactory data) {
		return node.childrenAccept(new DefinitionUseStatementListVisitor(dataflow, analysis, parent, errorHandler), data);
	}
}
