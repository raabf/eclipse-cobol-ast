package parser.analysis.cobol.controlflow.graph;

import parser.analysis.cobol.controlflow.graph.annotations.NameAnnotation;
import parser.analysis.cobol.controlflow.graph.visitors.CobolControlflowAnalysis;
import convex.step2.parser.SimpleNode;

public abstract class CobolControlflowProgramPart extends CobolControlflowNode implements ICobolControlflowProgramPart {
	protected CobolControlflowNode currentNode;

	public CobolControlflowProgramPart(CobolControlflowAnalysis analysis, String name) {
		super(analysis, name);
		this.currentNode = this;
	}

	@Override
	public final CobolControlflowEdge controlflow(CobolControlflowNode nextNode) {
		CobolControlflowEdge result;

		if (currentNode.equals(this)) {
			result = super.controlflow(nextNode);
		} else {
			result = currentNode.controlflow(nextNode);
		}
		this.currentNode = nextNode;

		return result;
	}

	@Override
	public final CobolControlflowEdge controlflow(CobolControlflowNode nextNode, SimpleNode ast) {
		CobolControlflowEdge result;

		if (currentNode.equals(this)) {
			result = super.controlflow(nextNode, ast);
		} else {
			result = currentNode.controlflow(nextNode, ast);
		}
		this.currentNode = nextNode;

		return result;
	}

	public CobolControlflowNode getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(CobolControlflowNode newCurrentNode) {
		this.currentNode = newCurrentNode;
	}

	public void finish() {
		CobolControlflowEdge edge = this.currentNode.controlflow(getEndNode());
		edge.annotate(new NameAnnotation("FINISH"));
		this.currentNode = getEndNode();
	}

}
