package parser.analysis.cobol.controlflow.graph;

import parser.analysis.cobol.controlflow.graph.visitors.CobolControlflowAnalysis;
import parser.analysis.cobol.structure.representation.CobolParagraphRepresentation;
import parser.analysis.cobol.structure.representation.CobolProcedureSectionRepresentation;
import parser.analysis.cobol.structure.representation.DefaultCobolStructureRepresentationVisitor;
import parser.analysis.cobol.structure.representation.impl.AbstractCobolStructureRepresentation;

public class CobolCallableControlflowProgramPart extends CobolControlflowProgramPart {
	private final AbstractCobolStructureRepresentation representation;
	private final CobolControlflowNode endNode;

	public CobolCallableControlflowProgramPart(CobolControlflowAnalysis analysis,
			AbstractCobolStructureRepresentation representation, CobolControlflowNode endNode) {
		super(analysis, representation.getName());
		this.endNode = endNode;
		this.representation = representation;
	}

	public CobolCallableControlflowProgramPart(CobolControlflowAnalysis analysis,
			AbstractCobolStructureRepresentation representation) {
		this(analysis, representation, new CobolControlflowNode(analysis));
	}

	public CobolCallableControlflowProgramPart getChildRecursive(CobolControlflowAnalysis analysis, String name) {
		ChildFinder childFinder = new ChildFinder(analysis, name);
		representation.accept(childFinder);
		return childFinder.getResult();
	}

	private static final class ChildFinder extends DefaultCobolStructureRepresentationVisitor {
		private CobolCallableControlflowProgramPart result = null;
		private final CobolControlflowAnalysis analysis;
		private final String name;

		public ChildFinder(CobolControlflowAnalysis analysis, String name) {
			this.analysis = analysis;
			this.name = name;
		}

		@Override
		public void visit(CobolProcedureSectionRepresentation representation) {
			if (representation.getName().equals(name)) {
				result = analysis.getDeclarationByStructure(representation);
			} else {
				super.visit(representation);
			}
		}

		@Override
		public void visit(CobolParagraphRepresentation representation) {
			if (representation.getName().equals(name)) {
				result = analysis.getDeclarationByStructure(representation);
			}
		}

		public CobolCallableControlflowProgramPart getResult() {
			return result;
		}
	}

	@Override
	public String toString() {
		return representation.getDisplayName();
	}

	@Override
	public CobolControlflowNode getEndNode() {
		return endNode;
	}
}
