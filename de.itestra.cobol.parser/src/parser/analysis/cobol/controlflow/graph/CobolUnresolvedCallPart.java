package parser.analysis.cobol.controlflow.graph;

import parser.analysis.cobol.controlflow.graph.visitors.CobolControlflowAnalysis;

/**
 * if a perform or goto statement points to a section/paragraph/etc. which is
 * not known, this unresolved program part is given as a dummy filler
 */

public class CobolUnresolvedCallPart extends CobolControlflowProgramPart {

	public CobolUnresolvedCallPart(CobolControlflowAnalysis analysis) {
		super(analysis, "unresolvedPart");
		this.currentNode = this;
	}

	@Override
	public CobolControlflowNode getEndNode() {
		return this;
	}
}
