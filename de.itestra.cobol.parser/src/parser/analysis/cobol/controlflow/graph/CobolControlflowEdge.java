package parser.analysis.cobol.controlflow.graph;

import java.util.Collection;
import java.util.Collections;

import parser.analysis.cobol.controlflow.graph.annotations.CobolControlflowEdgeAnnotation;
import parser.analysis.cobol.controlflow.graph.annotations.NameAnnotation;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public abstract class CobolControlflowEdge {
	private final CobolControlflowNode previousNode;
	private final CobolControlflowNode nextNode;
	private final Multimap<Class<? extends CobolControlflowEdgeAnnotation>, CobolControlflowEdgeAnnotation> annotations = HashMultimap
			.create();

	public CobolControlflowEdge(CobolControlflowNode previous, CobolControlflowNode next) {
		this.previousNode = previous;
		this.nextNode = next;
	}

	public CobolControlflowNode getNextNode() {
		return nextNode;
	}

	public CobolControlflowNode getPreviousNode() {
		return previousNode;
	}

	public void annotate(CobolControlflowEdgeAnnotation annotation) {
		annotations.put(annotation.getClass(), annotation);
	}

	public Collection<CobolControlflowEdgeAnnotation> getAnnotations() {
		return Collections.unmodifiableCollection(annotations.values());
	}

	public Collection<CobolControlflowEdgeAnnotation> getAnnotations(
			Class<? extends CobolControlflowEdgeAnnotation> type) {
		return Collections.unmodifiableCollection(annotations.get(type));
	}

	public boolean hasAnnotation(Class<? extends CobolControlflowEdgeAnnotation> type) {
		return !getAnnotations(type).isEmpty();
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();

		Collection<CobolControlflowEdgeAnnotation> names = getAnnotations(NameAnnotation.class);
		for (CobolControlflowEdgeAnnotation annotation : names) {
			result.append(annotation.toString());
		}

		return result.toString();
	}

	public abstract boolean isStatement();

	public abstract StatementControlflowEdge asStatement();
}