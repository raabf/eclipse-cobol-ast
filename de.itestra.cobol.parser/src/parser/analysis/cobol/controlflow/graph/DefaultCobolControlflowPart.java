package parser.analysis.cobol.controlflow.graph;

import parser.analysis.cobol.controlflow.graph.visitors.CobolControlflowAnalysis;

public class DefaultCobolControlflowPart extends CobolControlflowProgramPart {
	private final CobolControlflowNode endNode;

	public DefaultCobolControlflowPart(CobolControlflowAnalysis analysis, String name) {
		super(analysis, name);
		this.endNode = new CobolControlflowNode(analysis);
	}

	@Override
	public CobolControlflowNode getEndNode() {
		return endNode;
	}

}
