package parser.analysis.cobol.controlflow.graph;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Queue;

import parser.analysis.cobol.controlflow.graph.annotations.ExitAnnotation;
import parser.analysis.cobol.controlflow.graph.visitors.CobolControlflowAnalysis;

public class CobolControlflowUnreachableNodes {
	public Collection<CobolControlflowNode> findUnreachableNodes(CobolControlflowAnalysis analysis) {
		Queue<CobolControlflowNode> helperQueue = new ArrayDeque<CobolControlflowNode>();

		Collection<CobolControlflowNode> allNodes = analysis.getAllNodes();

		CobolProgramNode programPart = analysis.getProgramPart();
		Queue<CobolControlflowNode> reachedNodesQueue = new ArrayDeque<CobolControlflowNode>();
		helperQueue.add(programPart);
		while (!helperQueue.isEmpty()) {
			CobolControlflowNode firstNode = helperQueue.remove();
			reachedNodesQueue.add(firstNode);
			allNodes.remove(firstNode);
			for (int i = 0; i < firstNode.getEdges().size(); i++) {
				CobolControlflowEdge edge = firstNode.getEdges().get(i);
				if (!edge.hasAnnotation(ExitAnnotation.class)) {
					CobolControlflowNode nextNode = edge.getNextNode();
					if (!reachedNodesQueue.contains(nextNode) && !helperQueue.contains(nextNode)) {
						helperQueue.add(nextNode);
					}
				}
			}
		}

		allNodes.remove(programPart.getEndNode());

		return allNodes;
	}

}
