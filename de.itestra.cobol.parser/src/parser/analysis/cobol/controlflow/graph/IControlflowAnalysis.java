package parser.analysis.cobol.controlflow.graph;

public interface IControlflowAnalysis<AST> {

	public void run(AST ast);
}
