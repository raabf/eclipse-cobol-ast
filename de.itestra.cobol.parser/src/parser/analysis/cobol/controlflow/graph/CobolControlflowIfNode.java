package parser.analysis.cobol.controlflow.graph;

import java.util.LinkedList;

import parser.analysis.cobol.controlflow.graph.annotations.NameAnnotation;
import parser.analysis.cobol.controlflow.graph.visitors.CobolControlflowAnalysis;
import convex.step2.parser.SimpleNode;

public class CobolControlflowIfNode extends DefaultCobolControlflowPart {

	private CobolControlflowProgramPart ifBranch;
	private CobolControlflowProgramPart elseBranch;

	public CobolControlflowIfNode(CobolControlflowAnalysis analysis, SimpleNode ast) {
		super(analysis, "IF");

		ifBranch = new DefaultCobolControlflowPart(analysis, "IF_BRANCH");
		elseBranch = new DefaultCobolControlflowPart(analysis, "ELSE_BRANCH");

		edges = new LinkedList<CobolControlflowEdge>();
		CobolControlflowEdge ifEdge = new StatementControlflowEdge(this, ifBranch, ast);
		ifEdge.annotate(new NameAnnotation("IF"));
		edges.add(ifEdge);
		CobolControlflowEdge elseEdge = new StatementControlflowEdge(this, elseBranch, ast);
		elseEdge.annotate(new NameAnnotation("ELSE"));
		edges.add(elseEdge);

		this.currentNode = getEndNode();
	}

	public CobolControlflowProgramPart getIfBranch() {
		return ifBranch;
	}

	public CobolControlflowProgramPart getElseBranch() {
		return elseBranch;
	}

	@Override
	public void finish() {
		CobolControlflowEdge ifEdge = new VirtualControlflowEdge(ifBranch.getEndNode(), this.getEndNode());
		ifEdge.annotate(new NameAnnotation("IF_(END)"));
		ifBranch.getEndNode().edges.add(ifEdge);

		CobolControlflowEdge endElseEdge = new VirtualControlflowEdge(elseBranch.getEndNode(), this.getEndNode());
		endElseEdge.annotate(new NameAnnotation("ELSE_(END)"));
		elseBranch.getEndNode().edges.add(endElseEdge);

		if (elseBranch.edges.isEmpty()) {
			CobolControlflowEdge edge = elseBranch.controlflow(elseBranch.getEndNode());
			edge.annotate(new NameAnnotation("EmptyElse"));
		}
	}
}
