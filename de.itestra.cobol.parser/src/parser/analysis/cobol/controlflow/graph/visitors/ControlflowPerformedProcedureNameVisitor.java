package parser.analysis.cobol.controlflow.graph.visitors;

import java.util.LinkedList;
import java.util.List;

import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;

import com.google.common.collect.ImmutableList;

import convex.step2.parser.ASTCobolWord;
import convex.step2.parser.CobolParserDefaultVisitor;

public class ControlflowPerformedProcedureNameVisitor extends CobolParserDefaultVisitor {
	private LinkedList<String> qualifiedName = new LinkedList<String>();

	public ControlflowPerformedProcedureNameVisitor() {
	}

	@Override
	public Object visit(ASTCobolWord node, RepresentationFactory data) {
		qualifiedName.addFirst(node.jjtGetValue().toString());
		return super.visit(node, data);
	}

	public List<String> getQualifiedName() {
		return ImmutableList.copyOf(qualifiedName);
	}
}