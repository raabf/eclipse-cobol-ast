package parser.analysis.cobol.controlflow.graph.visitors;

import parser.analysis.cobol.IdentifierVisitor;
import parser.analysis.cobol.controlflow.graph.CobolControlflowEdge;
import parser.analysis.cobol.controlflow.graph.CobolControlflowNode;
import parser.analysis.cobol.controlflow.graph.CobolControlflowProgramPart;
import parser.analysis.cobol.controlflow.graph.CobolUnresolvedCallPart;
import parser.analysis.cobol.controlflow.graph.annotations.NameAnnotation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import convex.step2.parser.ASTCallTargetStatement;
import convex.step2.parser.ASTIdentifier;
import convex.step2.parser.ASTLiteral;
import convex.step2.parser.CobolParserDefaultVisitor;
import convex.step2.parser.SimpleNode;

public class ControlflowCallStatementVisitor extends CobolParserDefaultVisitor {
	private final CobolControlflowProgramPart parent;
	private final CobolControlflowAnalysis analysis;

	public ControlflowCallStatementVisitor(CobolControlflowAnalysis analysis, CobolControlflowProgramPart parent) {
		this.parent = parent;
		this.analysis = analysis;
	}

	@Override
	public Object visit(ASTCallTargetStatement node, RepresentationFactory data) {
		return node.childrenAccept(new CallTargetStatementVisitor(analysis, parent), data);
	}

	private static class CallTargetStatementVisitor extends CobolParserDefaultVisitor {
		private final CobolControlflowProgramPart parent;
		private final CobolControlflowAnalysis analysis;

		public CallTargetStatementVisitor(CobolControlflowAnalysis analysis, CobolControlflowProgramPart parent) {
			this.parent = parent;
			this.analysis = analysis;
		}

		@Override
		public Object visit(ASTLiteral node, RepresentationFactory data) {
			IdentifierVisitor visitor = new IdentifierVisitor();
			node.childrenAccept(visitor, data);

			createEdgesToAndFromCalledPart(node, new CobolUnresolvedCallPart(analysis));

			return super.visit(node, data);
		}

		@Override
		public Object visit(ASTIdentifier node, RepresentationFactory data) {
			IdentifierVisitor visitor = new IdentifierVisitor();
			node.childrenAccept(visitor, data);

			createEdgesToAndFromCalledPart(node, new CobolUnresolvedCallPart(analysis));

			return super.visit(node, data);
		}

		private void createEdgesToAndFromCalledPart(SimpleNode node, CobolControlflowProgramPart callee) {
			CobolControlflowEdge edge = parent.controlflow(callee, node);
			edge.annotate(new NameAnnotation("unresolved call"));
			parent.setCurrentNode(new CobolControlflowNode(analysis));
			edge = callee.getEndNode().controlflow(parent.getCurrentNode());
			edge.annotate(new NameAnnotation("return from unresolved call"));
		}

		private boolean isBuiltinCall(String target) {
			return target.startsWith("X");
		}

		private String unquote(String target) {
			target = target.substring(1, target.length() - 1);
			return target;
		}
	}
}
