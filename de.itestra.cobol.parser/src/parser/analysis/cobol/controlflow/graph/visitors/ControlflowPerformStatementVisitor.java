package parser.analysis.cobol.controlflow.graph.visitors;

import parser.analysis.ParserWarning;
import parser.analysis.cobol.controlflow.graph.CobolControlflowEdge;
import parser.analysis.cobol.controlflow.graph.CobolControlflowNode;
import parser.analysis.cobol.controlflow.graph.CobolControlflowPerformNode;
import parser.analysis.cobol.controlflow.graph.CobolControlflowProgramPart;
import parser.analysis.cobol.controlflow.graph.CobolUnresolvedCallPart;
import parser.analysis.cobol.controlflow.graph.annotations.NameAnnotation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import convex.step2.parser.ASTPerformProcedureScopeClause;
import convex.step2.parser.ASTPerformUntilClause;
import convex.step2.parser.ASTStatementList;
import convex.step2.parser.CobolParserDefaultVisitor;
import convex.step2.parser.CobolParserVisitor;

public class ControlflowPerformStatementVisitor extends CobolParserDefaultVisitor {
	private final CobolParserVisitor statementVisitor;
	private final CobolControlflowPerformNode predecessor;
	private final CobolControlflowAnalysis analysis;

	public ControlflowPerformStatementVisitor(CobolParserVisitor statementVisitor, CobolControlflowAnalysis analysis,
			CobolControlflowPerformNode predecessor) {
		this.statementVisitor = statementVisitor;
		this.predecessor = predecessor;
		this.analysis = analysis;
	}

	@Override
	public Object visit(ASTPerformProcedureScopeClause node, RepresentationFactory data) {
		ControlflowPerformedProcedureNameVisitor visitor = new ControlflowPerformedProcedureNameVisitor();
		Object result = node.childrenAccept(visitor, data);

		CobolControlflowProgramPart callee;
		try {
			callee = analysis.resolveCall(visitor.getQualifiedName());
		} catch (ParserWarning e) {
			callee = new CobolUnresolvedCallPart(analysis);
		}
		CobolControlflowEdge edge = predecessor.controlflow(callee, node);
		edge.annotate(new NameAnnotation("PERFORM " + visitor.getQualifiedName()));
		predecessor.setCurrentNode(new CobolControlflowNode(analysis));
		edge = callee.getEndNode().controlflow(predecessor.getCurrentNode());
		edge.annotate(new NameAnnotation("RETURN from " + visitor.getQualifiedName()));

		return result;
	}

	@Override
	public Object visit(ASTPerformUntilClause node, RepresentationFactory data) {
		predecessor.loops();
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTStatementList node, RepresentationFactory data) {
		return node.childrenAccept(statementVisitor, data);
	}
}
