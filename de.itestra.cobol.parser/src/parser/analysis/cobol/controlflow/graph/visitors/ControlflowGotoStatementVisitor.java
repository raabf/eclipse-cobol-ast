package parser.analysis.cobol.controlflow.graph.visitors;

import parser.analysis.ParserWarning;
import parser.analysis.cobol.controlflow.graph.CobolControlflowEdge;
import parser.analysis.cobol.controlflow.graph.CobolControlflowProgramPart;
import parser.analysis.cobol.controlflow.graph.CobolUnresolvedCallPart;
import parser.analysis.cobol.controlflow.graph.annotations.NameAnnotation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import convex.step2.parser.ASTProcedureName;
import convex.step2.parser.CobolParserDefaultVisitor;

public class ControlflowGotoStatementVisitor extends CobolParserDefaultVisitor {
	private final CobolControlflowProgramPart predecessor;
	private final CobolControlflowAnalysis analysis;

	public ControlflowGotoStatementVisitor(CobolControlflowAnalysis analysis, CobolControlflowProgramPart predecessor) {
		this.predecessor = predecessor;
		this.analysis = analysis;
	}

	@Override
	public Object visit(ASTProcedureName node, RepresentationFactory data) {
		ControlflowPerformedProcedureNameVisitor visitor = new ControlflowPerformedProcedureNameVisitor();
		Object result = node.childrenAccept(visitor, data);

		CobolControlflowProgramPart callee;
		try {
			callee = analysis.resolveCall(visitor.getQualifiedName());
		} catch (ParserWarning e) {
			callee = new CobolUnresolvedCallPart(analysis);
		}
		CobolControlflowEdge edge = predecessor.controlflow(callee, node);
		edge.annotate(new NameAnnotation("GOTO " + visitor.getQualifiedName()));

		return result;
	}
}