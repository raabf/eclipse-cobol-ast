package parser.analysis.cobol.controlflow.graph.visitors;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import convex.step2.parser.ASTAcceptStatement;
import convex.step2.parser.ASTAddStatement;
import convex.step2.parser.ASTCallStatement;
import convex.step2.parser.ASTComputeStatement;
import convex.step2.parser.ASTCondition;
import convex.step2.parser.ASTDisplayStatement;
import convex.step2.parser.ASTDivideStatement;
import convex.step2.parser.ASTExitProgramStatement;
import convex.step2.parser.ASTGotoStatement;
import convex.step2.parser.ASTIfStatement;
import convex.step2.parser.ASTInitializeStatement;
import convex.step2.parser.ASTMoveStatement;
import convex.step2.parser.ASTMultiplyStatement;
import convex.step2.parser.ASTParagraph;
import convex.step2.parser.ASTPerformStatement;
import convex.step2.parser.ASTProcedureSection;
import convex.step2.parser.ASTSetStatement;
import convex.step2.parser.CobolParserDefaultVisitor;
import convex.step2.parser.SimpleNode;
import de.itestra.swgcommons.util.Scope;
import parser.analysis.AmbiguousIdentifierException;
import parser.analysis.ParserWarning;
import parser.analysis.UnresolvedIdentifierException;
import parser.analysis.cobol.controlflow.graph.CobolCallableControlflowProgramPart;
import parser.analysis.cobol.controlflow.graph.CobolControlflowEdge;
import parser.analysis.cobol.controlflow.graph.CobolControlflowIfNode;
import parser.analysis.cobol.controlflow.graph.CobolControlflowNode;
import parser.analysis.cobol.controlflow.graph.CobolControlflowPerformNode;
import parser.analysis.cobol.controlflow.graph.CobolControlflowProgramPart;
import parser.analysis.cobol.controlflow.graph.CobolProgramNode;
import parser.analysis.cobol.controlflow.graph.IControlflowAnalysis;
import parser.analysis.cobol.controlflow.graph.annotations.ExitAnnotation;
import parser.analysis.cobol.controlflow.graph.annotations.NameAnnotation;
import parser.analysis.cobol.definition.representation.CobolReadFieldRepresentation;
import parser.analysis.cobol.definition.representation.CobolWriteFieldRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentation;
import parser.analysis.cobol.structure.representation.impl.AbstractCobolStructureRepresentation;
import parser.analysis.cobol.structure.representation.impl.DataDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.DeclarativesRepresentation;
import parser.analysis.cobol.structure.representation.impl.EnvironmentDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.FieldDeclarationRepresentation;
import parser.analysis.cobol.structure.representation.impl.IdentificationDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.InternalCobolStructureRepresentationVisitor;
import parser.analysis.cobol.structure.representation.impl.InternalDefaultCobolStructureRepresentationVisitor;
import parser.analysis.cobol.structure.representation.impl.ParagraphRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProcedureDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProcedureSectionRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProgramRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.analysis.cobol.structure.representation.impl.SectionRepresentation;

public class CobolControlflowAnalysis extends InternalDefaultCobolStructureRepresentationVisitor
		implements IControlflowAnalysis<AbstractCobolStructureRepresentation> {
	private static final Logger logger = Logger.getLogger(CobolControlflowAnalysis.class);

	private Stack<CobolControlflowProgramPart> callStack = new Stack<CobolControlflowProgramPart>();
	private CobolProgramNode programPart;
	private Map<String, Map<Scope, CobolCallableControlflowProgramPart>> programPartNodes = new HashMap<String, Map<Scope, CobolCallableControlflowProgramPart>>();
	private Map<CobolStructureRepresentation, CobolCallableControlflowProgramPart> partsByStructure = Maps.newHashMap();

	private final AtomicInteger nodeCounter = new AtomicInteger();
	private Collection<CobolControlflowNode> nodes = Sets.newHashSet();

	@Override
	public void run(AbstractCobolStructureRepresentation ast) {
		long start = System.currentTimeMillis();
		ast.accept(new InternalDefaultCobolStructureRepresentationVisitor() {
			@Override
			public void visit(ProcedureSectionRepresentation representation) {
				putParagraphSectionNode(representation);
				super.visit(representation);
			}

			@Override
			public void visit(ParagraphRepresentation representation) {
				putParagraphSectionNode(representation);
				super.visit(representation);
			}

			@Override
			public void visit(ProgramRepresentation representation) {
				putProgramNode(representation);
				super.visit(representation);
			}
		});

		ast.accept(new ControlflowVisitor());

		logger.debug("Controlflow analysis duration: " + (System.currentTimeMillis() - start) + " ms");
	}

	public CobolProgramNode getProgramPart() {
		return programPart;
	}

	/**
	 * NOTE: not to be used by the client
	 *
	 * @param node
	 *            the node to register
	 */
	public int registerNode(CobolControlflowNode node) {
		nodes.add(node);
		return nodeCounter.getAndIncrement();
	}

	/**
	 * get a copy of the collection of all nodes within the controlflow graph
	 *
	 * @return a copy of the collection of all nodes within the controlflow
	 *         graph
	 */
	public Collection<CobolControlflowNode> getAllNodes() {
		return Sets.newHashSet(nodes);
	}

	private void putProgramNode(ProgramRepresentation representation) {
		CobolProgramNode node = programPart = new CobolProgramNode(this, representation);
		putNode(representation, node);
	}

	private void putParagraphSectionNode(AbstractCobolStructureRepresentation representation) {
		CobolCallableControlflowProgramPart node = new CobolCallableControlflowProgramPart(this, representation);
		putNode(representation, node);
	}

	private void putNode(AbstractCobolStructureRepresentation representation,
			CobolCallableControlflowProgramPart node) {
		if (!programPartNodes.containsKey(representation.getName())) {
			programPartNodes.put(representation.getName(), new HashMap<Scope, CobolCallableControlflowProgramPart>());
		}
		partsByStructure.put(representation, node);

		Map<Scope, CobolCallableControlflowProgramPart> scope2Node = programPartNodes.get(representation.getName());
		scope2Node.put(representation.getScope(), node);
	}

	public CobolControlflowProgramPart resolveCall(List<String> qualifiedName) throws ParserWarning {
		Iterator<String> iterator = qualifiedName.iterator();
		Preconditions.checkArgument(iterator.hasNext());
		CobolCallableControlflowProgramPart current = null;
		current = getDeclarationByName(iterator.next());

		while (current != null && iterator.hasNext()) {
			current = current.getChildRecursive(this, iterator.next());
		}

		return current;
	}

	public CobolCallableControlflowProgramPart getDeclarationByName(String name) throws ParserWarning {
		if (!programPartNodes.containsKey(name)) {
			throw new UnresolvedIdentifierException();
		}

		Collection<CobolCallableControlflowProgramPart> all = programPartNodes.get(name.toUpperCase()).values();
		if (all.size() == 1) {
			return all.iterator().next();
		} else if (all.size() > 1) {
			System.err.println("Could not determine declaration with name " + name);
			throw new AmbiguousIdentifierException();
			// return null;
		} else {
			throw new UnresolvedIdentifierException();
			// return null;
		}
	}

	public CobolCallableControlflowProgramPart getDeclarationByStructure(CobolStructureRepresentation representation) {
		return partsByStructure.get(representation);
	}

	private class ControlflowVisitor extends CobolParserDefaultVisitor
			implements InternalCobolStructureRepresentationVisitor {
		private void defaultVisit(AbstractCobolStructureRepresentation representation) {
			representation.childrenAccept(this);
		}

		@Override
		public void visit(DeclarativesRepresentation representation) {
			representation.childrenAccept(new InternalDefaultCobolStructureRepresentationVisitor() {
				@Override
				public void visit(ProcedureSectionRepresentation representation) {

				}
			});
		}

		@Override
		public void visit(ProgramRepresentation representation) {
			callStack.push(programPart);
			SimpleNode ast = representation.getAst();
			ast.childrenAccept(this, new RepresentationFactory());
			defaultVisit(representation);
			callStack.pop().finish();
		}

		@Override
		public void visit(ProcedureSectionRepresentation representation) {
			CobolControlflowProgramPart controlflowPart = programPartNodes.get(representation.getName())
					.get(representation.getScope());
			CobolControlflowEdge edge = callStack.peek().controlflow(controlflowPart);
			edge.annotate(new NameAnnotation("FALLTHROUGH"));
			callStack.push(controlflowPart);
			SimpleNode ast = representation.getAst();
			ast.childrenAccept(this, new RepresentationFactory());
			defaultVisit(representation);
			callStack.pop().finish();
		}

		@Override
		public void visit(ParagraphRepresentation representation) {
			CobolControlflowProgramPart controlflowPart = programPartNodes.get(representation.getName())
					.get(representation.getScope());
			CobolControlflowEdge edge = callStack.peek().controlflow(controlflowPart);
			edge.annotate(new NameAnnotation("FALLTHROUGH"));
			callStack.push(controlflowPart);
			SimpleNode ast = representation.getAst();
			ast.childrenAccept(this, new RepresentationFactory());
			defaultVisit(representation);
			callStack.pop().finish();
		}

		@Override
		public Object visit(ASTParagraph node, RepresentationFactory data) {
			// do not descend deeper, paragraphs will be investigated during
			// visitation of ParagraphRepresentation
			return data;
		}

		@Override
		public Object visit(ASTProcedureSection node, RepresentationFactory data) {
			// do not descend deeper, sections will be investigated during
			// visitation of SectionRepresentation
			return data;
		}

		@Override
		public Object visit(ASTMoveStatement node, RepresentationFactory data) {
			handleStatement(node);
			return data;
		}

		@Override
		public Object visit(ASTSetStatement node, RepresentationFactory data) {
			handleStatement(node);
			return data;
		}

		@Override
		public Object visit(ASTAcceptStatement node, RepresentationFactory data) {
			handleStatement(node);
			return data;
		}

		@Override
		public Object visit(ASTDisplayStatement node, RepresentationFactory data) {
			handleStatement(node);
			return data;
		}

		@Override
		public Object visit(ASTComputeStatement node, RepresentationFactory data) {
			handleStatement(node);
			return data;
		}

		@Override
		public Object visit(ASTInitializeStatement node, RepresentationFactory data) {
			handleStatement(node);
			return data;
		}

		@Override
		public Object visit(ASTDivideStatement node, RepresentationFactory data) {
			handleStatement(node);
			return data;
		}

		@Override
		public Object visit(ASTMultiplyStatement node, RepresentationFactory data) {
			handleStatement(node);
			return data;
		}

		@Override
		public Object visit(ASTAddStatement node, RepresentationFactory data) {
			handleStatement(node);
			return data;
		}

		@Override
		public Object visit(ASTPerformStatement node, RepresentationFactory data) {
			CobolControlflowPerformNode perform = new CobolControlflowPerformNode(CobolControlflowAnalysis.this);
			CobolControlflowEdge edge = callStack.peek().controlflow(perform);
			edge.annotate(new NameAnnotation("PERFORM"));
			callStack.push(perform);
			Object returnedObject = node.childrenAccept(
					new ControlflowPerformStatementVisitor(this, CobolControlflowAnalysis.this, perform), data);
			callStack.pop().finish();

			return returnedObject;

		}

		@Override
		public Object visit(ASTGotoStatement node, RepresentationFactory data) {
			Object result = node.childrenAccept(
					new ControlflowGotoStatementVisitor(CobolControlflowAnalysis.this, callStack.peek()), data);

			callStack.peek().setCurrentNode(new CobolControlflowNode(CobolControlflowAnalysis.this));

			return result;
		}

		@Override
		public Object visit(ASTIfStatement node, RepresentationFactory data) {

			int numChildrenIfStatement = node.jjtGetNumChildren();
			ASTCondition condition = (ASTCondition) node.jjtGetChild(0);
			CobolControlflowIfNode ifNode = new CobolControlflowIfNode(CobolControlflowAnalysis.this, node);

			if (numChildrenIfStatement > 1) {
				callStack.push(ifNode.getIfBranch());
				node.jjtGetChild(1).jjtAccept(this, data);
				callStack.pop().finish();
			}
			if (numChildrenIfStatement > 2) {
				callStack.push(ifNode.getElseBranch());
				node.jjtGetChild(2).jjtAccept(this, data);
				callStack.pop().finish();
			}

			ifNode.finish();

			CobolControlflowEdge edge = callStack.peek().controlflow(ifNode);
			edge.annotate(new NameAnnotation("IF"));
			return data;
		}

		@Override
		public Object visit(ASTCallStatement node, RepresentationFactory data) {
			return node.childrenAccept(
					new ControlflowCallStatementVisitor(CobolControlflowAnalysis.this, callStack.peek()), data);
		}

		@Override
		public Object visit(ASTExitProgramStatement node, RepresentationFactory data) {
			handleStatement(node).annotate(new ExitAnnotation());
			return data;
		}

		@Override
		public void visit(AbstractCobolStructureRepresentation representation) {
			defaultVisit(representation);
		}

		@Override
		public void visit(IdentificationDivisionRepresentation representation) {
			defaultVisit(representation);
		}

		@Override
		public void visit(EnvironmentDivisionRepresentation representation) {
			defaultVisit(representation);
		}

		@Override
		public void visit(DataDivisionRepresentation representation) {
			defaultVisit(representation);
		}

		@Override
		public void visit(ProcedureDivisionRepresentation representation) {
			defaultVisit(representation);
		}

		@Override
		public void visit(SectionRepresentation representation) {
			defaultVisit(representation);
		}

		@Override
		public void visit(FieldDeclarationRepresentation representation) {
			defaultVisit(representation);
		}

		@Override
		public void visit(CobolReadFieldRepresentation representation) {
			// TODO Auto-generated method stub

		}

		@Override
		public void visit(CobolWriteFieldRepresentation representation) {
			// TODO Auto-generated method stub

		}

		private CobolControlflowEdge handleStatement(SimpleNode node) {
			CobolControlflowEdge edge = callStack.peek().controlflow(
					new CobolControlflowNode(CobolControlflowAnalysis.this), node);
			edge.annotate(new NameAnnotation(node.jjtGetFirstToken().toString()));
			return edge;
		}
	}

}
