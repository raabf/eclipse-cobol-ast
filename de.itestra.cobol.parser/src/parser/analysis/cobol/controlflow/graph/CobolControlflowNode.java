package parser.analysis.cobol.controlflow.graph;

import java.util.LinkedList;
import java.util.List;

import convex.step2.parser.SimpleNode;
import parser.analysis.cobol.controlflow.graph.visitors.CobolControlflowAnalysis;

public class CobolControlflowNode {
	private final String name;
	protected List<CobolControlflowEdge> edges = new LinkedList<CobolControlflowEdge>();
	private final int id;

	public CobolControlflowNode(CobolControlflowAnalysis analysis, String name) {
		id = analysis.registerNode(this);
		this.name = name;
	}

	public CobolControlflowNode(CobolControlflowAnalysis analysis) {
		id = analysis.registerNode(this);
		this.name = "Node" + this.id;
	}

	public CobolControlflowEdge controlflow(CobolControlflowNode nextNode) {
		CobolControlflowEdge result = new VirtualControlflowEdge(this, nextNode);
		this.edges.add(result);
		return result;
	}

	public CobolControlflowEdge controlflow(CobolControlflowNode nextNode, SimpleNode ast) {
		StatementControlflowEdge result = new StatementControlflowEdge(this, nextNode, ast);
		this.edges.add(result);
		return result;
	}

	public List<CobolControlflowEdge> getEdges() {
		return edges;
	}

	public String getName() {
		String result = name + id;
		return result.replace("-", "_");
	}

	@Override
	public String toString() {
		return name;
	}

	public int getId() {
		return id;
	}
}
