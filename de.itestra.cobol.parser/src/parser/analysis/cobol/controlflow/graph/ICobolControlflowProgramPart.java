package parser.analysis.cobol.controlflow.graph;

public interface ICobolControlflowProgramPart {
	public CobolControlflowNode getEndNode();
}
