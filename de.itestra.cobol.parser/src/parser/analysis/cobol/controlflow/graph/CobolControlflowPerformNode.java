package parser.analysis.cobol.controlflow.graph;

import parser.analysis.cobol.controlflow.graph.annotations.NameAnnotation;
import parser.analysis.cobol.controlflow.graph.visitors.CobolControlflowAnalysis;

public class CobolControlflowPerformNode extends DefaultCobolControlflowPart {
	public CobolControlflowPerformNode(CobolControlflowAnalysis analysis) {
		super(analysis, "PERFORM");
	}

	public void loops() {
		CobolControlflowEdge edge = getEndNode().controlflow(this);
		edge.annotate(new NameAnnotation("LOOP"));
	}

	@Override
	public String toString() {
		return "PERFORM";
	}
}
