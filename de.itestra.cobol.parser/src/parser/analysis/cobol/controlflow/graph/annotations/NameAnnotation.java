package parser.analysis.cobol.controlflow.graph.annotations;

public class NameAnnotation implements CobolControlflowEdgeAnnotation {
	private final String name;

	public NameAnnotation(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}
}
