package parser.analysis.cobol.controlflow.graph;

import parser.analysis.cobol.controlflow.graph.visitors.CobolControlflowAnalysis;
import parser.analysis.cobol.structure.representation.impl.AbstractCobolStructureRepresentation;

public class CobolProgramNode extends CobolCallableControlflowProgramPart {
	public CobolProgramNode(CobolControlflowAnalysis analysis, AbstractCobolStructureRepresentation representation) {
		super(analysis, representation, new CobolProgramEndNode(analysis));
	}
}
