package parser.analysis.cobol.controlflow.graph;

public class VirtualControlflowEdge extends CobolControlflowEdge {
	public VirtualControlflowEdge(CobolControlflowNode previous, CobolControlflowNode next) {
		super(previous, next);
	}

	@Override
	public final boolean isStatement() {
		return false;
	}

	@Override
	public final StatementControlflowEdge asStatement() {
		throw new ClassCastException(this + " is not a StatementControlflowEdge");
	}
}
