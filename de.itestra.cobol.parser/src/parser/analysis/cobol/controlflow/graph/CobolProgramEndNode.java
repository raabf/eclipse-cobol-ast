package parser.analysis.cobol.controlflow.graph;

import parser.analysis.cobol.controlflow.graph.visitors.CobolControlflowAnalysis;

public class CobolProgramEndNode extends CobolControlflowNode {
	public CobolProgramEndNode(CobolControlflowAnalysis analysis) {
		super(analysis, "END_OF_PROGRAM");
	}

	@Override
	public CobolControlflowEdge controlflow(CobolControlflowNode nextNode) {
		// do nothing, there is no next node after the end node of a program
		// FIXME what to return here?
		return null;
	}
}
