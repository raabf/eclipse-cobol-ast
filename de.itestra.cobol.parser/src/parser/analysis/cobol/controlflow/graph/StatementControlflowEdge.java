package parser.analysis.cobol.controlflow.graph;

import convex.step2.parser.SimpleNode;

public class StatementControlflowEdge extends CobolControlflowEdge {
	private final SimpleNode ast;

	public StatementControlflowEdge(CobolControlflowNode previous, CobolControlflowNode next, SimpleNode ast) {
		super(previous, next);
		this.ast = ast;
	}

	@Override
	public final boolean isStatement() {
		return true;
	}

	@Override
	public final StatementControlflowEdge asStatement() {
		return this;
	}

	public SimpleNode getAst() {
		return ast;
	}
}
