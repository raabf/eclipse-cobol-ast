package parser.analysis.cobol.structure;

import parser.analysis.cobol.structure.representation.impl.EnvironmentDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import convex.step2.parser.ASTConfigurationSection;
import convex.step2.parser.ASTInputOutputSection;
import convex.step2.parser.CobolParserDefaultVisitor;

/*package*/class StructureEnvironmentDivisionVisitor extends CobolParserDefaultVisitor {
	private final EnvironmentDivisionRepresentation division;

	public StructureEnvironmentDivisionVisitor(EnvironmentDivisionRepresentation division) {
		this.division = division;
	}

	@Override
	public Object visit(ASTConfigurationSection node, RepresentationFactory data) {
		return node.childrenAccept(
				new StructureConfigurationSectionVisitor(data.section(node, "CONFIGURATION SECTION", division)), data);
	}

	@Override
	public Object visit(ASTInputOutputSection node, RepresentationFactory data) {
		return node.childrenAccept(
				new StructureInputOutputSectionVisitor(data.section(node, "INPUT OUTPUT SECTION", division)), data);
	}
}
