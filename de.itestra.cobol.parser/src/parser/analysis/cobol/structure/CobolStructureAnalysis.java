package parser.analysis.cobol.structure;

import java.util.List;

import org.apache.log4j.Logger;

import parser.analysis.base.structure.IStructureAnalysis;
import parser.analysis.cobol.structure.representation.impl.AbstractCobolStructureRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProgramRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import convex.step2.parser.ASTProgramUnit;
import convex.step2.parser.CobolParserDefaultVisitor;
import convex.step2.parser.Node;

/**
 * use package private methods to access symbol table
 */
public class CobolStructureAnalysis extends CobolParserDefaultVisitor implements IStructureAnalysis<Node> {
	private static final Logger logger = Logger.getLogger(CobolStructureAnalysis.class);
	
	private ProgramRepresentation root;
	private final RepresentationFactory factory = new RepresentationFactory();
	
	@Override
	public void run(Node ast) {
		long start = System.currentTimeMillis();
		ast.jjtAccept(this, factory);
		logger.debug("Structure Analysis duration: " + (System.currentTimeMillis() - start) + " ms");
	}
	
	@Override
	public Object visit(ASTProgramUnit node, RepresentationFactory data){
		root = data.program(node);
		return node.childrenAccept(new StructureDivisionsVisitor(root), data);
	}
	
	@Override
	public final ProgramRepresentation getProgramStructure() {
		return this.root;
	}

	@Override
	public final List<AbstractCobolStructureRepresentation> getProgramStructureFlat() {
		return factory.getRepresentations();
	}
}
