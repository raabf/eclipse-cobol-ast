package parser.analysis.cobol.structure;

import parser.analysis.cobol.structure.representation.impl.ProgramRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import convex.step2.parser.ASTProgramIdParagraph;
import convex.step2.parser.CobolParserDefaultVisitor;

/*package*/ class StructureIdentificationDivisionVisitor extends CobolParserDefaultVisitor {
	private final ProgramRepresentation program;

	public StructureIdentificationDivisionVisitor(ProgramRepresentation program) {
		this.program = program;
	}

	@Override
	public Object visit(ASTProgramIdParagraph node, RepresentationFactory data) {
		return node.childrenAccept(new StructureProgramIdParagraphVisitor(program), data);
	}
}
