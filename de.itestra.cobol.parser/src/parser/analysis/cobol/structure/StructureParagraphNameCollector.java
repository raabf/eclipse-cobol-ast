package parser.analysis.cobol.structure;

import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import convex.step2.parser.ASTCobolWord;
import convex.step2.parser.ASTParagraphName;
import convex.step2.parser.ASTSentence;
import convex.step2.parser.CobolParserDefaultVisitor;

public class StructureParagraphNameCollector extends CobolParserDefaultVisitor {
	private String name = "";

	@Override
	public Object visit(ASTParagraphName node, RepresentationFactory data) {
		name = ((ASTCobolWord) node.jjtGetChild(0)).jjtGetValue().toString();
		return data;
	}

	@Override
	public Object visit(ASTSentence node, RepresentationFactory data) {
		// do not descend deeper, as there may be further paragraph name node
		// within the sentence
		return data;
	}

	public String getName() {
		return name;
	}
}