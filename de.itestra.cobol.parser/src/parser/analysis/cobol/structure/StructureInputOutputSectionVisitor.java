package parser.analysis.cobol.structure;

import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.analysis.cobol.structure.representation.impl.SectionRepresentation;
import convex.step2.parser.ASTFileControlParagraph;
import convex.step2.parser.ASTIOControlParagraph;
import convex.step2.parser.CobolParserDefaultVisitor;

public class StructureInputOutputSectionVisitor extends CobolParserDefaultVisitor {
	private final SectionRepresentation section;

	public StructureInputOutputSectionVisitor(SectionRepresentation section) {
		this.section = section;
	}

	@Override
	public Object visit(ASTFileControlParagraph node, RepresentationFactory data) {
		data.paragraph(node, "FILE CONTROL", section);
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTIOControlParagraph node, RepresentationFactory data) {
		data.paragraph(node, "I-O-CONTROL", section);
		return super.visit(node, data);
	}
}