package parser.analysis.cobol.structure;

import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import convex.step2.parser.ASTCobolWord;
import convex.step2.parser.ASTSectionHeader;
import convex.step2.parser.ASTSectionName;
import convex.step2.parser.CobolParserDefaultVisitor;

public class StructureSectionNameCollector extends CobolParserDefaultVisitor {
	private String name = "";

	@Override
	public Object visit(ASTSectionHeader node, RepresentationFactory data) {
		return node.childrenAccept(new CobolParserDefaultVisitor() {
			@Override
			public Object visit(ASTSectionName node, RepresentationFactory data) {
				name = ((ASTCobolWord) node.jjtGetChild(0)).jjtGetValue().toString();
				return data;
			}
		}, data);
	}

	public String getName() {
		return name;
	}
}
