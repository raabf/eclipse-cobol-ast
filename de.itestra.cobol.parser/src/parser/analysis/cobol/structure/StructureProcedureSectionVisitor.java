package parser.analysis.cobol.structure;

import parser.analysis.cobol.structure.representation.impl.ProcedureSectionRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import convex.step2.parser.ASTParagraph;
import convex.step2.parser.ASTSentence;
import convex.step2.parser.CobolParserDefaultVisitor;

public class StructureProcedureSectionVisitor extends CobolParserDefaultVisitor {
	private final ProcedureSectionRepresentation section;

	public StructureProcedureSectionVisitor(ProcedureSectionRepresentation parentRepresentation) {
		this.section = parentRepresentation;
	}
	
	@Override
	public Object visit(ASTParagraph node, RepresentationFactory data) {
		section.setEmpty(false);
		StructureParagraphNameCollector nameCollector = new StructureParagraphNameCollector();
		node.childrenAccept(nameCollector, data);
		data.paragraph(node, nameCollector.getName(), section);
		return super.visit(node, data);
	}
	
	@Override
	public Object visit(ASTSentence node, RepresentationFactory data) {
		section.setEmpty(false);
		return super.visit(node, data);
	}
}
