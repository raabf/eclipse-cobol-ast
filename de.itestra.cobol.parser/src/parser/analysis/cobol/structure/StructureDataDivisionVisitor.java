package parser.analysis.cobol.structure;

import java.util.Stack;

import parser.analysis.cobol.structure.representation.impl.DataDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.FieldDeclarationRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.analysis.cobol.structure.representation.impl.SectionRepresentation;
import convex.step2.parser.ASTCommunicationSection;
import convex.step2.parser.ASTDataDescriptionEntry;
import convex.step2.parser.ASTFileSection;
import convex.step2.parser.ASTLevelName;
import convex.step2.parser.ASTLevelNumber;
import convex.step2.parser.ASTLinkageSection;
import convex.step2.parser.ASTLocalStorageSection;
import convex.step2.parser.ASTWorkingStorageSection;
import convex.step2.parser.CobolParserDefaultVisitor;

public class StructureDataDivisionVisitor extends CobolParserDefaultVisitor {
	private final DataDivisionRepresentation division;

	public StructureDataDivisionVisitor(DataDivisionRepresentation division) {
		this.division = division;
	}

	@Override
	public Object visit(ASTFileSection node, RepresentationFactory data) {
		SectionRepresentation section = data.section(node, "FILE SECTION", division);
		return node.childrenAccept(new DataDivisionSectionVisitor(section), data);
	}

	@Override
	public Object visit(ASTWorkingStorageSection node, RepresentationFactory data) {
		SectionRepresentation section = data.section(node, "WORKING-STORAGE SECTION", division);
		return node.childrenAccept(new DataDivisionSectionVisitor(section), data);
	}

	@Override
	public Object visit(ASTLocalStorageSection node, RepresentationFactory data) {
		SectionRepresentation section = data.section(node, "LOCAL-STORAGE SECTION", division);
		return node.childrenAccept(new DataDivisionSectionVisitor(section), data);
	}

	@Override
	public Object visit(ASTLinkageSection node, RepresentationFactory data) {
		SectionRepresentation section = data.section(node, "LINKAGE SECTION", division);
		return node.childrenAccept(new DataDivisionSectionVisitor(section), data);
	}

	@Override
	public Object visit(ASTCommunicationSection node, RepresentationFactory data) {
		data.section(node, "COMMUNICATION SECTION", division);
		return super.visit(node, data);
	}

	private static class DataDivisionSectionVisitor extends CobolParserDefaultVisitor {
		private final SectionRepresentation section;
		private Stack<FieldDeclarationRepresentation> hierarchy = new Stack<FieldDeclarationRepresentation>();

		public DataDivisionSectionVisitor(SectionRepresentation section) {
			this.section = section;
		}

		@Override
		public Object visit(ASTDataDescriptionEntry node, RepresentationFactory data) {
			return node.childrenAccept(new DataDescriptionEntryClauseVisitor(), data);
		}

		private class DataDescriptionEntryClauseVisitor extends CobolParserDefaultVisitor {
			private int level;

			@Override
			public Object visit(ASTLevelNumber node, RepresentationFactory data) {
				level = Integer.valueOf(node.jjtGetFirstToken().image);
				while (!hierarchy.isEmpty() && hierarchy.peek().getLevel() >= level) {
					hierarchy.pop();
				}
				return super.visit(node, data);
			}

			@Override
			public Object visit(ASTLevelName node, RepresentationFactory data) {
				FieldDeclarationRepresentation declaration;
				if (hierarchy.isEmpty()) {
					declaration = data.variableDeclaration(node, level, node.jjtGetFirstToken().image, section);
				} else {
					declaration = data
							.variableDeclaration(node, level, node.jjtGetFirstToken().image, hierarchy.peek());
				}
				hierarchy.push(declaration);
				return super.visit(node, data);
			}
		}
	}
}
