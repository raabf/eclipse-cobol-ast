package parser.analysis.cobol.structure;

import parser.analysis.cobol.structure.representation.impl.DeclarativesRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProcedureDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProcedureSectionRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import convex.step2.parser.ASTDeclarativeSection;
import convex.step2.parser.ASTParagraph;
import convex.step2.parser.ASTProcedureSection;
import convex.step2.parser.CobolParserDefaultVisitor;

public class StructureProcedureDivisionVisitor extends CobolParserDefaultVisitor {
	private final ProcedureDivisionRepresentation procedureDivision;
	
	public StructureProcedureDivisionVisitor(ProcedureDivisionRepresentation procedureDivision) {
		this.procedureDivision = procedureDivision;
	}
	
	@Override
	public Object visit(ASTDeclarativeSection node, RepresentationFactory data) {
		return node.childrenAccept(new DeclarativeSectionVisitor(data.declaratives(node, procedureDivision)), data);
	}
	
	@Override
	public Object visit(ASTProcedureSection node, RepresentationFactory data) {
		StructureSectionNameCollector nameCollector = new StructureSectionNameCollector();
		node.childrenAccept(nameCollector, data);
		ProcedureSectionRepresentation section = data.procedureSection(node, nameCollector.getName(), procedureDivision);
		return node.childrenAccept(new StructureProcedureSectionVisitor(section), data);
	}
	
	@Override
	public Object visit(ASTParagraph node, RepresentationFactory data){
		StructureParagraphNameCollector nameCollector = new StructureParagraphNameCollector();
		node.childrenAccept(nameCollector, data);
		data.paragraph(node, nameCollector.getName(), procedureDivision);
		return super.visit(node, data);
	}
	
	private static class DeclarativeSectionVisitor extends CobolParserDefaultVisitor {
		private final DeclarativesRepresentation declarativesSection;
		
		public DeclarativeSectionVisitor(DeclarativesRepresentation parent) {
			this.declarativesSection = parent;
		}
		
		@Override
		public Object visit(ASTProcedureSection node, RepresentationFactory data) {
			StructureSectionNameCollector nameCollector = new StructureSectionNameCollector();
			node.childrenAccept(nameCollector, data);
			ProcedureSectionRepresentation section = data.procedureSection(node, nameCollector.getName(), declarativesSection);
			return node.childrenAccept(new StructureProcedureSectionVisitor(section), data);
		}
	}
}
