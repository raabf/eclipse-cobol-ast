package parser.analysis.cobol.structure;

import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import parser.analysis.cobol.structure.representation.impl.SectionRepresentation;
import convex.step2.parser.ASTObjectComputerParagraph;
import convex.step2.parser.ASTSourceComputerParagraph;
import convex.step2.parser.ASTSpecialNamesParagraph;
import convex.step2.parser.CobolParserDefaultVisitor;

public class StructureConfigurationSectionVisitor extends CobolParserDefaultVisitor {
	private final SectionRepresentation section;

	public StructureConfigurationSectionVisitor(SectionRepresentation section) {
		this.section = section;
	}

	@Override
	public Object visit(ASTSourceComputerParagraph node, RepresentationFactory data) {
		data.paragraph(node, "SOURCE-COMPUTER", section);
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTObjectComputerParagraph node, RepresentationFactory data) {
		data.paragraph(node, "OBJECT-COMPUTER", section);
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTSpecialNamesParagraph node, RepresentationFactory data) {
		data.paragraph(node, "SPECIAL-NAMES", section);
		return super.visit(node, data);
	}
}