package parser.analysis.cobol.structure;

import parser.analysis.cobol.structure.representation.impl.ProgramRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import convex.step2.parser.ASTCobolWord;
import convex.step2.parser.CobolParserDefaultVisitor;

public class StructureProgramIdParagraphVisitor extends CobolParserDefaultVisitor {
	private final ProgramRepresentation program;

	public StructureProgramIdParagraphVisitor(ProgramRepresentation program) {
		this.program = program;
	}

	@Override
	public Object visit(ASTCobolWord node, RepresentationFactory data) {
		program.setProgramName(node.jjtGetValue().toString());
		return super.visit(node, data);
	}
}