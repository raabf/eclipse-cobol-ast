package parser.analysis.cobol.structure;

import parser.analysis.cobol.structure.representation.impl.ProgramRepresentation;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import convex.step2.parser.ASTDataDivision;
import convex.step2.parser.ASTEnvironmentDivision;
import convex.step2.parser.ASTIdentificationDivision;
import convex.step2.parser.ASTProcedureDivision;
import convex.step2.parser.CobolParserDefaultVisitor;

/*package*/class StructureDivisionsVisitor extends CobolParserDefaultVisitor {
	private final ProgramRepresentation program;

	public StructureDivisionsVisitor(ProgramRepresentation program) {
		this.program = program;
	}

	@Override
	public Object visit(ASTIdentificationDivision node, RepresentationFactory data) {
		data.identificationDivision(node, program);
		return node.childrenAccept(
				new StructureIdentificationDivisionVisitor(program), data);
	}

	@Override
	public Object visit(ASTEnvironmentDivision node, RepresentationFactory data) {
		return node.childrenAccept(new StructureEnvironmentDivisionVisitor(data.environmentDivision(node, program)), data);
	}

	@Override
	public Object visit(ASTDataDivision node, RepresentationFactory data) {
		return node.childrenAccept(new StructureDataDivisionVisitor(data.dataDivision(node, program)), data);
	}

	@Override
	public Object visit(ASTProcedureDivision node, RepresentationFactory data) {
		return node.childrenAccept(new StructureProcedureDivisionVisitor(data.procedureDivision(node, program)), data);
	}
}
