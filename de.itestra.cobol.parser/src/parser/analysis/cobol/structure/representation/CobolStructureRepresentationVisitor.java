package parser.analysis.cobol.structure.representation;

public interface CobolStructureRepresentationVisitor {
	public void visit(CobolStructureRepresentation representation);

	public void visit(CobolProgramRepresentation representation);

	public void visit(CobolProcedureSectionRepresentation representation);

	public void visit(CobolParagraphRepresentation representation);
}
