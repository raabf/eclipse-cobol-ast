package parser.analysis.cobol.structure.representation;

import parser.analysis.base.structure.representation.ICallableDeclarationRepresentation;

public interface CobolCallableDeclarationRepresentation extends CobolStructureRepresentation, ICallableDeclarationRepresentation {

}
