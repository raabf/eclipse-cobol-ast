package parser.analysis.cobol.structure.representation;

import parser.analysis.base.structure.representation.IStructureRepresentation;

public interface CobolStructureRepresentation extends IStructureRepresentation {
	public void accept(CobolStructureRepresentationVisitor visitor);

	public void childrenAccept(CobolStructureRepresentationVisitor visitor);
}
