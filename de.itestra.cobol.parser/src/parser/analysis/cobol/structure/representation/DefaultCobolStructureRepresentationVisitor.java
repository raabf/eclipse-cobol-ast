package parser.analysis.cobol.structure.representation;

/**
 * default noop-implementation
 */
public class DefaultCobolStructureRepresentationVisitor implements CobolStructureRepresentationVisitor {
	private void defaultVisit(CobolStructureRepresentation representation) {
		representation.childrenAccept(this);
	}

	@Override
	public void visit(CobolStructureRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(CobolProgramRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(CobolProcedureSectionRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(CobolParagraphRepresentation representation) {
		defaultVisit(representation);
	}
}
