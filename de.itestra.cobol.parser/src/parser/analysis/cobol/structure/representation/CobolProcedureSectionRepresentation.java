package parser.analysis.cobol.structure.representation;

public interface CobolProcedureSectionRepresentation extends CobolCallableDeclarationRepresentation {
	public boolean isEmpty();
}
