package parser.analysis.cobol.structure.representation.impl;

import parser.analysis.cobol.definition.representation.CobolReadFieldRepresentation;
import parser.analysis.cobol.definition.representation.CobolWriteFieldRepresentation;


public class InternalDefaultCobolStructureRepresentationVisitor implements InternalCobolStructureRepresentationVisitor {
	private void defaultVisit(AbstractCobolStructureRepresentation representation) {
		representation.childrenAccept(this);
	}
	
	@Override
	public void visit(AbstractCobolStructureRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(ProgramRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(IdentificationDivisionRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(EnvironmentDivisionRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(DataDivisionRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(ProcedureDivisionRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(DeclarativesRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(SectionRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(ProcedureSectionRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(ParagraphRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(FieldDeclarationRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(CobolReadFieldRepresentation representation) {
		defaultVisit(representation);
	}

	@Override
	public void visit(CobolWriteFieldRepresentation representation) {
		defaultVisit(representation);
	}
}
