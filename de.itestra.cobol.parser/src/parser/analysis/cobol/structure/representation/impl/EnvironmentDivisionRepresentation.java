package parser.analysis.cobol.structure.representation.impl;

import parser.analysis.cobol.structure.representation.CobolStructureRepresentationVisitor;
import convex.step2.parser.SimpleNode;
import de.itestra.swgcommons.util.Scope;

public class EnvironmentDivisionRepresentation extends AbstractCobolStructureRepresentation {
	public EnvironmentDivisionRepresentation(SimpleNode ast, String displayName, Scope scope) {
		super(ast, displayName, scope);
	}
	
	@Override
	public void accept(CobolStructureRepresentationVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public void accept(InternalCobolStructureRepresentationVisitor visitor) {
		visitor.visit(this);
	}
}
