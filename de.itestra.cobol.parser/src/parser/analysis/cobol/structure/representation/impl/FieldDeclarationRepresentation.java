package parser.analysis.cobol.structure.representation.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import parser.analysis.base.definition.representation.IReadDataRepresentation;
import parser.analysis.base.definition.representation.IWriteDataRepresentation;
import parser.analysis.base.structure.representation.IDataDeclarationRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentationVisitor;

import com.google.common.collect.Sets;

import convex.step2.parser.SimpleNode;
import de.itestra.swgcommons.util.Scope;

public class FieldDeclarationRepresentation extends AbstractCobolStructureRepresentation implements
		IDataDeclarationRepresentation {
	private final int level;
	private Set<FieldDeclarationRepresentation> childVariables = Sets.newHashSet();
	private final Collection<IReadDataRepresentation> reads = Sets.newHashSet();
	private final Collection<IWriteDataRepresentation> writes = Sets.newHashSet();

	public FieldDeclarationRepresentation(SimpleNode ast, int level, String displayName, Scope scope) {
		super(ast, displayName, scope);
		this.level = level;
	}

	@Override
	public void accept(CobolStructureRepresentationVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void accept(InternalCobolStructureRepresentationVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void addRead(IReadDataRepresentation read) {
		reads.add(read);
	}

	@Override
	public Collection<IReadDataRepresentation> getReads() {
		return Collections.unmodifiableCollection(reads);
	}

	@Override
	public void addWrite(IWriteDataRepresentation write) {
		writes.add(write);
	}

	@Override
	public Collection<IWriteDataRepresentation> getWrites() {
		return Collections.unmodifiableCollection(writes);
	}

	@Override
	protected void addChild(AbstractCobolStructureRepresentation child) {
		super.addChild(child);
		if (IDataDeclarationRepresentation.class.isAssignableFrom(child.getClass())) {
			addChildVariable((FieldDeclarationRepresentation) child);
		}
	}

	public int getLevel() {
		return level;
	}

	private void addChildVariable(FieldDeclarationRepresentation child) {
		childVariables.add(child);
	}

	public FieldDeclarationRepresentation getChild(String name) {
		FieldDeclarationRepresentation result = null;

		for (FieldDeclarationRepresentation child : childVariables) {
			if (child.getName().equals(name.toUpperCase())) {
				result = child;
				break;
			}
		}

		return result;
	}

	@Override
	public FieldDeclarationRepresentation getChildRecursive(String name) {
		FieldDeclarationRepresentation result = null;

		if (getName().equals(name.toUpperCase())) {
			result = this;
		} else {
			for (FieldDeclarationRepresentation child : childVariables) {
				result = child.getChildRecursive(name);
				if (result != null) {
					break;
				}
			}
		}

		return result;
	}
}
