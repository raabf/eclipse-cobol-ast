package parser.analysis.cobol.structure.representation.impl;

import parser.analysis.cobol.definition.representation.CobolReadFieldRepresentation;
import parser.analysis.cobol.definition.representation.CobolWriteFieldRepresentation;

public interface InternalCobolStructureRepresentationVisitor {
	public void visit(AbstractCobolStructureRepresentation representation);
	
	public void visit(ProgramRepresentation representation);
	
	public void visit(ProcedureSectionRepresentation representation);

	public void visit(ParagraphRepresentation representation);
	
	public void visit(IdentificationDivisionRepresentation representation);

	public void visit(EnvironmentDivisionRepresentation representation);

	public void visit(DataDivisionRepresentation representation);

	public void visit(ProcedureDivisionRepresentation representation);

	public void visit(DeclarativesRepresentation representation);

	public void visit(SectionRepresentation representation);

	public void visit(FieldDeclarationRepresentation representation);

	public void visit(CobolReadFieldRepresentation representation);

	public void visit(CobolWriteFieldRepresentation representation);
}
