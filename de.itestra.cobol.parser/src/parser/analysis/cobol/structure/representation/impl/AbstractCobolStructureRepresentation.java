package parser.analysis.cobol.structure.representation.impl;

import java.util.ArrayList;
import java.util.List;

import org.conqat.lib.scanner.IToken;

import parser.analysis.base.structure.representation.impl.BasicStructureRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentationVisitor;
import convex.step2.parser.SimpleNode;
import de.itestra.swgcommons.util.Scope;

public abstract class AbstractCobolStructureRepresentation extends BasicStructureRepresentation implements CobolStructureRepresentation {
	private AbstractCobolStructureRepresentation parent;
	private List<AbstractCobolStructureRepresentation> children = new ArrayList<AbstractCobolStructureRepresentation>();
	private final SimpleNode ast;

	public AbstractCobolStructureRepresentation(SimpleNode ast, String displayName, Scope scope) {
		super((IToken)ast.jjtGetFirstToken().getValue(), (IToken)ast.jjtGetLastToken().getValue(), displayName, scope);
		this.ast = ast;
	}
	
	public SimpleNode getAst() {
		return ast;
	}

	@Override
	public abstract void accept(CobolStructureRepresentationVisitor visitor);
	
	public abstract void accept(InternalCobolStructureRepresentationVisitor visitor);

	@Override
	public void childrenAccept(CobolStructureRepresentationVisitor visitor) {
		for (AbstractCobolStructureRepresentation child : getChildren()) {
			child.accept(visitor);
		}
	}
	
	public void childrenAccept(InternalCobolStructureRepresentationVisitor visitor) {
		for (AbstractCobolStructureRepresentation child : getChildren()) {
			child.accept(visitor);
		}
	}
	
	public void print(String prefix) {
		System.out.println(prefix + getName() + ": " + getClass().getSimpleName() + " in " + getScope());
		for (AbstractCobolStructureRepresentation child : getChildren()) {
			child.print(prefix + "  ");
		}
	}
	
	@Override
	public AbstractCobolStructureRepresentation getParent() {
		return this.parent;
	}

	public final void setParent(AbstractCobolStructureRepresentation parent) {
		this.parent = parent;
	}

	@Override
	public List<AbstractCobolStructureRepresentation> getChildren() {
		return this.children;
	}

	protected void addChild(AbstractCobolStructureRepresentation child) {
		children.add(child);
	}
}
