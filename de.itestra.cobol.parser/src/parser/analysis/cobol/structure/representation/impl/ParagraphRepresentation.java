package parser.analysis.cobol.structure.representation.impl;

import parser.analysis.cobol.structure.representation.CobolParagraphRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentationVisitor;
import convex.step2.parser.SimpleNode;
import de.itestra.swgcommons.util.Scope;

public class ParagraphRepresentation extends AbstractCobolStructureRepresentation implements CobolParagraphRepresentation {
	private boolean exitsProgram = false;

	public ParagraphRepresentation(SimpleNode ast, String displayName, Scope scope) {
		super(ast, displayName, scope);
	}

	@Override
	public void accept(CobolStructureRepresentationVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public void accept(InternalCobolStructureRepresentationVisitor visitor) {
		visitor.visit(this);
	}

	public boolean isExitsProgram() {
		return exitsProgram;
	}

	public void exitsProgram() {
		this.exitsProgram = true;
	}
}
