package parser.analysis.cobol.structure.representation.impl;

import parser.analysis.cobol.structure.representation.CobolProcedureSectionRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentationVisitor;
import convex.step2.parser.SimpleNode;
import de.itestra.swgcommons.util.Scope;

public class ProcedureSectionRepresentation extends SectionRepresentation implements CobolProcedureSectionRepresentation {
	private boolean empty = true;
	
	public ProcedureSectionRepresentation(SimpleNode ast, String displayName, Scope scope) {
		super(ast, displayName, scope);
	}

	@Override
	public void accept(CobolStructureRepresentationVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void accept(InternalCobolStructureRepresentationVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public boolean isEmpty() {
		return empty;
	}
	
	public void setEmpty(boolean empty) {
		this.empty = empty;
	}
}
