package parser.analysis.cobol.structure.representation.impl;

import java.util.ArrayList;
import java.util.List;

import convex.step2.parser.ASTDataDivision;
import convex.step2.parser.ASTEnvironmentDivision;
import convex.step2.parser.ASTIdentificationDivision;
import convex.step2.parser.ASTProcedureDivision;
import convex.step2.parser.ASTProgramUnit;
import convex.step2.parser.SimpleNode;
import de.itestra.swgcommons.util.Scope;

public class RepresentationFactory {
	private List<AbstractCobolStructureRepresentation> representations = new ArrayList<AbstractCobolStructureRepresentation>();

	public List<AbstractCobolStructureRepresentation> getRepresentations() {
		return representations;
	}

	public final ProgramRepresentation program(ASTProgramUnit node) {
		ProgramRepresentation result = new ProgramRepresentation(node, "", Scope.getRoot()); // FIXME
																								// this
																								// scope
																								// is
																								// incorrect
																								// for
																								// nested
																								// programs...
		representations.add(result);
		return result;
	}

	public final IdentificationDivisionRepresentation identificationDivision(ASTIdentificationDivision node,
			AbstractCobolStructureRepresentation parent) {
		IdentificationDivisionRepresentation result = new IdentificationDivisionRepresentation(node,
				"IDENTIFICATION DIVISION", parent.getSelfScope());
		representations.add(result);
		result.setParent(parent);
		parent.addChild(result);
		return result;
	}

	public final EnvironmentDivisionRepresentation environmentDivision(ASTEnvironmentDivision node,
			AbstractCobolStructureRepresentation parent) {
		EnvironmentDivisionRepresentation result = new EnvironmentDivisionRepresentation(node, "ENVIRONMENT DIVISION",
				parent.getSelfScope());
		representations.add(result);
		result.setParent(parent);
		parent.addChild(result);
		return result;
	}

	public final DataDivisionRepresentation dataDivision(ASTDataDivision node,
			AbstractCobolStructureRepresentation parent) {
		DataDivisionRepresentation result = new DataDivisionRepresentation(node, "DATA DIVISION", parent.getSelfScope());
		representations.add(result);
		result.setParent(parent);
		parent.addChild(result);
		return result;
	}

	public final ProcedureDivisionRepresentation procedureDivision(ASTProcedureDivision node,
			AbstractCobolStructureRepresentation parent) {
		ProcedureDivisionRepresentation result = new ProcedureDivisionRepresentation(node, "PROCEDURE DIVISION",
				parent.getSelfScope());
		representations.add(result);
		result.setParent(parent);
		parent.addChild(result);
		return result;
	}

	public final DeclarativesRepresentation declaratives(SimpleNode node, AbstractCobolStructureRepresentation parent) {
		DeclarativesRepresentation result = new DeclarativesRepresentation(node, "DECLARATIVES", parent.getSelfScope());
		representations.add(result);
		result.setParent(parent);
		parent.addChild(result);
		return result;
	}

	public final SectionRepresentation section(SimpleNode node, String name, AbstractCobolStructureRepresentation parent) {
		SectionRepresentation result = new SectionRepresentation(node, name, parent.getSelfScope());
		representations.add(result);
		result.setParent(parent);
		parent.addChild(result);
		return result;
	}

	public final ProcedureSectionRepresentation procedureSection(SimpleNode node, String name,
			AbstractCobolStructureRepresentation parent) {
		ProcedureSectionRepresentation result = new ProcedureSectionRepresentation(node, name, parent.getSelfScope());
		representations.add(result);
		result.setParent(parent);
		parent.addChild(result);
		return result;
	}

	public final ParagraphRepresentation paragraph(SimpleNode node, String name,
			AbstractCobolStructureRepresentation parent) {
		ParagraphRepresentation result = new ParagraphRepresentation(node, name, parent.getSelfScope());
		representations.add(result);
		result.setParent(parent);
		parent.addChild(result);
		return result;
	}

	public final FieldDeclarationRepresentation variableDeclaration(SimpleNode node, int level, String name,
			AbstractCobolStructureRepresentation parentStructure) {
		FieldDeclarationRepresentation result = new FieldDeclarationRepresentation(node, level, name,
				parentStructure.getSelfScope());
		representations.add(result);
		result.setParent(parentStructure);
		parentStructure.addChild(result);
		return result;
	}
}
