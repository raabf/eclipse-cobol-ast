package parser.analysis.cobol.structure.representation.impl;

import parser.analysis.cobol.structure.representation.CobolProgramRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentationVisitor;
import convex.step2.parser.SimpleNode;
import de.itestra.swgcommons.util.Scope;

public class ProgramRepresentation extends AbstractCobolStructureRepresentation implements CobolProgramRepresentation {
	private String programName = null;

	public ProgramRepresentation(SimpleNode ast, String displayName, Scope scope) {
		super(ast, displayName, scope);
	}

	@Override
	public void accept(CobolStructureRepresentationVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void accept(InternalCobolStructureRepresentationVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String getName() {
		return getDisplayName().toUpperCase();
	}

	@Override
	public String getDisplayName() {
		return programName == null ? "" : programName;
	}

	// FIXME protect this
	public void setProgramName(String programName) {
		this.programName = programName;
	}

	@Override
	public Scope getSelfScope() {
		if (programName == null) {
			return getScope();
		} else {
			return Scope.getChild(getScope(), programName);
		}
	}
}
