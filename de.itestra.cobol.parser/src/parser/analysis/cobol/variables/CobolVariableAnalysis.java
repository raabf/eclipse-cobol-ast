package parser.analysis.cobol.variables;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.conqat.lib.scanner.IToken;

import parser.analysis.base.variables.ISymbolTable;
import parser.analysis.base.variables.IVariableAnalysis;
import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;
import convex.step2.parser.ASTCobolWord;
import convex.step2.parser.ASTDataDescriptionEntry;
import convex.step2.parser.ASTLevelName;
import convex.step2.parser.ASTLevelName66;
import convex.step2.parser.ASTLevelName77;
import convex.step2.parser.ASTLevelName88;
import convex.step2.parser.ASTLevelNumber;
import convex.step2.parser.CobolParserDefaultVisitor;
import convex.step2.parser.Node;

public class CobolVariableAnalysis extends CobolParserDefaultVisitor implements IVariableAnalysis<Node> {

	private CobolSymbolTable st;
	private boolean in_lvl_clause;
	
	//current level nr
	private int cur_level;
	//current level name
	private String cur_level_name;
	//current start_position
	private int cur_start;
	//current end_position
	private int cur_end;
	//Stack with level names
	private Stack<Integer> level_stack;
	//List with deferred variable names
	private List<String> deferred_variables;
	
	public CobolVariableAnalysis(){
		this.st = new CobolSymbolTable();
		this.in_lvl_clause = false;
		this.cur_level = -1;
		this.level_stack = new Stack<Integer>();
		this.deferred_variables = new ArrayList<String>();
	}
	
	@Override
	public void run(Node ast) {
		st.clear();
		
		//TODO use data?
		ast.jjtAccept(this, null);
	}
	
	@Override
	public Object visit(ASTDataDescriptionEntry node, RepresentationFactory data) {
		
		//level-no is first child
		ASTLevelNumber num = (ASTLevelNumber) node.jjtGetChild(0);
		IToken tok = (IToken) num.jjtGetFirstToken().getValue();
		int level = Integer.valueOf(tok.getText());
		
		//level-name is second child - obtain by going down the tree
		node.jjtGetChild(1).jjtAccept(this, data);
		
		if(this.level_stack.isEmpty()){
			//new variable hierarchy
			this.level_stack.push(level);
			this.deferred_variables.add(cur_level_name);
			//memorize start position
			this.cur_start = ((IToken)node.jjtGetFirstToken().getValue()).getOffset();
		}
		else if(this.level_stack.peek() < level){
			//we observed a level that is "deeper" than the previous one
			this.level_stack.push(level);
			this.deferred_variables.add(cur_level_name);
		}
		else if(this.level_stack.peek() == level && this.level_stack.size() > 1){
			//we are on the same level as before -> just defer the variable
			this.deferred_variables.add(cur_level_name);
		}
		else{
			//we observed a variable with the same level but we are not within a 
			//variable hierarchy -> clean the stack
			this.level_stack.clear();
			for(String s : this.deferred_variables){
				this.st.addSymbol(s, cur_start, cur_end);
			}
			this.deferred_variables.clear();
			this.level_stack.push(level);
			this.cur_start = ((IToken)node.jjtGetFirstToken().getValue()).getOffset();
			this.deferred_variables.add(cur_level_name);
		}
		
		this.cur_end = ((IToken)node.jjtGetLastToken().getValue()).getEndOffset()+2;
		
		return node;
	}

	@Override
	public Object visit(ASTLevelName66 node, RepresentationFactory data) {
		this.in_lvl_clause = true;
		if(node.jjtGetNumChildren() == 0){
			this.cur_level_name = node.jjtGetFirstToken().image;
		}
		node.childrenAccept(this, data);
		this.in_lvl_clause = false;
		return node;
	}

	@Override
	public Object visit(ASTLevelName77 node, RepresentationFactory data) {
		this.in_lvl_clause = true;
		if(node.jjtGetNumChildren() == 0){
			this.cur_level_name = node.jjtGetFirstToken().image;
		}
		node.childrenAccept(this, data);
		this.in_lvl_clause = false;
		return node;
	}

	@Override
	public Object visit(ASTLevelName88 node, RepresentationFactory data) {
		this.in_lvl_clause = true;
		if(node.jjtGetNumChildren() == 0){
			this.cur_level_name = node.jjtGetFirstToken().image;
		}
		node.childrenAccept(this, data);
		this.in_lvl_clause = false;
		return node;
	}

	@Override
	public Object visit(ASTLevelName node, RepresentationFactory data) {
		this.in_lvl_clause = true;
		if(node.jjtGetNumChildren() == 0){
			this.cur_level_name = node.jjtGetFirstToken().image;
		}
		node.childrenAccept(this, data);
		this.in_lvl_clause = false;
		return node;
	}

	@Override
	public Object visit(ASTCobolWord node, RepresentationFactory data) {
		if(this.in_lvl_clause){
			this.cur_level_name = (String) node.jjtGetValue();
		}
		return node;
	}

	@Override
	public ISymbolTable getSymbolTable() {
		return this.st;
	}

}
