package parser.analysis.cobol.variables;

import java.util.HashMap;
import java.util.Map;

import parser.analysis.base.variables.ISymbolTable;

public class CobolSymbolTable implements ISymbolTable {

	private Map<String, Integer> declarationStartLocations;
	private Map<String, Integer> declarationEndLocations;
	
	public CobolSymbolTable(){
		this.declarationEndLocations = new HashMap<String, Integer>();
		this.declarationStartLocations = new HashMap<String, Integer>();
	}
	
	@Override
	public int getDeclarationStart(String varname) {
		Integer start = declarationStartLocations.get(varname);
		if(start == null){
			return -1;
		}
		return start.intValue();
	}

	@Override
	public int getDeclarationEnd(String varname) {
		Integer end = declarationEndLocations.get(varname);
		if(end == null){
			return -1;
		}
		return end.intValue();
	}
	
	public void addSymbol(String name, int decl_start_offset, int decl_end_offset){
		this.declarationEndLocations.put(name, decl_end_offset);
		this.declarationStartLocations.put(name, decl_start_offset);
	}
	
	public void clear(){
		this.declarationEndLocations.clear();
		this.declarationStartLocations.clear();
	}

}
