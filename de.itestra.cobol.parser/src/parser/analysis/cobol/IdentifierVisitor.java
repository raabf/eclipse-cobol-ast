package parser.analysis.cobol;

import java.util.LinkedList;
import java.util.List;

import parser.analysis.cobol.structure.representation.impl.RepresentationFactory;

import com.google.common.collect.Lists;

import convex.step2.parser.ASTCobolWord;
import convex.step2.parser.CobolParserDefaultVisitor;

public class IdentifierVisitor extends CobolParserDefaultVisitor {
	private LinkedList<ASTCobolWord> qualifiedName = Lists.newLinkedList();

	@Override
	public Object visit(ASTCobolWord node, RepresentationFactory data) {
		qualifiedName.addFirst(node);
		return super.visit(node, data);
	}

	public List<ASTCobolWord> getQualifiedName() {
		return qualifiedName;
	}
}
