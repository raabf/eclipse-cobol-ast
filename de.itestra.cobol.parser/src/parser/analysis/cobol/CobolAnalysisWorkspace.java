package parser.analysis.cobol;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Map;

import org.apache.log4j.Logger;
import org.conqat.lib.scanner.ELanguage;
import org.conqat.lib.scanner.ILenientScanner;
import org.conqat.lib.scanner.ScannerFactory;

import parser.CustomTokenManager;
import parser.analysis.AnalysisException;
import parser.analysis.base.IAnalysisWorkspace;
import parser.analysis.base.definition.representation.IDefinitionUseRepresentation;
import parser.analysis.cobol.definition.CobolDefinitionUseAnalysis;
import parser.analysis.cobol.structure.CobolStructureAnalysis;
import parser.analysis.cobol.structure.representation.impl.AbstractCobolStructureRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProgramRepresentation;
import parser.errorhandling.ILowLevelErrorHandler;
import parser.tokenprovider.ExcludeCopybooksTokenProvider;
import parser.tokenprovider.ITokenProvider;
import parser.tokenprovider.filter.AmbigiousTokenFilter;
import parser.tokenprovider.filter.ExecSQLFilter;
import parser.tokenprovider.filter.IgnoredTokensFilter;
import parser.tokenprovider.filter.SpecialTokenHandler;

import com.google.common.collect.Maps;

import convex.step2.parser.CobolParser;
import convex.step2.parser.CobolParserTokenManager;
import convex.step2.parser.Node;
import convex.step2.parser.ParseException;
import convex.step2.parser.SimpleNode;
import de.itestra.swgcommons.preprocessing.OnTheFlyPreprocessingFilter;

public class CobolAnalysisWorkspace implements IAnalysisWorkspace {
	private static final Logger logger = Logger.getLogger(CobolAnalysisWorkspace.class);

	private ILenientScanner scanner;
	private CobolParser parser;

	private Map<File, ProgramRepresentation> structureRepresentations = Maps.newHashMap();
	private Map<File, IDefinitionUseRepresentation> controlflowRepresentations = Maps.newHashMap();
	private Map<File, IDefinitionUseRepresentation> dataflowRepresentations = Maps.newHashMap();

	private final ILowLevelErrorHandler errorHandler;

	public CobolAnalysisWorkspace(ILowLevelErrorHandler errorHandler) {
		this.errorHandler = errorHandler;
	}

	@Override
	public ProgramRepresentation getStructureRepresentation(File file) {
		if (!structureRepresentations.containsKey(file)) {
			analyzeStructure(file);
		}
		return structureRepresentations.get(file);
	}
	
	public Node getAST(File file) throws IOException, ParseException {
		return parse(file);
	}

	@Override
	public AbstractCobolStructureRepresentation analyzeStructure(File file) {
		try {
			CobolStructureAnalysis analysis = createStructureAnalysis();
			analysis.run(parse(file));
			ProgramRepresentation result = analysis.getProgramStructure();
			structureRepresentations.put(file, result);
			return result;
		} catch (IOException | ParseException e) {
			throw new AnalysisException(e);
		}
	}

	@Override
	public IDefinitionUseRepresentation getDataflowRepresentation(File file) {
		if (!dataflowRepresentations.containsKey(file)) {
			analyzeDataflow(file);
		}
		return dataflowRepresentations.get(file);
	}

	@Override
	public IDefinitionUseRepresentation analyzeDataflow(File file) {
		CobolDefinitionUseAnalysis analysis = new CobolDefinitionUseAnalysis(errorHandler);
		analysis.run(getStructureRepresentation(file));
		IDefinitionUseRepresentation result = analysis.getRepresentation();
		dataflowRepresentations.put(file, result);
		return result;
	}

	@Override
	public void clearAnalyses(File file) {
		structureRepresentations.remove(file);
		controlflowRepresentations.remove(file);
		dataflowRepresentations.remove(file);
	}

	private CobolStructureAnalysis createStructureAnalysis() {
		return new CobolStructureAnalysis();
	}

	private Node parse(File file) throws IOException, ParseException {
		long start = System.currentTimeMillis();
		CobolParserTokenManager tokenManager = tokenManager(reader(file), file.getAbsolutePath());
		if (parser == null) {
			parser = new CobolParser(tokenManager);
		} else {
			parser.ReInit(tokenManager);
		}
		parser.CompilationUnit();
		logger.debug("Parsed file " + file + " in " + (System.currentTimeMillis() - start));
		return parser.getRootNode();
	}

	private final Reader reader(File file) throws IOException {
		OnTheFlyPreprocessingFilter result = new OnTheFlyPreprocessingFilter();

		result.setMinRelevantColumn(6);
		result.setMaxRelevantColumn(71);
		result.reset(file);

		return result;
	}

	private CobolParserTokenManager tokenManager(Reader reader, String originId) throws IOException {
		if (scanner == null) {
			scanner = ScannerFactory.newLenientScanner(ELanguage.COBOL, reader, originId);
		} else {
			scanner.reset(reader, originId);
		}
		ITokenProvider filterChain = new ExcludeCopybooksTokenProvider(scanner);
		filterChain = new ExecSQLFilter(filterChain);
		filterChain = new IgnoredTokensFilter(filterChain);
		filterChain = new AmbigiousTokenFilter(filterChain);
		filterChain = new SpecialTokenHandler(filterChain);
		return new CustomTokenManager(filterChain);
	}
}
