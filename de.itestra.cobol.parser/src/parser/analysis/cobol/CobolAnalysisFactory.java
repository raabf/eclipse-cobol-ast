package parser.analysis.cobol;

import parser.IAnalysisFactory;
import parser.analysis.cobol.structure.CobolStructureAnalysis;
import parser.analysis.cobol.variables.CobolVariableAnalysis;
import convex.step2.parser.Node;

public class CobolAnalysisFactory implements IAnalysisFactory {

	private CobolStructureAnalysis	structureAnalysis;
	private CobolVariableAnalysis	variableAnalysis;	
	
	public CobolAnalysisFactory(){
		this.structureAnalysis = new CobolStructureAnalysis();
		this.variableAnalysis = new CobolVariableAnalysis();
	}
	
	public void refreshAnalysis(Node ast){
		structureAnalysis.run(ast);
		variableAnalysis.run(ast);
	}

	@Override
	public CobolVariableAnalysis getVariableAnalysis() {
		return this.variableAnalysis;
	}
	
	@Override
	public CobolStructureAnalysis getStructureAnalysis() {
		return structureAnalysis;
	}
}
