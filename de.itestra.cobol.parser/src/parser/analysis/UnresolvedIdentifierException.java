package parser.analysis;

public class UnresolvedIdentifierException extends ParserWarning {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public UnresolvedIdentifierException() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getMessage() {
		return "There is no declaration of this identifier.";
	}
}
