package parser.analysis;

public class AmbiguousIdentifierException extends ParserWarning {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public AmbiguousIdentifierException() {
		super();
	}

	@Override
	public String getMessage() {
		return "There is more than one declaration of this identifier.";
	}
}
