package parser.analysis;

public class AnalysisException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public AnalysisException(Throwable cause) {
		super(cause);
	}

	public AnalysisException() {
		super();
	}
}
