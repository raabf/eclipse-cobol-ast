package parser;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.conqat.lib.scanner.IToken;

import parser.errorhandling.InvalidTokenMappingException;
import parser.tokenprovider.ITokenProvider;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import convex.step2.parser.Token;

public class CustomTokenManager extends AbstractCobolTokenManager {
	// set of tokens which are provided as identifiers, but are actually
	// keywords
	private static Set<String> falseIdentifiers = Sets.newHashSet(Arrays.asList("CONSOLE", "AUTOMATIC", "EXCLUSIVE",
			"MANUAL", "ROLLBACK", "COMMIT", "FLOAT", "FUNCTION", "INCLUDE", "WHERE", "DISTINCT", "AMMSC","APROXNUM", "AS", "ASC", "AUTHORIZATION", "BETWEEN", "CREATE", "CHECK", "COMPARISON", "CURRENT", "CURSOR", "DECIMAL", "DECLARE", "DEFAUL", "DESC", "DISTINCT", "DROP", "PRECISION", "EMPTY", "ESCAPE", "EXISTS", "FETCH", "FOREIGN", "GOTO", "GRANT", "HAVING", "INDICATOR", "INTEGER", "COMMACHAR", "INTNUM", "INSERT", "INTO", "LIKE", "OPTION", "PARAMETER", "PRIMARY", "PRIVILEGES", "PUBLIC", "REAL", "SCHEMA", "SMALLINT", "SOME", "SQL", "UNIQUE", "UNION", "UPDATE", "USER", "WHENEVER", "SQLERROR", "SQLWARNING", "FOUND", "CHAR"));


	private final ITokenProvider predecessor;
	private List<IToken> filteredTokens = Lists.newLinkedList();
	
	public CustomTokenManager(ITokenProvider predecessor) {
		this.predecessor = predecessor;
	}

	@Override
	public Token getNextToken() {
		try {
			return toToken(nextToken());
		} catch (IOException e) {
			// FIXME what to throw here?
			throw new RuntimeException(e);
		}
	}

	private Token toToken(final IToken itoken) {
		Token return_token = new ConqatToken(itoken);

		// columns are not calculated and set
		return_token.beginColumn = 0;
		return_token.beginLine = itoken.getLineNumber();
		return_token.endColumn = 0;
		return_token.endLine = itoken.getLineNumber();
		return_token.image = itoken.getText();
		// check if token is an actual token instead of an identifier
		if (falseIdentifiers.contains(itoken.getText().toUpperCase())) {
			return_token.kind = TerminalMapping.map.get(itoken.getText().toUpperCase());
		} else {
			// Throw a runtime exception if token mapping can't be found
			if (TerminalMapping.map.get(itoken.getType().name()) == null) {
				throw new InvalidTokenMappingException("Terminal " + itoken.getType().name() + " not known");
			}
			// check if in picture clause
			return_token.kind = TerminalMapping.map.get(itoken.getType().name());
		}
		return_token.next = null;// toNextToken(nextToken);
		return return_token;
	}

	@Override
	public IToken nextToken() throws IOException {
		return predecessor.nextToken();
	}

	@Override
	public List<IToken> getFilteredTokens() {
		return filteredTokens;
	}
}
