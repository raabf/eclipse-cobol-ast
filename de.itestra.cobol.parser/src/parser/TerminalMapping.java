package parser;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Map;

import convex.step2.parser.CobolParserConstants;


public class TerminalMapping {
	
	public static Map<String, Integer> map = new LinkedHashMap<String, Integer>();
	
	static {
		Class<?> c = CobolParserConstants.class;
		Field[] fields = c.getFields();
		
		for(Field f : fields){
			String name = f.getName();
			if(!f.getName().equals("tokenImage")){
				try {
					int val;
					val = f.getInt(f);
					map.put(name, val);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// if one of these exceptions happens, it makes no sense to continue
					e.printStackTrace();
					// exit 
					System.exit(-1);
				}
			}
		}
		
		//custom fixes
		map.put("INCLUDE", CobolParserConstants.INCLUDE);
		map.put("WHERE", CobolParserConstants.WHERE);
		map.put("AMMSC", CobolParserConstants.AMMSC);
		map.put("APROXNUM", CobolParserConstants.APROXNUM);
		map.put("AS", CobolParserConstants.AS);
		map.put("ASC", CobolParserConstants.ASC);
		map.put("AUTHORIZATION", CobolParserConstants.AUTHORIZATION);
		map.put("BETWEEN", CobolParserConstants.BETWEEN);
		map.put("CREATE", CobolParserConstants.CREATE); 
		map.put("CHECK", CobolParserConstants.CHECK);
		map.put("COMPARISON", CobolParserConstants.COMPARISON);
		map.put("CURRENT", CobolParserConstants.CURRENT);
		map.put("CURSOR", CobolParserConstants.CURSOR);
		map.put("DECIMAL", CobolParserConstants.DECIMAL);
		map.put("DECLARE", CobolParserConstants.DECLARE);
		map.put("DEFAUL", CobolParserConstants.DEFAUL);
		map.put("DESC", CobolParserConstants.DESC);
		map.put("DISTINCT", CobolParserConstants.DISTINCT);
		map.put("DROP", CobolParserConstants.DROP);
		map.put("PRECISION", CobolParserConstants.PRECISION);
		map.put("EMPTY", CobolParserConstants.EMPTY);
		map.put("ESCAPE", CobolParserConstants.ESCAPE);
		map.put("EXISTS", CobolParserConstants.EXISTS);
		map.put("FETCH", CobolParserConstants.FETCH);
		map.put("FLOAT", CobolParserConstants.FLOAT);
		map.put("FOREIGN", CobolParserConstants.FOREIGN);
		map.put("GOTO", CobolParserConstants.GOTO);
		map.put("GRANT", CobolParserConstants.GRANT);
		map.put("HAVING", CobolParserConstants.HAVING);
		map.put("INDICATOR", CobolParserConstants.INDICATOR);
		map.put("INTNUM", CobolParserConstants.INTNUM);
		map.put("INSERT", CobolParserConstants.INSERT);
		map.put("LIKE", CobolParserConstants.LIKE);
		map.put("OPTION", CobolParserConstants.OPTION);
		map.put("PARAMETER", CobolParserConstants.PARAMETER);
		map.put("PRIMARY", CobolParserConstants.PRIMARY);
		map.put("PRIVILEGES", CobolParserConstants.PRIVILEGES);
		map.put("PUBLIC", CobolParserConstants.PUBLIC);
		map.put("REAL", CobolParserConstants.REAL);
		map.put("SCHEMA", CobolParserConstants.SCHEMA);
		map.put("SMALLINT", CobolParserConstants.SMALLINT);
		map.put("SOME", CobolParserConstants.SOME);
		map.put("SQL", CobolParserConstants.SQL);
		map.put("UNION", CobolParserConstants.UNION);
		map.put("UNIQUE", CobolParserConstants.UNIQUE);
		map.put("UPDATE", CobolParserConstants.UPDATE);
		map.put("USER", CobolParserConstants.USER);
		map.put("VIEW", CobolParserConstants.VIEW);
		map.put("WHENEVER", CobolParserConstants.WHENEVER);
		map.put("FOUND", CobolParserConstants.FOUND);
		map.put("SQLERROR", CobolParserConstants.SQLERROR);
		map.put("sqlwarning", CobolParserConstants.SQLWARNING);
		map.put("IDENTIFIER", CobolParserConstants.COBOL_WORD);
		map.put("INTEGER", CobolParserConstants.INTEGER);
		map.put("INTO", CobolParserConstants.INTO);
		map.put("MULT", CobolParserConstants.ASTERISKCHAR);
		map.put("CHAR", CobolParserConstants.CHAR);
		map.put("DIV", CobolParserConstants.SLASHCHAR);
		map.put("PLUS", CobolParserConstants.PLUSCHAR);
		map.put("MINUS", CobolParserConstants.MINUSCHAR);
		map.put("EQ", CobolParserConstants.EQUALCHAR);
		map.put("GT", CobolParserConstants.MORETHANCHAR);
		map.put("GTEQ", CobolParserConstants.MORETHANOREQUAL);
		map.put("LT", CobolParserConstants.LESSTHANCHAR);
		map.put("LTEQ", CobolParserConstants.LESSTHANOREQUAL);
		map.put("STRING_LITERAL", CobolParserConstants.QUOTEDSTRING);
		map.put("INTEGER_LITERAL", CobolParserConstants.LEVEL_NUMBER);
		map.put("LPAREN", CobolParserConstants.LPARENCHAR);
		map.put("RPAREN", CobolParserConstants.RPARENCHAR);
		map.put("CONSOLE", CobolParserConstants.CONSOLE);
		map.put("AUTOMATIC", CobolParserConstants.AUTOMATIC);
		map.put("EXCLUSIVE", CobolParserConstants.EXCLUSIVE);
		map.put("MANUAL", CobolParserConstants.MANUAL);
		map.put("ROLLBACK", CobolParserConstants.ROLLBACK);
		map.put("LOCAL_STORAGE", CobolParserConstants.LOCAL_STORAGE);
		map.put("COLON", CobolParserConstants.COLONCHAR);
		//these terminals are delivered by ConQAT, but should rather be COBOL_WORDs
		map.put("FUNCTION", CobolParserConstants.COBOL_WORD);
		map.put("EC", CobolParserConstants.COBOL_WORD);
		map.put("FORMAT", CobolParserConstants.COBOL_WORD);
		//this is kind of nasty...
		map.put("FLOATING_POINT_LITERAL", CobolParserConstants.COBOL_WORD);
	}
}
