package parser.errorhandling;

public interface IErrorHandler {
	
	/*
	 * Callback for recoverable errors.
	 */
	public void onParseError(int line, int column, int offset, String message);
	
	/*
	 * Callback for non-recoverable errors (parsing failed completely).
	 */
	public void onParsingFailed(int line, int column, int offset, String message);
	
}
