package parser.errorhandling;

import convex.step2.parser.ParseException;
import convex.step2.parser.SimpleNode;

public interface ILowLevelErrorHandler {

	/*
	 * Callback for low-level parser errors. Takes a javaCC ParseException. Note
	 * that the exception is not thrown but only used for data retrieval!
	 */
	public void onError(ParseException e);

	public void onWarning(SimpleNode node, Message warning);
}
