package parser.errorhandling;

public class InvalidTokenMappingException extends RuntimeException {

	public InvalidTokenMappingException(){
		super();
	}
	
	public InvalidTokenMappingException(String message){
		super(message);
	}
}
