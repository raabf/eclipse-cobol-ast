package parser.errorhandling;

public class ExceptionMessage implements Message {
	private final Throwable throwable;

	public ExceptionMessage(Throwable t) {
		throwable = t;
	}

	@Override
	public String getMessage() {
		return throwable.getMessage();
	}
}
