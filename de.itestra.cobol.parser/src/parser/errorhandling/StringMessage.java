package parser.errorhandling;

public class StringMessage implements Message {
	private final String message;

	public StringMessage(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
