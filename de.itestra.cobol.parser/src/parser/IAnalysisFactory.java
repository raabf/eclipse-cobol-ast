package parser;

import parser.analysis.base.structure.IStructureAnalysis;
import parser.analysis.base.variables.IVariableAnalysis;

public interface IAnalysisFactory {

	/*
	 * All analysis regarding code structure.
	 */
	public IStructureAnalysis<?> getStructureAnalysis();
	
	/*
	 * All analysis regarding variables.
	 */
	public IVariableAnalysis<?> getVariableAnalysis();
}
