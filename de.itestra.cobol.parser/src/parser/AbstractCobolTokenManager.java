package parser;

import convex.step2.parser.CobolParserTokenManager;

public abstract class AbstractCobolTokenManager extends CobolParserTokenManager implements ITokenManager {
	public AbstractCobolTokenManager() {
		super(null);
	}
}
