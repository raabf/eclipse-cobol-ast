package parser;
import java.io.File;
import java.io.IOException;
import java.util.List;

import org.conqat.lib.scanner.IToken;

import parser.errorhandling.IErrorHandler;
import convex.step2.parser.ParseException;


public interface IParser {
	
	/*
	 * Returns list of filtered tokens.
	 * Filtered tokens do not contain tokens ignored during parsing phase.
	 */
	public List<IToken> getFilteredTokens();
	
	/*
	 * Returns an analysis factory.
	 * The analysis factory provides an interface to several analysis.
	 */
	public IAnalysisFactory getAnalysisFactory();
	
	/*
	 * Trigger parsing.
	 */
	public void parse() throws ParseException;
	
	/*
	 * Trigger parsing on given string.
	 */
	public void reparse(String s) throws ParseException, IOException;
	
	/*
	 * Trigger scanner manually without parsing.
	 */
	public void rescan(String s) throws IOException;
	public void rescan(File f) throws IOException;
	
	/*
	 * Register error handler for all kind of parser errors.
	 */
	public void addErrorHandler(IErrorHandler handler);
	public void removeErrorHandler(IErrorHandler handler);
}
