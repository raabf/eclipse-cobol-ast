package parser.analysis.cobol;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import parser.analysis.cobol.structure.representation.CobolProcedureSectionRepresentation;
import parser.analysis.cobol.structure.representation.CobolProgramRepresentation;
import parser.analysis.cobol.structure.representation.DefaultCobolStructureRepresentationVisitor;
import convex.step2.parser.ParseException;

public class StructureRepresentationVisitorTest extends AbstractStructureAnalysisTest {
	private DeclarationVisitor visitor;
	
	@Override
	@Before
	public void setup() {
		super.setup();
		visitor = new DeclarationVisitor();
	}
	
	@Test
	public void testStructureRepresentationVisitorOnCOBSMO01() throws IOException, ParseException {
		analyzeFromClasspath("COBSMO01.cob");
		visit();
		assertDeclarations(new String[] { "COBSMO01", "STEUERUNG", "EINGABEN-PRUEFEN", "DATEN-VERARBEITEN",
				"SEL-AVTUE-ZIV", "INS-AVTUE-ZIV", "UPD-AVTUE-ZIV", "DATEIEN-SCHLIESSEN" });
	}

	@Test
	public void testStructureRepresentationVisitorOnCOBSMO02() throws IOException, ParseException {
		analyzeFromClasspath("COBSMO02.cob");
		visit();
		assertDeclarations(new String[] { "COBSMO02", "VERSIONS-KONTROLLE" });
	}
	
	private void visit() {
		getStructure().accept(visitor);
	}
	
	private void assertDeclarations(String... declarationNames) {
		Assert.assertEquals(declarationNames.length, visitor.declarations.size());
		Assert.assertArrayEquals(declarationNames, visitor.declarations.toArray(new String[declarationNames.length]));
	}

	private static final class DeclarationVisitor extends DefaultCobolStructureRepresentationVisitor {
		private List<String> declarations = new ArrayList<String>();

		@Override
		public void visit(CobolProgramRepresentation representation) {
			declarations.add(representation.getName());
			super.visit(representation);
		}
		
		@Override
		public void visit(CobolProcedureSectionRepresentation representation) {
			declarations.add(representation.getName());
			super.visit(representation);
		}
	}
}
