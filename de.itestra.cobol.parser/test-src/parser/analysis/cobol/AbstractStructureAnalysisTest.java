package parser.analysis.cobol;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.conqat.lib.scanner.ELanguage;
import org.conqat.lib.scanner.ILenientScanner;
import org.conqat.lib.scanner.ScannerFactory;
import org.junit.Assert;
import org.junit.Before;

import parser.CustomTokenManager;
import parser.analysis.base.definition.representation.UnresolvedControlflowCallableDefinitionRepresentation;
import parser.analysis.base.structure.representation.IStructureRepresentation;
import parser.analysis.cobol.structure.CobolStructureAnalysis;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentation;
import parser.analysis.cobol.structure.representation.impl.DataDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.EnvironmentDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.FieldDeclarationRepresentation;
import parser.analysis.cobol.structure.representation.impl.IdentificationDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.ParagraphRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProcedureDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProgramRepresentation;
import parser.analysis.cobol.structure.representation.impl.SectionRepresentation;
import parser.tokenprovider.ExcludeCopybooksTokenProvider;
import parser.tokenprovider.ITokenProvider;
import parser.tokenprovider.filter.AmbigiousTokenFilter;
import parser.tokenprovider.filter.ExecSQLFilter;
import parser.tokenprovider.filter.IgnoredTokensFilter;
import parser.tokenprovider.filter.SpecialTokenHandler;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import convex.step2.parser.CobolParser;
import convex.step2.parser.CobolParserTokenManager;
import convex.step2.parser.ParseException;
import de.itestra.swgcommons.preprocessing.OnTheFlyPreprocessingFilter;

public class AbstractStructureAnalysisTest {
	// Divisions
	protected static final ClassAndName IDENTIFICATION_DIVISION = classAndName(
			IdentificationDivisionRepresentation.class,
			"IDENTIFICATION DIVISION");
	protected static final ClassAndName ENVIRONMENT_DIVISION = classAndName(
			EnvironmentDivisionRepresentation.class, "ENVIRONMENT DIVISION");
	protected static final ClassAndName DATA_DIVISION = classAndName(
			DataDivisionRepresentation.class, "DATA DIVISION");
	protected static final ClassAndName PROCEDURE_DIVISION = classAndName(
			ProcedureDivisionRepresentation.class, "PROCEDURE DIVISION");

	// Sections
	protected static final ClassAndName CONFIGURATION_SECTION = classAndName(
			SectionRepresentation.class, "CONFIGURATION SECTION");
	protected static final ClassAndName WORKING_STORAGE_SECTION = classAndName(
			SectionRepresentation.class, "WORKING-STORAGE SECTION");
	protected static final ClassAndName LINKAGE_SECTION = classAndName(
			SectionRepresentation.class, "LINKAGE SECTION");

	protected static final ClassAndName DECLARATIVES = classAndName(
			SectionRepresentation.class, "DECLARATIVES");

	private CobolStructureAnalysis analysis;

	@Before
	public void setup() {
		BasicConfigurator.configure();
		analysis = new CobolStructureAnalysis();
	}

	protected final CobolStructureAnalysis getAnalysis() {
		return analysis;
	}

	protected final CobolStructureRepresentation getStructure() {
		return analysis.getProgramStructure();
	}

	protected final void analyzeFromClasspath(String filename)
			throws IOException, ParseException {
		CobolParser parser = new CobolParser(tokenManager(
				classpathReader(filename), filename));

		parser.CompilationUnit();
		getAnalysis().run(parser.getRootNode());
	}

	private final Reader classpathReader(String filename) throws IOException {
		OnTheFlyPreprocessingFilter result = new OnTheFlyPreprocessingFilter();

		result.setMinRelevantColumn(6);
		result.setMaxRelevantColumn(71);
		// result.setExtraColumnBehaviour(ExtraColumnBehaviour.blank);
		result.reset(getClass().getResourceAsStream("/" + filename));

		return result;
	}

	private static CobolParserTokenManager tokenManager(Reader reader,
			String originId) throws IOException {
		ILenientScanner scanner = ScannerFactory.newLenientScanner(
				ELanguage.COBOL, reader, originId);
		ITokenProvider filterChain = new ExcludeCopybooksTokenProvider(scanner);
		filterChain = new ExecSQLFilter(filterChain);
		filterChain = new IgnoredTokensFilter(filterChain);
		filterChain = new AmbigiousTokenFilter(filterChain);
		filterChain = new SpecialTokenHandler(filterChain);
		return new CustomTokenManager(filterChain);
	}

	protected final static void assertSubStructures(
			IStructureRepresentation representation, ClassAndName... structures) {
		List<? extends IStructureRepresentation> children = representation
				.getChildren();
		assertStructures(children, structures);
	}

	protected static void assertStructures(
			List<? extends IStructureRepresentation> children,
			ClassAndName... structures) {
		// to avoid having to check for each statement, we exclude some of them
		// here which of course reduces covered code but we'll notice errors in
		// detection of those statements in other tests (such as
		// DataflowAnalysisTest)
		List<? extends IStructureRepresentation> filtered = filterStructures(children);
		Assert.assertEquals(structures.length, filtered.size());
		for (int i = 0; i < structures.length; i++) {
			IStructureRepresentation child = filtered.get(i);
			// FIXME unresolveable
			// Assert.assertThat(child, Is.is(structures[i].clazz));
			Assert.assertEquals(structures[i].name, child.getName());
		}
	}

	protected static List<? extends IStructureRepresentation> filterStructures(
			List<? extends IStructureRepresentation> children) {
		List<? extends IStructureRepresentation> filtered = Lists
				.newArrayList(Iterables.filter(children,
						new Predicate<IStructureRepresentation>() {
					@Override
					public boolean apply(
							IStructureRepresentation element) {
						Class<? extends IStructureRepresentation> clazz = element
								.getClass();
						return !clazz
								.isAssignableFrom(FieldDeclarationRepresentation.class);
					}
				}));
		return filtered;
	}

	protected void printStructure() {
		printStructure(getStructure(), "");
	}

	private void printStructure(IStructureRepresentation representation,
			String prefix) {
		System.out.println(prefix + representation.getName() + ": "
				+ representation.getClass().getSimpleName());

		for (IStructureRepresentation child : representation.getChildren()) {
			printStructure(child, prefix + "  ");
		}
	}

	protected static ClassAndName section(String name) {
		return classAndName(SectionRepresentation.class, name);
	}

	protected static ClassAndName paragraph(String name) {
		return classAndName(ParagraphRepresentation.class, name);
	}

	protected static ClassAndName program(String name) {
		return classAndName(ProgramRepresentation.class, name);
	}

	protected static ClassAndName variableDeclaration(String name) {
		return classAndName(FieldDeclarationRepresentation.class, name);
	}

	protected static ClassAndName unresolved(String name) {
		return classAndName(UnresolvedControlflowCallableDefinitionRepresentation.class, name);
	}

	protected static ClassAndName classAndName(Class<?> clazz, String name) {
		return new ClassAndName(clazz, name);
	}

	protected static final class ClassAndName {
		protected final String name;
		protected final Class<?> clazz;

		private ClassAndName(Class<?> clazz, String name) {
			this.clazz = clazz;
			this.name = name;
		}
	}
}
