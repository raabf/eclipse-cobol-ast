package parser.analysis.cobol;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import parser.analysis.cobol.controlflow.graph.CobolCallableControlflowProgramPart;
import parser.analysis.cobol.controlflow.graph.CobolControlflowEdge;
import parser.analysis.cobol.controlflow.graph.CobolControlflowIfNode;
import parser.analysis.cobol.controlflow.graph.CobolControlflowNode;
import parser.analysis.cobol.controlflow.graph.CobolControlflowProgramPart;
import parser.analysis.cobol.controlflow.graph.CobolControlflowUnreachableNodes;
import parser.analysis.cobol.controlflow.graph.CobolProgramEndNode;
import parser.analysis.cobol.controlflow.graph.CobolProgramNode;
import parser.analysis.cobol.controlflow.graph.visitors.CobolControlflowAnalysis;
import parser.analysis.cobol.structure.representation.impl.AbstractCobolStructureRepresentation;

import com.google.common.collect.Lists;

import convex.step2.parser.ParseException;

public class ControlflowAnalysisTest extends AbstractStructureAnalysisTest {
	@Test
	public void testGoto() throws IOException, ParseException {
		analyzeFromClasspath("controlflow/goto.cob");

		CobolControlflowAnalysis analyis = new CobolControlflowAnalysis();
		analyis.run(getAbstractStructure());

		Collection<CobolControlflowNode> unreachable = new CobolControlflowUnreachableNodes()
				.findUnreachableNodes(analyis);
		Assert.assertEquals(2, unreachable.size());
	}

	@Test
	public void testReachabilityAnalysis() throws IOException, ParseException {
		analyzeFromClasspath("controlflow/reachability.cob");

		CobolControlflowAnalysis analyis = new CobolControlflowAnalysis();
		analyis.run(getAbstractStructure());

		Collection<CobolControlflowNode> unreachable = new CobolControlflowUnreachableNodes()
				.findUnreachableNodes(analyis);
		Assert.assertEquals(6, unreachable.size());
	}

	@Test
	public void testEmptyProgram() throws IOException, ParseException {
		List<CobolControlflowAssertionNode> expected = Lists.newLinkedList();

		// PROGRAM
		expected.add(new CobolControlflowAssertionNode(CobolProgramNode.class, 1));
		// PROGRAM-END
		expected.add(new CobolControlflowAssertionNode(CobolProgramEndNode.class, 0));

		assertControlflow("controlflow/empty.cob", expected);
	}

	@Test
	public void testSingleSection() throws IOException, ParseException {
		List<CobolControlflowAssertionNode> expected = Lists.newLinkedList();

		// PROGRAM
		expected.add(new CobolControlflowAssertionNode(CobolProgramNode.class, 1));
		// SECTION1
		expected.add(new CobolControlflowAssertionNode(CobolCallableControlflowProgramPart.class, 1));
		// SECTION1-END
		expected.add(new CobolControlflowAssertionNode(CobolControlflowNode.class, 1));
		// PROGRAM-END
		expected.add(new CobolControlflowAssertionNode(CobolProgramEndNode.class, 0));

		assertControlflow("controlflow/singleSection.cob", expected);
	}

	@Test
	public void testIfSection() throws IOException, ParseException {
		List<CobolControlflowAssertionNode> expected = Lists.newLinkedList();

		// PROGRAM
		expected.add(new CobolControlflowAssertionNode(CobolProgramNode.class, 1));
		// IF
		expected.add(new CobolControlflowAssertionNode(CobolControlflowIfNode.class, 2));
		// THEN
		expected.add(new CobolControlflowAssertionNode(CobolControlflowProgramPart.class, 1));
		// THEN-END
		expected.add(new CobolControlflowAssertionNode(CobolControlflowNode.class, 1));
		// END-IF
		expected.add(new CobolControlflowAssertionNode(CobolControlflowNode.class, 1));
		// PROGRAM-END
		expected.add(new CobolControlflowAssertionNode(CobolProgramEndNode.class, 0));
		// ELSE
		expected.add(new CobolControlflowAssertionNode(CobolControlflowProgramPart.class, 1));
		// ELSE-END
		expected.add(new CobolControlflowAssertionNode(CobolControlflowNode.class, 1));
		// END-IF
		expected.add(new CobolControlflowAssertionNode(CobolControlflowNode.class, 1));

		assertControlflow("controlflow/if.cob", expected);
	}

	// @Test
	// public void printTest() throws IOException, ParseException {
	// analyzeFromClasspath("COBSMO01.cob");
	//
	// CobolControlflowAnalysis analysis = new CobolControlflowAnalysis();
	// analysis.run(getAbstractStructure());
	//
	// dfs(analysis.getProgramPart());
	// }

	private void assertControlflow(String filename, List<CobolControlflowAssertionNode> expected) throws IOException,
			ParseException {
		analyzeFromClasspath(filename);

		CobolControlflowAnalysis analysis = new CobolControlflowAnalysis();
		analysis.run(getAbstractStructure());

		List<CobolControlflowNode> actual = dfs(analysis.getProgramPart());

		Assert.assertEquals(expected.size(), actual.size());

		for (int i = 0; i < expected.size(); i++) {
			assertNodesEqual(expected.get(i), actual.get(i));
		}
	}

	private void assertNodesEqual(CobolControlflowAssertionNode expected, CobolControlflowNode actual) {
		Assert.assertEquals(expected.clazz, actual.getClass());
		Assert.assertEquals(expected.edgeCount, actual.getEdges().size());
	}

	private List<CobolControlflowNode> dfs(CobolControlflowNode node) {
		return dfs(node, Lists.newLinkedList());
	}

	private List<CobolControlflowNode> dfs(CobolControlflowNode node, List<CobolControlflowEdge> edges) {
		List<CobolControlflowNode> result = Lists.newLinkedList();

		result.add(node);

		for (CobolControlflowEdge edge : node.getEdges()) {
			System.out.println(edge);
			if (!edges.contains(edge)) {
				edges.add(edge);
				List<CobolControlflowNode> dfs = dfs(edge.getNextNode(), edges);
				result.addAll(dfs);
			}
		}

		return result;
	}

	private AbstractCobolStructureRepresentation getAbstractStructure() {
		return (AbstractCobolStructureRepresentation) super.getStructure();
	}

	private static class CobolControlflowAssertionNode {
		private final Class<?> clazz;
		private final int edgeCount;

		public CobolControlflowAssertionNode(Class<? extends CobolControlflowNode> clazz, int edgeCount) {
			this.clazz = clazz;
			this.edgeCount = edgeCount;
		}
	}
}
