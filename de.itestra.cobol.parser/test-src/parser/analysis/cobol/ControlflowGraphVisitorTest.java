package parser.analysis.cobol;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;

import org.junit.Assert;
import org.junit.Test;

import parser.analysis.cobol.controlflow.graph.CobolCallableControlflowProgramPart;
import parser.analysis.cobol.controlflow.graph.CobolControlflowEdge;
import parser.analysis.cobol.controlflow.graph.CobolControlflowNode;
import parser.analysis.cobol.controlflow.graph.CobolProgramEndNode;
import parser.analysis.cobol.controlflow.graph.CobolProgramNode;
import parser.analysis.cobol.controlflow.graph.visitors.CobolControlflowAnalysis;
import parser.analysis.cobol.structure.representation.impl.AbstractCobolStructureRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProgramRepresentation;
import convex.step2.parser.ParseException;

public class ControlflowGraphVisitorTest extends AbstractStructureAnalysisTest {

	@Test
	public void testControllflowAnalysis() {
		CobolControlflowAnalysis dummyAnalysis = new CobolControlflowAnalysis();
		CobolControlflowAnalysis tester = new CobolControlflowAnalysis();

		try {
			analyzeFromClasspath("stuff(1).cob");

			// build wanted Graph
			ProgramRepresentation programStructure = getAnalysis().getProgramStructure();
			AbstractCobolStructureRepresentation cobolStructure = programStructure;
			CobolProgramNode programNode = new CobolProgramNode(dummyAnalysis, cobolStructure);
			List<AbstractCobolStructureRepresentation> children = cobolStructure.getChildren();
			AbstractCobolStructureRepresentation procedureDivision = null;
			for (AbstractCobolStructureRepresentation child : children) {
				if (child.getName().equals("PROCEDURE DIVISION")) {
					procedureDivision = child;
					break;
				}
			}
			AbstractCobolStructureRepresentation stuffSection = null;
			for (AbstractCobolStructureRepresentation child : procedureDivision.getChildren()) {
				if (child.getName().equals("STUFF")) {
					stuffSection = child;
					break;
				}
			}
			CobolCallableControlflowProgramPart stuffSectionNode = new CobolCallableControlflowProgramPart(
					dummyAnalysis, stuffSection);
			programNode.controlflow(stuffSectionNode);
			CobolControlflowNode moveNode = new CobolControlflowNode(dummyAnalysis);
			stuffSectionNode.controlflow(moveNode);
			CobolControlflowNode computeNode = new CobolControlflowNode(dummyAnalysis);
			moveNode.controlflow(computeNode);
			CobolControlflowNode finishNode = new CobolControlflowNode(dummyAnalysis);
			computeNode.controlflow(finishNode);
			AbstractCobolStructureRepresentation stuff2Section = null;
			for (AbstractCobolStructureRepresentation child : procedureDivision.getChildren()) {
				if (child.getName().equals("STUFF2")) {
					stuff2Section = child;
					break;
				}
			}
			CobolCallableControlflowProgramPart stuff2SectionNode = new CobolCallableControlflowProgramPart(
					dummyAnalysis, stuff2Section);
			finishNode.controlflow(stuff2SectionNode);
			CobolControlflowNode compute2Node = new CobolControlflowNode(dummyAnalysis);
			stuff2SectionNode.controlflow(compute2Node);
			CobolControlflowNode finish2Node = new CobolControlflowNode(dummyAnalysis);
			compute2Node.controlflow(finish2Node);
			CobolProgramEndNode endNode = new CobolProgramEndNode(dummyAnalysis);
			finish2Node.controlflow(endNode);
			System.out.println("Graph done");
			// wanted Graph done

			// get graph of program
			tester.run(programStructure);
			CobolProgramNode testerProgramPart = tester.getProgramPart();

			List<CobolControlflowEdge> expectedEdges = programNode.getEdges();
			List<CobolControlflowEdge> actualEdges = testerProgramPart.getEdges();
			Assert.assertEquals("wrong number of Edges", expectedEdges.size(), actualEdges.size());

			// Building Queue for nodes and number of edges of each node(for
			// exepted and actual graph
			// expectedChildrenQueue and actualChildrenQueue add all the new
			// children if not added yet
			// expectedQueue and actualQueue get every node of the graph in
			// breadth-first-order
			Queue<CobolControlflowNode> expectedChildrenQueue = new ArrayDeque<CobolControlflowNode>();
			for (int i = 0; i < expectedEdges.size(); i++) {
				expectedChildrenQueue.add(expectedEdges.get(i).getNextNode());
			}
			Queue<CobolControlflowNode> actualChildrenQueue = new ArrayDeque<CobolControlflowNode>();
			for (int i = 0; i < actualEdges.size(); i++) {
				actualChildrenQueue.add(actualEdges.get(i).getNextNode());
			}

			Queue<CobolControlflowNode> expectedQueue = new ArrayDeque<CobolControlflowNode>();
			Queue<Integer> expectedNumberOfEdges = new ArrayDeque<Integer>();
			while (!expectedChildrenQueue.isEmpty()) {
				CobolControlflowNode firstNode = expectedChildrenQueue.remove();
				expectedQueue.add(firstNode);
				expectedNumberOfEdges.add(firstNode.getEdges().size());
				for (int i = 0; i < firstNode.getEdges().size(); i++) {
					CobolControlflowNode nextNode = firstNode.getEdges().get(i).getNextNode();
					if (!expectedQueue.contains(nextNode) && !expectedChildrenQueue.contains(nextNode)) {
						expectedChildrenQueue.add(nextNode);
					}
				}
			}

			Queue<CobolControlflowNode> actualQueue = new ArrayDeque<CobolControlflowNode>();
			Queue<Integer> actualNumberOfEdges = new ArrayDeque<Integer>();
			while (!actualChildrenQueue.isEmpty()) {
				CobolControlflowNode firstNode = actualChildrenQueue.remove();
				actualQueue.add(firstNode);
				actualNumberOfEdges.add(firstNode.getEdges().size());
				for (int i = 0; i < firstNode.getEdges().size(); i++) {
					CobolControlflowNode nextNode = firstNode.getEdges().get(i).getNextNode();
					if (!actualQueue.contains(nextNode) && !actualChildrenQueue.contains(nextNode)) {
						actualChildrenQueue.add(nextNode);
					}
				}
			}

			// compare number of edges
			Assert.assertEquals("different number of nodes", expectedNumberOfEdges.size(), actualNumberOfEdges.size());
			int size = expectedNumberOfEdges.size();
			for (int i = 0; i < size; i++) {
				int expected = expectedNumberOfEdges.remove();
				int actual = actualNumberOfEdges.remove();
				Assert.assertEquals("different number of edges", expected, actual);
			}
			// compare classes of nodes
			Assert.assertEquals("different number of nodes", expectedQueue.size(), actualQueue.size());
			size = expectedQueue.size();
			for (int i = 0; i < size; i++) {
				Assert.assertEquals("node class wrong", expectedQueue.remove().getClass(), actualQueue.remove()
						.getClass());
			}

		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}