package parser.analysis.cobol;

import java.util.List;

import parser.analysis.base.structure.representation.IStructureRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentation;

public class StructureAnalysisTest extends AbstractStructureAnalysisTest {

	protected static void assertCOBSMO01(CobolStructureRepresentation structure) {
		// FIXME unresolveable
		// Assert.assertThat(structure, Is.is(ProgramRepresentation.class));

		assertSubStructures(structure, IDENTIFICATION_DIVISION,
				ENVIRONMENT_DIVISION, DATA_DIVISION, PROCEDURE_DIVISION);

		List<? extends IStructureRepresentation> divisions = structure
				.getChildren();
		IStructureRepresentation division = divisions.get(0);
		assertSubStructures(division);

		division = divisions.get(1);
		assertSubStructures(division, CONFIGURATION_SECTION);

		IStructureRepresentation section = division.getChildren().get(0);
		assertSubStructures(section, paragraph("SOURCE-COMPUTER"),
				paragraph("OBJECT-COMPUTER"), paragraph("SPECIAL-NAMES"));

		division = divisions.get(2);
		assertSubStructures(division, WORKING_STORAGE_SECTION, LINKAGE_SECTION);

		division = divisions.get(3);
		assertSubStructures(division, section("STEUERUNG"),
				section("EINGABEN-PRUEFEN"), section("DATEN-VERARBEITEN"),
				section("SEL-AVTUE-ZIV"), section("INS-AVTUE-ZIV"),
				section("UPD-AVTUE-ZIV"), section("DATEIEN-SCHLIESSEN"));
		section = division.getChildren().get(0);
		assertSubStructures(section, paragraph("STEU-START"),
				paragraph("STEU-UPGM-ENDE"), paragraph("STEU-ENDE"));
		section = division.getChildren().get(1);
		assertSubStructures(section, paragraph("EING-PRUEF-START"),
				paragraph("EING-PRUEF-ENDE"));
		section = division.getChildren().get(2);
		assertSubStructures(section, paragraph("DATEN-VERARB-START"),
				paragraph("DATEN-VERARB-ENDE"));
		section = division.getChildren().get(3);
		assertSubStructures(section, paragraph("SEL-AVTUE-ZIV-START"),
				paragraph("SEL-AVTUE-ZIV-ENDE"));
		section = division.getChildren().get(4);
		assertSubStructures(section, paragraph("INS-AVTUE-ZIV-START"),
				paragraph("INS-AVTUE-ZIV-ENDE"));
		section = division.getChildren().get(5);
		assertSubStructures(section, paragraph("UPD-AVTUE-ZIV-START"),
				paragraph("UPD-AVTUE-ZIV-ENDE"));
		section = division.getChildren().get(6);
		assertSubStructures(section, paragraph("DSS-START"),
				paragraph("DSS-ENDE"));
	}

	protected static void assertCOBSMO02(CobolStructureRepresentation structure) {
		// FIXME unresolveable
		// Assert.assertThat(structure, Is.is(ProgramRepresentation.class));

		assertSubStructures(structure, IDENTIFICATION_DIVISION,
				ENVIRONMENT_DIVISION, DATA_DIVISION, PROCEDURE_DIVISION);

		List<? extends IStructureRepresentation> divisions = structure
				.getChildren();
		IStructureRepresentation division = divisions.get(0);
		assertSubStructures(division);

		division = divisions.get(1);
		assertSubStructures(division, CONFIGURATION_SECTION);

		IStructureRepresentation section = division.getChildren().get(0);
		assertSubStructures(section, paragraph("SOURCE-COMPUTER"),
				paragraph("OBJECT-COMPUTER"), paragraph("SPECIAL-NAMES"));

		division = divisions.get(2);
		assertSubStructures(division, WORKING_STORAGE_SECTION, LINKAGE_SECTION);

		division = divisions.get(3);
		assertSubStructures(division, paragraph("VERSION-PRUEFEN"),
				paragraph("EINGABE-PRUEFEN"),
				paragraph("ERMITTELN-ABTEILWERT"),
				paragraph("ERMITTELN-BENZINVERBRAUCH"),
				paragraph("ERMITTELN-AUFRUFNUMMER"),
				paragraph("ERMITTELN-AUSGABEKOSTEN"),
				paragraph("ERMITTELN-SITZPLATZ"),
				paragraph("ERMITTELN-HOSENRISS"),
				paragraph("ERMITTELN-HOSENRISS-1"),
				paragraph("ERMITT-WIANLKUCHEN"), paragraph("FEHLER-PAAM"),
				paragraph("ENDE"), section("VERSIONS-KONTROLLE"));

		section = filterStructures(division.getChildren()).get(12);
		assertSubStructures(section, paragraph("VERSIONS-KONTROLLE-START"),
				paragraph("VERSIONS-KONTROLLE-ENDE"));
	}
}
