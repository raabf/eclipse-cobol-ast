package parser.analysis.cobol;

import java.io.IOException;
import java.io.Reader;

import org.apache.log4j.BasicConfigurator;
import org.conqat.lib.scanner.ELanguage;
import org.conqat.lib.scanner.ILenientScanner;
import org.conqat.lib.scanner.ScannerFactory;
import org.junit.Before;
import org.junit.Test;

import parser.CustomTokenManager;
import parser.tokenprovider.ITokenProvider;
import parser.tokenprovider.IncludeCopybooksTokenProvider;
import parser.tokenprovider.filter.AmbigiousTokenFilter;
import parser.tokenprovider.filter.ExecSQLFilter;
import parser.tokenprovider.filter.IgnoredTokensFilter;
import parser.tokenprovider.filter.SpecialTokenHandler;
import convex.step2.parser.CobolParser;
import convex.step2.parser.CobolParserTokenManager;
import convex.step2.parser.ParseException;
import de.itestra.swgcommons.preprocessing.OnTheFlyPreprocessingFilter;

public class CopybookInclusionTest {
	@Before
	public void setup() {
		BasicConfigurator.configure();
	}
	
	@Test 
	public void test() throws IOException, ParseException{
		analyzeFromClasspath("CPYTEST.cob");
	}

	protected final void analyzeFromClasspath(String filename) throws IOException, ParseException {
		CobolParser parser = new CobolParser(tokenManager(classpathReader(filename), filename));
	
		parser.CompilationUnit();
	}

	private final Reader classpathReader(String filename) throws IOException {
		OnTheFlyPreprocessingFilter result = new OnTheFlyPreprocessingFilter();

		result.setMinRelevantColumn(6);
		result.setMaxRelevantColumn(71);
		result.reset(getClass().getResourceAsStream("/" + filename));

		return result;
	}
	
	private static CobolParserTokenManager tokenManager(Reader reader, String originId) throws IOException {
		ILenientScanner scanner = ScannerFactory.newLenientScanner(ELanguage.COBOL, reader, originId);
		ITokenProvider filterChain = new IncludeCopybooksTokenProvider(scanner, new ClasspathCopybookProvider());
		filterChain = new ExecSQLFilter(filterChain);
		filterChain = new IgnoredTokensFilter(filterChain);
		filterChain = new AmbigiousTokenFilter(filterChain);
		filterChain = new SpecialTokenHandler(filterChain);
		return new CustomTokenManager(filterChain);
	}
}
