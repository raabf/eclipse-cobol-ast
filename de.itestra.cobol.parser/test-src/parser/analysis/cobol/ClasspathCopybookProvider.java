package parser.analysis.cobol;

import java.io.IOException;

import org.conqat.lib.scanner.ELanguage;
import org.conqat.lib.scanner.ILenientScanner;
import org.conqat.lib.scanner.ScannerFactory;

import parser.tokenprovider.CopybookResolver;
import de.itestra.swgcommons.preprocessing.OnTheFlyPreprocessingFilter;

public class ClasspathCopybookProvider implements CopybookResolver {
	@Override
	public ILenientScanner resolve(String scope, String name) throws IOException {
		OnTheFlyPreprocessingFilter result = new OnTheFlyPreprocessingFilter();

		result.setMinRelevantColumn(6);
		result.setMaxRelevantColumn(71);
		result.reset(getClass().getResourceAsStream("/" + name + ".cpy"));

		return ScannerFactory.newLenientScanner(ELanguage.COBOL, result, name);
	}

}
