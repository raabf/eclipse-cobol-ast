package parser.analysis.cobol;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import parser.analysis.base.definition.representation.ICallRepresentation;
import parser.analysis.base.definition.representation.ICallableDefinitionRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentation;
import parser.errorhandling.ILowLevelErrorHandler;
import parser.errorhandling.Message;

import com.google.common.collect.Lists;

import convex.step2.parser.ParseException;
import convex.step2.parser.SimpleNode;

public class WorkspaceTest extends StructureAnalysisTest {
	@Test
	public void testCommentFilter() throws IOException, ParseException {
		analyzeFromClasspath("dckonv2.cbl");
		assertStructures(getAnalysis().getProgramStructureFlat(), program("DCKONV"), IDENTIFICATION_DIVISION,
				DATA_DIVISION, WORKING_STORAGE_SECTION, PROCEDURE_DIVISION, paragraph("STARTA"), paragraph("PARA"),
				paragraph("ANOTHER-PARA"));
	}

	@Test
	public void testStructureAnalysisCOBSMO01() throws IOException, ParseException {
		analyzeFromClasspath("COBSMO01.cob");
		assertStructures(getAnalysis().getProgramStructureFlat(), program("COBSMO01"), IDENTIFICATION_DIVISION,
				ENVIRONMENT_DIVISION, CONFIGURATION_SECTION, paragraph("SOURCE-COMPUTER"),
				paragraph("OBJECT-COMPUTER"), paragraph("SPECIAL-NAMES"), DATA_DIVISION, WORKING_STORAGE_SECTION,
				LINKAGE_SECTION, PROCEDURE_DIVISION, section("STEUERUNG"), paragraph("STEU-START"),
				paragraph("STEU-UPGM-ENDE"), paragraph("STEU-ENDE"), section("EINGABEN-PRUEFEN"),
				paragraph("EING-PRUEF-START"), paragraph("EING-PRUEF-ENDE"), section("DATEN-VERARBEITEN"),
				paragraph("DATEN-VERARB-START"), paragraph("DATEN-VERARB-ENDE"), section("SEL-AVTUE-ZIV"),
				paragraph("SEL-AVTUE-ZIV-START"), paragraph("SEL-AVTUE-ZIV-ENDE"), section("INS-AVTUE-ZIV"),
				paragraph("INS-AVTUE-ZIV-START"), paragraph("INS-AVTUE-ZIV-ENDE"), section("UPD-AVTUE-ZIV"),
				paragraph("UPD-AVTUE-ZIV-START"), paragraph("UPD-AVTUE-ZIV-ENDE"), section("DATEIEN-SCHLIESSEN"),
				paragraph("DSS-START"), paragraph("DSS-ENDE"));
		assertCOBSMO01(getStructure());
	}

	@Test
	public void testStructureAnalysisCOBSMO02() throws IOException, ParseException {
		analyzeFromClasspath("COBSMO02.cob");
		assertStructures(getAnalysis().getProgramStructureFlat(), program("COBSMO02"), IDENTIFICATION_DIVISION,
				ENVIRONMENT_DIVISION, CONFIGURATION_SECTION, paragraph("SOURCE-COMPUTER"),
				paragraph("OBJECT-COMPUTER"), paragraph("SPECIAL-NAMES"), DATA_DIVISION, WORKING_STORAGE_SECTION,
				LINKAGE_SECTION, PROCEDURE_DIVISION, paragraph("VERSION-PRUEFEN"), paragraph("EINGABE-PRUEFEN"),
				paragraph("ERMITTELN-ABTEILWERT"), paragraph("ERMITTELN-BENZINVERBRAUCH"),
				paragraph("ERMITTELN-AUFRUFNUMMER"), paragraph("ERMITTELN-AUSGABEKOSTEN"),
				paragraph("ERMITTELN-SITZPLATZ"), paragraph("ERMITTELN-HOSENRISS"), paragraph("ERMITTELN-HOSENRISS-1"),
				paragraph("ERMITT-WIANLKUCHEN"), paragraph("FEHLER-PAAM"), paragraph("ENDE"),
				section("VERSIONS-KONTROLLE"), paragraph("VERSIONS-KONTROLLE-START"),
				paragraph("VERSIONS-KONTROLLE-ENDE"));
		assertCOBSMO02(getStructure());
	}

	@Test
	public void testWorkspaceStructureAnalysis() {
		CobolAnalysisWorkspace workspace = new CobolAnalysisWorkspace(new ILowLevelErrorHandler() {
			@Override
			public void onWarning(SimpleNode node, Message warning) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onError(ParseException e) {
				// TODO Auto-generated method stub

			}
		});
		File file = new File(getClass().getResource("/COBSMO01.cob").getFile());
		CobolStructureRepresentation structure = workspace.getStructureRepresentation(file);
		assertCOBSMO01(structure);

		Assert.assertSame(structure, workspace.getStructureRepresentation(file));
		Assert.assertNotSame(structure, workspace.analyzeStructure(file));
	}

	private static void assertChildren(ICallableDefinitionRepresentation node, ClassAndName... names) {
		List<? extends ICallRepresentation> children = Lists.newArrayList(node.getChildren());
		Assert.assertEquals(names.length, children.size());

		for (int i = 0; i < names.length; i++) {
			if (node.getChildren().get(i).getTarget().isResolved()) {
				// FIXME unresolveable
				// Assert.assertThat(children.get(i).getTarget().getStructureRepresentation(),
				// Is.is(names[i].clazz));
			} else {
				// FIXME unresolveable
				// Assert.assertThat(children.get(i).getTarget(),
				// Is.is(UnresolvedControlflowCalleeNode.class));
			}
			Assert.assertEquals(names[i].name, children.get(i).getTarget().getName());
		}
	}
}
