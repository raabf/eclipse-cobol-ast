package parser.analysis.cobol;

import java.io.File;

import junit.framework.Assert;

import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.Test;

import parser.analysis.base.definition.representation.IDataAccessRepresentation;
import parser.analysis.base.definition.representation.IDefinitionUseRepresentation;
import parser.analysis.base.structure.representation.IDataDeclarationRepresentation;
import parser.errorhandling.ILowLevelErrorHandler;
import parser.errorhandling.Message;

import com.google.common.collect.Multimap;

import convex.step2.parser.ParseException;
import convex.step2.parser.SimpleNode;

public class DataflowAnalysisTest {
	private CobolAnalysisWorkspace workspace;

	@Before
	public void setup() {
		BasicConfigurator.configure();
		workspace = new CobolAnalysisWorkspace(new ILowLevelErrorHandler() {

			@Override
			public void onError(ParseException e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onWarning(SimpleNode node, Message warning) {
				// TODO Auto-generated method stub

			}
		});
	}

	@Test
	public void testQualifiedMove() {
		IDefinitionUseRepresentation dataflow = workspace.analyzeDataflow(new File(getClass().getResource(
				"/qualifiedMove.cbl").getFile()));
		Assert.assertEquals(26, dataflow.getDataDefinitions().size());
		Multimap<? extends IDataDeclarationRepresentation, ? extends IDataAccessRepresentation> reads = dataflow
				.getReadAccesses();
		Assert.assertEquals(1, reads.size());
		IDataDeclarationRepresentation read = reads.entries().iterator().next().getKey();
		Assert.assertEquals("USER-TEST", read.getName());

		Multimap<? extends IDataDeclarationRepresentation, ? extends IDataAccessRepresentation> writes = dataflow
				.getWriteAccesses();
		Assert.assertEquals(1, writes.size());
		IDataDeclarationRepresentation write = writes.entries().iterator().next().getKey();
		Assert.assertEquals("PASS-TEST", write.getName());
	}

	@Test
	public void testCall() {
		IDefinitionUseRepresentation dataflow = workspace.analyzeDataflow(new File(getClass().getResource("/call.cbl")
				.getFile()));
		Assert.assertEquals(14, dataflow.getDataDefinitions().size());
		Assert.assertEquals(10, dataflow.getReadAccesses().values().size());
		Assert.assertEquals(5, dataflow.getWriteAccesses().values().size());
	}

	@Test
	public void testMultiply() {
		IDefinitionUseRepresentation dataflow = workspace.analyzeDataflow(new File(getClass().getResource(
				"/multiply.cbl").getFile()));
		Assert.assertEquals(3, dataflow.getDataDefinitions().size());
		Assert.assertEquals(6, dataflow.getReadAccesses().values().size());
		Assert.assertEquals(4, dataflow.getWriteAccesses().values().size());
	}

	@Test
	public void testDivide() {
		IDefinitionUseRepresentation dataflow = workspace.analyzeDataflow(new File(getClass()
				.getResource("/divide.cbl").getFile()));
		Assert.assertEquals(3, dataflow.getDataDefinitions().size());
		Assert.assertEquals(6, dataflow.getReadAccesses().values().size());
		Assert.assertEquals(4, dataflow.getWriteAccesses().values().size());
	}

	@Test
	public void testAdd() {
		IDefinitionUseRepresentation dataflow = workspace.analyzeDataflow(new File(getClass().getResource("/add.cbl")
				.getFile()));
		Assert.assertEquals(3, dataflow.getDataDefinitions().size());
		Assert.assertEquals(6, dataflow.getReadAccesses().values().size());
		Assert.assertEquals(4, dataflow.getWriteAccesses().values().size());
	}

	@Test
	public void testSubtract() {
		IDefinitionUseRepresentation dataflow = workspace.analyzeDataflow(new File(getClass().getResource(
				"/subtract.cbl").getFile()));
		Assert.assertEquals(3, dataflow.getDataDefinitions().size());
		Assert.assertEquals(6, dataflow.getReadAccesses().values().size());
		Assert.assertEquals(4, dataflow.getWriteAccesses().values().size());
	}
}
