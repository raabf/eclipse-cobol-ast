      $set de-edit"1"
       IDENTIFICATION DIVISION.
       PROGRAM-ID.                      KDV-BWS.
       DATA DIVISION.
         WORKING-STORAGE SECTION.
       01  VAL1 PIC 9(9).
       01  VAL2 PIC 9(9).
       01  VAL3 PIC 9(9).
       PROCEDURE DIVISION.
         SUBTRACT 2 FROM VAL1.
         
         SUBTRACT VAL1 FROM VAL2.
         
         SUBTRACT 2 FROM VAL2 GIVING VAL1.
         
         SUBTRACT VAL1 FROM VAL2 GIVING VAL3.
         
         EXIT PROGRAM.