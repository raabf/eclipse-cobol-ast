      $set de-edit"1"
       IDENTIFICATION DIVISION.
       PROGRAM-ID.                      KDV-BWS.
       DATA DIVISION.
         WORKING-STORAGE SECTION.
       01  VAL1 PIC 9(9).
       01  VAL2 PIC 9(9).
       01  VAL3 PIC 9(9).
       PROCEDURE DIVISION.
         Main SECTION.
           DISPLAY "I am the Main section"
           PARA1.
             DISPLAY "I am the PARA1 paragraph".
             PERFORM Additional.
           END-P.
             GO TO Additional.
             DISPLAY "I am the END-P paragraph".
         Additional SECTION.
           DISPLAY "I am the Additional section".
           PARA2.
             DISPLAY "I am the PARA2 paragraph".
             PERFORM PARA3.
             GO TO END-P.
           PARA3.
             DISPLAY "I am the PARA3 paragraph".
           END-P.
             DISPLAY "I am the END-P paragraph".