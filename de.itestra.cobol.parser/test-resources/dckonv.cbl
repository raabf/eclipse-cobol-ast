      ******************************************************************
      *    DEC-KONV        UEBERNAHME A10130(BAS) --> A10130(COBOL)    *
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. DCKONV.
       AUTHOR. PROFI-SOFTWARE LH.
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       OBJECT-COMPUTER. IBM-AT.
       SPECIAL-NAMES. CONSOLE IS CRT.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      *
           SELECT ARTIKEL ASSIGN "A10130.dat"
           ORGANIZATION IS INDEXED
           ACCESS MODE IS DYNAMIC
           LOCK MODE AUTOMATIC
           WITH LOCK ON RECORD
           RECORD KEY IS ART-NR
           ALTERNATE RECORD KEY IS ART-3 OF ARTIKELSTAMM WITH DUPLICATES
           FILE STATUS IS FS.
      *
           SELECT ARTIKEL-B ASSIGN "DCASTAMM"
           ORGANIZATION IS LINE SEQUENTIAL
           ACCESS MODE SEQUENTIAL
           FILE STATUS IS FS.
      *
           SELECT A10131 ASSIGN "A10131.DAT"
           ORGANIZATION IS INDEXED
           ACCESS MODE IS DYNAMIC
           LOCK MODE AUTOMATIC
           WITH LOCK ON RECORD
           RECORD KEY IS A10131-KEY
           FILE STATUS IS FS.
      *
           SELECT A10132 ASSIGN "A10132.DAT"
           ORGANIZATION IS INDEXED
           ACCESS MODE IS DYNAMIC
           LOCK MODE AUTOMATIC
           WITH LOCK ON RECORD
           RECORD KEY IS ART-2-1 OF A10132-SATZ
           FILE STATUS IS FS.
      *
           SELECT A10138 ASSIGN "A10138.DAT"
           ORGANIZATION IS INDEXED
           ACCESS MODE IS DYNAMIC
           LOCK MODE AUTOMATIC
           WITH LOCK ON RECORD
           RECORD KEY IS A10138-KEY
           FILE STATUS IS FS.
      *
           SELECT KONTROLLE ASSIGN "PROTOKOL.DAT"
           ORGANIZATION IS RELATIVE
           ACCESS MODE IS DYNAMIC
           RELATIVE KEY IS PRO-KEY
           LOCK MODE IS EXCLUSIVE.
      *
      *
       DATA DIVISION.
       FILE SECTION.
      
           COPY "A10131.DEF".
           COPY "A10132.DEF".
           COPY "A10138.DEF".
           COPY "PROTOKOL.DEF".
      
      ******************************************************************
      *     A10130.DEF       ARTIKELSTAMM                 SL =         *
      ******************************************************************
       FD  ARTIKEL.
       01  ARTIKELSTAMM.
           03 ART-NR.
              04 ART-1       PIC X(20).
              04 ART-2       PIC 9(5).
           03 ART-3          PIC X(36).
           03 ART-4          PIC X(36).
           03 ART-5          PIC 9(5).
           03 ART-6          PIC X.
           03 ART-7          PIC 9(5).
           03 ART-8          PIC X.
           03 ART-9          PIC XXX.
           03 ART-10         PIC XX.
           03 ART-11         PIC XXXX.
           03 ART-12         PIC X.
           03 ART-13         PIC X.
           03 ART-14         PIC X.
           03 ART-15         PIC X.
           03 ART-16         PIC 99V99.
           03 ART-17         PIC X.
           03 ART-18         PIC XX.
           03 ART-19         PIC X.
           03 ART-20         PIC XX.
           03 ART-21         PIC S9(8)V999.
           03 ART-22         PIC S9(8)V999.
           03 ART-23         PIC S9(8)V99.
           03 ART-24         PIC S9(8)V99.
           03 ART-25         PIC S9(8)V99.
           03 ART-26         PIC S9(8)V99.
           03 ART-27         PIC S99999999V999.
           03 ART-28         PIC X.
           03 ART-29         PIC S999999V99.
           03 ART-30.
              04 TT          PIC XX.
              04 FILLER      PIC X.
              04 MM          PIC XX.
              04 FILLER      PIC X.
              04 JJ          PIC XX.
           03 ART-31         PIC S999999V99.
           03 ART-32         PIC S999999V99.
           03 ART-33         PIC S999999V99.
           03 ART-34         PIC S999999V99.
           03 ART-35         PIC XX.
           03 ART-36         PIC XXXXXX.
           03 ART-37         PIC S999999V99.
           03 ART-38         PIC X(20).
           03 ART-39         PIC 9(5).
           03 ART-40         PIC X.
           03 ART-41         PIC 999V999.
           03 ART-42         PIC S9(5)V999.
           03 ART-43         PIC X.
           03 ART-44         PIC 99V999.
           03 ART-45         PIC 99V99.
           03 ART-46         PIC 9(05).
           03 ART-47         PIC 9(05).
           03 ART-48         PIC X(20).
      
      ******************************************************************
      *     DCASTAMM          ARTIKELSTAMM-DECKERT          SL = 200   *
      ******************************************************************
       FD  ARTIKEL-B.
       01  ARTIKELSTAMM-B.
           04 ART-1          PIC X(03).
           04 ART-2          PIC X(04).
           03 ART-3          PIC X(04).
           03 ART-4          PIC X(07).
           03 ART-5          PIC X(30).
           03 ART-6          PIC X(30).
           03 ART-7          PIC X(02).
           03 FILLER         PIC X(14).
           03 ART-15         PIC X(02).
           03 ART-16         PIC X(02).
           03 ART-17.
              05 ART-17-1    PIC X(05).
              05 ART-17-VZ   PIC X.
           03 ART-18.
              05 ART-18-1    PIC X(05).
              05 ART-18-VZ   PIC X.
           03 ART-19.
              05 ART-19-1    PIC X(08).
              05 ART-19-VZ   PIC X.
           03 ART-20.
              05 ART-20-1    PIC X(08).
              05 ART-20-VZ   PIC X.
           03 ART-21.
              05 ART-21-1    PIC X(08).
              05 ART-21-VZ   PIC X.
           03 ART-22.
              05 ART-22-1    PIC X(08).
              05 ART-22-VZ   PIC X.
           03 ART-23.
              05 ART-23-1    PIC X(08).
              05 ART-23-VZ   PIC X.
           03 ART-24         PIC X(01).
           03 ART-25         PIC 9(08).
           03 ART-26         PIC 9(08).
           03 ART-27         PIC 9(08).
           03 ART-28         PIC 9(08).
           03 ART-29         PIC 9(08).
           03 ART-30         PIC X(04).
      *
       WORKING-STORAGE SECTION.
      *
       01  A                    PIC 99.
       01  AA                   PIC 99.
       01  AAA                  PIC 99.
       01  AA1                  PIC 99.
       01  ABS                  PIC 99.
       01  AUTO-EIN             PIC X.
       01  COMMI                PIC 9.
       01  DAT-L.
           02 RESULT-L          PIC 99 comp-x.
           02 FUNCTION-L        PIC 99 comp-x VALUE 18.
           02 PARAMETER-L.
              03 PA-L           PIC 99 comp-x VALUE 8.
              03 PA-N           PIC X(8).
      *
       01  FELD                 PIC X.
       01  FELD1                PIC X(11).
       01  FELD11 REDEFINES FELD1 PIC X OCCURS 11.
       01  FELD6                PIC 99999.
       01  FELD9                PIC 9(9).
       01  FS.
            03 FS-1             PIC X.
            03 FS-2             PIC X.
       01  FS9 REDEFINES FS     PIC 9(4) comp-x.
       01  FS99                 PIC 9(3).
       01  HILF                 PIC X.
       01  LEER-24              PIC X(24).
       01  MERKER               PIC 9.
       01  M-F8                 PIC 9.
       01  R-FELD               PIC S9(12)V999.
       01  PRO-KEY              PIC 999.
       01  SFELD1       PIC X(36).
       01  SFELD11 REDEFINES SFELD1 PIC X OCCURS 36.
       01  SFELD2       PIC X(36).
       01  SFELD22 REDEFINES SFELD2 PIC X OCCURS 36.
       01  Z                    PIC 99.
      *
      **************************************
       01  UEBERGABE-X.
           02 TMNR              PIC 99.
           02 kdvuser           PIC X(15).
           02 PASS              PIC X(15).
           02 KONTROLL-KEY      PIC 999.
           02 STEU              PIC 9(5).
      *
      ******************************************************************
      ******************************************************************
       PROCEDURE DIVISION.
       DECLARATIVES.
       E-A-FEHLER SECTION.
           USE AFTER ERROR PROCEDURE ON ARTIKEL-B ARTIKEL
                                        A10132 A10131.
       STATUS-ABFRAGE.
           IF FS-1 = "1"
              CALL X"E5"
              DISPLAY "DATEI-ENDE" AT 2303.
           IF FS-2 = "2" GO ENDE.
           IF FS-1 = "3"
              DISPLAY "FEHLER NICHT DEFINIERT" AT 2303.
           IF FS-1 = "9"
              PERFORM ERRCHK
           ELSE
              STOP " "
              STOP RUN.
       ENDE.
           EXIT.
       END DECLARATIVES.
      ******************************************************************
      ******************************************************************
       STEUER SECTION.
       NSTART.
           PERFORM EROEFF-DAT.
           PERFORM MASKE.
       LES.
           PERFORM A10130-B-LESEN.
           IF Z = 10 GO ENDE.
           PERFORM A10130-B-UMWAND.
           PERFORM A10130-SCHR.
           PERFORM A10131-SCHR.
           PERFORM A10132-SCHR.
           PERFORM A10138-SCHR.
           GO LES.
       ENDE.
           PERFORM PROG-ENDE.
      ******************************************************************
       EROEFF-DAT SECTION.
           OPEN INPUT ARTIKEL-B.
           OPEN I-O ARTIKEL.
           OPEN I-O A10131.
           OPEN I-O A10132.
           OPEN I-O A10138.
      ******************************************************************
       MASKE SECTION.
           MOVE 0 TO Z.
           MOVE SPACES TO ARTIKELSTAMM.
           MOVE SPACES TO ARTIKELSTAMM-B.
           MOVE SPACES TO A10131-SATZ.
           MOVE SPACES TO A10132-SATZ.
           DISPLAY SPACES.
           DISPLAY "Bitte warten !" AT 1740.
      ******************************************************************
       A10130-B-LESEN SECTION.
           READ ARTIKEL-B RECORD AT END MOVE 10 TO Z.
       ENDE.
           EXIT.
      ******************************************************************
       A10130-B-UMWAND SECTION.
           INITIALIZE ARTIKELSTAMM.
      
           STRING  ART-1 OF ARTIKELSTAMM-B DELIMITED BY SIZE,
                   ART-2 OF ARTIKELSTAMM-B DELIMITED BY SIZE,
                   INTO ART-1 OF ARTIKELSTAMM.
           MOVE ART-3 OF ARTIKELSTAMM-B TO ART-7 OF ARTIKELSTAMM.
           MOVE ART-5 OF ARTIKELSTAMM-B TO ART-3 OF ARTIKELSTAMM.
           MOVE ART-6 OF ARTIKELSTAMM-B TO ART-4 OF ARTIKELSTAMM.
           MOVE ART-7 OF ARTIKELSTAMM-B TO ART-9 OF ARTIKELSTAMM.
      
           IF ART-15 OF ARTIKELSTAMM-B = " 0"
              MOVE "0" TO ART-12 OF ARTIKELSTAMM.
           IF ART-15 OF ARTIKELSTAMM-B = " 1"
              MOVE "2" TO ART-12 OF ARTIKELSTAMM.
           IF ART-15 OF ARTIKELSTAMM-B = " 0"
              MOVE "3" TO ART-12 OF ARTIKELSTAMM.
      
           IF ART-16 OF ARTIKELSTAMM-B = " 0"
              MOVE "0" TO ART-8 OF ARTIKELSTAMM.
           IF ART-16 OF ARTIKELSTAMM-B = " 1"
              MOVE "1" TO ART-8 OF ARTIKELSTAMM.
           IF ART-16 OF ARTIKELSTAMM-B = " 2"
              MOVE "2" TO ART-8 OF ARTIKELSTAMM.
      
           MOVE ZERO TO FELD6.
           MOVE ART-17-1 OF ARTIKELSTAMM-B TO FELD6.
           COMPUTE FELD6 = FELD6 / 1000.
           IF ART-17-VZ OF ARTIKELSTAMM-B = "-"
              COMPUTE FELD6 = FELD6 * -1.
           MOVE FELD6 TO ART-5 OF ARTIKELSTAMM.
      
           MOVE ZERO TO FELD6.
           MOVE ART-18-1 OF ARTIKELSTAMM-B TO FELD6.
           COMPUTE FELD6 = FELD6 / 1000.
           IF ART-18-VZ OF ARTIKELSTAMM-B = "-"
              COMPUTE FELD6 = FELD6 * -1.
           MOVE FELD6 TO ART-44 OF ARTIKELSTAMM.
      
           MOVE ZERO TO FELD9.
           MOVE ART-19-1 OF ARTIKELSTAMM-B TO FELD9.
           IF ART-19-VZ OF ARTIKELSTAMM-B = "-"
              COMPUTE FELD9 = FELD9 * -1.
           MOVE FELD9 TO ART-27 OF ARTIKELSTAMM.
      
           MOVE ART-25 OF ARTIKELSTAMM-B TO ART-34 OF ARTIKELSTAMM.
           MOVE ART-28 OF ARTIKELSTAMM-B TO ART-31 OF ARTIKELSTAMM.
      
           DISPLAY "ART.-NR.: " AT 0305.
           DISPLAY ART-1 OF ARTIKELSTAMM AT 0315.
           DISPLAY ART-2 OF ARTIKELSTAMM AT 1972.
      
       ENDE.
           EXIT.
      ******************************************************************
       UMSETZ SECTION.
           MOVE 0 TO MERKER.
           PERFORM DREH VARYING A FROM 1 BY 1 UNTIL A = 11.
           GO ENDE.
       DREH.
           IF FELD11(A) = "-" MOVE " " TO FELD11(A)
              MOVE 1 TO MERKER.
       ENDE.
           MOVE FELD1 TO R-FELD.
           IF MERKER = 1
              COMPUTE R-FELD = R-FELD * -1.
           EXIT.
      ******************************************************************
       A10130-SCHR SECTION.
           WRITE ARTIKELSTAMM.
       ENDE.
           EXIT.
      ******************************************************************
       A10131-SCHR SECTION.
           MOVE ART-1 OF ARTIKELSTAMM-B TO ART-1 OF A10131-SATZ.
           MOVE ART-2 OF ARTIKELSTAMM-B TO ART-2 OF A10131-SATZ.
           MOVE ART-3 OF ARTIKELSTAMM-B TO ART-3 OF A10131-SATZ.
           WRITE A10131-SATZ.
       ENDE.
           EXIT.
      ******************************************************************
       A10132-SCHR SECTION.
           MOVE ART-1 OF ARTIKELSTAMM-B TO ART-1 OF A10132-SATZ.
           MOVE ART-2 OF ARTIKELSTAMM-B TO ART-2 OF A10132-SATZ.
           MOVE ART-3 OF ARTIKELSTAMM-B TO ART-3 OF A10132-SATZ.
           WRITE A10132-SATZ.
       ENDE.
           EXIT.
      ******************************************************************
       A10138-SCHR SECTION.
           MOVE ART-1 OF ARTIKELSTAMM-B TO ART-1 OF A10138-SATZ.
           MOVE ART-1 OF ARTIKELSTAMM TO SFELD1.
           IF SFELD11(1) = SPACES
              UNSTRING ART-1 OF ARTIKELSTAMM DELIMITED BY ALL " "
                 INTO SFELD1 ART-1 OF A10138-SATZ.
           MOVE ART-2 OF ARTIKELSTAMM-B TO ART-2 OF A10138-SATZ.
           MOVE ART-3 OF ARTIKELSTAMM-B TO ART-3 OF A10138-SATZ.
           WRITE A10138-SATZ.
       ENDE.
           EXIT.
      ******************************************************************
       HILFE SECTION.
      *    CALL X"B7" USING FUNCTION,PARAMETERS,BS-H1.
      *    STOP " ".
      *    PERFORM BILDSCHIRM.
      ******************************************************************
       PROG-ENDE SECTION.
           CLOSE ARTIKEL-B.
           MOVE 18 TO FUNCTION-L.
           MOVE "A10130-B.DAT" TO PA-N.
      *    CALL X"91" USING RESULT-L, FUNCTION-L, PARAMETER-L.
           CLOSE ARTIKEL.
           CLOSE A10131.
           CLOSE A10132.
           CLOSE A10138.
           EXIT PROGRAM.
           STOP RUN.
      ******************************************************************
       ERRCHK SECTION.
           MOVE LOW-VALUE TO FS-1.
           MOVE FS9 TO FS99.
           IF FS99 = 018 GO ENDE.
           IF FS99 < 013 OR > 207 DISPLAY SPACES.
           IF FS99 = 007 DISPLAY "KEIN SPEICHERPLATZ
      -    " MEHR ZUR VERF?GUNG !" AT 1230 GO RAUS.
           IF FS99 = 013 DISPLAY "DATEI NICHT GEFUNDEN !" AT 1230
                             GO RAUS.
           IF FS99 = 208 ADD 1 TO COMMI
                              DISPLAY "FEHLER IM MULTI-USER SYSTEM !"
                              AT 1230
                              IF COMMI > 1 GO RAUS
                              ELSE COMMIT CLOSE ARTIKEL-B ARTIKEL
                                                A10131 A10132
                              GO NEUSTART.
           IF FS99 = 027 DISPLAY "DRUCKER NICHT BETRIEBSBEREIT !"
                           AT 2303  STOP " " GO ENDE.
           IF FS99 = 065 DISPLAY "DATEI WIRD BEARBEITET !" AT 2303
                           STOP " " GO ENDE.
           IF FS99 = 068 DISPLAY "SATZ WIRD BEARBEITET !" AT 2303
                           STOP " " GO ENDE.
           IF FS99 = 213 COMMIT GO ENDE.
       RAUS.
           OPEN I-O KONTROLLE.
           MOVE KONTROLL-KEY TO PRO-KEY.
           READ KONTROLLE RECORD.
           MOVE FS99 TO PRO-FEHLER.
           REWRITE PROTO-DAT.
           CALL X"E5".
           STOP "Bitte rufen Sie Ihren Programmhersteller an !".
           CALL X"83" USING AUTO-EIN.
           IF AUTO-EIN = "+"  CLOSE KONTROLLE  EXIT PROGRAM.
           STOP RUN.
       NEUSTART.
           STOP " " GO NSTART.
       ENDE.
           EXIT.
      ******************************************************************
       ENDE3 SECTION.
           EXIT PROGRAM.
           STOP RUN.
      ******************************************************************