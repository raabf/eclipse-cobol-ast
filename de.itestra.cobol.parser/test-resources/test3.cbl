       DATA DIVISION.
       PROCEDURE DIVISION.
        	   
		    
		     
				  EXEC SQL
 						CREATE TABLE TMP_FOO AS 
   						SELECT col2, 
          				NVL(:TMP-VAL INDICATOR :I-TMP-VAL, f.foo)
     					FROM some_table s,
          				some_other_table 
    					WHERE s.col1 = f.bar
      					AND col1 IS NOT NULL
      					AND f.foo NOT LIKE 'fooo%'
						END-EXEC