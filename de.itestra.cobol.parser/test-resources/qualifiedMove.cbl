      $set de-edit"1"
       IDENTIFICATION DIVISION.
       PROGRAM-ID.                      KDV-BWS.
       DATA DIVISION.
         WORKING-STORAGE SECTION.
       01  TESTF.
           02 RESULT-TEST    PIC 99 COMP-X VALUE 0.
           02 FUNCTION-TEST  PIC 99 COMP-X VALUE 3.
           02 PARAMETER-TEST.
              03 L-TEST      PIC 99 COMP-X VALUE 32.
              03 TEXT-TEST-1.
                 04 TMNR-TEST   PIC 99 VALUE 0.
                 04 USER-TEST   PIC X(15).
                 04 PASS-TEST   PIC X(15).
              03 TEXT-TEST-2.
                 04 TMNR-TEST   PIC 99 VALUE 0.
                 04 USER-TEST   PIC X(15).
                 04 PASS-TEST   PIC X(15).
       01  TESTE.
           02 RESULT-TEST    PIC 99 COMP-X VALUE 0.
           02 FUNCTION-TEST  PIC 99 COMP-X VALUE 3.
           02 PARAMETER-TEST.
              03 L-TEST      PIC 99 COMP-X VALUE 32.
              03 TEXT-TEST-1.
                 04 TMNR-TEST   PIC 99 VALUE 0.
                 04 USER-TEST   PIC X(15).
                 04 PASS-TEST   PIC X(15).
              03 TEXT-TEST-2.
                 04 TMNR-TEST   PIC 99 VALUE 0.
                 04 USER-TEST   PIC X(15).
                 04 PASS-TEST   PIC X(15).
       PROCEDURE DIVISION.
       
      *    TODO this must be interpreted as 
      *    reads of several children of LOGONF and 
      *    writes to several children of LOGONG     
      *    MOVE CORRESPONDING LOGONF TO LOGONG
      
           MOVE USER-TEST IN TEXT-TEST-2 IN TESTE 
             TO PASS-TEST IN TEXT-TEST-1 IN TESTF
         EXIT PROGRAM.