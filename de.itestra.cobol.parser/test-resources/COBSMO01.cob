       IDENTIFICATION DIVISION.
       PROGRAM-ID.   COBSMO01.
       AUTHOR.       John Doe.
       ENVIRONMENT DIVISION.
      ******************************************************************
      *-----------------------------------------------------------------
       CONFIGURATION SECTION.
      *-----------------------------------------------------------------
       SOURCE-COMPUTER.  IBM-370.
       OBJECT-COMPUTER.  IBM-370.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
      *
      ******************************************************************
       DATA DIVISION.
      ******************************************************************
      *-----------------------------------------------------------------
       WORKING-STORAGE SECTION.
      *-----------------------------------------------------------------
       01  PGM-RELEASE-STAND.
           05 FILLER                        PIC X(01)      VALUE 'R'.
        05 PGM-RELEASE-NR                PIC 9(02)      VALUE 71.
           05 FILLER                        PIC X(02)      VALUE '/V'.
        05 PGM-VERSION-NR                PIC 9(02)      VALUE 00.
      ***
      *** HILFSFELDER
      ***
       01  HF-HILFSFELDER.
      *    ** PGM-NAME DES UNTERMODULS
           05 HF-MODUL-NAME                 PIC X(08)      VALUE SPACES.
      ***
      *** ALLGEMEINE COPY-STRECKEN
      ***
      *** HILFSFELDER FUER BASIS-DATEN UND -FUNKTIONEN WIE DER AUSGABE
      *** DER HINWEIS- UND FEHLERMELDUNGEN
           COPY XHFGGLDW.
      *** HILFSFELDER ZUR ERZEUGUNG DES SYSTEMUNABH. RETURN-CODES
           COPY XRETCODW.
      *** HILFSFELDER ZUR FELD-AUFBEREITUNG
           COPY XFLDAPAW.
      *** BESCHREIBUNG DES PROGRAMM-UEBERGABEBEREICHS
           COPY TEST1.
      *
      *-----------------------------------------------------------------
      *** COPY-STRECKEN FUER DEN SQL-ZUGRIFF, Z. B. HOSTVARIABLEN    ***
      *-----------------------------------------------------------------
      *** BEGINN DER DEKLARATION
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
      *** SQL COMMUNICATION AREA
           EXEC SQL INCLUDE SQLCA         END-EXEC.
      *** HOSTVARIABLEN FUER "AAVVV_VVV"
           EXEC SQL INCLUDE AAVTUEV0.CPY  END-EXEC.
      *** HOSTVARIABLEN FUER "BAMES_MESSAGES"
           EXEC SQL INCLUDE XBAJJSV0.CPY  END-EXEC.
      
      *** PGM-SPEZIFISCHE HOSTVARIABLEN
       01  HV-PGM-SPEZ-HOST-VARIABLEN.
      *    ** DATUM-ZEIT DES SYSTEMS
           05 HV-SYS-DATUM-ZEIT             PIC X(14).
      *    ** HILFSFELDER
           05 HV-USE-DATUM                  PIC X(08).
      
      *** ENDE DER DEKLARATION
           EXEC SQL END   DECLARE SECTION END-EXEC.
      *
      *-----------------------------------------------------------------
       LINKAGE SECTION.
      *-----------------------------------------------------------------
      *** UEBERGABE-BEREICH ZUR MELDUNG VON FEHLERN
           COPY XUTTELDW.
      *** UEBERGABE-BEREICH ZUR MELDUNG VON SQL-ZUGRIFFS-FEHLERN
           COPY XSQLIITW.
      *** UEBERGABE-BEREICH DES MODULS "AUUSREMU" ZUR ERMITTLUNG VON
      *** ASSIGN-INHALTEN
       01  AVZIZIPU-UEBERGABE.
           05 FILLER                        PIC X(40).
           05 AVZIZIPU-COPY-RELEASE-STAND   PIC X(07).
      *
      ******************************************************************
       PROCEDURE DIVISION USING XUGGELD-UEBERGABE,
                                XSEUMEL-UEBERGABE,
                                AVZIZIPU-UEBERGABE.
      ******************************************************************
      *-----------------------------------------------------------------
       STEUERUNG SECTION.
      *-----------------------------------------------------------------
      *** DIESE SECTION STEUERT DEN PROGRAMMVERLAUF
      ***
       STEU-START.
      *    **
      *    ** MODUL-NAMEN UND HILFSFELDER INITIALISIEREN
      *    **
           INITIALIZE                          HF-HILFSFELDER,
                                               XSEUMEL-AUSGABE.
           MOVE 'AVZIZIPU'                  TO HF-MODUL-NAME,
                                               XHFGGLD-PGM-NAME.
           PERFORM XUGRTZHP-INIT.
      *    **
      *    ** VERSIONSVERGLEICH DURCHFUEHREN
      *    **
           IF AVZIZIPU-COPY-RELEASE-STAND
                                      NOT = XVZHHUP-COPY-RELEASE-STAND
      *       ** WIRD MIT EINER FALSCHEN VERSION DES UEBERGABEBEREICHS
      *       ** AUFGERUFEN
              MOVE 0121                     TO XUGGELD-L03-FEHLER-NR
              MOVE 'AVZIZIPU'               TO XUGGELD-RC-FELD
              MOVE AVZIZIPU-COPY-RELEASE-STAND
                                            TO XUGGELD-RC-INHALT
              PERFORM XUGRTZHP-UPGM-ENDE
           END-IF.
      *    **
      *    ** EINGABEN UEBERTRAGEN UND RUECKGABE-BEREICH INITIALISIEREN
      *    **
           MOVE AVZIZIPU-UEBERGABE          TO XVZHHUP-UEBERGABE.
           MOVE SPACES                      TO XVZHHUP-AUSGABE.
      *    **
      *    ** EINGABE-DATEN AUF FORMALE RICHTIGKEIT PRUEFEN
      *    **
           PERFORM EINGABEN-PRUEFEN.
      *    **
      *    ** EINGABE-DATEN AUSWERTEN UND ENTSPRECHEND VERARBEITEN
      *    **
           PERFORM DATEN-VERARBEITEN.
      *    **
      *    ** DATEN UEBERTRAGEN UND RUECKGABE-BEREICH INITIALISIEREN
      *    **
           MOVE XVZHHUP-UEBERGABE           TO AVZIZIPU-UEBERGABE.
      *
       STEU-UPGM-ENDE.
      *    ** SCHLIESSEN DER DATEIEN UND CURSOR UND EXIT PROGRAM
           PERFORM XUGRTZHP-UPGM-ENDE.
      *
       STEU-ENDE.
      *** ENDE STEUERUNG
           EXIT PROGRAM.
      *-----------------------------------------------------------------
       EINGABEN-PRUEFEN SECTION.
      *-----------------------------------------------------------------
      *** FORMALE PRUEFUNG DER EINGABEN
      ***
       EING-PRUEF-START.
           IF XVZHHUP-E-GWWPI-ID NOT NUMERIC
              MOVE 'IST NICHT NUMERISCH' TO XUGGELD-DEF-L03-FEHLERTEXT
              MOVE 0100                     TO XUGGELD-L03-FEHLER-NR
              MOVE 'XVZHHUP-E-GWWPI-ID'     TO XUGGELD-RC-FELD
              MOVE XVZHHUP-E-GWWPI-ID-X     TO XUGGELD-RC-INHALT
              PERFORM XUGRTZHP-UPGM-ENDE
           END-IF.
      *
           IF XVZHHUP-E-GWWPI-ID = ZERO
              MOVE 'HAT UNZULAESSIGEN INHALT'
                                         TO XUGGELD-DEF-L03-FEHLERTEXT
              MOVE 0102                     TO XUGGELD-L03-FEHLER-NR
              MOVE 'XVZHHUP-E-GWWPI-ID'     TO XUGGELD-RC-FELD
              MOVE XVZHHUP-E-GWWPI-ID-X     TO XUGGELD-RC-INHALT
              PERFORM XUGRTZHP-UPGM-ENDE
           END-IF.
      *
           IF    XVZHHUP-E-DATUM = SPACES
              OR XVZHHUP-E-DATUM = ZEROES
              MOVE 'HAT UNZULAESSIGEN INHALT'
                                         TO XUGGELD-DEF-L03-FEHLERTEXT
              MOVE 0102                     TO XUGGELD-L03-FEHLER-NR
              MOVE 'XVZHHUP-E-DATUM'        TO XUGGELD-RC-FELD
              MOVE XVZHHUP-E-DATUM          TO XUGGELD-RC-INHALT
              PERFORM XUGRTZHP-UPGM-ENDE
           END-IF.
      *
           IF     XVZHHUP-E-RELEVANZ NOT = 'J'
              AND XVZHHUP-E-RELEVANZ NOT = 'N'
              MOVE 'HAT UNZULAESSIGEN INHALT'
                                         TO XUGGELD-DEF-L03-FEHLERTEXT
              MOVE 0102                     TO XUGGELD-L03-FEHLER-NR
              MOVE 'XVZHHUP-E-RELEVANZ'     TO XUGGELD-RC-FELD
              MOVE XVZHHUP-E-RELEVANZ       TO XUGGELD-RC-INHALT
              PERFORM XUGRTZHP-UPGM-ENDE
           END-IF.
      *
           IF    (    XVZHHUP-E-RELEVANZ   = 'N'
                  AND XVZHHUP-E-KMAZDA NOT = SPACE)
              OR (    XVZHHUP-E-RELEVANZ   = 'J'
                  AND XVZHHUP-E-KMAZDA NOT = '1'
                  AND XVZHHUP-E-KMAZDA NOT = '2'
                  AND XVZHHUP-E-KMAZDA NOT = '3'
                  AND XVZHHUP-E-KMAZDA NOT = '4')
              MOVE 'HAT UNZULAESSIGEN INHALT'
                                         TO XUGGELD-DEF-L03-FEHLERTEXT
              MOVE 0102                     TO XUGGELD-L03-FEHLER-NR
              MOVE 'XVZHHUP-E-KMAZDA'       TO XUGGELD-RC-FELD
              MOVE XVZHHUP-E-KMAZDA         TO XUGGELD-RC-INHALT
              PERFORM XUGRTZHP-UPGM-ENDE
           END-IF.
      *
           IF XVZHHUP-E-PROZ-BENZ NOT NUMERIC
              MOVE 'IST NICHT NUMERISCH' TO XUGGELD-DEF-L03-FEHLERTEXT
              MOVE 0100                     TO XUGGELD-L03-FEHLER-NR
              MOVE 'XVZHHUP-E-PROZ-BENZ'    TO XUGGELD-RC-FELD
              MOVE XVZHHUP-E-PROZ-BENZ-X    TO XUGGELD-RC-INHALT
              PERFORM XUGRTZHP-UPGM-ENDE
           END-IF.
      *
           IF     XVZHHUP-E-RELEVANZ      = 'N'
              AND XVZHHUP-E-PROZ-BENZ NOT = ZERO
              MOVE 'HAT UNZULAESSIGEN INHALT'
                                         TO XUGGELD-DEF-L03-FEHLERTEXT
              MOVE 0102                     TO XUGGELD-L03-FEHLER-NR
              MOVE 'XVZHHUP-E-PROZ-BENZ'    TO XUGGELD-RC-FELD
              MOVE XVZHHUP-E-PROZ-BENZ      TO XUGGELD-RC-09-09
              PERFORM XUGRTZHP-UPGM-ENDE
           END-IF.
      *
       EING-PRUEF-ENDE.
      *** ENDE EINGABEN-PRUEFEN
           EXIT.
      *-----------------------------------------------------------------
       DATEN-VERARBEITEN SECTION.
      *-----------------------------------------------------------------
      *** EINGABE-DATEN AUSWERTEN UND ENTSPRECHEND VERARBEITEN
      ***
       DATEN-VERARB-START.
      *    **
      *    ** LETZTEN BEKANNTEN EINTRAG FUER DEN SCHLUMPF
      *    ** SCHLUMPFEN, DER KLEINER ODER GLEICH DEM UEBERGEBENEN DATUM
      *    ** IST.
      *    **
           PERFORM SEL-AVTUE-ZIV.
      *    **
      *    ** UEBERTRAGE DIE ERMITTELTEN DATEN IN DEN AUSGABE-BEREICH
      *    **
           MOVE AVTUE-DAT                   TO XVZHHUP-A-DATUM.
           MOVE AVTUE-RELEVANZ              TO XVZHHUP-A-RELEVANZ.
           MOVE AVTUE-KMAZDA                TO XVZHHUP-A-KMAZDA.
           MOVE AVTUE-PROZ-BENZ             TO XVZHHUP-A-PROZ-BENZ.
      *    **
      *    ** WEICHEN DIE GELESENEN DATEN VON DEN UEBERGEBENEN DATEN AB,
      *    ** ERFOLGT IN ABHAENGIGKEIT VOM DATUM DER GELESENEN DATEN EIN
      *    ** UPDATE DER BESTEHENDEN DATEN ODER EIN INSERT DER UEBERGE-
      *    ** BENEN DATEN.
      *    **
           IF    AVTUE-RELEVANZ  NOT = XVZHHUP-E-RELEVANZ
              OR AVTUE-KMAZDA    NOT = XVZHHUP-E-KMAZDA
              OR AVTUE-PROZ-BENZ NOT = XVZHHUP-E-PROZ-BENZ
              INITIALIZE                       AVTUE-AVTUE-ZIV
              MOVE XVZHHUP-E-GWWPI-ID       TO AVTUE-GWWPI-ID
              MOVE XVZHHUP-E-DATUM          TO AVTUE-DAT
              MOVE XVZHHUP-E-RELEVANZ       TO AVTUE-RELEVANZ
              MOVE XVZHHUP-E-KMAZDA         TO AVTUE-KMAZDA
              MOVE XVZHHUP-E-PROZ-BENZ      TO AVTUE-PROZ-BENZ
              IF XVZHHUP-E-DATUM NOT = XVZHHUP-A-DATUM
                 PERFORM INS-AVTUE-ZIV
              ELSE
                 PERFORM UPD-AVTUE-ZIV
              END-IF
              SET  XVZHHUP-A-MIT-UEBERNAHME TO TRUE
           END-IF.
      *
       DATEN-VERARB-ENDE.
      *** ENDE DATEN-VERARBEITEN
           EXIT.
      ******************************************************************
      *** PROCEDURE-COPY-STRECKEN OHNE SQL-ZUGRIFF                   ***
      ******************************************************************
      *** SYSTEMABH. SQL-RETURNCODE IN SYSTEMUNABH. RETURNCODE UMSETZEN
           COPY XSQLHRZP.
      *** SECTION ZUR AUFBEREITUNG VON FELDERN
           COPY XFLDAILL.
      *** FEHLERUEBERGABE AUS UNTERPROGRAMMEN
           COPY XUGRTZHP.
      *
      ******************************************************************
      *** PROCEDURE-COPY-STRECKEN MIT SQL-ZUGRIFF                    ***
      ******************************************************************
      *
      ******************************************************************
      *** SECTIONS MIT SQL-ZUGRIFF                                   ***
      ******************************************************************
      *-----------------------------------------------------------------
       SEL-AVTUE-ZIV SECTION.
      *-----------------------------------------------------------------
      *** SEL - SELECT
      ***
       SEL-AVTUE-ZIV-START.
           INITIALIZE                          AVTUE-AVTUE-ZIV.
           MOVE XVZHHUP-E-GWWPI-ID          TO AVTUE-GWWPI-ID.
           MOVE XVZHHUP-E-DATUM             TO HV-USE-DATUM.
      *
           MOVE "AAVVV_VVV"                 TO XSEUMEL-A-SQL-TABELLE.
           MOVE 'SELECT'                    TO XRETCOD-ZUGRIFF-ART.
      *
           EXEC SQL
                SELECT
                   TO_CHAR(AV_ZIV_DAT,
                          'YYYYMMDD'),
                   AV_ZIV_RELEVANZ,
                   AV_ZIV_KMAZDA,
                   AV_ZIV_PROZ_BENZ
                INTO
                   :AVTUE-DAT
                   INDICATOR :I-AVTUE-DAT,
                   :AVTUE-RELEVANZ
                   INDICATOR :I-AVTUE-RELEVANZ,
                   :AVTUE-KMAZDA
                   INDICATOR :I-AVTUE-KMAZDA,
                   :AVTUE-PROZ-BENZ
                   INDICATOR :I-AVTUE-PROZ-BENZ
                FROM
                   AAVVV_VVV
                WHERE
                   GW_WPI_ID =
                   :AVTUE-GWWPI-ID
               AND AV_ZIV_DAT <=
                    TO_DATE (:HV-USE-DATUM,'YYYYMMDD')
                ORDER BY AV_ZIV_DAT DESC
           END-EXEC.
      *
           PERFORM XSQLHRZP-RETCODE-FUELLEN.
           IF XRETCOD-OK-RNF
              GO TO SEL-AVTUE-ZIV-ENDE
           END-IF.
      *
           MOVE SPACES                      TO XFLDAPA-AT-AUSGABE-TAB,
                                               XFLDAPA-ET-EINGABE-TAB,
                                               XFLDAPA-ET-TRENN-CHAR.
           MOVE ZERO                        TO XFLDAPA-AT-ANZ.
      *
           MOVE 'GW_WPI_ID'                 TO XFLDAPA-ET-ALPHA-FELD.
           MOVE '='                         TO XFLDAPA-ET-TRENN-CHAR.
           PERFORM XFLDAILL-ET-AT-UEBERNAHME.
           MOVE AVTUE-GWWPI-ID              TO XFLDAPA-ET-18-00.
           MOVE '*'                         TO XFLDAPA-ET-TRENN-CHAR.
           PERFORM XFLDAILL-ET-AT-UEBERNAHME.
           MOVE 'AV_ZIV_DAT<'               TO XFLDAPA-ET-ALPHA-FELD.
           MOVE '='                         TO XFLDAPA-ET-TRENN-CHAR.
           PERFORM XFLDAILL-ET-AT-UEBERNAHME.
           MOVE HV-USE-DATUM                TO XFLDAPA-ET-ALPHA-FELD.
           MOVE '*'                         TO XFLDAPA-ET-TRENN-CHAR.
           PERFORM XFLDAILL-ET-AT-UEBERNAHME.
      *
           MOVE XFLDAPA-AT-AUSGABE-TAB      TO XSEUMEL-A-ZUGRIFF-KEY.
           MOVE SQLERRD IN SQLCA (3)        TO XSEUMEL-A-SQLERRD3.
           MOVE SQLCA                       TO XSEUMEL-A-SQLCA.
           MOVE XRETCOD-RETCODE             TO XSEUMEL-A-RETCODE.
           MOVE XRETCOD-SQLCODE             TO XSEUMEL-A-SQLCODE.
           MOVE XRETCOD-RC-TEXT             TO XSEUMEL-A-RC-TEXT.
           MOVE XRETCOD-ZUGRIFF-ART         TO XSEUMEL-A-ZUGRIFF-ART.
           MOVE HF-MODUL-NAME               TO XSEUMEL-A-MODUL-NAME.
           MOVE PGM-RELEASE-STAND           TO XSEUMEL-A-MODUL-RELEASE.
      *
           MOVE 9999                        TO XUGGELD-L03-FEHLER-NR.
           PERFORM XUGRTZHP-UPGM-ENDE.
      *
       SEL-AVTUE-ZIV-ENDE.
           EXIT.
      *-----------------------------------------------------------------
       INS-AVTUE-ZIV SECTION.
      *-----------------------------------------------------------------
      *** INS - INSERT INTO
      ***
       INS-AVTUE-ZIV-START.
           MOVE "AAVVV_VVV"                 TO XSEUMEL-A-SQL-TABELLE.
           MOVE 'INSERT'                    TO XRETCOD-ZUGRIFF-ART.
           MOVE SPACES                      TO AVTUE-KFZUD-GEAEND-VON.
           STRING 'A1' XSEUMEL-E-PGM-NAME
                                  DELIMITED BY SIZE
                                          INTO AVTUE-KFZUD-GEAEND-VON.
           MOVE 1                           TO AVTUE-KFZUD-TRIG-KNZ.
           INITIALIZE                          I-AVTUE-AVTUE-ZIV.
      *
           EXEC SQL
                INSERT INTO
                   AAVVV_VVV
                   (
                   GW_WPI_ID,
                   AV_ZIV_DAT,
                   BA_AUD_GEAEND_AM,
                   BA_AUD_GEAEND_VON,
                   BA_AUD_TRIG_KNZ,
                   AV_ZIV_RELEVANZ,
                   AV_ZIV_KMAZDA,
                   AV_ZIV_PROZ_BENZ
                   )
                VALUES
                   (
                   :AVTUE-GWWPI-ID
                   INDICATOR :I-AVTUE-GWWPI-ID,
                    TO_DATE (:AVTUE-DAT
                   INDICATOR :I-AVTUE-DAT,
                             'YYYYMMDD'),
                    SYSDATE,
                   :AVTUE-KFZUD-GEAEND-VON
                   INDICATOR :I-AVTUE-KFZUD-GEAEND-VON,
                   :AVTUE-KFZUD-TRIG-KNZ
                   INDICATOR :I-AVTUE-KFZUD-TRIG-KNZ,
                   :AVTUE-RELEVANZ
                   INDICATOR :I-AVTUE-RELEVANZ,
                   :AVTUE-KMAZDA
                   INDICATOR :I-AVTUE-KMAZDA,
                   :AVTUE-PROZ-BENZ
                   INDICATOR :I-AVTUE-PROZ-BENZ
                   )
           END-EXEC.
      *
           PERFORM XSQLHRZP-RETCODE-FUELLEN.
           IF XRETCOD-OK-DUPREC
              GO TO INS-AVTUE-ZIV-ENDE
           END-IF.
      *
           MOVE SPACES                      TO XFLDAPA-AT-AUSGABE-TAB,
                                               XFLDAPA-ET-EINGABE-TAB,
                                               XFLDAPA-ET-TRENN-CHAR.
           MOVE ZERO                        TO XFLDAPA-AT-ANZ.
      *
           MOVE 'GW_WPI_ID'                 TO XFLDAPA-ET-ALPHA-FELD.
           MOVE '='                         TO XFLDAPA-ET-TRENN-CHAR.
           PERFORM XFLDAILL-ET-AT-UEBERNAHME.
           MOVE AVTUE-GWWPI-ID              TO XFLDAPA-ET-18-00.
           MOVE '*'                         TO XFLDAPA-ET-TRENN-CHAR.
           PERFORM XFLDAILL-ET-AT-UEBERNAHME.
           MOVE 'AV_ZIV_DAT'                TO XFLDAPA-ET-ALPHA-FELD.
           MOVE '='                         TO XFLDAPA-ET-TRENN-CHAR.
           PERFORM XFLDAILL-ET-AT-UEBERNAHME.
           MOVE AVTUE-DAT                   TO XFLDAPA-ET-ALPHA-FELD.
           MOVE '*'                         TO XFLDAPA-ET-TRENN-CHAR.
           PERFORM XFLDAILL-ET-AT-UEBERNAHME.
      *
           MOVE XFLDAPA-AT-AUSGABE-TAB      TO XSEUMEL-A-ZUGRIFF-KEY.
           MOVE SQLERRD IN SQLCA (3)        TO XSEUMEL-A-SQLERRD3.
           MOVE SQLCA                       TO XSEUMEL-A-SQLCA.
           MOVE XRETCOD-RETCODE             TO XSEUMEL-A-RETCODE.
           MOVE XRETCOD-SQLCODE             TO XSEUMEL-A-SQLCODE.
           MOVE XRETCOD-RC-TEXT             TO XSEUMEL-A-RC-TEXT.
           MOVE XRETCOD-ZUGRIFF-ART         TO XSEUMEL-A-ZUGRIFF-ART.
           MOVE HF-MODUL-NAME               TO XSEUMEL-A-MODUL-NAME.
           MOVE PGM-RELEASE-STAND           TO XSEUMEL-A-MODUL-RELEASE.
      *
           MOVE 9999                        TO XUGGELD-L03-FEHLER-NR.
           PERFORM XUGRTZHP-UPGM-ENDE.
      *
       INS-AVTUE-ZIV-ENDE.
           EXIT.
      *-----------------------------------------------------------------
       UPD-AVTUE-ZIV SECTION.
      *-----------------------------------------------------------------
      *** UPD - UPDATE
      ***
       UPD-AVTUE-ZIV-START.
           MOVE "AAVVV_VVV"                 TO XSEUMEL-A-SQL-TABELLE.
           MOVE 'UPDATE'                    TO XRETCOD-ZUGRIFF-ART.
           MOVE SPACES                      TO AVTUE-KFZUD-GEAEND-VON.
           STRING 'A1' XSEUMEL-E-PGM-NAME
                                  DELIMITED BY SIZE
                                          INTO AVTUE-KFZUD-GEAEND-VON.
           MOVE 1                           TO AVTUE-KFZUD-TRIG-KNZ.
           INITIALIZE                          I-AVTUE-AVTUE-ZIV.
      *
           EXEC SQL
                UPDATE
                   AAVVV_VVV
                SET
                   BA_AUD_GEAEND_AM = SYSDATE,
                   BA_AUD_GEAEND_VON = :AVTUE-KFZUD-GEAEND-VON
                   INDICATOR :I-AVTUE-KFZUD-GEAEND-VON,
                   BA_AUD_TRIG_KNZ = :AVTUE-KFZUD-TRIG-KNZ
                   INDICATOR :I-AVTUE-KFZUD-TRIG-KNZ,
                   AV_ZIV_RELEVANZ = :AVTUE-RELEVANZ
                   INDICATOR :I-AVTUE-RELEVANZ,
                   AV_ZIV_KMAZDA = :AVTUE-KMAZDA
                   INDICATOR :I-AVTUE-KMAZDA,
                   AV_ZIV_PROZ_BENZ = :AVTUE-PROZ-BENZ
                   INDICATOR :I-AVTUE-PROZ-BENZ
                WHERE
                   GW_WPI_ID =
                   :AVTUE-GWWPI-ID
               AND AV_ZIV_DAT =
                    TO_DATE (:AVTUE-DAT,'YYYYMMDD')
           END-EXEC.
      *
           PERFORM XSQLHRZP-RETCODE-FUELLEN.
           IF XRETCOD-OK
              GO TO UPD-AVTUE-ZIV-ENDE
           END-IF.
      *
           MOVE SPACES                      TO XFLDAPA-AT-AUSGABE-TAB,
                                               XFLDAPA-ET-EINGABE-TAB,
                                               XFLDAPA-ET-TRENN-CHAR.
           MOVE ZERO                        TO XFLDAPA-AT-ANZ.
      *
           MOVE 'GW_WPI_ID'                 TO XFLDAPA-ET-ALPHA-FELD.
           MOVE '='                         TO XFLDAPA-ET-TRENN-CHAR.
           PERFORM XFLDAILL-ET-AT-UEBERNAHME.
           MOVE AVTUE-GWWPI-ID              TO XFLDAPA-ET-18-00.
           MOVE '*'                         TO XFLDAPA-ET-TRENN-CHAR.
           PERFORM XFLDAILL-ET-AT-UEBERNAHME.
           MOVE 'AV_ZIV_DAT'                TO XFLDAPA-ET-ALPHA-FELD.
           MOVE '='                         TO XFLDAPA-ET-TRENN-CHAR.
           PERFORM XFLDAILL-ET-AT-UEBERNAHME.
           MOVE AVTUE-DAT                   TO XFLDAPA-ET-ALPHA-FELD.
           MOVE '*'                         TO XFLDAPA-ET-TRENN-CHAR.
           PERFORM XFLDAILL-ET-AT-UEBERNAHME.
      *
           MOVE XFLDAPA-AT-AUSGABE-TAB      TO XSEUMEL-A-ZUGRIFF-KEY.
           MOVE SQLERRD IN SQLCA (3)        TO XSEUMEL-A-SQLERRD3.
           MOVE SQLCA                       TO XSEUMEL-A-SQLCA.
           MOVE XRETCOD-RETCODE             TO XSEUMEL-A-RETCODE.
           MOVE XRETCOD-SQLCODE             TO XSEUMEL-A-SQLCODE.
           MOVE XRETCOD-RC-TEXT             TO XSEUMEL-A-RC-TEXT.
           MOVE XRETCOD-ZUGRIFF-ART         TO XSEUMEL-A-ZUGRIFF-ART.
           MOVE HF-MODUL-NAME               TO XSEUMEL-A-MODUL-NAME.
           MOVE PGM-RELEASE-STAND           TO XSEUMEL-A-MODUL-RELEASE.
      *
           MOVE 9999                        TO XUGGELD-L03-FEHLER-NR.
           PERFORM XUGRTZHP-UPGM-ENDE.
      *
       UPD-AVTUE-ZIV-ENDE.
           EXIT.
      *-----------------------------------------------------------------
       DATEIEN-SCHLIESSEN SECTION.
      *-----------------------------------------------------------------
      *** MUSS DIE LETZTE SECTION IM SOURCE-CODE DES UNTERPROGRAMMS SEIN
      ***
       DSS-START.
      *
       DSS-ENDE.
           EXIT.
      *
      ******************************************************************
      ******************************************************************
