      $set de-edit"1"
       IDENTIFICATION DIVISION.
       PROGRAM-ID.                      KDV-BWS.
       DATA DIVISION.
         WORKING-STORAGE SECTION.
       01  VAL1 PIC 9(9).
       01  VAL2 PIC 9(9).
       01  VAL3 PIC 9(9).
       01  VAL4 PIC 9(9) OCCURS 30.
       PROCEDURE DIVISION.
         ADD VAL2 VAL3 3 TO VAL4(1).
         
         SUBTRACT VAL1 VAL2 FROM VAL3 GIVING VAL2.

         SUBTRACT VAL3 VAL4(1) FROM VAL2(VAL1).
         
         ADD VAL2 VAL3 GIVING VAL4(2).

         ADD 2 TO VAL2 GIVING VAL1.
         
         ADD VAL1 TO VAL2 GIVING VAL3 END-ADD.
         
         EXIT PROGRAM.