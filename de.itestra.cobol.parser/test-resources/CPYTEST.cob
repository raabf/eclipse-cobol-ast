       IDENTIFICATION DIVISION.
       PROGRAM-ID.   CPYTEST.
       
       DATA DIVISION.
         WORKING-STORAGE SECTION.
      ***
      *** HILFSFELDER FUER BASIS-DATEN UND -FUNKTIONEN WIE DER AUSGABE
      *** DER HINWEIS- UND FEHLERMELDUNGEN
           COPY COPYBOOK.
      *** HILFSFELDER ZUR ERZEUGUNG DES SYSTEMUNABH. RETURN-CODES
           COPY "COPYBOOK".
      *
      ******************************************************************
       PROCEDURE DIVISION.
           EXIT PROGRAM.