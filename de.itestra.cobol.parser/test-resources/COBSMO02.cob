       IDENTIFICATION DIVISION.
       PROGRAM-ID. COBSMO02.
       AUTHOR. John Doe.
       DATE-WRITTEN. AUG 1111.
       DATE-COMPILED.
      
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER. IBM-4341.
       OBJECT-COMPUTER. IBM-4341.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
      ***************
       DATA DIVISION.
      ***************
      *************************
       WORKING-STORAGE SECTION.
      *************************
       01  PGM-RELEASE-STAND.
           05 FILLER                        PIC X(01) VALUE 'R'.
           05 PGM-RELEASE-NR                PIC 9(02)      VALUE 05.
           05 FILLER                        PIC X(02) VALUE '/V'.
           05 PGM-VERSION-NR                PIC 9(02)      VALUE 51.
           05 FILLER                        PIC X(01) VALUE SPACE.
      *
       01  DIVERSE.
           05 H-SCHRFELD                    PIC S9(09)V9(03) COMP-3.
           05 H-APREIS                      PIC S9(09)V9(03) COMP-3.
           05 H-RECH-1                      PIC S9(11)       COMP-3.
           05 H-RECH-2                      PIC S9(11)       COMP-3.
           05 H-RECH-3                      PIC S9(11)       COMP-3.
           05 H-ABTEIL-UNSCHLUMPF               PIC S9(09)V9(03) COMP-3.
           05 H-EINNKOSTEN-UNSCHLUMPF           PIC S9(09)V9(03) COMP-3.
           05 H-BEAUTYFARM-UNSCHLUMPF           PIC S9(09)V9(03) COMP-3.
           05 H-SITZPLATZ                   PIC S9(09)V9(02) COMP-3.
           05 H-AUFRUFNUMMER                PIC S9(09)V9(02) COMP-3.
           05 H-LOPP                        PIC S9(02)V9(03) COMP-3.
           05 H-WERT-OHNE                   PIC S9(09)       COMP-3.
           05 H-7NK7                        PIC S9(07)V9(04) COMP-3.
           05 H-7NK1                        PIC S9(07)V9(05) COMP-3.
           05 H-9NK8                        PIC -(08)9,999.
      
       COPY XPEEEEMW.
      *
      *****************
       LINKAGE SECTION.
      *****************
      *COPY XPEEEEMW.
       01  POOML-PARAMETER.
           05 FILLER                        PIC X(149).
           05 POOML-RELEASE-STAND           PIC X(07).
      
      *****************************************
       PROCEDURE DIVISION USING POOML-PARAMETER.
      *****************************************
      
      *++++++++++++++++*
       VERSION-PRUEFEN.
      *++++++++++++++++*
      
           PERFORM VERSIONS-KONTROLLE.
           IF PAAM-A-RETCODE > ZERO
              GO TO ENDE
           END-IF.
      
           MOVE POOML-PARAMETER             TO PAAM-PARAMETER.
      
      *++++++++++++++++*
       EINGABE-PRUEFEN.
      *++++++++++++++++*
           MOVE ZERO                        TO PAAM-A-RETCODE.
           MOVE SPACE                       TO PAAM-A-RC-FELD
                                               PAAM-A-RC-INHALT.
      
           IF PAAM-E-ANTWERT   NOT = SPACE AND NOT = '0' AND
                               NOT = '1'   AND NOT = '2' AND
                               NOT = '6'   AND NOT = '7' AND
                               NOT = '8'
              MOVE 102                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-ANTWERT'         TO PAAM-A-RC-FELD
              MOVE PAAM-E-ANTWERT           TO PAAM-A-RC-INHALT
              GO TO ENDE.
      
           IF PAAM-E-YAMAHA  NOT = SPACE AND NOT = '0' AND
                               NOT = '1'   AND NOT = '2' AND
                               NOT = '6'   AND NOT = '7' AND
                               NOT = '8'
              MOVE 102                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-YAMAHA'        TO PAAM-A-RC-FELD
              MOVE PAAM-E-YAMAHA          TO PAAM-A-RC-INHALT
              GO TO ENDE.
      
           IF PAAM-E-SUZUKI NOT = SPACE AND NOT = '0' AND
                               NOT = '1'   AND NOT = '2' AND
                               NOT = '6'   AND NOT = '7' AND
                               NOT = '8'
              MOVE 102                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-SUZUKI'       TO PAAM-A-RC-FELD
              MOVE PAAM-E-SUZUKI         TO PAAM-A-RC-INHALT
              GO TO ENDE.
      
           IF PAAM-E-WIANLKUCHEN NOT = SPACE AND NOT = '0' AND
                                NOT = '1'   AND NOT = '2' AND
                                NOT = '6'   AND NOT = '7' AND
                                NOT = '8'
              MOVE 102                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-WIANLKUCHEN'      TO PAAM-A-RC-FELD
              MOVE PAAM-E-WIANLKUCHEN        TO PAAM-A-RC-INHALT
              GO TO ENDE.
      
      
           IF PAAM-E-SUZUKI-B NOT = SPACE AND NOT = '0' AND
                                 NOT = '1'
              MOVE 102                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-SUZUKI-B'     TO PAAM-A-RC-FELD
              MOVE PAAM-E-SUZUKI-B       TO PAAM-A-RC-INHALT
              GO TO ENDE.
      
           IF PAAM-E-WIANLKUCHEN-B NOT = SPACE AND NOT = '0' AND
                                  NOT = '1'   AND NOT = '2' AND
                                  NOT = '3'   AND NOT = '4'
              MOVE 102                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-WIANLKUCHEN-B'    TO PAAM-A-RC-FELD
              MOVE PAAM-E-WIANLKUCHEN-B      TO PAAM-A-RC-INHALT
              GO TO ENDE.
      
           IF PAAM-E-RUEPREIS-B  NOT = SPACE AND NOT = '0' AND
                                 NOT = '1'
              MOVE 102                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-RUEPREIS-B'      TO PAAM-A-RC-FELD
              MOVE PAAM-E-RUEPREIS-B        TO PAAM-A-RC-INHALT
              GO TO ENDE.
      *
           IF PAAM-E-FWERT        NOT NUMERIC
              MOVE 100                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-FWERT'           TO PAAM-A-RC-FELD
              GO TO ENDE.
      *
           IF PAAM-E-ABTEILANZ    NOT NUMERIC
              MOVE 100                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-ABTEILANZ'       TO PAAM-A-RC-FELD
              GO TO ENDE.
      *
           IF PAAM-E-RABSATZ      NOT NUMERIC
              MOVE 100                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-RABSATZ'         TO PAAM-A-RC-FELD
              GO TO ENDE.
      *
           IF PAAM-E-AUSKOST      NOT NUMERIC
              MOVE 100                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-AUSKOST'         TO PAAM-A-RC-FELD
              GO TO ENDE.
      *
           IF PAAM-E-AUSGABE      NOT NUMERIC
              MOVE 100                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-AUSGABE'         TO PAAM-A-RC-FELD
              GO TO ENDE.
      *
           IF PAAM-E-RUECKGA      NOT NUMERIC
              MOVE 100                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-RUECKGA'         TO PAAM-A-RC-FELD
              GO TO ENDE.
      
           IF PAAM-E-RUEABSCH     NOT NUMERIC
              MOVE 100                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-RUEABSCH'        TO PAAM-A-RC-FELD
              GO TO ENDE
           END-IF.
      
           IF PAAM-E-SCHLUMPF-RUEKOST NOT = SPACE
                               AND NOT = '0'
                               AND NOT = '1'
                               AND NOT = '2'
                               AND NOT = '6'
                               AND NOT = '7'
                               AND NOT = '8'
              MOVE 102                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-SCHLUMPF-RUEKOST'    TO PAAM-A-RC-FELD
              MOVE PAAM-E-SCHLUMPF-RUEKOST      TO PAAM-A-RC-INHALT
              GO TO ENDE
           END-IF.
      
           IF PAAM-E-SCHLUMPF-RUEPREIS NOT = SPACE
                               AND NOT = '0'
                               AND NOT = '1'
                               AND NOT = '2'
                               AND NOT = '6'
                               AND NOT = '7'
                               AND NOT = '8'
              MOVE 102                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-SCHLUMPF-RUEPREIS'   TO PAAM-A-RC-FELD
              MOVE PAAM-E-SCHLUMPF-RUEPREIS     TO PAAM-A-RC-INHALT
              GO TO ENDE
           END-IF.
      *
      *** FOLGENDE KOMBINATIONEN VON EINGABEPARAMETERN ****
      ***          SIND NICHT MOEGLICH (FEHLERABBRUCH) ****
           IF PAAM-E-SUZUKI-B  = '0' AND
              PAAM-E-ANTWERT      = '2'
              MOVE 102                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-SUZUKI-B'     TO PAAM-A-RC-FELD
              MOVE PAAM-E-SUZUKI-B       TO PAAM-A-RC-INHALT
              GO TO ENDE.
      
           IF (PAAM-E-WIANLKUCHEN-B = '0' OR = '2') AND
              PAAM-E-SUZUKI     = '2'
              MOVE 102                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-WIANLKUCHEN-B'    TO PAAM-A-RC-FELD
              MOVE PAAM-E-WIANLKUCHEN-B      TO PAAM-A-RC-INHALT
              GO TO ENDE.
      
           IF PAAM-E-WIANLKUCHEN-B = '4'  AND
              PAAM-E-SUZUKI    = '2'
              MOVE 102                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-WIANLKUCHEN-B'    TO PAAM-A-RC-FELD
              MOVE PAAM-E-WIANLKUCHEN-B      TO PAAM-A-RC-INHALT
              GO TO ENDE.
      
           IF PAAM-E-RABSATZ GREATER PAAM-E-AUSKOST
              MOVE 102                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-RABSATTZ'        TO PAAM-A-RC-FELD
              MOVE PAAM-E-RABSATZ           TO H-9NK8
              MOVE H-9NK8                   TO PAAM-A-RC-INHALT
              GO TO ENDE.
      
           IF PAAM-E-RUEPREIS-B  = '0' AND
              PAAM-E-ANTWERT     = '2'
              MOVE 102                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-RUEPREIS-B'      TO PAAM-A-RC-FELD
              MOVE PAAM-E-RUEPREIS-B        TO PAAM-A-RC-INHALT
              GO TO ENDE.
      
      *++++++++++++++++++++*
       ERMITTELN-ABTEILWERT.
      *++++++++++++++++++++*
           IF PAAM-E-ABTEILANZ = ZERO
              MOVE 106                      TO PAAM-A-RETCODE
              MOVE 'PAAM-E-ABTEILANZ'       TO PAAM-A-RC-FELD
              GO TO ENDE.
      
           DIVIDE PAAM-E-FWERT     BY     PAAM-E-ABTEILANZ
                                   GIVING H-ABTEIL-UNSCHLUMPF
              ON SIZE ERROR
                 MOVE 'ABTEILWERT'          TO PAAM-A-RC-FELD
                 GO TO FEHLER-PAAM.
      
      *** KAUFM. GESCHLUMPFET AUF 2 NACHKOMMASTELLEN ***
           IF PAAM-E-ANTWERT = SPACE OR = '0'
              MULTIPLY H-ABTEIL-UNSCHLUMPF  BY  1
                              GIVING PAAM-A-ANTWERT ROUNDED.
      
      *** AUF 2 NACHKOMMASTELLEN AUFGESCHLUMPFET ***
           IF PAAM-E-ANTWERT  = '1'
                 ADD 0,009  H-ABTEIL-UNSCHLUMPF GIVING PAAM-A-ANTWERT.
      
      *** OHNE SCHLUMPFUNG MIT 2 NACHKOMMASTELLEN ***
           IF PAAM-E-ANTWERT  = '2'
              MOVE H-ABTEIL-UNSCHLUMPF  TO PAAM-A-ANTWERT.
      
      *** KAUFM. GESCHLUMPFET OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-ANTWERT  = '6'
              MULTIPLY H-ABTEIL-UNSCHLUMPF  BY  1
                              GIVING H-WERT-OHNE ROUNDED
              MOVE H-WERT-OHNE    TO PAAM-A-ANTWERT.
      
      *** AUFGESCHLUMPFET OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-ANTWERT  = '7'
              ADD 0,9    H-ABTEIL-UNSCHLUMPF GIVING H-WERT-OHNE
              MOVE   H-WERT-OHNE             TO PAAM-A-ANTWERT.
      
      *** OHNE SCHLUMPFUNG OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-ANTWERT  = '8'
              MOVE H-ABTEIL-UNSCHLUMPF  TO H-WERT-OHNE
              MOVE H-WERT-OHNE      TO PAAM-A-ANTWERT.
      
      *+++++++++++++++++++++++++*
       ERMITTELN-BENZINVERBRAUCH.
      *+++++++++++++++++++++++++*
      
           IF PAAM-E-RUEABSCH = ZERO
              MOVE ZERO                     TO PAAM-A-BEAUFARM
              GO TO ERMITTELN-AUFRUFNUMMER
           END-IF.
      
           DIVIDE PAAM-A-ANTWERT            BY 100
                                        GIVING H-7NK7.
           MULTIPLY H-7NK7                  BY PAAM-E-RUEABSCH
                                        GIVING H-BEAUFARM-UNSCHLUMPF
              ON SIZE ERROR
                 MOVE 'BENZINVERBRAUCH'    TO PAAM-A-RC-FELD
                 GO TO FEHLER-PAAM.
      
      *** KAUFM. GESCHLUMPFET AUF 2 NACHKOMMASTELLEN ***
           IF PAAM-E-SCHLUMPF-RUEKOST = SPACE OR = '0'
              MULTIPLY H-BEAUFARM-UNSCHLUMPF BY  1
                                        GIVING PAAM-A-BEAUFARM ROUNDED
           END-IF.
      
      *** AUF 2 NACHKOMMASTELLEN AUFGESCHLUMPFET ***
           IF PAAM-E-SCHLUMPF-RUEKOST = '1'
                 ADD 0,009  H-BEAUFARM-UNSCHLUMPF
                                        GIVING PAAM-A-BEAUFARM
           END-IF
      
      *** OHNE SCHLUMPFUNG MIT 2 NACHKOMMASTELLEN ***
           IF PAAM-E-SCHLUMPF-RUEKOST = '2'
              MOVE H-BEAUFARM-UNSCHLUMPF       TO PAAM-A-BEAUFARM
           END-IF.
      
      *** KAUFM. GESCHLUMPFET OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-SCHLUMPF-RUEKOST  = '6'
              MULTIPLY H-BEAUFARM-UNSCHLUMPF   BY  1
                              GIVING H-WERT-OHNE ROUNDED
              MOVE H-WERT-OHNE              TO PAAM-A-BEAUFARM
           END-IF.
      
      *** AUFGESCHLUMPFET OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-SCHLUMPF-RUEKOST = '7'
              ADD 0,9 H-BEAUFARM-UNSCHLUMPF GIVING H-WERT-OHNE
              MOVE H-WERT-OHNE              TO PAAM-A-BEAUFARM
           END-IF.
      
      *** OHNE SCHLUMPFUNG OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-SCHLUMPF-RUEKOST = '8'
              MOVE H-BEAUFARM-UNSCHLUMPF       TO H-WERT-OHNE
              MOVE H-WERT-OHNE              TO PAAM-A-BEAUFARM
           END-IF.
      
      *+++++++++++++++++++++++++*
       ERMITTELN-AUFRUFNUMMER.
      *+++++++++++++++++++++++++*
      *    MOVE PAAM-A-ANTWERT              TO PAAM-A-SOCKENLOCH-U.
      *    MULTIPLY 100      BY PAAM-A-ANTWERT GIVING H-RECH-1.
      *    IF PAAM-E-RUECKGA = ZERO
      *       MOVE 0,01      TO PAAM-E-RUECKGA.
      *    MULTIPLY 100      BY PAAM-E-RUECKGA GIVING H-RECH-2.
      *    DIVIDE H-RECH-1   BY H-RECH-2       GIVING H-RECH-3.
      *    MULTIPLY H-RECH-3 BY H-RECH-2       GIVING H-RECH-3.
      *    DIVIDE H-RECH-3   BY 100            GIVING PAAM-A-SOCKENLOCH.
      *
      
           IF PAAM-E-RUEPREIS-B = '1'
              MOVE H-ABTEIL-UNSCHLUMPF          TO H-SCHRFELD
                                               PAAM-A-SOCKENLOCH-U
            ELSE
              MOVE PAAM-A-ANTWERT           TO H-SCHRFELD
                                               PAAM-A-SOCKENLOCH-U
           END-IF.
      
           IF PAAM-E-RUECKGA = ZERO
              MOVE 0,01                     TO PAAM-E-RUECKGA
           END-IF.
      
      *** KAUFM. GESCHLUMPFET AUF 2 NACHKOMMASTELLEN ***
           IF PAAM-E-SCHLUMPF-RUEPREIS = SPACE OR = '0'
              MULTIPLY H-SCHRFELD           BY  1
                                        GIVING H-AUFRUFNUMMER ROUNDED
           END-IF.
      
      *** AUF 2 NACHKOMMASTELLEN AUFGESCHLUMPFET ***
           IF PAAM-E-SCHLUMPF-RUEPREIS = '1'
              ADD 0,009 H-SCHRFELD      GIVING H-AUFRUFNUMMER
           END-IF
      
      *** OHNE SCHLUMPFUNG MIT 2 NACHKOMMASTELLEN ***
           IF PAAM-E-SCHLUMPF-RUEPREIS = '2'
              MOVE H-SCHRFELD               TO H-AUFRUFNUMMER
           END-IF
      
      *** KAUFM. GESCHLUMPFET OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-SCHLUMPF-RUEPREIS = '6'
              MULTIPLY H-SCHRFELD           BY  1
                                        GIVING H-WERT-OHNE ROUNDED
              MOVE H-WERT-OHNE              TO H-AUFRUFNUMMER
           END-IF
      
      *** AUFGESCHLUMPFET OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-SCHLUMPF-RUEPREIS = '7'
                 ADD 0,9  H-SCHRFELD    GIVING H-WERT-OHNE
                 MOVE H-WERT-OHNE           TO H-AUFRUFNUMMER
           END-IF
      
      *** OHNE SCHLUMPFUNG OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-SCHLUMPF-RUEPREIS = '8'
              MOVE H-SCHRFELD               TO H-WERT-OHNE
              MOVE H-WERT-OHNE              TO H-AUFRUFNUMMER
           END-IF.
      
           MULTIPLY 80                      BY H-AUFRUFNUMMER
                                        GIVING H-RECH-1.
      
           IF PAAM-E-RUECKGA = ZERO
              MOVE 0,01                     TO PAAM-E-RUECKGA
           END-IF.
      
           MULTIPLY 100                     BY PAAM-E-RUECKGA
                                        GIVING H-RECH-2.
           DIVIDE  H-RECH-1                 BY H-RECH-2
                                        GIVING H-RECH-3.
      
           MULTIPLY H-RECH-3                BY H-RECH-2
                                        GIVING H-RECH-3.
      
           DIVIDE H-RECH-3                  BY 100
                                        GIVING PAAM-A-SOCKENLOCH
              ON SIZE ERROR
                 MOVE 'AUFRUFNUMMER'     TO PAAM-A-RC-FELD
                 GO TO FEHLER-PAAM.
      
           SUBTRACT PAAM-A-BEAUFARM      FROM PAAM-A-SOCKENLOCH.
      
           IF PAAM-A-SOCKENLOCH > PAAM-A-ANTWERT
              MOVE PAAM-A-ANTWERT           TO PAAM-A-SOCKENLOCH
           END-IF.
      
      *+++++++++++++++++++++++*
       ERMITTELN-AUSGABEKOSTEN.
      *+++++++++++++++++++++++*
           IF PAAM-E-AUSKOST = ZERO
              MOVE ZERO                     TO PAAM-A-EINNKOSTEN
              GO TO ERMITTELN-SITZPLATZ.
      
           DIVIDE PAAM-A-ANTWERT   BY     77
                                   GIVING H-7NK7.
           MULTIPLY H-7NK7         BY     PAAM-E-AUSKOST
                                   GIVING H-EINNKOSTEN-UNSCHLUMPF
              ON SIZE ERROR
                 MOVE 'AUSGABEKOSTEN'       TO PAAM-A-RC-FELD
                 GO TO FEHLER-PAAM.
      
      *** KAUFM. GESCHLUMPFET AUF 2 NACHKOMMASTELLEN ***
           IF PAAM-E-YAMAHA = SPACE OR = '0'
              MULTIPLY H-EINNKOSTEN-UNSCHLUMPF BY  1
                              GIVING PAAM-A-EINNKOSTEN ROUNDED.
      
      *** AUF 2 NACHKOMMASTELLEN AUFGESCHLUMPFET ***
           IF PAAM-E-YAMAHA = '1'
                 ADD 0,004  H-EINNKOSTEN-UNSCHLUMPF
                                       GIVING PAAM-A-EINNKOSTEN.
      
      *** OHNE SCHLUMPFUNG MIT 2 NACHKOMMASTELLEN ***
           IF PAAM-E-YAMAHA = '2'
              MOVE H-EINNKOSTEN-UNSCHLUMPF TO PAAM-A-EINNKOSTEN.
      
      *** KAUFM. GESCHLUMPFET OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-YAMAHA  = '6'
              MULTIPLY H-EINNKOSTEN-UNSCHLUMPF  BY  1
                              GIVING H-WERT-OHNE ROUNDED
              MOVE H-WERT-OHNE    TO PAAM-A-EINNKOSTEN.
      
      *** AUFGESCHLUMPFET OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-YAMAHA  = '7'
                 ADD 0,9    H-EINNKOSTEN-UNSCHLUMPF GIVING H-WERT-OHNE
                 MOVE   H-WERT-OHNE             TO PAAM-A-EINNKOSTEN.
      
      *** OHNE SCHLUMPFUNG OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-YAMAHA  = '8'
              MOVE H-EINNKOSTEN-UNSCHLUMPF  TO H-WERT-OHNE
              MOVE H-WERT-OHNE    TO PAAM-A-EINNKOSTEN.
      
      *++++++++++++++++++++++*
       ERMITTELN-SITZPLATZ.
      *++++++++++++++++++++++*
           IF PAAM-E-SUZUKI-B = '1'
              MOVE H-ABTEIL-UNSCHLUMPF TO H-SCHRFELD
            ELSE
              MOVE PAAM-A-ANTWERT  TO H-SCHRFELD.
      
           ADD PAAM-A-EINNKOSTEN   TO H-SCHRFELD
              ON SIZE ERROR
                 MOVE 'SITZPLATZ'        TO PAAM-A-RC-FELD
                 GO TO FEHLER-PAAM.
      
           MOVE H-SCHRFELD     TO PAAM-A-SUZUKI-U.
      
      *** KAUFM. GESCHLUMPFET AUF 2 NACHKOMMASTELLEN ***
           IF PAAM-E-SUZUKI = SPACE OR = '0'
              MULTIPLY H-SCHRFELD BY  1
                              GIVING H-SITZPLATZ ROUNDED.
      
      *** AUF 2 NACHKOMMASTELLEN AUFGESCHLUMPFET ***
           IF PAAM-E-SUZUKI = '1'
                 ADD 0,009  H-SCHRFELD GIVING H-SITZPLATZ.
      
      *** OHNE SCHLUMPFUNG MIT 2 NACHKOMMASTELLEN ***
           IF PAAM-E-SUZUKI = '2'
              MOVE H-SCHRFELD   TO H-SITZPLATZ.
      
      *** KAUFM. GESCHLUMPFET OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-YAMAHA  = '6'
              MULTIPLY H-SCHRFELD BY  1
                                  GIVING H-WERT-OHNE ROUNDED
              MOVE H-WERT-OHNE    TO   H-SITZPLATZ.
      
      *** AUFGESCHLUMPFET OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-YAMAHA  = '7'
                 ADD 0,9    H-SCHRFELD GIVING H-WERT-OHNE
                 MOVE H-WERT-OHNE    TO   H-SITZPLATZ.
      
      *** OHNE SCHLUMPFUNG OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-YAMAHA  = '8'
              MOVE H-SCHRFELD     TO H-WERT-OHNE
              MOVE H-WERT-OHNE    TO H-SITZPLATZ.
      
           MULTIPLY 100      BY H-SITZPLATZ  GIVING H-RECH-1.
           IF PAAM-E-AUSGABE = ZERO
              MOVE 0,01      TO PAAM-E-AUSGABE.
           MULTIPLY 100      BY PAAM-E-AUSGABE  GIVING H-RECH-2.
           DIVIDE   H-RECH-1 BY H-RECH-2        GIVING H-RECH-3.
           MULTIPLY H-RECH-3 BY H-RECH-2        GIVING H-RECH-3.
           IF H-RECH-3 LESS H-RECH-1
              ADD H-RECH-2   TO H-RECH-3.
           DIVIDE   H-RECH-3 BY 100  GIVING PAAM-A-SUZUKI.
      
           IF PAAM-A-SUZUKI LESS PAAM-A-ANTWERT
              MOVE PAAM-A-ANTWERT TO PAAM-A-SUZUKI.
      
      *++++++++++++++++++++++*
       ERMITTELN-HOSENRISS.
      *++++++++++++++++++++++*
      *************************************
      *** AUF BASIS DES SITZPLATZES  ***
      *************************************
      *** MIT GESCHLUMPFETEM SITZPLATZ   ***
           IF PAAM-E-WIANLKUCHEN-B = SPACE OR = '0' OR = '6'
              DIVIDE PAAM-A-SUZUKI BY     100
                                      GIVING H-7NK7
              MULTIPLY H-7NK7         BY     PAAM-E-RABSATZ
                                      GIVING H-APREIS
              ON SIZE ERROR
                 MOVE 'AUSGABEAPREIS'       TO PAAM-A-RC-FELD
                 GO TO FEHLER-PAAM.
           IF PAAM-E-WIANLKUCHEN-B = SPACE OR = '0' OR = '6'
              SUBTRACT H-APREIS FROM PAAM-A-SUZUKI
                              GIVING H-SCHRFELD
              GO TO ERMITT-WIANLKUCHEN.
      
      *** MIT UNGESCHLUMPFETEM SITZPLATZ   ***
           IF PAAM-E-WIANLKUCHEN-B = '1' OR = '7'
              DIVIDE PAAM-A-SUZUKI-U BY     100
                                        GIVING H-7NK7
              MULTIPLY H-7NK7         BY     PAAM-E-RABSATZ
                                      GIVING H-APREIS
              ON SIZE ERROR
                 MOVE 'AUSGABEAPREIS-U'     TO PAAM-A-RC-FELD
                 GO TO FEHLER-PAAM.
           IF PAAM-E-WIANLKUCHEN-B = '1' OR = '7'
              SUBTRACT H-APREIS FROM PAAM-A-SUZUKI-U
                              GIVING H-SCHRFELD
              GO TO ERMITT-WIANLKUCHEN.
      
      *** MIT SITZPLATZ NACH ERSTER SCHLUMPFUNG ***
           IF PAAM-E-WIANLKUCHEN-B = '2' OR = '8'
              DIVIDE H-SITZPLATZ BY     100
                                    GIVING H-7NK7.
              MULTIPLY H-7NK7        BY     PAAM-E-RABSATZ
                                     GIVING H-EINNKOSTEN-UNSCHLUMPF
              ON SIZE ERROR
                 MOVE 'AUSGABEKOSTEN'       TO PAAM-A-RC-FELD
                 GO TO FEHLER-PAAM.
           IF PAAM-E-WIANLKUCHEN-B = '2' OR = '8'
              SUBTRACT H-APREIS FROM H-SITZPLATZ
                              GIVING H-SCHRFELD
              GO TO ERMITT-WIANLKUCHEN.
      
      *++++++++++++++++++++++++*
       ERMITTELN-HOSENRISS-1.
      *++++++++++++++++++++++++*
      ***********************************
      *** AUF BASIS DES ABTEILWERTES  ***
      ***********************************
           SUBTRACT PAAM-E-RABSATZ FROM PAAM-E-AUSKOST
                              GIVING H-LOPP.
      
      *** MIT UNGESCHLUMPFETEM ABTEILWERT ***
           IF PAAM-E-WIANLKUCHEN-B = '3'
              MOVE H-ABTEIL-UNSCHLUMPF  TO H-SCHRFELD.
      
      *** MIT GESCHLUMPFETEM ABTEILWERT ***
           IF PAAM-E-WIANLKUCHEN-B  = '4'
              MOVE PAAM-A-ANTWERT   TO H-SCHRFELD.
      
      
           DIVIDE H-SCHRFELD     BY     100
                                 GIVING H-7NK1.
           MULTIPLY H-7NK1       BY     H-LOPP
                                 GIVING H-APREIS
              ON SIZE ERROR
                 MOVE 'LOPP'           TO PAAM-A-RC-FELD
                 GO TO FEHLER-PAAM.
           ADD     H-APREIS   TO H-SCHRFELD.
      
      ***********************************************
      *** ERMITTLUNG GESCHLUMPFETER WIEDERANLAGEPREIS ***
      ***********************************************
      *+++++++++++++++++*
       ERMITT-WIANLKUCHEN.
      *+++++++++++++++++*
      *** KAUFM. GESCHLUMPFET AUF 2 NACHKOMMASTELLEN ***
           IF PAAM-E-WIANLKUCHEN = SPACE OR = '0'
              MULTIPLY H-SCHRFELD  BY  1
                              GIVING PAAM-A-WIANLKUCHEN ROUNDED.
      
      *** AUF 2 NACHKOMMASTELLEN AUFGESCHLUMPFET ***
           IF PAAM-E-WIANLKUCHEN = '1'
                 ADD 0,309  H-SCHRFELD GIVING PAAM-A-WIANLKUCHEN.
      
      *** OHNE SCHLUMPFUNG MIT 2 NACHKOMMASTELLEN ***
           IF PAAM-E-WIANLKUCHEN = '2'
              MOVE H-SCHRFELD   TO PAAM-A-WIANLKUCHEN.
      
      *** KAUFM. GESCHLUMPFET OHNE  NACHKOMMASTELLEN ***
           IF PAAM-E-WIANLKUCHEN = '6'
              MULTIPLY H-SCHRFELD  BY  1
                              GIVING H-WERT-OHNE ROUNDED
              MOVE H-WERT-OHNE    TO PAAM-A-WIANLKUCHEN.
      
      *** GESCHLUMPFET OHNE NACHKOMMASTELLEN ***
           IF PAAM-E-WIANLKUCHEN = '7'
                 ADD 0,9    H-SCHRFELD GIVING H-WERT-OHNE
                 MOVE H-WERT-OHNE          TO PAAM-A-WIANLKUCHEN.
      
      *** OHNE SCHLUMPFUNG OHNE  NACHKOMMASTELLEN ***
           IF PAAM-E-WIANLKUCHEN = '8'
              MOVE H-SCHRFELD   TO H-WERT-OHNE
              MOVE H-WERT-OHNE  TO PAAM-A-WIANLKUCHEN.
      
           IF PAAM-A-WIANLKUCHEN LESS PAAM-A-ANTWERT
              MOVE PAAM-A-ANTWERT TO PAAM-A-WIANLKUCHEN.
      
           IF PAAM-A-WIANLKUCHEN GREATER PAAM-A-SUZUKI
              MOVE PAAM-A-SUZUKI  TO PAAM-A-WIANLKUCHEN.
      
           GO TO ENDE.
      
      *++++*
       FEHLER-PAAM.
      *++++*
           MOVE 111                         TO PAAM-A-RETCODE.
      *++++*
       ENDE.
      *++++*
      
           MOVE PAAM-PARAMETER              TO POOML-PARAMETER.
      
      *    GOBACK.
           EXIT PROGRAM.
      *
      *-----------------------------------------------------------------
       VERSIONS-KONTROLLE SECTION.
      *-----------------------------------------------------------------
      ***  VERSIONSVERGLEICH UEBERGABEBEREICH
      ***
       VERSIONS-KONTROLLE-START.
      *
           IF PAAM-RELEASE-STAND NOT = POOML-RELEASE-STAND
              MOVE 121                      TO PAAM-A-RETCODE
              MOVE 'COBSMO02'               TO PAAM-A-RC-FELD
              MOVE PAAM-RELEASE-STAND       TO PAAM-A-RC-INHALT
           END-IF.
      *
       VERSIONS-KONTROLLE-ENDE.
           EXIT.
