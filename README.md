Cobol Eclipse Plugin
====================

Useful links:
- Eclipse Plugin FAQ: https://wiki.eclipse.org/The_Official_Eclipse_FAQs#Implementing_Support_for_Your_Own_Language
- Small Demo Plugin: https://versioncontrolseidl.in.tum.de/petter/MiniJavaIDE/blob/master
- Larger Plugin, also with graph layouting for automata: https://versioncontrolseidl.in.tum.de/petter/cup-eclipse-plugin
- Cobol Tutorial: http://www.csis.ul.ie/cobol/course/Default.htm

Prerequisites:
==============
- Java 8 or higher
- Eclipse 4.5.x
- Eclipse PDE (Plugin Development Environment) Plugin installed via Marketplace

Building the project:
=====================

- Import Eclipse project from git / clone this repo
- Open both Eclipse projects from the MASTER branch
- first run de.itestra.cobol.parser/build.xml as ant build
- refresh both imported projects to load the changes
- run the Cobol-Eclipse plugin from COBOL_plugin/plugin.xml