package de.tum.in.www2.cobol.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import de.tum.in.www2.cobol.editors.CobolEditor;
public class CommentHandler extends AbstractHandler {
	CobolEditor editor;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		editor = (CobolEditor) HandlerUtil.getActiveEditorChecked(event);
		ITextSelection textSelection = (ITextSelection) editor.getSite().getSelectionProvider().getSelection();
		

		int lengthSelection=textSelection.getLength();
		if(lengthSelection == 0)
			return null;
		
		String text = editor.getDocument().get();
		int textLength = editor.getDocument().get().length();
		int start = textSelection.getStartLine(),end = textSelection.getEndLine(),offsetLine = textSelection.getOffset();
		if(offsetLine + lengthSelection -1 == textLength -1 && text.charAt(textLength-1)!= '\n')
			{
				text = text + "\n";
				textLength++;
			}
		
		String[] splitText = text.split("\n");
		
		String commentSign = "*";
		text = "";
		for(int i = 0; i<start; i++)
		{
			text = text + splitText[i] + "\n";
		}
		
		//Check to see if there are any comments in selected text
		int counterComment=0;
		for(int i = start; i<=end; i++ )
		{
			String splitT = splitText[i];
			if(!splitT.substring(6,7).equals(commentSign))
				counterComment++;
		}

		if(counterComment > 0 || counterComment > end - start)
			for(int i = start; i<=end; i++ )
			{
				String splitT = splitText[i];
				
				if(splitT.length() > 5)
					text = text + splitT.substring(0,6) + commentSign + splitT.substring(7);
				else
					text = text + "      " + commentSign;
				
				if(i!=end)
					text= text + "\n";
			}
		else
			for(int i = start; i<=end; i++ )
			{
				String splitT = splitText[i];
				String uncomment = " ";
				if(splitT.length() > 5 )
					text = text + splitT.substring(0,6) + uncomment + splitT.substring(7);
				else
					text = text + "      " + uncomment;
				
				if(i!=end)
					text= text + "\n";
			}

		
		for(int i = end+1; i<splitText.length; i++)
		{
			if(i==end+1)
			text = text + "\n";
			text = text + splitText[i];
			if(i!=splitText.length-1)
				text = text + "\n";
		}
			
		editor.getDocument().set(text);
		editor.setHighlightRange(offsetLine, lengthSelection, true);
		return null;
	}

	

}

