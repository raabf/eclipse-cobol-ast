package de.tum.in.www2.cobol.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

import de.tum.in.www2.cobol.editors.CobolEditor;
import de.tum.in.www2.cobol.editors.views.CallHierarchyView;
import de.tum.in.www2.cobol.model.Model;
import de.tum.in.www2.util.CallHierarchyGenerator;

public class OpenCallHierarchyHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		CobolEditor editor = (CobolEditor) HandlerUtil.getActiveEditorChecked(event);
		ITextSelection textSelection = (ITextSelection) editor.getSite().getSelectionProvider().getSelection();
		int offset = textSelection.getOffset();
		Model model = editor.getModel();
		CallHierarchyView callHierarchyView = CallHierarchyView.getInstance();
		if (callHierarchyView == null) {
			try {
				HandlerUtil.getActiveWorkbenchWindow(event).getActivePage()
						.showView("de.tum.in.www2.cobol.editors.views.callhierarchy");
				callHierarchyView = CallHierarchyView.getInstance();
			} catch (PartInitException e) {
				e.printStackTrace();
			}
		}
		callHierarchyView.setEditor(editor);
		callHierarchyView.setInput(new CallHierarchyGenerator(offset, model));
		callHierarchyView.setFocus();

		return null;
	}

}