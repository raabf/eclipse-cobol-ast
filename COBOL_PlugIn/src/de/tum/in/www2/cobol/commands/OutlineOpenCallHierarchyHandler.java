package de.tum.in.www2.cobol.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

import parser.analysis.cobol.structure.representation.CobolStructureRepresentation;
import de.tum.in.www2.cobol.editors.CobolEditor;
import de.tum.in.www2.cobol.editors.views.CallHierarchyView;
import de.tum.in.www2.cobol.model.Model;
import de.tum.in.www2.util.CallHierarchyGenerator;

public class OutlineOpenCallHierarchyHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		CobolEditor editor = (CobolEditor) HandlerUtil.getActiveEditorChecked(event);
		Model model = editor.getModel();
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof TreeSelection) {
			TreeSelection treeSelection = (TreeSelection) selection;
			if (treeSelection.getPaths().length == 1) {
				CobolStructureRepresentation representation = (CobolStructureRepresentation) treeSelection
						.getFirstElement();
				int offset = representation.getStartOffset();
				CallHierarchyView callHierarchyView = CallHierarchyView.getInstance();
				if (callHierarchyView == null) {
					try {
						HandlerUtil.getActiveWorkbenchWindow(event).getActivePage()
								.showView("de.tum.in.www2.cobol.editors.views.callhierarchy");
						callHierarchyView = CallHierarchyView.getInstance();
					} catch (PartInitException e) {
						e.printStackTrace();
					}
				}
				callHierarchyView.setEditor(editor);
				callHierarchyView.setInput(new CallHierarchyGenerator(offset, model));
				callHierarchyView.setFocus();
			}

		}
		return null;
	}
}