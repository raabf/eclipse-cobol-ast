package de.tum.in.www2.cobol.commands;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import de.tum.in.www2.cobol.editors.CobolEditor;
import de.tum.in.www2.cobol.model.Model;
import de.tum.in.www2.cobol.model.references.OccurenceByOffsetComparator;
import de.tum.in.www2.cobol.model.references.Occurrence;

public class RenameSymbolHandler extends AbstractHandler {
	CobolEditor editor;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		editor = (CobolEditor) HandlerUtil.getActiveEditorChecked(event);
		ITextSelection textSelection = (ITextSelection) editor.getSite().getSelectionProvider().getSelection();
		int offset = textSelection.getOffset();

		Model model = editor.getModel();

		Occurrence occurrence = model.findOccurrence(offset, 0);

		if (occurrence != null) {
			String oldName = occurrence.getDisplayName();
			List<Occurrence> all = occurrence.getAllOccurences();
			Comparator<Occurrence> comparator = new OccurenceByOffsetComparator();
			Collections.sort(all, comparator);
			Shell activeShell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
			RenameDialog dialog = new RenameDialog(oldName, activeShell);
			dialog.create();
			if (dialog.open() == Window.OK) {
				String newName = dialog.getNewName();
				IDocument doc = editor.getDocument();
				int lengthOld = occurrence.getPosition().getLength();
				editor.beginCompoundChange();
				for (Occurrence occ : all) {
					offset = occ.getPosition().getOffset();
					try {
						doc.replace(offset, lengthOld, newName);
					} catch (BadLocationException e) {
						e.printStackTrace();
					}
				}
				editor.endCompoundChange();
			}

		} else {
			System.out.println("Can't be renamed");
			return null;
		}
		// TODO finish method
		return null;
	}

	static class RenameDialog extends TitleAreaDialog {
		private Text newNameControl;
		private String oldName;
		private String newName;

		public String getNewName() {
			return newName;
		}

		public RenameDialog(String oldName, Shell parentShell) {
			super(parentShell);
			this.oldName = oldName;
		}

		@Override
		public void create() {
			super.create();
			setTitle("Rename Symbol");
			setMessage("Rename Terminals and Non-Terminals ...", IMessageProvider.INFORMATION);
		}

		@Override
		protected void okPressed() {
			newName = newNameControl.getText();
			super.okPressed();
		}

		@Override
		protected Control createDialogArea(Composite parent) {
			Composite area = (Composite) super.createDialogArea(parent);
			Composite container = new Composite(area, SWT.NONE);
			container.setLayoutData(new GridData(GridData.FILL_BOTH));
			GridLayout layout = new GridLayout(2, false);
			container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			container.setLayout(layout);

			Label labelOldName = new Label(container, SWT.NONE);
			labelOldName.setText("Old name:");

			GridData dataOldName = new GridData();
			dataOldName.grabExcessHorizontalSpace = true;
			dataOldName.horizontalAlignment = GridData.FILL;

			Label dummy = new Label(container, SWT.BORDER);
			dummy.setText(oldName);
			dummy.setLayoutData(dataOldName);

			Label labelNewName = new Label(container, SWT.NONE);
			labelNewName.setText("New name:");

			GridData dataNewName = new GridData();
			dataNewName.grabExcessHorizontalSpace = true;
			dataNewName.horizontalAlignment = GridData.FILL;

			newNameControl = new Text(container, SWT.BORDER);
			newNameControl.setLayoutData(dataNewName);
			newNameControl.setText(oldName);
			newNameControl.setSelection(0, oldName.length());

			return area;
		}

		@Override
		protected boolean isResizable() {
			return true;
		}

	}

}
