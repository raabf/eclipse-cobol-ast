package de.tum.in.www2.cobol.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import parser.analysis.base.structure.representation.IStructureRepresentation;
import de.tum.in.www2.cobol.editors.CobolEditor;
import de.tum.in.www2.cobol.model.Model;
import de.tum.in.www2.cobol.model.references.Declaration;
import de.tum.in.www2.cobol.model.references.Occurrence;

public class OpenReferenceHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		CobolEditor editor = (CobolEditor) HandlerUtil.getActiveEditorChecked(event);
		ITextSelection textSelection = (ITextSelection) editor.getSite().getSelectionProvider().getSelection();
		int offset = textSelection.getOffset();

		Model model = editor.getModel();
		Occurrence occurrence = model.findOccurrence(offset, 0);

		if (occurrence != null) {
			Declaration declaration = occurrence.getDeclaration();
			IStructureRepresentation structureRepresentation = declaration.getStructureRepresentation();
			editor.getSelectionProvider().setSelection(
					new TextSelection(structureRepresentation.getStartOffset(), structureRepresentation.getStartToken()
							.getText().length()));
		}
		return null;
	}

}
