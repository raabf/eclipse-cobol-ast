package de.tum.in.www2.cobol.commands;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Collection;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.part.FileEditorInput;

import convex.step2.parser.ParseException;
import convex.step2.parser.SimpleNode;
import de.tum.in.cobol.commontypes.instructions.Instruction;
import de.tum.in.cobol.controlflow.ControlFlowGraph;
import de.tum.in.cobol.controlflow.ControlFlowGraphBuilder;
import de.tum.in.cobol.controlflow.exporters.DotExporter;
import de.tum.in.www2.cobol.ast2minicobol.AST2MiniCobolVisitor;
import de.tum.in.www2.cobol.editors.CobolEditor;
import parser.analysis.cobol.CobolAnalysisWorkspace;
import parser.errorhandling.ILowLevelErrorHandler;
import parser.errorhandling.Message;

public class OpenControlflowGraphHandler extends AbstractHandler {

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        CobolEditor editor = (CobolEditor) HandlerUtil.getActiveEditorChecked(event);
        File file = ((FileEditorInput) editor.getEditorInput()).getFile().getLocation().toFile();
        try {
            CobolAnalysisWorkspace workspace = new CobolAnalysisWorkspace(new ILowLevelErrorHandler() {
                @Override
                public void onWarning(SimpleNode node, Message warning) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onError(ParseException e) {
                    // TODO Auto-generated method stub

                }
            });

            SimpleNode root = (SimpleNode) workspace.getAST(file);

            AST2MiniCobolVisitor visitor = new AST2MiniCobolVisitor();

            root.childrenAccept(visitor, null);

            Collection<Instruction> instructions = visitor.getInstructions();
            instructions.stream().forEach(System.out::println);
            
            ControlFlowGraphBuilder builder = new ControlFlowGraphBuilder();

            ControlFlowGraph controlFlowGraph = builder.build(instructions);
            
            DotExporter exporter = new DotExporter();
            String dotGraph = exporter.export(controlFlowGraph);

            String homePath = System.getProperty("user.home");
            String filePath = Paths.get(homePath, "Desktop", "dot-graph.txt").toString();

            try (PrintWriter fileWriter = new PrintWriter(filePath)) {
                fileWriter.println(dotGraph);
            } catch (FileNotFoundException e) {
                System.out.print("Couldn't write to file ");
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return null;
    }
}
