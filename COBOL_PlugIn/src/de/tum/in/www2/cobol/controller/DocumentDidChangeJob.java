package de.tum.in.www2.cobol.controller;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.conqat.lib.scanner.ELanguage;
import org.conqat.lib.scanner.ILenientScanner;
import org.conqat.lib.scanner.ScannerFactory;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.part.FileEditorInput;

import parser.CustomTokenManager;
import parser.analysis.cobol.CobolAnalysisFactory;
import parser.analysis.cobol.structure.CobolStructureAnalysis;
import parser.tokenprovider.ExcludeCopybooksTokenProvider;
import parser.tokenprovider.ITokenProvider;
import parser.tokenprovider.filter.AmbigiousTokenFilter;
import parser.tokenprovider.filter.ExecSQLFilter;
import parser.tokenprovider.filter.IgnoredTokensFilter;
import parser.tokenprovider.filter.SpecialTokenHandler;
import convex.step2.parser.CobolParser;
import convex.step2.parser.Node;
import convex.step2.parser.ParseException;
import de.tum.in.www2.cobol.controller.Controller.JobsToDo;
import de.tum.in.www2.cobol.editors.CobolEditor;
import de.tum.in.www2.cobol.editors.GUIErrorReporter;
import de.tum.in.www2.util.cobol.ReaderFactory;

public class DocumentDidChangeJob extends Job {

	boolean running = false;
	CobolEditor editor = null;

	List<DocumentEvent> documentEvents;
	EnumSet<JobsToDo> jobs;
	IDocument document;

	private List<JobStatus> jobStatusList;
	public long revNumber;

	// FIXME move this to somewhere else...
	private CobolAnalysisFactory analysisFactory = new CobolAnalysisFactory();

	public DocumentDidChangeJob(CobolEditor editor) {
		super("" + editor.hashCode());
		this.editor = editor;
		this.jobStatusList = new ArrayList<JobStatus>();
	}

	public boolean running() {
		return running;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		running = true;

		jobStatusList.clear();

		SetupJob setup = new SetupJob(editor, revNumber, this);
		setup.setSystem(true);
		setup.schedule();

		try {
			setup.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (setup.getResult() == Status.CANCEL_STATUS) {
			return Status.CANCEL_STATUS;
		}

		if (editor == null || editor.getDocument() == null) {
			System.err.println("DocumentDidChangeJob.run(): editor or document NULL");
			return Status.CANCEL_STATUS;
		}

		IFile file = ((FileEditorInput) editor.getEditorInput()).getFile();

		if (jobs.contains(JobsToDo.parseCode)) {
			GUIErrorReporter reporter = new GUIErrorReporter(file);
			try {
				ILenientScanner scanner = ScannerFactory.newLenientScanner(ELanguage.COBOL,
						ReaderFactory.createReader(document, 0, document.getLength()), file.getName());

				ITokenProvider filterChain = new ExcludeCopybooksTokenProvider(scanner);
				filterChain = new ExecSQLFilter(filterChain);
				filterChain = new IgnoredTokensFilter(filterChain);
				filterChain = new AmbigiousTokenFilter(filterChain);
				filterChain = new SpecialTokenHandler(filterChain);
				CustomTokenManager tm = new CustomTokenManager(filterChain);

				CobolParser parser = new CobolParser(tm);
				parser.add_error_handler(reporter);
				parser.CompilationUnit();
				Node ast = parser.getRootNode();

				CobolStructureAnalysis analysis = analysisFactory.getStructureAnalysis();
				analysis.run(ast);

				jobStatusList.add(new JobStatus(JobsToDo.parseCode, false));
				ParserResultJob resultModelJob = new ParserResultJob(editor, analysis, revNumber, reporter);
				resultModelJob.setSystem(true);
				resultModelJob.schedule();

				try {
					resultModelJob.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} catch (ParseException e) {
				reporter.onError(e);
				e.printStackTrace();
			}
			
			editor.checkDocumentForTabs(reporter);
			editor.SpaceBetweenCodeComment(reporter);
			reporter.commitErrors();
			reporter.commitWarnings();
		}

		documentEvents.clear();
		jobs.clear();

		running = false;

		CallbackJob callback = new CallbackJob(editor, jobStatusList);
		callback.setSystem(true);
		callback.schedule();
		return Status.OK_STATUS;
	}
}
