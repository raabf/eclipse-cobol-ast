package de.tum.in.www2.cobol.controller;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.progress.UIJob;

import de.tum.in.www2.cobol.editors.CobolEditor;

public class SetupJob extends UIJob {
	private DocumentDidChangeJob job;
	private CobolEditor editor;
	private long revision;

	public SetupJob(CobolEditor editor, long revision, DocumentDidChangeJob job) {
		super("Parser Setup UI Job");
		this.editor = editor;
		this.revision = revision;
		this.job = job;
	}

	@Override
	public IStatus runInUIThread(IProgressMonitor monitor) {
		Controller controller = Controller.getInstance(editor);

		if (job.documentEvents == null || job.documentEvents.isEmpty()) {
			job.documentEvents = controller.popAllDocumentEvents();
		} else {
			job.documentEvents.addAll(controller.popAllDocumentEvents());
		}

		if (job.jobs == null || job.jobs.isEmpty()) {
			job.jobs = controller.popJobsToDo();
		} else {
			job.jobs.addAll(controller.popJobsToDo());
		}

		if (editor == null || editor.hasBeenDisposed()) {
			return Status.CANCEL_STATUS;
		}

		IDocument document = editor.getDocument();

		// TODO cancel if wrong revision

		job.document = document;

		return Status.OK_STATUS;
	}

}
