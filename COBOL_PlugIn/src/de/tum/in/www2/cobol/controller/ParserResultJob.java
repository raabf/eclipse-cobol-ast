package de.tum.in.www2.cobol.controller;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.progress.UIJob;

import parser.analysis.cobol.structure.CobolStructureAnalysis;
import parser.errorhandling.ILowLevelErrorHandler;
import de.tum.in.www2.cobol.editors.CobolEditor;
import de.tum.in.www2.cobol.model.Model;

public class ParserResultJob extends UIJob {
	private CobolEditor editor;
	private CobolStructureAnalysis parserResult;
	private long parserModelRevisionNumber;
	private ILowLevelErrorHandler errorReporter;

	public ParserResultJob(CobolEditor editor, CobolStructureAnalysis parserResult, long revisionNumber,
			ILowLevelErrorHandler errorReporter) {
		super("Parser Result UI Job");
		this.editor = editor;
		this.parserResult = parserResult;
		parserModelRevisionNumber = revisionNumber;
		this.errorReporter = errorReporter;
	}

	@Override
	public IStatus runInUIThread(IProgressMonitor monitor) {
		if (editor == null || editor.hasBeenDisposed()) {
			return Status.CANCEL_STATUS;
		}

		IDocument document = editor.getDocument();
		if (document != null) {
			Model model = Model.getInstanceForDocument(document);
			model.setStructureAnalysis(parserResult, errorReporter);
			return Status.OK_STATUS;
		}
		return Status.CANCEL_STATUS;
	}
}
