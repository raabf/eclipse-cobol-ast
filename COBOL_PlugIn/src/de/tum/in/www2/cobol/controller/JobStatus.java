package de.tum.in.www2.cobol.controller;

import de.tum.in.www2.cobol.controller.Controller.JobsToDo;

public class JobStatus {
	private boolean failed;
	private String message;
	private JobsToDo affectedJob;

	public JobStatus(boolean failed) {
		this.failed = failed;
	}

	public JobStatus(JobsToDo affectedJob, boolean failed) {
		this(affectedJob, failed, null);
	}

	public JobStatus(JobsToDo affectedJob, boolean failed, String message) {
		this.affectedJob = affectedJob;
		this.failed = failed;
		this.message = message;
	}

	public boolean hasFailed() {
		return failed;
	}

	public String getMessage() {
		return message;
	}

	public JobsToDo getAffectedJob() {
		return affectedJob;
	}
}
