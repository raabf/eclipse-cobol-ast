package de.tum.in.www2.cobol.controller;

import java.util.EnumSet;

import de.tum.in.www2.cobol.controller.Controller.JobsToDo;

public interface IRegisterForControllerChanges {

	EnumSet<JobsToDo> getRequiredJobs();

	void jobStatusChanged(JobStatus status);
}
