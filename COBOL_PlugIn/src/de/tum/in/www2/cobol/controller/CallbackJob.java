package de.tum.in.www2.cobol.controller;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.progress.UIJob;

import de.tum.in.www2.cobol.editors.CobolEditor;

class CallbackJob extends UIJob {
	private CobolEditor editor = null;
	private List<JobStatus> jobStatusList;

	public CallbackJob(CobolEditor editor, List<JobStatus> jobStatusList) {
		super("" + editor.hashCode());
		this.editor = editor;
		this.jobStatusList = jobStatusList;
	}

	@Override
	public IStatus runInUIThread(IProgressMonitor monitor) {
		for (JobStatus status : jobStatusList) {
			Controller.getInstance(editor).notifyObserversOfJobStatus(status);
		}

		if (editor == null || editor.hasBeenDisposed()) {
			return Status.CANCEL_STATUS;
		}

		Controller.getInstance(editor).notifyChangeJobFinished();
		return Status.OK_STATUS;
	}
}