package de.tum.in.www2.cobol.controller;

import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITypedRegion;

import de.tum.in.www2.cobol.editors.CobolEditor;

public class Controller {

	public static enum JobsToDo {
		doNothing, parseCode
	};

	private static WeakHashMap<CobolEditor, Controller> instances = new WeakHashMap<CobolEditor, Controller>();

	public static Controller getInstance(CobolEditor editor) {
		if (editor == null) {
			throw new RuntimeException("editor must not be null.");
		}
		Controller instance = instances.get(editor);
		if (instance != null) {
			return instance;
		} else {
			instances.put(editor, new Controller(editor));
			return instances.get(editor);
		}
	}

	private CobolEditor editor;
	private List<DocumentEvent> documentEvents;
	private DocumentDidChangeJob job = null;

	private Controller(CobolEditor editor) {
		this.editor = editor;
		job = new DocumentDidChangeJob(editor);
		job.setSystem(true);
		addJobToDo(JobsToDo.parseCode);
		job.schedule();
		documentEvents = new LinkedList<DocumentEvent>();
	}

	private Set<IRegisterForControllerChanges> controllerObservers = Collections
			.newSetFromMap(new WeakHashMap<IRegisterForControllerChanges, Boolean>());
	private final EnumSet<JobsToDo> jobElements = EnumSet.noneOf(JobsToDo.class);

	/**
	 * Request a run of all necessary jobs, this method returns true if the jobs
	 * will run and false if no job is required because the actual model
	 * revision has the same number as the document revision
	 *
	 * @return if the job will ever run
	 */
	public boolean requestJobRun() {
		return true;
	}

	private static final int SET_CHANGED_AFTER_SECONDS = 1;

	private void runJobs(IDocument document) {
		if (job.getState() == Job.NONE || (!job.running() && job.getState() == Job.SLEEPING)) {
			job.cancel();

			// job.revNumber = RevisionManager.increment(document);

			job.schedule(SET_CHANGED_AFTER_SECONDS * 1000);
		} else {
			// RevisionManager.increment(document);
			changeJobReRunRequested = System.currentTimeMillis();
		}
	}

	private void addJobToDo(JobsToDo job) {
		synchronized (jobElements) {
			jobElements.add(job);
		}
	}

	// if the changeJob takes some time, it's good to know if it has to be
	// reexecuted after it finishes
	private long changeJobReRunRequested = 0;
	private final Object lock = new Object();

	/*
	 * notify callback, that the changeJob did finish
	 */
	public void notifyChangeJobFinished() {
		synchronized (lock) {
		}
	}

	public void notifyObserversOfJobStatus(JobStatus status) {
		synchronized (controllerObservers) {
			for (IRegisterForControllerChanges observer : controllerObservers) {
				observer.jobStatusChanged(status);
			}
		}
	}

	public EnumSet<JobsToDo> popJobsToDo() {
		synchronized (jobElements) {
			final EnumSet<JobsToDo> popJobs = EnumSet.noneOf(JobsToDo.class);
			popJobs.addAll(jobElements);
			jobElements.clear();
			return popJobs;
		}
	}

	public List<DocumentEvent> popAllDocumentEvents() {
		synchronized (documentEvents) {
			List<DocumentEvent> popList = this.documentEvents;
			this.documentEvents = new LinkedList<DocumentEvent>();
			return popList;
		}
	}

	public void registerObserver(IRegisterForControllerChanges observer) {
		synchronized (controllerObservers) {
			controllerObservers.add(observer);
		}
	}

	public void unregisterObserver(IRegisterForControllerChanges observer) {
		synchronized (controllerObservers) {
			controllerObservers.remove(observer);
		}
	}

	/*
	 * notify method that's to be called, when a change in the document happens
	 */
	public void notifyChange(DocumentEvent event) {
		synchronized (lock) {

			addDocumentEvent(event);

			IDocument document = editor.getDocument();

			// RevisionManager.increment(document);

			EnumSet<JobsToDo> observedJobs = EnumSet.noneOf(JobsToDo.class);
			synchronized (controllerObservers) {
				for (IRegisterForControllerChanges observer : controllerObservers) {
					observedJobs.addAll(observer.getRequiredJobs());
				}
			}
			EnumSet<JobsToDo> possibleJobs = EnumSet.noneOf(JobsToDo.class);
			try {
				ITypedRegion region = document.getPartition(event.getOffset());
				if (event.getText().trim().length() == 0
						&& (document.getLength() <= (event.getOffset() + 1) || document.getChar(event.getOffset() + 1) == (' '))) {
					if (event.getText().equals("")) {
						char beforeDelete = '+'; // DUMMY
						int offset = event.getOffset();
						if (offset > 0) {
							beforeDelete = document.get().charAt(offset - 1);
						}
						possibleJobs.add(JobsToDo.parseCode);
					}
				} else {
					String regionType = region.getType();
					possibleJobs.add(JobsToDo.parseCode);
				}
			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			for (JobsToDo job : possibleJobs) {
				if (observedJobs.contains(job)) {
					addJobToDo(job);
				}
			}

			runJobs(document);
		}
	}

	public void addDocumentEvent(DocumentEvent e) {
		synchronized (documentEvents) {
			this.documentEvents.add(e);
		}
	}
}
