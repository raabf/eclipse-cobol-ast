package de.tum.in.www2.cobol.editors;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.CompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;

import de.tum.in.www2.cobol.model.Model;
import de.tum.in.www2.util.KeyWordConstants;

public class CobolContentAssistProcessor implements IContentAssistProcessor {

	private Model model;

	public CobolContentAssistProcessor(Model instanceForDocument) {
		this.model = instanceForDocument;
	}

	@Override
	public ICompletionProposal[] computeCompletionProposals(ITextViewer viewer, int offset) {
		List<CompletionProposal> completions = new LinkedList<>();
		IDocument document = viewer.getDocument();
		String prefix = lastWord(document, offset);

		List<String> proposals = new LinkedList<String>();

		// add variable names and paragraph and section names
		for (String paragraphSectionId : model.getParagraphSectionIdentifiers()) {
			if (paragraphSectionId.toLowerCase().startsWith(prefix.toLowerCase())) {
				proposals.add(paragraphSectionId);
			}
		}

		List<String> keyWordsWithPrefix = KeyWordConstants.getKeyWordTrie().getWordsWithPrefix(prefix);
		for (String keyWord : keyWordsWithPrefix) {
			proposals.add(keyWord);
		}

		Collections.sort(proposals);

		for (String proposal : proposals) {
			completions.add(new CompletionProposal(proposal, offset - prefix.length(), prefix.length(), proposal
					.length()));
		}

		return completions.toArray(new ICompletionProposal[] {});
	}

	private String lastWord(IDocument doc, int offset) {
		try {
			for (int n = offset - 1; n >= 0; n--) {
				char c = doc.getChar(n);
				if (!Character.isJavaIdentifierPart(c)) {
					return doc.get(n + 1, offset - n - 1);
				}
			}
		} catch (BadLocationException e) {
			// no last word
		}
		return "";
	}

	@Override
	public char[] getCompletionProposalAutoActivationCharacters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IContextInformation[] computeContextInformation(ITextViewer viewer, int offset) {
		return null;
	}

	@Override
	public char[] getContextInformationAutoActivationCharacters() {
		return null;
	}

	@Override
	public String getErrorMessage() {
		return null;
	}

	@Override
	public IContextInformationValidator getContextInformationValidator() {
		return null;
	}

}
