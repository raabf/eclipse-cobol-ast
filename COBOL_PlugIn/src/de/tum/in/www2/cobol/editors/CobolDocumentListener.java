package de.tum.in.www2.cobol.editors;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentListener;

import de.tum.in.www2.cobol.controller.Controller;

public class CobolDocumentListener implements IDocumentListener {

	private CobolEditor editor;

	public CobolDocumentListener(CobolEditor e) {
		this.editor = e;
	}

	private int linesRemoved = 0;
	private int removedLastLineColumns = 0;

	@Override
	public void documentAboutToBeChanged(DocumentEvent event) {
		IDocument document = event.getDocument();
		try {
			int offset = event.getOffset();
			int startLineNumber = document.getLineOfOffset(offset);
			int endOffset = offset + event.getLength();
			int endLineNumber = document.getLineOfOffset(endOffset);
			linesRemoved = endLineNumber - startLineNumber;
			removedLastLineColumns = endOffset - document.getLineOffset(endLineNumber);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		return;
	}

	private void patchModel(DocumentEvent event) {
		IDocument document = event.getDocument();

	}

	@Override
	public void documentChanged(DocumentEvent event) {
		patchModel(event);
		Controller controller = Controller.getInstance(editor);
		controller.notifyChange(event);

	}

}
