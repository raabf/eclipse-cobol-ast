package de.tum.in.www2.cobol.editors;

import java.io.IOException;

import org.conqat.lib.scanner.ELanguage;
import org.conqat.lib.scanner.ILenientScanner;
import org.conqat.lib.scanner.ScannerFactory;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.contentassist.ContentAssistant;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.ITokenScanner;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.source.IAnnotationHover;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;
import org.eclipse.swt.SWT;

import de.tum.in.www2.cobol.model.Model;
import de.tum.in.www2.util.cobol.ReaderFactory;

public class CobolSourceViewerConfiguration extends SourceViewerConfiguration {

	private CobolEditor editor;
	private IAnnotationHover hover;

	public CobolSourceViewerConfiguration(CobolEditor cobolEditor) {
		this.editor = cobolEditor;
	}

	@Override
	public PresentationReconciler getPresentationReconciler(ISourceViewer sourceViewer) {
		PresentationReconciler pr = new PresentationReconciler();
		DefaultDamagerRepairer ddr = new CobolDamageRepairer(new TokenScanner());
		pr.setRepairer(ddr, IDocument.DEFAULT_CONTENT_TYPE);
		pr.setDamager(ddr, IDocument.DEFAULT_CONTENT_TYPE);

		return pr;
	}

	static class TokenScanner implements ITokenScanner {

		private ILenientScanner scanner;
		private org.conqat.lib.scanner.IToken lastToken;
		private int offset = 0;

		@Override
		public void setRange(IDocument document, int offset, int length) {
			// TODO: add origin Id?
			this.offset = offset;
			scanner = ScannerFactory.newLenientScanner(ELanguage.COBOL,
					ReaderFactory.createReader(document, offset, length), "");
		}

		static final Token keyword = new Token(new TextAttribute(CobolEditor.KEYWORD, null, SWT.BOLD));
		static final Token division = new Token(new TextAttribute(CobolEditor.DIVISION, null, SWT.BOLD));
		static final Token literal = new Token(new TextAttribute(CobolEditor.STRING));
		static final Token section = new Token(new TextAttribute(CobolEditor.SECTION));
		static final Token comment = new Token(new TextAttribute(CobolEditor.COMMENT));
		static final Token identifier = new Token(new TextAttribute(CobolEditor.IDENTIFIER));
		static final Token operator = new Token(new TextAttribute(CobolEditor.OPERATOR));

		@Override
		public IToken nextToken() {
			try {
				lastToken = scanner.getNextToken();
			} catch (IOException e) {
				lastToken = null;
				e.printStackTrace();
				return Token.EOF;
			}

			// TODO: test!
			IToken returnedToken;
			if (lastToken.getType().isKeyword()) return keyword;
			if (lastToken.getType().isLiteral()) return  literal;
			if (lastToken.getType().isOperator()) return operator;
			
			switch (lastToken.getType()) {
			case EOF:
				returnedToken = Token.EOF;
				break;
			case DIVISION:
				returnedToken = division;
				break;
			case IDENTIFICATION:
				returnedToken = division;
				break;
			case ENVIRONMENT:
				returnedToken = division;
				break;
			case DATA:
				returnedToken = division;
				break;
			case PROCEDURE:
				returnedToken = division;
				break;
			case SECTION:
				returnedToken = section;
				break;
			case CONFIGURATION:
				returnedToken = section;
				break;
			case WORKING_STORAGE:
				returnedToken = section;
				break;
			case LINKAGE:
				returnedToken = section;
				break;
			case IDENTIFIER:
				returnedToken = identifier;
				break;
			case TRADITIONAL_COMMENT:
				returnedToken = comment;
				break;
			default:
				returnedToken = Token.UNDEFINED;
				break;
			}
			return returnedToken;
		}

		@Override
		public int getTokenOffset() {
			return offset + lastToken.getOffset();
		}

		@Override
		public int getTokenLength() {
			return lastToken.getText().length();
		}

	}

	@Override
	public IAnnotationHover getAnnotationHover(ISourceViewer sourceViewer) {
		return new CobolAnnotationHover();
	}

	@Override
	public IContentAssistant getContentAssistant(ISourceViewer sourceViewer) {
		ContentAssistant assistant = new ContentAssistant();
		IContentAssistProcessor processor = new CobolContentAssistProcessor(Model.getInstanceForDocument(editor
				.getDocument()));
		assistant.setContentAssistProcessor(processor, IDocument.DEFAULT_CONTENT_TYPE);
		assistant.setAutoActivationDelay(500);
		assistant.enableAutoActivation(true);

		assistant.setInformationControlCreator(getInformationControlCreator(sourceViewer));

		return assistant;
	}

	@Override
	public ITextHover getTextHover(ISourceViewer sv, String contentType) {
		return new TextHover(editor.getModel());
	}

}
