package de.tum.in.www2.cobol.editors;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.IVerticalRuler;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.text.source.projection.ProjectionAnnotation;
import org.eclipse.jface.text.source.projection.ProjectionAnnotationModel;
import org.eclipse.jface.text.source.projection.ProjectionSupport;
import org.eclipse.jface.text.source.projection.ProjectionViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.SourceViewerDecorationSupport;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

import parser.analysis.base.structure.representation.IStructureRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentation;
import de.tum.in.www2.cobol.controller.Controller;
import de.tum.in.www2.cobol.controller.Controller.JobsToDo;
import de.tum.in.www2.cobol.controller.IRegisterForControllerChanges;
import de.tum.in.www2.cobol.controller.JobStatus;
import de.tum.in.www2.cobol.editors.views.CobolOutlinePage;
import de.tum.in.www2.cobol.model.ICobolParserChangeObserver;
import de.tum.in.www2.cobol.model.Model;
import de.tum.in.www2.cobol.model.references.Declaration;
import de.tum.in.www2.cobol.model.references.Occurrence;
import de.tum.in.www2.cobol.model.references.Reference;

public class CobolEditor extends TextEditor implements ICobolParserChangeObserver, IRegisterForControllerChanges {
	private final class ModifiedClickHandler implements KeyListener, MouseListener {
		private boolean ctrl = false;
		private Cursor oldCursor;

		@Override
		public void keyReleased(KeyEvent e) {
			if (e.keyCode == SWT.MOD1) {
				ctrl = false;
				CobolEditor editor = CobolEditor.this;
				editor.getSite().getShell().setCursor(oldCursor);
			}
		}

		@Override
		public void keyPressed(KeyEvent e) {
			if (e.keyCode == SWT.MOD1) {
				ctrl = true;
				Shell shell = getSite().getShell();
				oldCursor = shell.getCursor();
				Cursor cursor = new Cursor(Display.getCurrent(), SWT.CURSOR_HAND);
				shell.setCursor(cursor);
			}
		}

		@Override
		public void mouseDoubleClick(MouseEvent e) {
		}

		@Override
		public void mouseDown(MouseEvent e) {
		}

		@Override
		public void mouseUp(MouseEvent e) {
			if (ctrl) {
				CobolEditor editor = CobolEditor.this;
				ITextSelection textSelection = (ITextSelection) editor.getSite().getSelectionProvider().getSelection();
				int offset = textSelection.getOffset();

				Model model = editor.getModel();
				Occurrence occurrence = model.findOccurrence(offset, 0);

				if (occurrence != null) {
					Declaration declaration = occurrence.getDeclaration();
					IStructureRepresentation structureRepresentation = declaration.getStructureRepresentation();
					editor.getSelectionProvider().setSelection(
							new TextSelection(structureRepresentation.getStartOffset(), structureRepresentation
									.getStartToken().getText().length()));
				}
			}
		}
	}

	public static final String READ_OCCURRENCE = "org.eclipse.jdt.ui.occurrences";
	public static final String WRITE_OCCURRENCE = "org.eclipse.jdt.ui.occurrences.write";

	private boolean hasRequestedPostSaveSyntaxRefresh;
	private boolean disposed = false;

	private ProjectionAnnotationModel annotationModel;
	private Annotation[] oldAnnotations;

	private static Color gray;
	private static Color blue;
	private static Color brightBlue;
	private static Color green;
	private static Color orange;
	private static Color purple;
	private static Color blueGreen;
	private static Color red;
	private static Color pink;
	private static Color brown;

	static {
		Display display = Display.getCurrent();

		gray = new Color(display, 150, 150, 150);
		blue = new Color(display, 53, 87, 197);
		brightBlue = new Color(display, 38, 99, 236);
		green = new Color(display, 44, 173, 57);
		orange = new Color(display, 220, 150, 50);
		purple = new Color(display, 177, 33, 114);
		blueGreen = new Color(display, 43, 126, 136);
		red = new Color(display, 255, 0, 0);
		pink = new Color(display, 255, 0, 255);
		brown = new Color(display, 132, 90, 49);
	}

	public static final Color HYPERLINK = blue;

	public static final Color KEYWORD = blue;
	public static final Color DIVISION = blue;
	public static final Color COMMENT = gray;
	public static final Color TERMINAL = orange;
	public static final Color NONTERMINAL = green;
	public static final Color SPECIAL_ERROR_TERMINAL = purple;
	public static final Color SECTION = red;
	public static final Color STRING = red;
	public static final Color IDENTIFIER = pink;
	public static final Color OPERATOR = brown;

	public CobolEditor() {
		super();
		setSourceViewerConfiguration(new CobolSourceViewerConfiguration(this));
	}

	public void beginCompoundChange() {
		((SourceViewer) getSourceViewer()).getUndoManager().beginCompoundChange();
	}

	public void endCompoundChange() {
		((SourceViewer) getSourceViewer()).getUndoManager().endCompoundChange();
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		super.init(site, input);
		getModel().registerModelObserver(this);
		Controller.getInstance(this).registerObserver(this);

		IDocumentProvider dp = getDocumentProvider();
		if (dp == null) {
			return;
		}
		IDocument doc = dp.getDocument(getEditorInput());
		if (doc == null) {
			return;
		}
		doc.addDocumentListener(new CobolDocumentListener(this));
	}

	/**
	 * Make Line numbers visible
	 */
	@Override
	protected boolean isLineNumberRulerVisible() {
		return true;
	}

	@Override
	protected boolean isOverviewRulerVisible() {
		return true;
	}

	@Override
	public void modelChanged(Model model) {
		updateFoldingStructure(model);
		if (hasRequestedPostSaveSyntaxRefresh) {
			getSourceViewer().invalidateTextPresentation();
			hasRequestedPostSaveSyntaxRefresh = false;
		}
	}

	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		ProjectionViewer viewer = (ProjectionViewer) getSourceViewer();
		ModifiedClickHandler listener = new ModifiedClickHandler();
		viewer.getTextWidget().addKeyListener(listener);
		viewer.getTextWidget().addMouseListener(listener);
		ProjectionSupport projectionSupport = new ProjectionSupport(viewer, getAnnotationAccess(), getSharedColors());
		projectionSupport.install();
		viewer.doOperation(ProjectionViewer.TOGGLE);
		annotationModel = viewer.getProjectionAnnotationModel();
	}

	private void updateFoldingStructure(Model model) {
		List<CobolStructureRepresentation> foldables = model.getFoldables();

		Annotation[] annotations = new Annotation[foldables.size()];
		Map<Annotation, Position> newAnnotations = new HashMap<>();

		for (int i = 0; i < foldables.size(); i++) {
			CobolStructureRepresentation foldable = foldables.get(i);
			ProjectionAnnotation annotation = new ProjectionAnnotation();
			int end = foldable.getEndOffset();
			int start = foldable.getStartOffset();
			newAnnotations.put(annotation, new Position(start, end - start));
			annotations[i] = annotation;
		}

		annotationModel.modifyAnnotations(oldAnnotations, newAnnotations, null);
		oldAnnotations = annotations;
	}

	public Model getModel() {
		return Model.getInstanceForDocument(getDocument());
	}

	public IDocument getDocument() {
		IDocumentProvider provider = getDocumentProvider();
		if (provider != null) {
			return provider.getDocument(getEditorInput());
		}

		// This exception is here on purpose, because it should never
		// happen. If it does, this indicates a bug elsewhere.
		// Please fix that bug instead and do not remove this.
		throw new RuntimeException("IDocumentProvider is null!");
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		super.doSave(monitor);
		// TODO: implement (after having the AST in the model
	}

	public boolean hasBeenDisposed() {
		return disposed;
	}

	@Override
	public void dispose() {
		super.dispose();
		disposed = true;
	}

	@Override
	public EnumSet<JobsToDo> getRequiredJobs() {
		return EnumSet.of(JobsToDo.parseCode);
	}

	@Override
	public void jobStatusChanged(JobStatus status) {
		// TODO Auto-generated method stub
	}

	/**
	 * adding outline view
	 */
	private CobolOutlinePage contentOutlinePage;

	@Override
	public Object getAdapter(Class required) {
		//System.out.println(required);
		if (IContentOutlinePage.class.equals(required)) {
			if (contentOutlinePage == null) {
				contentOutlinePage = new CobolOutlinePage(this);
			}
			return contentOutlinePage;
		}
		return super.getAdapter(required);
	}

	@Override
	protected SourceViewerDecorationSupport getSourceViewerDecorationSupport(ISourceViewer viewer) {
		if (fSourceViewerDecorationSupport == null) {
			fSourceViewerDecorationSupport = new CobolSourceViewerDecorationSupport(viewer, getOverviewRuler(),
					getAnnotationAccess(), getSharedColors());
			configureSourceViewerDecorationSupport(fSourceViewerDecorationSupport);
		}
		return fSourceViewerDecorationSupport;
	}

	@Override
	protected ISourceViewer createSourceViewer(Composite parent, IVerticalRuler ruler, int styles) {
		ProjectionViewer viewer = new ProjectionViewer(parent, ruler, getOverviewRuler(), isOverviewRulerVisible(),
				styles);
		getSourceViewerDecorationSupport(viewer);
		return viewer;
	}

	@Override
	protected void handleCursorPositionChanged() {
		super.handleCursorPositionChanged();
		ITextSelection selection = (ITextSelection) getSelectionProvider().getSelection();

		clearReferenceMarkers();

		IStructureRepresentation structure = getModel().findStructureRepresentation(selection.getOffset(),
				selection.getLength());
		if (structure != null) {
			contentOutlinePage.setSelection(structure);
			markStructureRepresentation(structure);
		}

		Occurrence occurrence = getModel().findOccurrence(selection.getOffset(), selection.getLength());
		if (occurrence != null) {
			contentOutlinePage.setSelection(occurrence.getDeclaration().getStructureRepresentation());
			markReferences(occurrence.getDeclaration());
		}
	}

	public void markStructureRepresentation(IStructureRepresentation representation) {
		IAnnotationModel annotations = getDocumentProvider().getAnnotationModel(getEditorInput());

		annotations.addAnnotation(new Annotation(WRITE_OCCURRENCE, false, ""), createPosition(representation));
	}

	public void markReferences(Declaration declaration) {
		IAnnotationModel annotations = getDocumentProvider().getAnnotationModel(getEditorInput());

		annotations.addAnnotation(new Annotation(WRITE_OCCURRENCE, false, ""),
				createPosition(declaration.getStructureRepresentation()));
		for (Reference reference : declaration.getReferences()) {
			annotations.addAnnotation(reference.createAnnotation(), reference.getPosition());
		}
	}

	public void clearReferenceMarkers() {
		IAnnotationModel annotations = getDocumentProvider().getAnnotationModel(getEditorInput());

		Iterator iterator = annotations.getAnnotationIterator();
		Set<Annotation> toRemove = new HashSet<Annotation>();
		while (iterator.hasNext()) {
			Annotation next = (Annotation) iterator.next();
			if (next.getType().equals(READ_OCCURRENCE) || next.getType().equals(WRITE_OCCURRENCE)) {
				toRemove.add(next);
			}
		}

		for (Annotation annotation : toRemove) {
			annotations.removeAnnotation(annotation);
		}
	}

	private Position createPosition(IStructureRepresentation representation) {
		return new Position(representation.getStartToken().getOffset(), representation.getStartToken().getText()
				.length());
	}
	
	public void checkDocumentForTabs(GUIErrorReporter reporter)
	{
		IDocument document = this.getDocument();
		String text = document.get() + '\n';
		
		if(text.contains("\t"))
		{
			String[] list = text.split("\n");
			int offset = 0;
			for(int i =0; i<list.length; i++)
			{
				
				int sentenceLen = list[i].length();
				if(sentenceLen <= 0)
				{	
					offset += list[i].length() + 1;
					continue;
				}
				
				int column = 0;
				String temp = list[i].substring(column, sentenceLen);
				
				while(temp.contains("\t"))
				{
					column = temp.indexOf("\t", column);
					temp = temp.replaceFirst("\t", "t");
					reporter.onWarning(i, column, offset + column, 1, "This line contains a tab character");
				}
				
				offset += list[i].length() + 1;
			}
		}
	}
	
	public void SpaceBetweenCodeComment(GUIErrorReporter reporter)
	{
		IDocument document = this.getDocument();
		String text = document.get() + '\n';
		
		int offset=0;
		String[] list = text.split("\n");
		for(int i =0; i<list.length; i++)
		{
			int length = list[i].length()+1;
			if(length > 73 && list[i].charAt(71) != ' ' && list[i].charAt(72) != ' ')
				reporter.onWarning(i, offset + 59, offset +70, 4, "No space between code and comment. Possible error.");
			
			offset += length;
		}
	}
}
