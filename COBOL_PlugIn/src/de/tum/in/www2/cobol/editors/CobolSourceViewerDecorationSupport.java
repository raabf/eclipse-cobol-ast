package de.tum.in.www2.cobol.editors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.ITextViewerExtension2;
import org.eclipse.jface.text.MarginPainter;
import org.eclipse.jface.text.source.IAnnotationAccess;
import org.eclipse.jface.text.source.IOverviewRuler;
import org.eclipse.jface.text.source.ISharedTextColors;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.texteditor.SourceViewerDecorationSupport;

public class CobolSourceViewerDecorationSupport extends SourceViewerDecorationSupport {

	private ISourceViewer sourceViewer = null;

	private List<MarginPainter> marginPainters = new ArrayList<MarginPainter>();

	private List<Integer> rulerColumns = Arrays.asList(6, 7, 72, 80);
	//6, 7, 11, 72, 80
	public CobolSourceViewerDecorationSupport(ISourceViewer sourceViewer, IOverviewRuler overviewRuler,
			IAnnotationAccess annotationAccess, ISharedTextColors sharedTextColors) {
		super(sourceViewer, overviewRuler, annotationAccess, sharedTextColors);
		this.sourceViewer = sourceViewer;
	}

	@Override
	public void dispose() {
		super.dispose();
		clearMargins();
	}

	@Override
	public void install(IPreferenceStore store) {
		super.install(store);
		showMargins();
	}

	private void showMargins() {
		if (marginPainters != null && marginPainters.size() != 0) {
			return;
		}
		if (sourceViewer instanceof ITextViewerExtension2) {
			for (Integer column : rulerColumns) {

				MarginPainter marginPainter = new MarginPainter(sourceViewer);
				marginPainter = new MarginPainter(sourceViewer);

				Display display = Display.getCurrent();
				marginPainter.setMarginRulerColor(new Color(display, 200, 200, 200));

				marginPainter.setMarginRulerWidth(0);
				marginPainter.setMarginRulerColumn(column);
				marginPainter.setMarginRulerStyle(SWT.LINE_SOLID);
				ITextViewerExtension2 extension = (ITextViewerExtension2) sourceViewer;
				extension.addPainter(marginPainter);
				marginPainters.add(marginPainter);
			}
		}
	}

	private void clearMargins() {
		if (marginPainters != null && marginPainters.size() != 0) {
			marginPainters.clear();
			marginPainters = new ArrayList<MarginPainter>();
		}
	}
}
