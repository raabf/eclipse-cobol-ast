package de.tum.in.www2.cobol.editors;

import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.TextPresentation;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.ITokenScanner;
import org.eclipse.jface.text.rules.Token;

public class CobolDamageRepairer extends DefaultDamagerRepairer {

	public CobolDamageRepairer(ITokenScanner scanner) {
		super(scanner);
	}

	@Override
	public void createPresentation(TextPresentation presentation, ITypedRegion region) {

		int lastStart = region.getOffset();
		int length = 0;
		boolean firstToken = true;
		IToken lastToken = Token.UNDEFINED;
		TextAttribute lastAttribute = getTokenTextAttribute(lastToken);

		fScanner.setRange(fDocument, lastStart, region.getLength());

		while (true) {
			IToken token = fScanner.nextToken();
			if (token.isEOF()) {
				break;
			}

			TextAttribute attribute = getTokenTextAttribute(token);
			if (!firstToken) {
				addRange(presentation, lastStart, length, lastAttribute);
			}
			firstToken = false;
			lastToken = token;
			lastAttribute = attribute;
			lastStart = fScanner.getTokenOffset();
			length = fScanner.getTokenLength();
		}

		addRange(presentation, lastStart, length, lastAttribute);
	}

}
