package de.tum.in.www2.cobol.editors;


import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Region;
import parser.analysis.base.structure.representation.IStructureRepresentation;
import de.tum.in.www2.cobol.model.Model;
import de.tum.in.www2.cobol.model.references.Occurrence;

public class TextHover implements ITextHover {

	private Model model;

	public TextHover(Model model) {
		this.model = model;
	}

	@Override
	public IRegion getHoverRegion(ITextViewer textViewer, int offset) {
		return new Region(offset, 0);
	}

	@Override
	public String getHoverInfo(ITextViewer textViewer, IRegion region) {

		int offset = region.getOffset();
		
		Occurrence declaration = model.findOccurrence(offset, 0);

		if (declaration != null) {
			IStructureRepresentation structureRepresentation = model.findOccurrence(offset, 0).getDeclaration()
					.getStructureRepresentation();

			if (structureRepresentation != null) {
				try {
					IDocument document = textViewer.getDocument();

					int lineOfOffsetFirstToken = document.getLineOfOffset(structureRepresentation.getStartOffset());
					int offsetOfLineFirstToken = document.getLineOffset(lineOfOffsetFirstToken);
					int lineOfOffsetEndToken = document.getLineOfOffset(structureRepresentation.getEndOffset());
					int lineNumberWholeDocument = document.getNumberOfLines();
					int offsetOfNextLineEndToken;
					if (lineNumberWholeDocument <= lineOfOffsetEndToken + 1) {
						offsetOfNextLineEndToken = document.getLineOffset(lineOfOffsetEndToken)
								+ document.getLineLength(lineOfOffsetEndToken);
					} else {
						offsetOfNextLineEndToken = document.getLineOffset(lineOfOffsetEndToken + 1);
					}

					Object[] o = structureRepresentation.getScope().getScopePath();
					String pathInfo = "";
					String tabs = "";
					if (o.length > 2)
						for (int i = 1; i < o.length; i++) {
							tabs = tabs + "\t";
							pathInfo = pathInfo + tabs + o[i].toString() + "\n";
						}

					tabs = tabs + "\t";
					String fileText = pathInfo + tabs + document
							.get(offsetOfLineFirstToken, offsetOfNextLineEndToken - offsetOfLineFirstToken).trim();

					return fileText;
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

}