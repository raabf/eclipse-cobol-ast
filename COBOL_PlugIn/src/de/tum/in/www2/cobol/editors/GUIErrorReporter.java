package de.tum.in.www2.cobol.editors;

import java.util.LinkedList;
import java.util.List;

import org.conqat.lib.scanner.ETokenType;
import org.conqat.lib.scanner.IToken;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.widgets.Display;

import parser.errorhandling.ILowLevelErrorHandler;
import parser.errorhandling.Message;
import convex.step2.parser.ParseException;
import convex.step2.parser.SimpleNode;
import convex.step2.parser.Token;

public class GUIErrorReporter implements ILowLevelErrorHandler {
	private static final String COBOL_MARKER = "cobol.problem";

	public static class MarkerMessage {
		public String message;
		public int lineNumber, offset, column, length;

		public MarkerMessage(String message, int lineNumber, int offset, int column, int length) {
			this.message = message;
			this.lineNumber = lineNumber;
			this.offset = offset;
			this.column = column;
			this.length = length;
		}
	}

	private List<MarkerMessage> errors = new LinkedList<>();
	private List<MarkerMessage> warnings = new LinkedList<>();
	private IFile file;

	public void commitErrors() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				IMarker[] oldMarkers;
				try {
					oldMarkers = file.findMarkers(COBOL_MARKER, false, 10);
					for (IMarker m : oldMarkers) {
						if (((int) m.getAttribute(IMarker.SEVERITY)) == IMarker.SEVERITY_ERROR) {
							m.delete();
						}
					}

					for (MarkerMessage markerMessage : errors) {
						IMarker marker;
						marker = file.createMarker(COBOL_MARKER);
						marker.setAttribute(IMarker.TRANSIENT, true);
						marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
						marker.setAttribute(IMarker.MESSAGE, markerMessage.message);

						marker.setAttribute(IMarker.LINE_NUMBER, markerMessage.lineNumber);
						marker.setAttribute(IMarker.CHAR_START, markerMessage.offset);
						marker.setAttribute(IMarker.CHAR_END, markerMessage.offset + markerMessage.length);
					}
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void commitWarnings() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				IMarker[] oldMarkers;
				try {
					oldMarkers = file.findMarkers(COBOL_MARKER, false, 10);
					for (IMarker m : oldMarkers) {
						if (((int) m.getAttribute(IMarker.SEVERITY)) == IMarker.SEVERITY_WARNING) {
							m.delete();
						}
					}

					for (MarkerMessage markerMessage : warnings) {
						IMarker marker;
						marker = file.createMarker(COBOL_MARKER);
						marker.setAttribute(IMarker.TRANSIENT, true);
						marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING);
						marker.setAttribute(IMarker.MESSAGE, markerMessage.message);

						marker.setAttribute(IMarker.LINE_NUMBER, markerMessage.lineNumber);
						marker.setAttribute(IMarker.CHAR_START, markerMessage.offset);
						marker.setAttribute(IMarker.CHAR_END, markerMessage.offset + markerMessage.length);
					}
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public GUIErrorReporter(IFile file) {
		this.file = file;
		errors = new LinkedList<MarkerMessage>();
		warnings = new LinkedList<MarkerMessage>();
	}

	public void clear() {
		errors.clear();
	}

	public void onError(int line, int column, int offset, int length, String message) {
		// line + 1 because our line numbers start with 1 and the line numbers
		// of the exception start with 0
		errors.add(new MarkerMessage(message, line + 1, offset, column, length));
	}

	@Override
	public void onWarning(SimpleNode node, Message warning) {
		Token firstToken = node.jjtGetFirstToken();
		IToken iToken = ((IToken) firstToken.getValue());
		onWarning(firstToken.beginLine, firstToken.beginColumn, iToken.getOffset(), iToken.getText().length(),
				warning.getMessage());

	}

	public void onWarning(int line, int column, int offset, int length, String message) {
		warnings.add(new MarkerMessage(message, line + 1, offset, column, length));
	}

	@Override
	public void onError(ParseException e) {
		Token nextToken = e.currentToken.next;
		while (nextToken != null && ((IToken) nextToken.getValue()).getType() != ETokenType.EOF
				&& ((IToken) nextToken.getValue()).getType() != ETokenType.DOT) {
			nextToken = nextToken.next;
		}

		if (nextToken != null) {
			// underline from the beginning of the parsing error until the next
			// dot, or if there is no dot after the error, underline until the
			// end of the file
			onError(e.currentToken.next.beginLine, e.currentToken.next.beginColumn,
					((IToken) e.currentToken.next.getValue()).getOffset(), ((IToken) nextToken.getValue()).getOffset()
							- ((IToken) e.currentToken.next.getValue()).getOffset(), getErrorMessage(e));
		} else {
			// if the nextToken is null, only underline the token, where the
			// error is
			onError(e.currentToken.next.beginLine, e.currentToken.next.beginColumn,
					((IToken) e.currentToken.next.getValue()).getOffset(), ((IToken) e.currentToken.next.getValue())
							.getText().length(), getErrorMessage(e));
		}
	}

	private String getErrorMessage(ParseException e) {

		StringBuffer expected = new StringBuffer();
		int maxSize = 0;
		// taken from ParseException.initialize method
		for (int i = 0; i < e.expectedTokenSequences.length; i++) {
			if (maxSize < e.expectedTokenSequences[i].length) {
				maxSize = e.expectedTokenSequences[i].length;
			}
			for (int j = 0; j < e.expectedTokenSequences[i].length; j++) {
				expected.append(e.tokenImage[e.expectedTokenSequences[i][j]]).append(' ');
			}
			if (e.expectedTokenSequences[i][e.expectedTokenSequences[i].length - 1] != 0) {
				expected.append("...");
			}
		}
		return "Syntax error on token type <" + ((IToken) e.currentToken.next.getValue()).getType().toString()
				+ "> with Value \"" + ((IToken) e.currentToken.next.getValue()).getText()
				+ "\"; expected token of type " + expected.toString();
	}
}
