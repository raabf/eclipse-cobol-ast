package de.tum.in.www2.cobol.editors;

import java.util.Iterator;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationHover;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.ui.texteditor.MarkerAnnotation;

public class CobolAnnotationHover implements IAnnotationHover {

	@Override
	public String getHoverInfo(ISourceViewer sourceViewer, int lineNumber) {
		Iterator<Annotation> annotationIterator = sourceViewer.getAnnotationModel().getAnnotationIterator();

		while (annotationIterator.hasNext()) {
			Annotation annotation = annotationIterator.next();
			if (annotation.getClass().equals(MarkerAnnotation.class)) {
				try {
					((MarkerAnnotation) annotation).getMarker();

					// minus one because the line numbers which the user sees
					// start counting at 1
					// the line numbers the tool seees start counting at 0
					Integer line = (Integer) ((MarkerAnnotation) annotation).getMarker().getAttribute(
							IMarker.LINE_NUMBER) - 1;
					if (line == lineNumber) {
						return annotation.getText();
					}
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}
}
