package de.tum.in.www2.cobol.editors.views;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.part.ViewPart;

public class ControllflowGraphView extends ViewPart {

	private static ControllflowGraphView instance;
	private Image sourceImage = null;
	private Label label;

	public static ControllflowGraphView getInstance() {
		return instance;
	}

	@Override
	public void createPartControl(Composite parent) {
		instance = this;
		label = new Label(parent, 0);
	}

	public void setInputImage(String filePath) {
		label.setImage(loadImage(filePath));
	}

	@Override
	public void setFocus() {
		label.setFocus();
	}

	private Image loadImage(String filename) {
		if (sourceImage != null && !sourceImage.isDisposed()) {
			sourceImage.dispose();
			sourceImage = null;
		}
		sourceImage = new Image(Display.getCurrent(), filename);
		return sourceImage;
	}

}
