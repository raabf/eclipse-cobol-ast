package de.tum.in.www2.cobol.editors.views;

import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.IElementComparer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.contentoutline.ContentOutlinePage;

import parser.analysis.base.structure.representation.IStructureRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentation;
import parser.analysis.cobol.structure.representation.impl.DataDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.DeclarativesRepresentation;
import parser.analysis.cobol.structure.representation.impl.EnvironmentDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.FieldDeclarationRepresentation;
import parser.analysis.cobol.structure.representation.impl.IdentificationDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.ParagraphRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProcedureDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProcedureSectionRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProgramRepresentation;
import parser.analysis.cobol.structure.representation.impl.SectionRepresentation;
import de.tum.in.www2.cobol.editors.CobolEditor;
import de.tum.in.www2.cobol.model.ICobolParserChangeObserver;
import de.tum.in.www2.cobol.model.Model;

class CobolLabelProvider extends LabelProvider {

	@Override
	public String getText(Object element) {
		if (CobolStructureRepresentation.class.isAssignableFrom(element.getClass())) {
			CobolStructureRepresentation representation = (CobolStructureRepresentation) element;
			return representation.getDisplayName();
		}
		return "";
	}


	@Override
	public Image getImage(Object element) {
		org.eclipse.ui.ISharedImages si = PlatformUI.getWorkbench().getSharedImages();

		if (ProgramRepresentation.class.isAssignableFrom(element.getClass())) {
			return si.getImageDescriptor(ISharedImages.IMG_OBJ_PROJECT).createImage();
		}

		// Division Representations
		if (DataDivisionRepresentation.class.isAssignableFrom(element.getClass())) {
			return si.getImageDescriptor(ISharedImages.IMG_ETOOL_DEF_PERSPECTIVE).createImage();
		}
		if (ProcedureDivisionRepresentation.class.isAssignableFrom(element.getClass())) {
			return si.getImageDescriptor(ISharedImages.IMG_TOOL_NEW_WIZARD).createImage();
		}
		if (EnvironmentDivisionRepresentation.class.isAssignableFrom(element.getClass())) {
			return si.getImageDescriptor(ISharedImages.IMG_ETOOL_SAVEALL_EDIT).createImage();
		}
		if (IdentificationDivisionRepresentation.class.isAssignableFrom(element.getClass())) {
			return si.getImageDescriptor(ISharedImages.IMG_TOOL_PASTE).createImage();
		}

		if (ProcedureSectionRepresentation.class.isAssignableFrom(element.getClass())) {
			return si.getImageDescriptor(ISharedImages.IMG_DEF_VIEW).createImage();
		}
		if (SectionRepresentation.class.isAssignableFrom(element.getClass())) {
			return si.getImageDescriptor(ISharedImages.IMG_OBJ_FOLDER).createImage();
		}

		if (DeclarativesRepresentation.class.isAssignableFrom(element.getClass())) {
			return si.getImageDescriptor(ISharedImages.IMG_OBJS_TASK_TSK).createImage();
		}

		if (ParagraphRepresentation.class.isAssignableFrom(element.getClass())) {
			return si.getImageDescriptor(ISharedImages.IMG_OBJ_FILE).createImage();
		}
		if (FieldDeclarationRepresentation.class.isAssignableFrom(element.getClass())) {
			return si.getImageDescriptor(ISharedImages.IMG_ETOOL_SAVE_EDIT).createImage();
		}
		
		return super.getImage(element);
	}



}



class ContentProvider implements ITreeContentProvider {
	@Override
	public void dispose() {
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

	@Override
	public Object[] getChildren(Object element) {
		if (CobolStructureRepresentation.class.isAssignableFrom(element.getClass())) {
			CobolStructureRepresentation representation = (CobolStructureRepresentation) element;
			return representation.getChildren().toArray();
		}
		return new Object[0];
	}

	@Override
	public Object[] getElements(Object element) {
		if (element != null && Model.class.isAssignableFrom(element.getClass())) {
			return new Object[] { ((Model) element).getStructureRepresentation() };
		}
		return new Object[0];
	}

	@Override
	public Object getParent(Object element) {
		
		if (CobolStructureRepresentation.class.isAssignableFrom(element.getClass())) {
			CobolStructureRepresentation representation = (CobolStructureRepresentation) element;
			return representation.getParent();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (CobolStructureRepresentation.class.isAssignableFrom(element.getClass())) {
			CobolStructureRepresentation representation = (CobolStructureRepresentation) element;
			return representation.getChildren().size() > 0;
		}
		return false;
	}
	
	

}

public class CobolOutlinePage extends ContentOutlinePage implements ICobolParserChangeObserver {
	private final CobolEditor editor;

	public CobolOutlinePage(CobolEditor cobolEditor) {
		cobolEditor.getModel().registerModelObserver(this);
		editor = cobolEditor;
	}


	@Override
	public void createControl(Composite parent) {
		super.createControl(parent);
		TreeViewer viewer = getTreeViewer();
		viewer.setContentProvider(new ContentProvider());
		viewer.setLabelProvider(new CobolLabelProvider());
		viewer.addSelectionChangedListener(this);
		viewer.setUseHashlookup(true);
		viewer.setComparer(new IElementComparer() 
		{ 
			   @Override 
			   public int hashCode(Object element) { 
			    return element.hashCode(); 
			   } 
			   @Override 
			   public boolean equals(Object a, Object b) { 
				   if(a instanceof Model && b instanceof Model)
					   return ((Model) a).equals((Model) b); 
			    return a.equals(b); 
			   } 
		}
			  );
	}
	

	/*
	 * set cursor position for Outline
	 */
	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		//System.out.println("SelectionChanged");
		super.selectionChanged(event);
		ISelection selection = event.getSelection();
		if (selection.isEmpty()) {
		} else {
			if (selection instanceof TreeSelection) {
				TreeSelection treeSelection = (TreeSelection) selection;
				if (treeSelection.getPaths().length == 1) {
					CobolStructureRepresentation representation = (CobolStructureRepresentation) treeSelection
							.getFirstElement();
					int offset = representation.getStartOffset();
					int length = representation.getStartToken().getText().length();
					editor.getSelectionProvider().setSelection(new TextSelection(offset, length));
				}
			}
		}
	}

	
	@Override
	public void modelChanged(Model model) {
		TreeViewer treeViewer =  getTreeViewer();
		if(treeViewer.getInput() == null)
		treeViewer.setInput(model);
		else
		{
			treeViewer.refresh();
		}
		
		//treeViewer.expandAll();

		treeViewer.expandToLevel(2);	
	}
	



	public void setSelection(IStructureRepresentation representation) {
		TreeViewer viewer = getTreeViewer();
		IStructureRepresentation[] repArray = new IStructureRepresentation[1];
		repArray[0] = representation;
		TreePath representationTreePath = new TreePath(repArray);
		ISelection selection = new TreeSelection(representationTreePath);
		viewer.removeSelectionChangedListener(this);
		viewer.setSelection(selection);
		viewer.addSelectionChangedListener(this);
	}
	
}
