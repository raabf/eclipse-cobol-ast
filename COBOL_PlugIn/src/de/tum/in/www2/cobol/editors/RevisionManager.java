package de.tum.in.www2.cobol.editors;

import java.util.WeakHashMap;

import org.eclipse.jface.text.IDocument;

/**
 *       ONLY USE FROM UI THREAD!
 *
 * NOTE: We can only guarantee that the document has not changed
 *       by processing things on the UI thread. Therefore,  
 *       there is no point in making this class thread safe.
 * */
public final class RevisionManager {

	private static WeakHashMap<IDocument,Long> map = new WeakHashMap<IDocument,Long>();
	
	public static long get(IDocument doc) {
		if (!map.containsKey(doc))
			return 0;
		return map.get(doc);
	} 

	public static long increment(IDocument doc) {
		if (doc == null)
			return 0;
		long current = 0;
		if (map.containsKey(doc))
			current = map.get(doc);
		long newValue = current+1;
		map.put(doc, newValue);
		return newValue;
	}
	
}
