package de.tum.in.www2.cobol.editors.views;

import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;

import de.tum.in.www2.cobol.editors.CobolEditor;
import de.tum.in.www2.util.CallHierarchy;
import de.tum.in.www2.util.CallHierarchyGenerator;

public class CallHierarchyView extends ViewPart {

	CallHierarchyGenerator inputCallHierarchyGenerator;
	TreeViewer treeViewer;
	Composite parent;
	CobolEditor editor;
	private static CallHierarchyView instance;

	public static CallHierarchyView getInstance() {
		return instance;
	}

	public CallHierarchyView() {
		super();
		instance = this;
	}

	@Override
	public void init(IViewSite site) throws PartInitException {
		super.init(site);
	}

	@Override
	public void createPartControl(Composite parent) {
		// Create viewer.
		treeViewer = new TreeViewer(parent);
		treeViewer.setContentProvider(new CallHierarchyContentProvider());
		treeViewer.setLabelProvider(new CallHierarchyLabelProvider());
		treeViewer.setInput(inputCallHierarchyGenerator);
		this.parent = parent;
	}

	public void setInput(CallHierarchyGenerator input) {
		this.inputCallHierarchyGenerator = input;
		if (inputCallHierarchyGenerator != null) {
			treeViewer.setInput(inputCallHierarchyGenerator);
		}
	}

	@Override
	public void setFocus() {
		treeViewer.getControl().setFocus();
	}

	private void addSelectionChangedListener() {
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				// if the selection is empty clear the label
				if (event.getSelection().isEmpty()) {
					return;
				}
				if (event.getSelection() instanceof IStructuredSelection) {
					ISelection selection = event.getSelection();
					if (selection.isEmpty()) {
						System.out.println("NO SELECTION");
					} else {
						if (selection instanceof TreeSelection) {
							TreeSelection treeSelection = (TreeSelection) selection;
							if (treeSelection.getPaths().length == 1) {
								CallHierarchy selectedCallHierarchy = (CallHierarchy) treeSelection.getFirstElement();
								Position positionSelectedCallHierarchy = selectedCallHierarchy.getPositionWhereCalled();
								int offset = positionSelectedCallHierarchy.getOffset();
								int length = positionSelectedCallHierarchy.getLength();
								editor.getSelectionProvider().setSelection(new TextSelection(offset, length));
							}
						}
					}
				}
			}
		});
	}

	public void setEditor(CobolEditor editor) {
		this.editor = editor;
		this.addSelectionChangedListener();
	}
}
