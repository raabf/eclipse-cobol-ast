package de.tum.in.www2.cobol.editors.views;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import de.tum.in.www2.util.CallHierarchy;
import de.tum.in.www2.util.CallHierarchyGenerator;

public class CallHierarchyContentProvider implements ITreeContentProvider {
	@Override
	public void dispose() {
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

	@Override
	public Object[] getChildren(Object element) {
		if (CallHierarchy.class.isAssignableFrom(element.getClass())) {
			CallHierarchy callHierarchy = (CallHierarchy) element;
			return callHierarchy.getChildren().toArray();
		}
		return new Object[0];
	}

	@Override
	public Object[] getElements(Object element) {
		if (element != null && CallHierarchyGenerator.class.isAssignableFrom(element.getClass())) {
			CallHierarchy root = ((CallHierarchyGenerator) element).getRoot();
			if (root != null) {
				return new Object[] { root };
			}
		}
		return new Object[0];
	}

	@Override
	public Object getParent(Object element) {
		if (CallHierarchy.class.isAssignableFrom(element.getClass())) {
			CallHierarchy callHierarchy = (CallHierarchy) element;
			return callHierarchy.getParent();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {

		if (CallHierarchy.class.isAssignableFrom(element.getClass())) {
			CallHierarchy representation = (CallHierarchy) element;
			return representation.getChildren().size() > 0;
		}
		return false;

	}

}
