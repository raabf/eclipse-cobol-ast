package de.tum.in.www2.cobol.editors.views;

import org.eclipse.jface.viewers.LabelProvider;

import de.tum.in.www2.util.CallHierarchy;

public class CallHierarchyLabelProvider extends LabelProvider {
	@Override
	public String getText(Object element) {
		if (CallHierarchy.class.isAssignableFrom(element.getClass())) {
			CallHierarchy callHierarchy = (CallHierarchy) element;
			return callHierarchy.getName();
		}
		return "";
	}
}