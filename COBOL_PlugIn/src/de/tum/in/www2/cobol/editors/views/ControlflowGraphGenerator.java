package de.tum.in.www2.cobol.editors.views;

import java.util.HashSet;
import java.util.Stack;

import parser.analysis.cobol.controlflow.graph.CobolControlflowEdge;
import parser.analysis.cobol.controlflow.graph.CobolControlflowNode;
import parser.analysis.cobol.controlflow.graph.CobolControlflowProgramPart;
import parser.analysis.cobol.controlflow.graph.CobolProgramNode;
import parser.analysis.cobol.controlflow.graph.CobolUnresolvedCallPart;

public class ControlflowGraphGenerator {

	private int numberParanthesesOpened = 0;

	public String getGraphString(CobolProgramNode program) {
		String programGraph = "digraph G {\n" + getNodeRepresentation(program, new HashSet<CobolControlflowNode>(),
				false, new Stack<PathOfPart>(), new HashSet<CobolControlflowEdge>(), null) + "}";
		while (numberParanthesesOpened > 0) {
			programGraph += "}";
			numberParanthesesOpened--;
		}

		return programGraph;
	}

	private String getNodeRepresentation(CobolControlflowNode node, HashSet<CobolControlflowNode> printedNodes,
			boolean isEndNodeInPart, Stack<PathOfPart> callStack, HashSet<CobolControlflowEdge> edgesPrinted,
			CobolControlflowEdge edgeToNode) {
		String result = "";
		String nodeIdentifier = node.getName();
		PathOfPart path = null;
		if (CobolControlflowProgramPart.class.isAssignableFrom(node.getClass())) {

			if (!callStack.isEmpty()) {
				result += edgeToNode.getPreviousNode().getName();
				result += "-> " + nodeIdentifier + "[ label=\"" + edgeToNode + "\" ]" + ";\n";
			}
			path = new PathOfPart(true, node);
			callStack.push(path);
			result += "\n subgraph cluster" + nodeIdentifier + "{\n";
			numberParanthesesOpened++;
			String startNode = " " + nodeIdentifier;
			result += startNode + ";\n";
			CobolControlflowNode endNodeOfThisNode = ((CobolControlflowProgramPart) node).getEndNode();
			if (endNodeOfThisNode == node) {
				result += "}\n ";
				numberParanthesesOpened--;
				callStack.pop();
			}
			if (endNodeOfThisNode != node) {
				result += "" + endNodeOfThisNode.getName() + ";\n";
			}
		} else {
			result += edgeToNode.getPreviousNode().getName() + "->";
			result += " " + nodeIdentifier + "[ label=\"" + edgeToNode + "\" ]" + ";\n";
			if (!callStack.isEmpty()) {
				path = callStack.peek();
			}

		}

		if (!callStack.isEmpty() && ((CobolControlflowProgramPart) callStack.peek().part).getEndNode() == node) {
			PathOfPart part = callStack.pop();
			if (part.isLastPath) {
				result += "}\n ";
				numberParanthesesOpened--;
			} else {
				printedNodes.remove(node);
				return result;
			}
		}

		int countEdges = node.getEdges().size();
		int counter = 0;
		for (CobolControlflowEdge edge : node.getEdges()) {
			counter++;
			CobolControlflowNode directNextNode = edge.getNextNode();
			if (!printedNodes.contains(directNextNode)) {
				printedNodes.add(directNextNode);
				boolean isLastNode = false;
				if (!callStack.isEmpty()
						&& directNextNode == ((CobolControlflowProgramPart) callStack.peek().part).getEndNode()) {
					isLastNode = true;
				}
				Stack<?> clonedStack = (Stack<?>) callStack.clone();
				if (CobolControlflowProgramPart.class.isAssignableFrom(node.getClass())
						&& !CobolUnresolvedCallPart.class.isAssignableFrom(node.getClass())) {
					((Stack<PathOfPart>) clonedStack).pop();
					((Stack<PathOfPart>) clonedStack).push(new PathOfPart(countEdges == counter, node));
				}
				result += getNodeRepresentation(directNextNode, printedNodes, isLastNode,
						(Stack<PathOfPart>) clonedStack, edgesPrinted, edge);
			} else if (!edgesPrinted.contains(edge)) {
				result += nodeIdentifier + "->";
				result += " " + directNextNode.getName() + "[ label=\"" + edge + "\" ]" + ";\n";
			}
			edgesPrinted.add(edge);
		}

		return result;

	}

	class PathOfPart {
		protected boolean isLastPath;
		protected CobolControlflowNode part;

		protected PathOfPart(boolean isLast, CobolControlflowNode part) {
			this.isLastPath = isLast;
			this.part = part;
		}

	}

}
