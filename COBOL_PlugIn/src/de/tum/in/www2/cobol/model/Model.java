package de.tum.in.www2.cobol.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.WeakHashMap;

import org.eclipse.jface.text.IDocument;

import parser.analysis.base.definition.representation.ICallRepresentation;
import parser.analysis.base.definition.representation.ICallableDefinitionRepresentation;
import parser.analysis.base.definition.representation.IDefinitionUseRepresentation;
import parser.analysis.base.definition.representation.IReadDataRepresentation;
import parser.analysis.base.definition.representation.IWriteDataRepresentation;
import parser.analysis.base.structure.representation.IDataDeclarationRepresentation;
import parser.analysis.base.structure.representation.IStructureRepresentation;
import parser.analysis.cobol.controlflow.graph.CobolControlflowEdge;
import parser.analysis.cobol.controlflow.graph.CobolControlflowNode;
import parser.analysis.cobol.controlflow.graph.CobolControlflowUnreachableNodes;
import parser.analysis.cobol.controlflow.graph.visitors.CobolControlflowAnalysis;
import parser.analysis.cobol.definition.CobolDefinitionUseAnalysis;
import parser.analysis.cobol.structure.CobolStructureAnalysis;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProgramRepresentation;
import parser.errorhandling.ILowLevelErrorHandler;
import parser.errorhandling.StringMessage;

import com.google.common.collect.Multimap;

import de.tum.in.www2.cobol.model.references.CalleeDeclaration;
import de.tum.in.www2.cobol.model.references.DataDeclaration;
import de.tum.in.www2.cobol.model.references.Declaration;
import de.tum.in.www2.cobol.model.references.FindSelectedStructureRepresentationVisitor;
import de.tum.in.www2.cobol.model.references.Occurrence;

public class Model {

	private CobolStructureAnalysis structureAnalysis;
	private CobolDefinitionUseAnalysis dataflowAnalysis;

	/**
	 * Keep Model generation internal and centrally managed via
	 * getInstanceForDocument(IDoc)
	 */
	private Model() {
		this.modelObservers = new ArrayList<ICobolModelObserverBase>();
	}

	/**
	 * Area to deal with a model per file pretty generic, so just copied/pasted
	 */
	private static WeakHashMap<IDocument, Model> instances = new WeakHashMap<IDocument, Model>();

	public static void deleteForDocument(IDocument document) {
		if (document == null) {
			return;
		}
		if (instances.containsKey(document)) {
			Model m = instances.get(document);
			instances.remove(document);
			if (m != null) {
				m.dispose();
			}
			System.out.println(">>>>>>>> REMOVED MODEL INSTANCE - new length: " + instances.size());
		}
	}

	public static Model getInstanceForDocument(IDocument document) {
		if (document == null) {
			return null;
		}

		// IMPORTANT: The model itself must not save a reference to the
		// document.
		// Otherwise document instances might not get reclaimed.

		if (!instances.containsKey(document)) {
			instances.put(document, new Model());
			System.out.println(">>>>>>>> CREATED NEW MODEL INSTANCE for Cobol !!!!!!!!!!!!!!!");
		}
		return instances.get(document);
	}

	/**
	 * Area to (un-)register new observers to the model pretty generic, so just
	 * copied/pasted
	 */

	private final Object observerLock = new Object();
	private List<ICobolModelObserverBase> modelObservers;
	private List<CobolStructureRepresentation> foldables;
	private List<String> paragraphSectionIdentifiers;
	private CobolControlflowAnalysis controlflowAnalysis;

	private <T> void notifyObservers(Class<T> cls) {
		for (ICobolModelObserverBase observer : modelObservers) {
			if (cls.isAssignableFrom(observer.getClass())) {
				observer.modelChanged(this);
			}
		}
	}

	public void registerModelObserver(ICobolModelObserverBase modelObserver) {
		if (modelObserver == null) {
			return;
		}
		synchronized (observerLock) {
			if (this.modelObservers.contains(modelObserver)) {
				return;
			}
			this.modelObservers.add(modelObserver);
		}
	}

	public boolean unregisterModelObserver(ICobolModelObserverBase modelObserver) {
		if (modelObserver == null) {
			return false;
		}
		synchronized (observerLock) {
			if (this.modelObservers.contains(modelObserver)) {
				this.modelObservers.remove(modelObserver);
				return true;
			}
			return false;
		}
	}

	public void dispose() {
		synchronized (observerLock) {
			if (modelObservers != null) {
				modelObservers.clear();
			}
		}
	}

	public void setStructureAnalysis(CobolStructureAnalysis analysis, ILowLevelErrorHandler errorReporter) {
		this.structureAnalysis = analysis;
		ProgramRepresentation representation = analysis.getProgramStructure();

		FoldingStructureVisitor foldableVisitor = new FoldingStructureVisitor();
		representation.accept(foldableVisitor);
		foldables = foldableVisitor.getFoldables();

		ParagraphSectionIdentifierStructureVisitor identifierVisitor = new ParagraphSectionIdentifierStructureVisitor();
		representation.accept(identifierVisitor);
		paragraphSectionIdentifiers = identifierVisitor.getIdentifiers();

		dataflowAnalysis = new CobolDefinitionUseAnalysis(errorReporter);
		dataflowAnalysis.run(representation);

		controlflowAnalysis = new parser.analysis.cobol.controlflow.graph.visitors.CobolControlflowAnalysis();
		controlflowAnalysis.run(representation);

		CobolControlflowUnreachableNodes reachability = new CobolControlflowUnreachableNodes();
		Collection<CobolControlflowNode> unreachables = reachability.findUnreachableNodes(controlflowAnalysis);
		for (CobolControlflowNode unreachable : unreachables) {
			for (CobolControlflowEdge edge : unreachable.getEdges()) {
				if (edge.isStatement()) {
					errorReporter.onWarning(edge.asStatement().getAst(), new StringMessage("unreachable code"));
				}
			}
		}

		notifyObservers(ICobolModelObserverBase.class);
	}

	public CobolStructureRepresentation getStructureRepresentation() {
		if (structureAnalysis != null) {
			return structureAnalysis.getProgramStructure();
		}
		return null;
	}

	public IDefinitionUseRepresentation getDataflowRepresentation() {
		if (dataflowAnalysis != null) {
			return dataflowAnalysis.getRepresentation();
		}
		return null;
	}

	public List<CobolStructureRepresentation> getFoldables() {
		if (foldables != null) {
			return foldables;
		}
		return Collections.emptyList();
	}

	public List<String> getParagraphSectionIdentifiers() {
		if (paragraphSectionIdentifiers != null) {
			return paragraphSectionIdentifiers;
		}
		return Collections.emptyList();
	}

	public IStructureRepresentation findStructureRepresentation(int offset, int length) {
		FindSelectedStructureRepresentationVisitor structureVisitor = new FindSelectedStructureRepresentationVisitor(
				offset, length);
		CobolStructureRepresentation structure = getStructureRepresentation();
		if (structure != null) {
			structure.accept(structureVisitor);
		}
		return structureVisitor.getRepresentation();
	}

	public Occurrence findOccurrence(int offset, int length) {
		Occurrence occurrence = null;

		if (getDataflowRepresentation() != null) {
			Collection<? extends IDataDeclarationRepresentation> declarations = getDataflowRepresentation()
					.getDataDefinitions();
			for (IDataDeclarationRepresentation dataDeclaration : declarations) {
				int start = dataDeclaration.getStartToken().getOffset();
				int tokenLength = dataDeclaration.getStartToken().getText().length();
				if (offset >= start && offset + length <= start + tokenLength) {
					occurrence = new DataDeclaration(dataDeclaration, true);
				}
			}

			if (occurrence == null) {
				Collection<? extends ICallableDefinitionRepresentation> callableDefinitions = getDataflowRepresentation()
						.getCallableDefinitions();
				for (ICallableDefinitionRepresentation callableDefinition : callableDefinitions) {
					int start = callableDefinition.getStructureRepresentation().getStartToken().getOffset();
					int tokenLength = callableDefinition.getStructureRepresentation().getStartToken().getText()
							.length();
					if (offset >= start && offset + length <= start + tokenLength && callableDefinition.isResolved()) {
						occurrence = new CalleeDeclaration(callableDefinition, true);
					}
				}
			}

			if (occurrence == null) {
				Multimap<? extends IDataDeclarationRepresentation, ? extends IReadDataRepresentation> readAccesses = getDataflowRepresentation()
						.getReadAccesses();
				for (IReadDataRepresentation read : readAccesses.values()) {
					int start = read.getStructureRepresentation().getStartToken().getOffset();
					int tokenLength = read.getStructureRepresentation().getStartToken().getText().length();
					if (offset >= start && offset + length <= start + tokenLength) {
						Declaration.DeclarationForReferenceCreator creator = new Declaration.DeclarationForReferenceCreator(
								read);
						occurrence = creator.getReference();
					}
				}
			}

			if (occurrence == null) {
				Multimap<? extends IDataDeclarationRepresentation, ? extends IWriteDataRepresentation> writeAccesses = getDataflowRepresentation()
						.getWriteAccesses();
				for (IWriteDataRepresentation write : writeAccesses.values()) {
					int start = write.getStructureRepresentation().getStartToken().getOffset();
					int tokenLength = write.getStructureRepresentation().getStartToken().getText().length();
					if (offset >= start && offset + length <= start + tokenLength) {
						Declaration.DeclarationForReferenceCreator creator = new Declaration.DeclarationForReferenceCreator(
								write);
						occurrence = creator.getReference();
					}
				}
			}

			if (occurrence == null) {
				Multimap<? extends ICallableDefinitionRepresentation, ? extends ICallRepresentation> calls = getDataflowRepresentation()
						.getCalls();
				for (ICallRepresentation call : calls.values()) {
					int start = call.getStartToken().getOffset();
					int tokenLength = call.getStartToken().getText().length();
					if (offset >= start && offset + length <= start + tokenLength && call.getTarget().isResolved()) {
						Declaration.DeclarationForReferenceCreator creator = new Declaration.DeclarationForReferenceCreator(
								call);
						occurrence = creator.getReference();
					}
				}
			}
		}
		return occurrence;
	}

	public CobolControlflowAnalysis getControlFlowAnalysis() {
		return controlflowAnalysis;
	}
}
