package de.tum.in.www2.cobol.model.references;

import parser.analysis.base.definition.representation.ICallRepresentation;
import parser.analysis.base.definition.representation.ICallableDefinitionRepresentation;

public class CalleeDeclaration extends Declaration {
	private final ICallableDefinitionRepresentation callee;

	public CalleeDeclaration(ICallableDefinitionRepresentation callee, boolean initializeReferences) {
		// FIXME we cannot guarantee there is a structure representation for the
		// callee here, it might be unresolved!
		super(callee.getStructureRepresentation());
		this.callee = callee;

		if (initializeReferences) {
			for (ICallRepresentation caller : callee.getCallers()) {
				references.add(new CallReference(this, caller));
			}
		}
	}

	@Override
	public boolean isCallee() {
		return true;
	}

	@Override
	public ICallableDefinitionRepresentation getCalleeNode() {
		return callee;
	}

}
