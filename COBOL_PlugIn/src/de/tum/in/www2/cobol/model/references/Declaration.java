package de.tum.in.www2.cobol.model.references;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.jface.text.Position;

import parser.analysis.base.definition.representation.ICallRepresentation;
import parser.analysis.base.definition.representation.IReadDataRepresentation;
import parser.analysis.base.definition.representation.IWriteDataRepresentation;
import parser.analysis.base.structure.representation.IStructureRepresentation;

public abstract class Declaration extends Occurrence {
	private final IStructureRepresentation representation;
	// FIXME private!!
	protected final Collection<Reference> references = new HashSet<Reference>();

	public Declaration(IStructureRepresentation representation) {
		super(new Position(representation.getStartOffset(), representation.getStartToken().getText().length()));
		this.representation = representation;
	}

	private Declaration(Position position, IStructureRepresentation representation) {
		super(position);
		this.representation = representation;
	}

	public IStructureRepresentation getStructureRepresentation() {
		return representation;
	}

	public Collection<Reference> getReferences() {
		return Collections.unmodifiableCollection(references);
	}

	@Override
	public Declaration getDeclaration() {
		return this;
	}

	@Override
	public String getName() {
		return getStructureRepresentation().getName();
	}

	@Override
	public String getDisplayName() {
		return getStructureRepresentation().getDisplayName();
	}

	@Override
	public List<Occurrence> getAllOccurences() {
		List<Occurrence> occurences = new LinkedList<Occurrence>();
		occurences.addAll(references);
		occurences.add(this);
		return occurences;
	}

	@Override
	public final boolean isCaller() {
		return false;
	}

	@Override
	public final ICallRepresentation getCallerNode() {
		throw new UnsupportedOperationException("Declaration " + this + " is not a caller");
	}

	public static class DeclarationForReferenceCreator {
		private final Declaration declaration;
		private final Reference reference;

		public DeclarationForReferenceCreator(ICallRepresentation node) {
			declaration = new CalleeDeclaration(node.getTarget(), false);
			reference = new CallReference(declaration, node);
			declaration.references.add(reference);

			for (ICallRepresentation caller : node.getTarget().getCallers()) {
				if (!caller.equals(node)) {
					declaration.references.add(new CallReference(declaration, caller));
				}
			}
		}

		public DeclarationForReferenceCreator(IReadDataRepresentation access) {
			declaration = new DataDeclaration(access.getDeclaration(), false);
			reference = new ReadDataReference(declaration, access);
			declaration.references.add(reference);

			for (IReadDataRepresentation read : access.getDeclaration().getReads()) {
				if (!read.equals(access)) {
					declaration.references.add(new ReadDataReference(declaration, read));
				}
			}

			for (IWriteDataRepresentation write : access.getDeclaration().getWrites()) {
				
				Boolean edit = true;
				for(Reference v : declaration.references)
				{
					Position positionTemp = v.getPosition();
					Position newPosition = new Position(write.getStructureRepresentation().getStartOffset(), write
							.getStructureRepresentation().getStartToken().getText().length());
					if(positionTemp!=null && positionTemp.equals(newPosition))
					{
						edit = false;
						break;
					}
				}
				if(edit)
				declaration.references.add(new WriteReference(declaration, write));
			}
		}

		public DeclarationForReferenceCreator(IWriteDataRepresentation access) {
			declaration = new DataDeclaration(access.getDeclaration(), false);
			reference = new WriteReference(declaration, access);
			declaration.references.add(reference);

			for (IReadDataRepresentation read : access.getDeclaration().getReads()) {
				declaration.references.add(new ReadDataReference(declaration, read));
			}

			for (IWriteDataRepresentation write : access.getDeclaration().getWrites()) {
				if (!write.equals(access)) {
					declaration.references.add(new WriteReference(declaration, write));
				}
			}
		}

		public Declaration getDeclaration() {
			return declaration;
		}

		public Reference getReference() {
			return reference;
		}
	}
}
