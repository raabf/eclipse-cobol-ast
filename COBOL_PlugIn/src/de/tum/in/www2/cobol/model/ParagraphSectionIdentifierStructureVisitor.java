package de.tum.in.www2.cobol.model;

import java.util.ArrayList;
import java.util.List;

import parser.analysis.cobol.structure.representation.CobolParagraphRepresentation;
import parser.analysis.cobol.structure.representation.CobolProcedureSectionRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentation;
import parser.analysis.cobol.structure.representation.DefaultCobolStructureRepresentationVisitor;
import parser.analysis.cobol.structure.representation.impl.FieldDeclarationRepresentation;

public class ParagraphSectionIdentifierStructureVisitor extends DefaultCobolStructureRepresentationVisitor {
	private List<String> identifiers = new ArrayList<String>();

	@Override
	public void visit(CobolProcedureSectionRepresentation representation) {
		identifiers.add(representation.getDisplayName());
		super.visit(representation);
	}

	@Override
	public void visit(CobolParagraphRepresentation representation) {
		identifiers.add(representation.getDisplayName());
		super.visit(representation);
	}

	@Override
	public void visit(CobolStructureRepresentation representation) {
		if (representation.getClass().equals(FieldDeclarationRepresentation.class)) {
			identifiers.add(representation.getDisplayName());
		}
		super.visit(representation);
	}

	public List<String> getIdentifiers() {
		return identifiers;
	}
}
