package de.tum.in.www2.cobol.model.references;

import parser.analysis.base.definition.representation.ICallableDefinitionRepresentation;
import parser.analysis.base.definition.representation.IReadDataRepresentation;
import parser.analysis.base.definition.representation.IWriteDataRepresentation;
import parser.analysis.base.structure.representation.IDataDeclarationRepresentation;

public class DataDeclaration extends Declaration {
	// FIXME remove flag from constructor
	public DataDeclaration(IDataDeclarationRepresentation declaration, boolean initializeReferences) {
		super(declaration);

		if (initializeReferences) {
			for (IReadDataRepresentation read : declaration.getReads()) {
				references.add(new ReadDataReference(this, read));
			}

			for (IWriteDataRepresentation write : declaration.getWrites()) {
				references.add(new WriteReference(this, write));
			}
		}
	}

	@Override
	public boolean isCallee() {
		return false;
	}

	@Override
	public ICallableDefinitionRepresentation getCalleeNode() {
		throw new UnsupportedOperationException("Declaration " + this + " is not a callee");
	}
}
