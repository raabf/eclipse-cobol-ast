package de.tum.in.www2.cobol.model;

public interface ICobolModelObserverBase {
	void modelChanged(Model model);
}