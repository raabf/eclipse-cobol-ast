package de.tum.in.www2.cobol.model;

import java.util.ArrayList;
import java.util.List;

import parser.analysis.cobol.structure.representation.CobolProcedureSectionRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentation;
import parser.analysis.cobol.structure.representation.DefaultCobolStructureRepresentationVisitor;
import parser.analysis.cobol.structure.representation.impl.DataDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.EnvironmentDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.IdentificationDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.ProcedureDivisionRepresentation;
import parser.analysis.cobol.structure.representation.impl.SectionRepresentation;

public class FoldingStructureVisitor extends DefaultCobolStructureRepresentationVisitor {
	private List<CobolStructureRepresentation> foldable = new ArrayList<CobolStructureRepresentation>();

	@Override
	public void visit(CobolProcedureSectionRepresentation representation) {
		foldable.add(representation);
		super.visit(representation);
	}

	@Override
	public void visit(CobolStructureRepresentation representation) {
		Class<? extends CobolStructureRepresentation> clazz = representation.getClass();
		if (IdentificationDivisionRepresentation.class.isAssignableFrom(clazz)
				|| EnvironmentDivisionRepresentation.class.isAssignableFrom(clazz)
				|| DataDivisionRepresentation.class.isAssignableFrom(clazz)
				|| SectionRepresentation.class.isAssignableFrom(clazz)
				|| ProcedureDivisionRepresentation.class.isAssignableFrom(clazz)) {
			foldable.add(representation);
		}
		super.visit(representation);
	}

	public List<CobolStructureRepresentation> getFoldables() {
		return foldable;
	}
}
