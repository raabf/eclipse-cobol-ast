package de.tum.in.www2.cobol.model.references;

import java.util.List;

import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;

import parser.analysis.base.definition.representation.ICallableDefinitionRepresentation;

public abstract class Reference extends Occurrence {
	private final Declaration declaration;

	public abstract Annotation createAnnotation();

	public Reference(Declaration declaration, Position position) {
		super(position);
		this.declaration = declaration;
	}

	@Override
	public List<Occurrence> getAllOccurences() {
		return getDeclaration().getAllOccurences();
	}

	@Override
	public Declaration getDeclaration() {
		return declaration;
	}

	@Override
	public String getName() {
		return getDeclaration().getName();
	}

	@Override
	public String getDisplayName() {
		return getDeclaration().getDisplayName();
	}

	@Override
	public final boolean isCallee() {
		return false;
	}

	@Override
	public final ICallableDefinitionRepresentation getCalleeNode() {
		throw new UnsupportedOperationException("Reference " + this + " is not a callee");
	}
}
