package de.tum.in.www2.cobol.model.references;

import java.util.List;

import org.eclipse.jface.text.Position;

import parser.analysis.base.definition.representation.ICallRepresentation;
import parser.analysis.base.definition.representation.ICallableDefinitionRepresentation;

public abstract class Occurrence {
	private final Position position;

	public Occurrence(Position position) {
		this.position = position;
	}

	public final Position getPosition() {
		return position;
	}

	public abstract String getName();

	public abstract String getDisplayName();

	public abstract Declaration getDeclaration();

	public abstract List<Occurrence> getAllOccurences();

	public abstract boolean isCaller();

	public abstract boolean isCallee();

	public abstract ICallRepresentation getCallerNode();

	public abstract ICallableDefinitionRepresentation getCalleeNode();
}
