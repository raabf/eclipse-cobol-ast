package de.tum.in.www2.cobol.model.references;

import java.util.Comparator;

public class OccurenceByOffsetComparator implements Comparator<Occurrence> {

	@Override
	public int compare(Occurrence o1, Occurrence o2) {
		if (o1.getPosition().offset == o2.getPosition().offset) {
			return 0;
		} else if (o1.getPosition().offset > o2.getPosition().offset) {
			return -1;
		} else {
			return 1;
		}
	}

}
