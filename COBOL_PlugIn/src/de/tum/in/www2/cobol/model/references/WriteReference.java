package de.tum.in.www2.cobol.model.references;

import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;

import parser.analysis.base.definition.representation.ICallRepresentation;
import parser.analysis.base.definition.representation.ICallableDefinitionRepresentation;
import parser.analysis.base.definition.representation.IWriteDataRepresentation;
import parser.analysis.base.structure.representation.IStructureRepresentation;
import de.tum.in.www2.cobol.editors.CobolEditor;

public class WriteReference extends Reference {
	public WriteReference(Declaration declaration, ICallableDefinitionRepresentation node) {
		super(declaration, new Position(node.getStructureRepresentation().getStartOffset(), node
				.getStructureRepresentation().getStartToken().getText().length()));
	}

	public WriteReference(Declaration declaration, IStructureRepresentation representation) {
		super(declaration, new Position(representation.getStartOffset(), representation.getStartToken().getText()
				.length()));
	}

	public WriteReference(Declaration declaration, IWriteDataRepresentation write) {
		super(declaration, new Position(write.getStructureRepresentation().getStartOffset(), write
				.getStructureRepresentation().getStartToken().getText().length()));
	}

	@Override
	public Annotation createAnnotation() {
		return new Annotation(CobolEditor.WRITE_OCCURRENCE, false, "");
	}

	@Override
	public boolean isCaller() {
		return false;
	}

	@Override
	public ICallRepresentation getCallerNode() {
		throw new UnsupportedOperationException("Reference " + this + " is not a caller!");
	}
}
