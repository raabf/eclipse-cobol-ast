package de.tum.in.www2.cobol.model.references;

import parser.analysis.base.structure.representation.IStructureRepresentation;
import parser.analysis.cobol.definition.representation.CobolDataAccessStructureRepresentation;
import parser.analysis.cobol.structure.representation.CobolProgramRepresentation;
import parser.analysis.cobol.structure.representation.CobolStructureRepresentation;
import parser.analysis.cobol.structure.representation.DefaultCobolStructureRepresentationVisitor;
import parser.analysis.cobol.structure.representation.impl.FieldDeclarationRepresentation;

public final class FindSelectedStructureRepresentationVisitor extends DefaultCobolStructureRepresentationVisitor {
	private final int offset;
	private final int length;
	private IStructureRepresentation representation = null;

	public FindSelectedStructureRepresentationVisitor(int offset, int length) {
		this.offset = offset;
		this.length = length;
	}

	@Override
	public void visit(CobolProgramRepresentation representation) {
		if (!checkSelection(representation)) {
			super.visit(representation);
		}
	}

	@Override
	public void visit(CobolStructureRepresentation representation) {
		if (!CobolDataAccessStructureRepresentation.class.isAssignableFrom(representation.getClass())
				&& !FieldDeclarationRepresentation.class.isAssignableFrom(representation.getClass())
				&& !checkSelection(representation)) {
			super.visit(representation);
		}
	}

	public IStructureRepresentation getRepresentation() {
		return representation;
	}

	private boolean checkSelection(CobolStructureRepresentation representation) {
		int start = representation.getStartToken().getOffset();
		int length = representation.getStartToken().getText().length();
		boolean result = offset >= start && offset + this.length <= start + length;

		if (result) {
			this.representation = representation;
		}

		return result;
	}
}