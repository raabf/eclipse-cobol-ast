package de.tum.in.www2.cobol.model.references;

import org.eclipse.jface.text.Position;

import parser.analysis.base.definition.representation.ICallRepresentation;
import parser.analysis.base.definition.representation.IReadDataRepresentation;

public class ReadDataReference extends ReadReference {
	public ReadDataReference(Declaration declaration, IReadDataRepresentation read) {
		super(declaration, new Position(read.getStructureRepresentation().getStartOffset(), read
				.getStructureRepresentation().getStartToken().getText().length()));
	}

	@Override
	public boolean isCaller() {
		return false;
	}

	@Override
	public ICallRepresentation getCallerNode() {
		throw new UnsupportedOperationException("Reference " + this + " is not a caller");
	}
}
