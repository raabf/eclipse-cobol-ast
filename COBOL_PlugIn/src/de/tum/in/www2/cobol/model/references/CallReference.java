package de.tum.in.www2.cobol.model.references;

import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;

import parser.analysis.base.definition.representation.ICallRepresentation;
import de.tum.in.www2.cobol.editors.CobolEditor;

public class CallReference extends ReadReference {
	private final ICallRepresentation caller;

	public CallReference(Declaration declaration, ICallRepresentation caller) {
		super(declaration, new Position(caller.getStartToken().getOffset(), caller.getStartToken().getText().length()));
		this.caller = caller;
	}

	@Override
	public Annotation createAnnotation() {
		return new Annotation(CobolEditor.READ_OCCURRENCE, false, "");
	}

	@Override
	public boolean isCaller() {
		return true;
	}

	@Override
	public ICallRepresentation getCallerNode() {
		return caller;
	}
}
