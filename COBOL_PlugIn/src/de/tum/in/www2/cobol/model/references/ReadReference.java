package de.tum.in.www2.cobol.model.references;

import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;

import de.tum.in.www2.cobol.editors.CobolEditor;

public abstract class ReadReference extends Reference {
	public ReadReference(Declaration declaration, Position position) {
		super(declaration, position);
	}

	@Override
	public Annotation createAnnotation() {
		return new Annotation(CobolEditor.READ_OCCURRENCE, false, "");
	}
}
