package de.tum.in.www2.util;

public class KeyWordConstants {

	private static KeyWordTrieNode keyWordTrie;

	// TOKEN taken from cobol.jj
	private static final String[] keyWords = { "accept", "access", "add", "address", "advancing", "after", "all",
			"alphabet", "alphabetic", "alphabetic-lower", "alphabetic-upper", "alphanumeric", "alphanumeric-edited",
			"alphanumeric-hashtable", "also", "alter", "alternate", "and", "any", "approximate", "are", "area",
			"areas", "ascending", "assign", "at", "author", "automatic", "before", "beginning", "binary",
			"binary-byte", "binary-double", "binary-long", "binary-m", "binary-rev", "binary-short", "bit", "blank",
			"block", "bottom", "by", "call", "call-convention", "cancel", "cd", "cf", "ch", "character", "characters",
			"class", "clock-units", "close", "cobol", "code", "code-set", "collating", "column", "comma", "commit",
			"common", "communication", "comp", "comp-1", "comp-1-a", "comp-1-e", "comp-1-m", "comp-1-mvs",
			"comp-1-rev", "comp-2", "comp-2-a", "comp-2-e", "comp-2-m", "comp-2-mvs", "comp-2-rev", "comp-3",
			"comp-3-a", "comp-3-m", "comp-4", "comp-4-m", "comp-5", "comp-5-m", "comp-6", "comp-a", "comp-b", "comp-d",
			"comp-m", "comp-n", "comp-p", "comp-s", "comp-x", "comp-x-rev", "component", "computational",
			"computational-1", "computational-1-a", "computational-1-e", "computational-1-m", "computational-1-mvs",
			"computational-1-rev", "computational-2", "computational-2-a", "computational-2-e", "computational-2-m",
			"computational-2-mvs", "computational-2-rev", "computational-3", "computational-3-a", "computational-3-m",
			"computational-4", "computational-4-m", "computational-5", "computational-5-m", "computational-6",
			"computational-a", "computational-b", "computational-d", "computational-m", "computational-n",
			"computational-p", "computational-s", "computational-x", "computational-x-rev", "compute", "configuration",
			"console", "contains", "content", "continue", "control", "controls", "converting", "copy", "corr",
			"corresponding", "count", "crt", "currency", "data", "date", "date-compiled", "date-written", "day",
			"day-of-week", "dbcs", "de", "debug-contents", "debug-item", "debug-line", "debug-name", "debug-sub-1",
			"debug-sub-2", "debug-sub-3", "debugging", "decimal-point", "declaratives", "default-font", "delete",
			"delimited", "delimiter", "depending", "descending", "descriptor", "destination", "detail", "disable",
			"display", "display-1", "display-ws", "divide", "division", "double", "down", "duplicates", "dynamic",
			"egcs", "egi", "else", "emi", "enable", "end", "end-add", "end-call", "end-compute", "end-delete",
			"end-divide", "end-evaluate", "end-if", "end-multiply", "end-of-page", "end-perform", "end-read",
			"end-receive", "end-return", "end-rewrite", "end-search", "end-start", "end-string", "end-subtract",
			"end-unstring", "end-write", "endinf", "enter", "entry", "environment", "eop", "equal", "error", "esi",
			"evaluate", "every", "exception", "exclusive", "exit", "extend", "external", "external-form", "false",
			"fd", "file", "file-control", "filler", "final", "first", "fixed-font", "float", "float-long",
			"float-short", "font", "footing", "for", "from", "function", "generate", "generic", "giving", "global",
			"go", "goback", "greater", "group", "handle", "heading", "high-value", "high-values", "i-o", "i-o-control",
			"id", "identification", "identified", "if", "ignore", "ignoring", "implicit", "in", "index", "indexed",
			"indicate", "initial", "initialize", "initiate", "input", "input-output", "inspect", "installation",
			"into", "invalid", "is", "jboolean", "jbyte", "jchar", "jdouble", "jfloat", "jint", "jlong",
			"jpacked-decimal", "jshort", "jstring", "just", "justified", "justify", "kanji", "kept", "key", "label",
			"large-font", "last", "leading", "left", "length", "less", "limit", "limits", "linage", "linage_counter",
			"line", "line-counter", "lines", "linkage", "local-storage", "lock", "lockfile", "low-value", "low-values",
			"manual", "medium-font", "memory", "merge", "message", "mode", "modules", "more-labels", "move",
			"multiple", "multiply", "national", "native", "negative", "next", "no", "not", "null", "nulls", "number",
			"numeric", "numeric-edited", "numeric-hashtable", "object", "object-computer", "object-hashtable",
			"occurs", "of", "off", "omitted", "on", "open", "optional", "or", "order", "organization", "other",
			"output", "overflow", "packed-decimal", "packed-decimal-a", "packed-decimal-e", "packed-decimal-h",
			"packed-decimal-i", "packed-decimal-m", "padding", "page", "page-counter", "password", "perform", "pf",
			"ph", "pic", "picture", "plus", "pointer", "position", "positive", "printing", "procedure",
			"procedure-pointer", "procedures", "proceed", "program", "program-id", "program-status", "prompt",
			"protected", "purge", "queue", "quote", "quotes", "random", "rd", "read", "receive", "receive-control",
			"record", "recording", "records", "redefines", "reel", "reference", "references", "relative", "release",
			"remainder", "removal", "renames", "replace", "replacing", "reply", "report", "reporting", "reports",
			"rerun", "reserve", "reset", "return", "return-code", "returned", "returning", "reversed", "rewind",
			"rewrite", "rf", "rh", "right", "rollback", "rounded", "run", "same", "sd", "search", "section",
			"security", "segment", "segment-limit", "select", "send", "sentence", "separate", "sequence", "sequential",
			"set", "shared", "shift-in", "shift-out", "sign", "signed-int", "signed-long", "signed-short", "size",
			"small-font", "sort", "sort-control", "sort-core-size", "sort-file-size", "sort-merge", "sort-message",
			"sort-mode-size", "sort-return", "source", "source-computer", "space", "spaces", "special-names", "sqlind",
			"standard", "standard-1", "standard-2", "start", "status", "stdcall", "stop", "string", "sub-queue-1",
			"sub-queue-2", "sub-queue-3", "subtract", "sum", "suppress", "symbolic", "sync", "synchronized", "table",
			"tally", "tallying", "tape", "terminal", "terminate", "test", "text", "than", "then", "thread", "through",
			"thru", "time", "times", "to", "top", "traditional-font", "trailing", "true", "type", "unit", "unlock",
			"unlockfile", "unlockrecord", "unsigned-int", "unsigned-long", "unsigned-short", "unstring", "until", "up",
			"upon", "usage", "use", "using", "value", "values", "varying", "wait", "when", "when-compiled", "window",
			"with", "words", "working-storage", "write", "zero", "zeroes", "zeros" };

	public static KeyWordTrieNode getKeyWordTrie() {
		if (keyWordTrie == null) {
			keyWordTrie = new KeyWordTrieNode();
			for (int i = 0; i < keyWords.length; i++) {
				keyWordTrie.addWord(keyWords[i]);
			}
		}
		return keyWordTrie;
	}
}
