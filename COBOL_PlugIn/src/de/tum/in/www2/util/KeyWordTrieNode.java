package de.tum.in.www2.util;

import java.util.LinkedList;
import java.util.List;

// taken and adapted from https://community.oracle.com/thread/2070706 (24.06.2015)

public class KeyWordTrieNode {
	private KeyWordTrieNode parent;
	private KeyWordTrieNode[] children;
	private boolean isLeaf; // Quick way to check if any children exist
	private boolean isWord; // Does this node represent the last character of a
							// word
	private char character; // The character this node represents
	private final int numberLetters = 26;
	private final int numberNumbers = 10;
	private final int numberHyphen = 1;
	private final int numberUnderScore = 1;

	private final int indexLetters = 0;
	private final int indexNumbers = numberLetters;
	private final int indexHyphen = numberLetters + numberNumbers;
	private final int indexUnderScore = numberLetters + numberNumbers + numberHyphen;

	/**
	 * Constructor for top level root node.
	 */
	public KeyWordTrieNode() {
		// all letters plus all numbers plus '-' plus '_'
		children = new KeyWordTrieNode[numberLetters + numberNumbers + numberHyphen + numberUnderScore];
		isLeaf = true;
		isWord = false;
	}

	/**
	 * Constructor for child node.
	 */
	public KeyWordTrieNode(char character) {
		this();
		this.character = character;
	}

	protected void addWord(String word) {
		isLeaf = false;
		int characterPosition = getIndexForChar(word.charAt(0));
		if (children[characterPosition] == null) {
			children[characterPosition] = new KeyWordTrieNode(word.charAt(0));
			children[characterPosition].parent = this;
		}
		if (word.length() > 1) {
			children[characterPosition].addWord(word.substring(1));
		} else {
			children[characterPosition].isWord = true;
		}
	}

	protected List<String> getWordsLowerThanThisNode() {
		List<String> list = new LinkedList<String>();
		if (isWord) {
			list.add(toString());
		}
		if (!isLeaf) {
			for (int i = 0; i < children.length; i++) {
				if (children[i] != null) {
					list.addAll(children[i].getWordsLowerThanThisNode());
				}
			}
		}
		return list;
	}

	public List<String> getWordsWithPrefix(String prefix) {
		KeyWordTrieNode node = this;
		for (char character : prefix.toCharArray()) {
			node = node.children[getIndexForChar(character)];
		}
		if (node != null) {
			return node.getWordsLowerThanThisNode();
		} else {
			return new LinkedList<String>();
		}
	}

	private int getIndexForChar(char character) {
		if (character >= 48 && character <= 57) {
			// character is a number
			return character - 48 + indexNumbers;
		} else if (character >= 65 && character <= 90) {
			// character is an upper case letter
			return character - 65 + indexLetters;
		} else if (character >= 97 && character <= 122) {
			// character is a lower case letter
			return character - 97 + indexLetters;
		} else if (character == 45) {
			// character is '-' (hyphen)
			return character - 45 + indexHyphen;
		} else if (character == 95) {
			// character is '_' (underscore)
			return character - 95 + indexUnderScore;
		} else {
			System.out.println(character + ": " + (int) character);
			return -1;
		}
	}

	@Override
	public String toString()

	{
		if (parent == null) {
			return "";
		} else {
			return parent.toString() + new String(new char[] { character });
		}

	}

}
