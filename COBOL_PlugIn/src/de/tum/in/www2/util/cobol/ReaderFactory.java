package de.tum.in.www2.util.cobol;

import org.eclipse.jface.text.IDocument;

import de.tum.in.www2.util.LineProcessingReader;
import de.tum.in.www2.util.LineProcessor;

public final class ReaderFactory {
	private ReaderFactory() {
		// do not instantiate
	}

	public static LineProcessingReader createReader(IDocument document, int offset, int length) {
		LineProcessingReader filter = new LineProcessingReader(document, offset, length);
		filter.addLineProcessor(new LineProcessor() {
			@Override
			public String process(String line) {
				String result = line;

				int startcol = result.length() > 6 ? 6 : result.length();
				int endcol = result.length() > 72 ? 72 : result.length();

				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < 6 && i < result.length(); i++) {
					sb.append(' ');
				}
				sb.append(result.substring(startcol, endcol));
				for (int i = 72; i < result.length(); i++) {
					sb.append(' ');
				}
				result = sb.toString();

				return result;
			}
		});
		return filter;
	}
}
