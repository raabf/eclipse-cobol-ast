package de.tum.in.www2.util;

import java.util.HashSet;

import org.conqat.lib.scanner.IToken;
import org.eclipse.jface.text.Position;

import parser.analysis.base.definition.representation.ICallRepresentation;
import parser.analysis.base.definition.representation.ICallableDefinitionRepresentation;

public class CallHierarchy {

	private ICallableDefinitionRepresentation calleeNode;
	private Position position;
	private HashSet<CallHierarchy> children;
	private CallHierarchy parent;

	public CallHierarchy(Position position, ICallableDefinitionRepresentation calleeNode) {
		this.position = position;
		this.calleeNode = calleeNode;
		children = new HashSet<CallHierarchy>();
	}

	public CallHierarchy(Position position, ICallableDefinitionRepresentation calleeNode, CallHierarchy parent) {
		this.position = position;
		this.calleeNode = calleeNode;
		children = new HashSet<CallHierarchy>();
		this.setParent(parent);
	}

	public HashSet<CallHierarchy> getChildren() {
		generateDirectChildren();
		return children;
	}

	public void addChild(CallHierarchy child) {
		child.setParent(this);
		children.add(child);
	}

	public void addChildren(HashSet<CallHierarchy> childrenToAdd) {
		for (CallHierarchy child : childrenToAdd) {
			addChild(child);
		}
	}

	public String getName() {
		return calleeNode.getName();
	}

	public CallHierarchy getParent() {
		return parent;
	}

	public void setParent(CallHierarchy parent) {
		this.parent = parent;
	}

	public Position getPositionWhereCalled() {
		return position;
	}

	private void generateDirectChildren() {
		for (ICallRepresentation caller : this.calleeNode.getCallers()) {

			ICallableDefinitionRepresentation calleeNode = caller.getParent();
			int startTokenOffset = caller.getStartToken().getOffset();
			IToken endToken = caller.getEndToken();
			CallHierarchy callHierarchy = new CallHierarchy(new Position(startTokenOffset, endToken.getText().length()
					+ endToken.getOffset() - startTokenOffset), calleeNode, this);
			if (!children.contains(callHierarchy)) {
				children.add(callHierarchy);
			}
		}
	}

	@Override
	public boolean equals(Object other) {
		if (this.getClass().isAssignableFrom(other.getClass())) {
			Position otherPositionWhereCalled = ((CallHierarchy) other).getPositionWhereCalled();
			Position thisPositionWhereCalled = this.getPositionWhereCalled();
			return otherPositionWhereCalled.getOffset() == thisPositionWhereCalled.getOffset()
					&& otherPositionWhereCalled.getLength() == thisPositionWhereCalled.getLength()
					&& ((CallHierarchy) other).getName().equals(this.getName());
		}
		return false;
	}

	@Override
	public int hashCode() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(this.position.length);
		buffer.append(this.position.offset);
		buffer.append(this.calleeNode.getName());
		return buffer.toString().hashCode();
	}
}
