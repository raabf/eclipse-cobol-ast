package de.tum.in.www2.util;

import org.eclipse.jface.text.Position;

import parser.analysis.base.definition.representation.ICallableDefinitionRepresentation;
import parser.analysis.base.structure.representation.IStructureRepresentation;
import de.tum.in.www2.cobol.model.Model;
import de.tum.in.www2.cobol.model.references.Occurrence;

public class CallHierarchyGenerator {

	private int offset;
	private Model model;

	public CallHierarchyGenerator(int offset, Model model) {
		this.offset = offset;
		this.model = model;
	}

	public CallHierarchy getRoot() {
		Occurrence occurrence = model.findOccurrence(offset, 0);

		ICallableDefinitionRepresentation callee;

		if (occurrence != null && occurrence.isCaller()) {
			callee = occurrence.getCallerNode().getTarget();
		} else if (occurrence != null && occurrence.isCallee()) {
			callee = occurrence.getCalleeNode();
		} else {
			return null;
		}

		IStructureRepresentation structureRepresentation = callee.getStructureRepresentation();
		int startOffset = structureRepresentation.getStartOffset();
		int endOffset = structureRepresentation.getStartToken().getText().length() - 1 + startOffset;
		CallHierarchy callHierarchy = new CallHierarchy(new Position(startOffset, endOffset - startOffset
				+ structureRepresentation.getEndToken().getText().length()), callee);

		return callHierarchy;
	}

}
