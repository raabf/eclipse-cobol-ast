package de.tum.in.www2.util;

public interface LineProcessor {
	/**
	 * @param line
	 *            the input line
	 * @return the processed line
	 */
	public String process(String line);
}
