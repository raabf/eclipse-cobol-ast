package de.tum.in.www2.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collection;
import java.util.HashSet;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;

public class LineProcessingReader extends Reader {
	private final IDocument document;
	private final int offset;
	private final int length;
	private final Collection<LineProcessor> processors = new HashSet<LineProcessor>();
	private StringReader processed;

	public LineProcessingReader(IDocument document, int offset, int length) {
		this.document = document;
		this.offset = offset;
		this.length = length;
	}

	public void addLineProcessor(LineProcessor processor) {
		processors.add(processor);
	}

	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
		process();
		return processed.read(cbuf, off, len);
	}

	@Override
	public void close() throws IOException {
		processed.close();
	}

	private void process() throws IOException {
		if (processed == null) {
			BufferedReader input = new BufferedReader(new StringReader(document.get()
					.substring(offset, offset + length)));
			StringWriter writer = new StringWriter();

			String defaultLineSeparator = Platform.getPreferencesService().getString("org.eclipse.core.runtime",
					"line.separator", System.getProperty("line.separator"), null);

			int linenr;
			try {
				linenr = document.getLineOfOffset(offset);
				while (input.ready()) {
					String line = input.readLine();
					if (line == null) {
						break;
					}
					for (LineProcessor processor : processors) {
						line = processor.process(line);
					}
					String lineSeparator = "";
					try {
						lineSeparator = document.getLineDelimiter(linenr);
					} catch (BadLocationException e) {
						lineSeparator = defaultLineSeparator;
					}
					writer.write(line);
					if (lineSeparator != null) {
						writer.write(lineSeparator);
					}
					linenr++;
				}

				processed = new StringReader(writer.toString());
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
		}
	}
}
